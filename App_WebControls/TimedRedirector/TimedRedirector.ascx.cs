﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class App_WebControls_TimedRedirectior_TimedRedirector : System.Web.UI.UserControl {
	string _redirectUrl;
	int _timeout;
	string _text;
	string _cssClass;
	bool _refreshParent;

	const string _redirectScript = "<script type=\"text/javascript\">"
		+ "/* <![CDATA[ */"
		+ "window.setTimeout('Redirect();', {TIMEOUT});"
		+ "function Redirect() {"
		+ "window.location = \"{URL}\";"
		+ "}"	
		+ "/* ]]> */"
		+ "</script>";

	const string _closeAndRefreshScript = "<script type=\"text/javascript\">"
		+ "/* <![CDATA[ */"
		+ "window.opener.location.reload();"
		+ "window.setTimeout('Redirect();', {TIMEOUT});"
		+ "function Redirect() {"
		+ "window.close();"
		+ "}"
		+ "/* ]]> */"
		+ "</script>";

	const string _closeAndRefreshTxt = "<br />This window will be closed in {TIMEOUT} seconds. If it doesn't, please use the close button, and refresh the originating page.";
	const string _redirectTxt = "<br />You will be redirected in {TIMEOUT} seconds. If the page doesn't refresh please click <a href=\"{URL}\">here</a>.";

	public App_WebControls_TimedRedirectior_TimedRedirector() {
		_timeout = 5000;
		_redirectUrl = "Default.aspx";
		_text = "";
		_cssClass = "";
		_refreshParent = false;
	}

	protected void Page_Load(object sender, EventArgs e) {
		
	}

	/// <summary>
	/// gets or sets the url to be used to redirect to
	/// </summary>
	public string RedirectUrl {
		get { return _redirectUrl; }
		set { 
			_redirectUrl = value;
			Render();
		}
	}

	/// <summary>
	/// gets or sets the delay before redirection (in milliseconds)
	/// </summary>
	public int Delay {
		get { return _timeout; }
		set { 
			_timeout = value;
			Render();
		}
	}

	/// <summary>
	/// updates the script with the current values
	/// </summary>
	private void Render() {
		if (CloseAndRefreshParent) {
			scriptLiteral.Text = _closeAndRefreshScript.Replace("{TIMEOUT}", _timeout.ToString());
			textLiteral.Text = (_cssClass == "" ? "<p>" : "<p class=\"" + _cssClass + "\">") + _text + _closeAndRefreshTxt.Replace("{TIMEOUT}", (Math.Round((decimal)_timeout / (decimal)1000, 1)).ToString()) + "<p>";
		} else {
			scriptLiteral.Text = _redirectScript.Replace("{URL}", Page.ResolveClientUrl(_redirectUrl)).Replace("{TIMEOUT}", _timeout.ToString());
			textLiteral.Text = (_cssClass == "" ? "<p>" : "<p class=\"" + _cssClass + "\">") + _text + _redirectTxt.Replace("{URL}", Page.ResolveClientUrl(_redirectUrl)).Replace("{TIMEOUT}", (Math.Round((decimal)_timeout / (decimal)1000, 1)).ToString()) + "<p>";
		}
	}



	/// <summary>
	/// gets or sets the text to go before the redirection message.
	/// </summary>
	public string Text {
		get {
			return _text;
		}
		set {
			_text = value;
			Render();
		}
	}

	/// <summary>
	/// gets or sets the component's css class
	/// </summary>
	public string CssClass {
		get { return _cssClass; }
		set { 
			_cssClass = value;
			Render(); 
		}
	}

	public bool CloseAndRefreshParent {
		get { return _refreshParent; }
		set { _refreshParent = value; }
	}
}
