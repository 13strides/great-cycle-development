﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NorthInfo.ascx.cs" Inherits="App_WebControls_EventInfo_NorthInfo" %>

<asp:Panel runat="server" ID="BallotOpenPanel" Visible="false">
 <div class="left-col">
    <div class="globalhook">
        <!--<h2>Enter Online</h2>-->
        <!--<asp:HyperLink runat="server" ID="clickAreaHyperLink">
           
            <asp:Image ImageUrl="/App_Images/Backgrounds/blank.gif" runat="server" ID="ImageAltStatus" AlternateText="" CssClass="clickarea" />
        </asp:HyperLink>
        <div class="hero_h322 c150_enterOnline">
           <h2><span>entry status:</span> Open</h2>
            <p>
                <strong>Event Date: </strong>16 June 2012
                <strong>Location: </strong>Keswick, The Lake District
                </p>-->
       <!--    <p>Choose your Distance</p>
        
        <div class="choose-distance tenKWidth"><img src="/images/distance2.jpg" /><a href="https://entry.enteronline.org/Login.aspx?StreamID=776"><span><br /></span>13 miles</a></div>
        <div class="choose-distance hmWidth"><img src="/images/distance1.jpg" /><a href="https://entry.enteronline.org/Login.aspx?StreamID=777"><span><br /></span>26 miles</a></div>
        <div class="choose-distance marathonWidth"><img src="/images/distance3.jpg" /><a href="https://entry.enteronline.org/Login.aspx?StreamID=778"><span><br /></span>52 miles</a></div>-->
        
       
                
       <!-- </div>-->
       
       <div id="earlyBird"><a name="tshirt" id="tshirt"></a>
    	<!--<h2>Participant's Information</h2>
        <p><a href="http://greatcycle.org/App_Files/Gr_Files/Daily-Mirror-Great-Manchester-Cycle-2012-Comps-Web.pdf" target="_blank"><img src="http://greatcycle.org/App_Files/Gr_Files/Daily-Mirror-Great-Manchester-Cycle-2012-Comps-Web.jpg" alt="" /></a></p>
        <p>Download your copy of the <a href="http://greatcycle.org/App_Files/Gr_Files/Daily-Mirror-Great-Manchester-Cycle-2012-Comps-Web.pdf" target="_blank">Participant's Information Booklet (.PDF)</a> containing all the event day info.</p>-->
         <h2>Congratulations</h2>
	 <p>Well done to all great cyclists, view your <a href="/results/default.aspx">results</a> and <a href="/photos/default.aspx">photos</a> online.</p>
        
    </div>
    
    
     <!-- 
       
       <div id="earlyBird"><a name="tshirt" id="tshirt"></a>
    	<h2>Participant Information</h2>
        <p><a href="http://greatcycle.org/App_Files/Gr_Files/Daily-Mirror-Great-Manchester-Cycle-2012-Comps-Web.pdf" target="_blank"><img src="http://greatcycle.org/App_Files/Gr_Files/Daily-Mirror-Great-Manchester-Cycle-2012-Comps-Web.jpg" alt="" /></a></p>
        <p>Exciting early bird offer for all <strong>adult</strong> entries taken prior to Friday 20 April 2012. Buy this top quality Great Manchester Cycle jersey for the discount price of £15, simply tick the box within the online entry process to include with your event entry. Available after Friday 20 April 2012 at £25.</p>
    
        </div> -->
    
    </div>
    
</div>
</asp:Panel>
<asp:Panel runat="server" ID="BallotClosedPanel" Visible="false">
 <div class="left-col">
    <div class="globalhook">
        <asp:Image ImageUrl="/App_Images/Backgrounds/blank.gif" runat="server" ID="Image1" AlternateText="" CssClass="clickarea" />
        <div class="hero_h322 c150_enterOnline entryClosed">
           <h2><span>entry status:</span> Closed</h2>
            <p>
                <strong>Event Date: </strong>16 June 2012
                <strong>Location: </strong>Keswick, The Lake District
            </p>
             
        </div>
    </div>
</div>
</asp:Panel>
