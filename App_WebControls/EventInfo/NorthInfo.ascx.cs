﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Ni.Events.StreamManagement;

public partial class App_WebControls_EventInfo_NorthInfo : System.Web.UI.UserControl
{
	string linkCookie;

	EventStreamContainer north1Mile;


	protected void Page_Load(object sender, EventArgs e) {
		EventStatusContent();
		if (!IsPostBack) {

			north1Mile = new EventStreamContainer(1);

			if (Request.Cookies["NLOPRCookie"] != null) {
                linkCookie = Request.Cookies["NLOPRCookie"].Value;

			}
			CreateRegisterNavLink();
		}
	}

	private void CreateRegisterNavLink() {
		string linkadd = "";
		if (linkCookie != String.Empty && linkCookie != null) {
			linkadd = "&" + linkCookie;
		}
		//RegistrationUrl
		int OverallStatus = 0;

		if ((SwimEventStatus)north1Mile.currentStream.Status != SwimEventStatus.Closed) {
			if ((SwimEventStatus)north1Mile.currentStream.Status == SwimEventStatus.ReminderService) {
				//eventReminderService.Visible = true;
				//event1MileEntryLink.Visible = false;
				//eventReminderService.NavigateUrl = ConfigurationManager.AppSettings["RegistrationUrl"] + north1Mile.currentStream._event_info_stream_id.ToString() + linkadd;
				OverallStatus += 1;
            } else if ((SwimEventStatus)north1Mile.currentStream.Status == SwimEventStatus.Ballot){
                //event1MileEntryLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + north1Mile.currentStream._event_info_stream_id.ToString() + linkadd;
                clickAreaHyperLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + north1Mile.currentStream._event_info_stream_id.ToString() + linkadd;
                //entryLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + north1Mile.currentStream._event_info_stream_id.ToString() + linkadd;
                OverallStatus += 5;
			} else {
				//event1MileEntryLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + north1Mile.currentStream._event_info_stream_id.ToString() + linkadd;
                clickAreaHyperLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + north1Mile.currentStream._event_info_stream_id.ToString() + linkadd;
                //entryLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + north1Mile.currentStream._event_info_stream_id.ToString() + linkadd; 

				OverallStatus += 10;
			}
		} else {

			//event1MileEntryLink.Visible = false;
		}



		if (OverallStatus >= 10) {
			//entryImage.ImageUrl = "~/App_Images/EventStatus/open.gif";
			//entryStatus.Text = "Open for entries";
            ImageAltStatus.AlternateText = "Open for entries, Event date: 31 March 2011, Location: The Olympic Park";
        } else if (OverallStatus >= 5){
            //entryImage.ImageUrl = "~/App_Images/Status/ballot.jpg";
            //entryStatus.Text = "Ballot Open";
            ImageAltStatus.AlternateText = "Ballot Open, Event date: 31 March 2011, Location: The Olympic Park";
		} else if (OverallStatus > 0) {
			//entryImage.ImageUrl = "~/App_Images/EventStatus/reminderService.gif";
			//entryStatus.Text = "Reminder Service";
            ImageAltStatus.AlternateText = "Reminder Service, Event date: 31 March 2011, Location: The Olympic Park";
		} else {
			//entryImage.ImageUrl = "~/App_Images/EventStatus/closed.gif";
			//entryStatus.Text = "Closed";
            ImageAltStatus.AlternateText = "Closed, Event date: 31 March 2011, Location: The Olympic Park";

		}



	}
private void EventStatusContent()
    {
        //DateTime showEventClosed = new DateTime(2011, 10, 27, 23, 59, 59);
       // if (DateTime.Now > showEventClosed)
       // {
           // BallotClosedPanel.Visible = true;
        //}
       // else
        //{
            BallotOpenPanel.Visible = true;
        //}

    }
}
