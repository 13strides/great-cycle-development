﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomePageSlider.ascx.cs" Inherits="App_WebControls_HomePageRows_HomePageSlider" %>
    <script type="text/javascript" src="App_Js/jquery.cycle.lite.js"></script>
    <script type="text/javascript" src="App_Js/fade.js"></script>
    <script type="text/javascript">
        <!--
        window.onload = function() {
            $("#nonJsPanel").hide();
            if ($.fn.cycle) { $('.slideshow-images').cycle({ timeout: 5000, speed:500, sync:1 }); }
        }
        -->
    </script>
    
    <div id="slider" style="height:320px;background-color:#1e72b2">
         
        
        <!--<div class="pikachoose"><ul id="pikame" class="jcarousel-skin-pika"></ul></div>-->
        
        
        <div class="slideshow-images">
            <a href="/Results/Default.aspx"><img src="App_Images/Slideshow/salford-results.jpg" alt="British Gas Great Salford Swim" title="British Gas Great Salford Swim" /></a>  
            <a href="/Events/British-Gas-Great-Scottish-Swim/Default.aspx"><img src="App_Images/Slideshow/now-open-scottish.jpg" alt="British Gas Great Scottish Swim" title="British Gas Great Scottish Swim" /></a>
            <a href="/Events/British-Gas-Great-North-Swim/Default.aspx"><img src="App_Images/Slideshow/Now-Open-North.jpg" alt="British Gas Great North Swim" title="British Gas Great North Swim" /></a>
            <a href="/Events/British-Gas-Great-London-Swim/Default.aspx"><img src="App_Images/Slideshow/Now-Open-London.jpg" alt="British Gas Great London Swim" title="British Gas Great London Swim" /></a>
            <a href="/Events/British-Gas-Great-East-Swim/Default.aspx"><img src="App_Images/Slideshow/Now-Open-East.jpg" alt="British Gas Great East Swim" title="British Gas Great East Swim" /></a>    
        <%--
        
        <asp:HyperLink runat="server" ID="greatDayLink" NavigateUrl="~/Events/British-Gas-Great-Salford-Swim/Default.aspx"><img src="App_Images/Slideshow/greatdayofsport.jpg" alt="British Gas Great Salford Swim" /></asp:HyperLink>--%>
        </div>
        
        
    </div><div class="thumbSlider"><ul>
                
            
                <li><asp:HyperLink runat="server" ID="eventsMenuLink" NavigateUrl="~/Events/Default.aspx"><img src="App_Images/2011/Events/news-item.png" alt="British Gas Great Swim Series" title="British Gas Great Swim Series"  class="img" /></asp:HyperLink></li>
                <li><asp:HyperLink runat="server" ID="eventsSalfordLink" NavigateUrl="~/Events/British-Gas-Great-Salford-Swim/Default.aspx"><img src="App_Images/2011/Events/salford.png" alt="British Gas Great Salford Swim" title="British Gas Great Salford Swim" class="img" /></asp:HyperLink></li>
                <li><asp:HyperLink runat="server" ID="eventsEastLink" NavigateUrl="~/Events/British-Gas-Great-East-Swim/Default.aspx"><img src="App_Images/2011/Events/East.png" alt="British Gas Great East Swim" title="British Gas Great East Swim" class="img" /></asp:HyperLink></li>
                <li><asp:HyperLink runat="server" ID="eventsNorthLink" NavigateUrl="~/Events/British-Gas-Great-North-Swim/Default.aspx"><img src="App_Images/2011/Events/north.png" alt="British Gas Great North Swim" title="British Gas Great North Swim" class="img" /></asp:HyperLink></li>
                <li><asp:HyperLink runat="server" ID="eventsLondonLink" NavigateUrl="~/Events/British-Gas-Great-London-Swim/Default.aspx"><img src="App_Images/2011/Events/london.png" alt="British Gas Great London Swim" title="British Gas Great London Swim" class="img" /></asp:HyperLink></li>
                <li><asp:HyperLink runat="server" ID="eventsScottishLink" NavigateUrl="~/Events/British-Gas-Great-Scottish-Swim/Default.aspx"><img src="App_Images/2011/Events/scottish.png" alt="British Gas Great Scottish Swim" title="British Gas Great Scottish Swim" class="img" /></asp:HyperLink></li>
              
                </ul>
        </div> 