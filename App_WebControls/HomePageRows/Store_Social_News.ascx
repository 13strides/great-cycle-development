﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Store_Social_News.ascx.cs" Inherits="App_WebControls_HomePageRows_TestControl1" %>

         <div class="box oneThirds">
        	<h2 class="box-title">Great Swim Shop</h2>
            
            	<p class="highlight"><a href="store/Default.aspx"><img  runat="server" src="~/App_Images/contentImages/homepanels/store/store-thumb.jpg" alt="Great Swim Shop in collaboration with blueseventy" class="box-thumb" /></a>We have everything you need to get the most out of your swimming right here in the Great Swim Shop, in collaboration with blueseventy.</p>
            
            <h3 class="box-sub-title"><a href="store/Default.aspx" target="_blank">Visit Great Swim Shop</a></h3>
            <div class="box-content">
                <ul class="links">
                	<li><a href="store/Default.aspx"><img  runat="server" src="~/App_Images/contentImages/homepanels/store/store3-list.jpg" alt="Swimming wetsuits available to buy and hire" /> <span>Wetsuit Hire</span></a></li>
                    <li><a href="store/Default.aspx"><img  runat="server" src="~/App_Images/contentImages/homepanels/store/store1-list.jpg" alt="Goggles designed for outdoor swimming" /> <span>Goggles</span></a></li>
                </ul>
                <div class="clearer"></div>
            </div>
            
        </div>
        
        <div class="box oneThirds">
        	<h2 class="box-title">Facebook &amp; Twitter</h2>
            <div class="highlight" id="socialThumbs">
            	<ul class="mycarousel jcarousel-skin-tango">
    				<li><a href="http://www.facebook.com/album.php?aid=242221&id=124286752281"><img  runat="server" src="~/App_Images/contentImages/homepanels/social/box-img.jpg" /></a></li>
                </ul>
            </div>
            <h3 class="box-sub-title"><a href="http://en-gb.facebook.com/greatswim#!/greatswim?v=photos" onclick="javascript:pageTracker._trackPageview('/EXIT/SOCIAL/FACEBOOK/Gals.html');" target="_blank">View Facebook Galleries</a></h3>
            <div class="box-content">
            	<ul class="links">
                	<li><a href="http://en-gb.facebook.com/greatswim#!/greatswim?v=wall"
                	     onclick="javascript:pageTracker._trackPageview('/EXIT/SOCIAL/FACEBOOK/Wall.html');" target="_blank"><img runat="server" src="~/App_Images/contentImages/homepanels/social/facebook.gif" alt="Extended community site for friends of Great Swim" /> <span>Join us on Facebook</span></a></li>
                    <li><a href="http://twitter.com/great_swim"
                        onclick="javascript:pageTracker._trackPageview('/EXIT/SOCIAL/TWITTER/Stream.html');" target="_blank"><img runat="server" src="~/App_Images/contentImages/homepanels/social/twitter.gif" alt="Great Swim on Twitter" /> <span>Follow us on Twitter</span></a></li>
                </ul>
                <div class="clearer"></div>
            </div>
        </div>
        
        <div class="box oneThirds row3">
        	<h2 class="box-title">Latest News</h2><a href="/News/Default.aspx"><img runat="server" src="~/App_Images/contentImages/tracker.jpg" border="0" /></a><h3 class="box-sub-title"><a href="/News/Default.aspx">More Great Swim News</a></h3>
            <div class="box-content">
            	     <asp:Repeater runat="server" ID="newsListRepeater">
            <HeaderTemplate><ul></HeaderTemplate>
            <ItemTemplate>
                 <li><a href="news/news_article.aspx?nid=<%# Eval("news_id") %>"><%# Eval("news_title") %></a></li>
                 
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
                <gr:RepeaterPager runat="server" Visible="false" ID="pager" PageSize="3" CssClass="pager" CurrentPageLinkCssClass="current-page-link" LinksCssClass="page-link" NextPreviousCssClass="next-prev-link" NextPreviousDisabledCssClass="next-prev-disabled"></gr:RepeaterPager>

                <div class="clearer"></div>
            </div>
        </div>
        
        <div class="clearer"></div>