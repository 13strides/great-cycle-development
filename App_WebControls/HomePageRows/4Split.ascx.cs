﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using ThirteenStrides.Web;
using GS.Generic.Web;

public partial class App_WebControls_HomePageRows_4Split : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        MsSqlConnection connection2 = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = { };
		//connection	
        DataSetResponse resp = connection2.RunCommand("gs_select_insp_stories_four", paras, "storys");

        if (resp.ReturnCode == 0)
        {
            storiesRepeater.DataSource = resp.Result.Tables["storys"].DefaultView;
            storiesRepeater.DataBind();
        }
        else
        {

        }
    }
}
