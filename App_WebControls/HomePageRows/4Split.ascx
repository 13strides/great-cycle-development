﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="4Split.ascx.cs" Inherits="App_WebControls_HomePageRows_4Split" %>

        <asp:Repeater runat="server" ID="storiesRepeater">
            <ItemTemplate>
                <div class="box oneQuarter row4">
        	        <h3 class="box-title"><%# Eval("Story_name") %></h3>
                	
                    <asp:HyperLink runat="server" ID="swimStoryLink" NavigateUrl='<%# "~/SwimStories/Story.aspx?sid=" + Eval("story_id") %>'>
                        <img src="App_Images/contentImages/homepanels/stories/thumb/<%# Eval("Story_id") %>.jpg" alt="<%# Eval("Story_name") %>" />
                    </asp:HyperLink>
                    
                    <div class="box-content">
            	        <p class="subcontent"><asp:HyperLink runat="server" ID="swimTextLink" NavigateUrl='<%# "~/SwimStories/Story.aspx?sid=" + Eval("story_id") %>'>
            	            &ldquo;...<%# Eval("Story_excerpt") %>&rdquo;
            	                </asp:HyperLink></p>
                    </div>
                    
                </div>
            </ItemTemplate>
        </asp:Repeater>

        
        <div class="clearer"></div>