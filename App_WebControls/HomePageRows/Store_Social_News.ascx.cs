﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GS.Generic.Web;
using ThirteenStrides.Database;
using System.Configuration;
using System.Data;


public partial class App_WebControls_HomePageRows_TestControl1 : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
		MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

		SqlParam[] paras = { new SqlParam("@event", SqlDbType.Int, -1) };

		DataSetResponse res = connection.RunCommand("gs_front_select_news", paras, "news");

		if (res.ReturnCode == 0) {
			if (res.Result.Tables["news"].Rows.Count > 0) {
				pager.ControlToPage = newsListRepeater;
				pager.DataSource = res.Result.Tables["news"].DefaultView;
				pager.DataBind();
			} else {
				//errorLiteral.Text = "The news failed to load" + res.ReturnCode.ToString() + res.ErrorMessage;
				pager.Visible = false;
			}
		} else {
			//errorLiteral.Text = "The news failed to load" + res.ReturnCode.ToString() + res.ErrorMessage;
		}
    }
}
