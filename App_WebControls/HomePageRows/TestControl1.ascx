﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TestControl1.ascx.cs" Inherits="App_WebControls_HomePageRows_TestControl1" %>
        <div class="box twoThirds row1 mapBackground">
        	<h2 class="box-title-white">Event Information / Interactive Map</h2>
            <div class="box-content" style="min-height:213px;">
             <p>British Gas Great East Swim<br />
             <br />
             British Gas Great London Swim<br /><br />
             British Gas Great Scottish Swim<br /><br />
             British Gas Great North Swim<br /><br />
             British Gas Great Salford Swim<br /><br /></p>
             <div class="clearer"></div>
            </div>
        </div>
        <div class="box oneThirds row1 advertisements">
            <div class="box-content">
            	<script type="text/javascript" src="http://banner.greatrun.org/Banner.aspx?b=12-44"></script>
        		<p style="font-size:0.65em; text-align:center;margin:0;">advertisement</p>
                <div class="clearer"></div>
            </div>
        </div>
        
        <div class="clearer"></div>