﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Training_SwimStories.ascx.cs" Inherits="App_WebControls_HomePageRows_TestControl1" %>
  <div class="box oneHalf row2">
        	<h2 class="box-title">Shape up for your swim</h2>
            <div class="box-content">
            	<img src="App_Images/contentImages/training-thumb.jpg" class="box-thumb-blue" />
				<dl>
                	<dt><asp:Image runat="server" ID="odSwimAdviceImg" ImageUrl="~/App_Images/contentImages/img-list1.jpg" AlternateText="Outdoor swimming advice"  /></dt>
                    <dd><asp:HyperLink runat="server" ID="odSwimAdviceLink" NavigateUrl="~/Training/TrainingAdvice.aspx">
                            Outdoor swimming advice
                        </asp:HyperLink></dd>
                    <dt><asp:Image runat="server" ID="wtwImage" ImageUrl="~/App_Images/contentImages/img-list2.jpg" AlternateText="What to wear" /></dt>
                    <dd><asp:HyperLink runat="server" ID="wtwLink" NavigateUrl="~/Training/WhatToWear.aspx">
                            What to wear
                        </asp:HyperLink></dd>
                    <dt><asp:Image runat="server" ID="ateImage" ImageUrl="~/App_Images/contentImages/img-list3.jpg" AlternateText="Swim for 2 Miles outdoors" /></dt>
                    <dd><asp:HyperLink runat="server" ID="ateLink" NavigateUrl="~/Training/AskTheExpert.aspx">
                            Ask the expert
                        </asp:HyperLink></dd>
                </dl>
                <div class="clearer"></div>
            </div>
        </div>

        
        
        <asp:Repeater runat="server" ID="storiesRepeater">
            <ItemTemplate>
                <div class="box oneHalf row2">
        	        <h2 class="box-title">Someone Like You</h3>
                	
                	<div class="box-content">
                        <asp:HyperLink runat="server" ID="swimStoryLink" NavigateUrl='<%# "~/SwimStories/Story.aspx?sid=" + Eval("story_id") %>'>
                            <img class="box-thumb-blue" src="App_Images/contentImages/homepanels/stories/home/<%# Eval("Story_id") %>.jpg" alt="<%# Eval("Story_name") %>" />
                        </asp:HyperLink>
                    <p><strong><%# Eval("Story_name") %></strong></p>
                    <blockquote>
            	        <p><asp:HyperLink runat="server" ID="swimTextLink" NavigateUrl='<%# "~/SwimStories/Story.aspx?sid=" + Eval("story_id") %>'>
            	            &ldquo;<%# Eval("Story_excerpt") %>&rdquo;
            	                </asp:HyperLink></p>
            	        <p><br /><asp:HyperLink CssClass="button readmore" runat="server" ID="HyperLink1" NavigateUrl='<%# "~/SwimStories/Story.aspx?sid=" + Eval("story_id") %>'>
            	            <span>Read the <%# Eval("Story_name") %> full story</span>
            	            </asp:HyperLink></p>
            	    </blockquote>
            	        <div class="clearer"></div>
                    </div>
                    
                </div>
            </ItemTemplate>
        </asp:Repeater><div class="clearer"></div>