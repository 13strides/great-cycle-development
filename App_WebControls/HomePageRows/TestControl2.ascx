﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TestControl2.ascx.cs" Inherits="App_WebControls_HomePageRows_TestControl1" %>
  <div class="box oneHalf row2">
        	<h2 class="box-title">Get in shape for your Swim</h2>
            <div class="box-content">
            	<img src="App_Images/contentImages/training-thumb.jpg" class="box-thumb-blue" />
				<dl>
                	<dt><img src="App_Images/contentImages/img-list1.jpg" alt="Swim for 1/2 Mile outdoors"  /></dt>
                    <dd><a href="#">Train for a 1/2 Mile Event</a></dd>
                    <dt><img src="App_Images/contentImages/img-list2.jpg" alt="Swim for 1 Mile outdoors" /></dt>
                    <dd><a href="#">Train for a 1 Mile Event</a></dd>
                    <dt><img src="App_Images/contentImages/img-list3.jpg" alt="Swim for 2 Miles outdoors" /></dt>
                    <dd><a href="#">Train for a 2 Mile Event</a></dd>
                </dl>
                <div class="clearer"></div>
            </div>
        </div>
        
        <div class="box oneHalf row2">
        	<h2 class="box-title">Swim Stories - Tony Mellett</h2>
            <div class="box-content">
            	<img src="App_Images/contentImages/athlete-thumb.jpg" class="box-thumb-blue" alt="Tony Mellett" />
                <blockquote>
                	<p class="quote">I could not get into the Great North Swim so I did the Great London swim, which I was impressed with, and while at that event I saw the Great East swim advertised and so entered as a challenge to see if I could better my time in London.</p>
                    <p class="author"><a href="SwimStories/">Read Tony's story</a></p>
                </blockquote>
                <div class="clearer"></div>
            </div>
        </div>
        
        <div class="clearer"></div>