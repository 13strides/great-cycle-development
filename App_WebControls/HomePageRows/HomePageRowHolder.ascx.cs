﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gs.Generic.HomePage;

public partial class App_WebControls_HomePageRows_HomePageRowHolder : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
		HomePageVersion ThisVersion = new HomePageVersion();
		foreach (HomePageRow row in ThisVersion.pageRows) {

			UserControl newControl = (UserControl)LoadControl("~" + row._controlPath);
			Panel1.Controls.Add(newControl);
		}

    }
}
