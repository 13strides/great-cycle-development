﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ThirteenStrides.Database;

public partial class App_WebControls_RandomQuote_RandomQuote : System.Web.UI.UserControl
{
    int _startNumber;
    int _finishNumber;
    /// <summary>
    /// gets or sets the start number for the random selection
    /// </summary>
    public int StartNumber
    {
        get { return _startNumber; }
        set
        {
            _startNumber = value;
            //BindQuote();
        }
    }

    ///<summary>
    ///gets or sets the finish number for the random selection
    ///</summary>
    public int FinishNumber
    {
        get { return _finishNumber; }
        set
        {
            _finishNumber = value;
            //BindQuote();
        }
    }
    public App_WebControls_RandomQuote_RandomQuote()
    {
        _startNumber = 4;
        _finishNumber = 5;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        BindQuote();
    }
    private void BindQuote()
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatAustralianRun"].ConnectionString);
        Random RandomClass = new Random();
        int RandomNumber = RandomClass.Next(_startNumber, _finishNumber);

        SqlParam[] paras = { };

        SqlDataReaderResponse res = connection.RunCommand("gar_front_select_quote", paras);

        if (res.ReturnCode == 0)
        {
            if (res.Result.Read())
            {
                try
                {
                    quoteStringLiteral.Text = res.Result["quote_text"].ToString();
                    quoteLink.NavigateUrl = res.Result["quote_link"].ToString();
                    quoteLink.Text = res.Result["quote_author"].ToString();
                }
                catch { }
            }
            res.Result.Close();
        }
    }
}
