﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RandomQuote.ascx.cs" Inherits="App_WebControls_RandomQuote_RandomQuote" %>
<div class="blog">
	    <h2 class="default">quote</h2>
	    <blockquote>
	        <p class="quote"><asp:literal runat="server" ID="quoteStringLiteral" /></p>
            <p class="author"><!--<asp:HyperLink runat="server" ID="quoteLink"><asp:Literal runat="server" ID="quoteAuthorLiteral" /></asp:HyperLink>--></p>
        </blockquote>
</div>