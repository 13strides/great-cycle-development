﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GS.Generic.Web;
using ThirteenStrides.Database;

public partial class App_WebControls_News_NewsHeadAndThumbnail : System.Web.UI.UserControl {
	private int _liveOnly = 1;
	private int _topX = 1;
	private int _articleId = 0;
	private int _itemCount = 0;
    private string _newsType = string.Empty;
	public int LiveOnly {
		get { return _liveOnly; }
		set { _liveOnly = value; }
	}

	public int TopX {
		get { return _topX; }
		set { _topX = value; }
	}

	public int ArticleId {
		get { return _articleId; }
		set { _articleId = value; }
	}

	public int ItemCount {
		get { return _itemCount; }
		private set { _itemCount = value; }
	}

    public string NewsType {
        get { return _newsType; }
        private set { _newsType = value; }
    }
	protected void Page_Load(object sender, EventArgs e) {
		BindData(_liveOnly, _topX, _articleId);
	}

	private void BindData(int liveOnly, int topX, int articleId) {
		MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

		SqlParam[] paras = { new SqlParam("@liveOnly", SqlDbType.Int, liveOnly),
                               new SqlParam("@top", SqlDbType.Int, topX),
                               new SqlParam("@articleId", SqlDbType.Int, articleId)
                           };

		DataSetResponse res = connection.RunCommand("gr_get_news_article_topX", paras, "news");
		if (res.ReturnCode == 0) {
			if (res.Result.Tables["news"].Rows.Count > 0) {
				_itemCount = res.Result.Tables["news"].Rows.Count;
				Repeater1.DataSource = res.Result.Tables["news"].DefaultView;
				Repeater1.DataBind();
			}
		}
        
        //if the request was getting a related data and if a related data not found,
        //display default news
        if ( articleId > 0 && _itemCount == 0 )
        {
            articleId =0;
            BindData(liveOnly, topX, articleId);
        }
        if (articleId > 0)
        {
            _newsType = "Related News";
        }
        else
        {
            _newsType = "Latest News";
        }
	}

	protected string TrimString(object obj) {
		string str = obj.ToString().Trim();
		if (str.Length > 40) {
			str = str.Substring(0, 40) + "...";
		}
		return str;
	}

	protected string GetImageFile(object obj) {
		string defaultPath = "/App_Files/ImageLibrary/NewsMedium/";
		string defaultImage = "/img/css/blog-news-item.jpg";
		string str = obj.ToString().Trim();
		if (str.Length > 0) {
			if (!str.StartsWith("~")) {
				str = defaultPath + str;
				if (!File.Exists(Server.MapPath(str))) {
					str = defaultImage;
				}
			} else {
				str = str.Remove(0, 1);
			}
		}
		return str;
	}

    protected string TrimDescription(object obj)
    {
        string[] desc = obj.ToString().Trim().Split(' ');
        string output = "";
        if (desc.Length >= 14)
        {
            for (int i = 0; i < 14; i++)
            {
                output += desc[i] + " ";
            }
        }
        else
        {
            output = obj.ToString();
        }
        return output + "...";
    }

	protected string GetDetailsLink(object obj) {
		return "/News/" + obj.ToString();
	}
}