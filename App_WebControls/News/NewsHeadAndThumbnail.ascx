﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewsHeadAndThumbnail.ascx.cs"
	Inherits="App_WebControls_News_NewsHeadAndThumbnail" %>
<asp:Repeater ID="Repeater1" runat="server">
	<ItemTemplate>
		<div class="rhc-post">
			<a href="<%# GetDetailsLink(Eval("FriendlyUrl")) %>">
				<img src='<%# GetImageFile(Eval("ImageFileName")) %>' alt="" /></a> <a class="rhc-post-title"
					href="<%# GetDetailsLink(Eval("FriendlyUrl")) %>">
					<%# TrimString(Eval("Title")) %></a>
			<p class="rhc-description"><a href="<%# GetDetailsLink(Eval("friendlyUrl")) %>">
				<%# TrimDescription(Eval("ShortDescription")) %></a></p>
		</div>
	</ItemTemplate>
</asp:Repeater>
<div class="clearer">
</div>