﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SwimStories.ascx.cs" Inherits="App_WebControls_SwimStories_SwimStories" %>
<div class="eventHomeSecondaryColumn clearfix">
    <h3>Inspiring Swim Stories</h3>
    <p class="eventList stories">
            <span class="date"><asp:HyperLink runat="server" ID="HyperLink2" NavigateUrl="~/SwimStories/PamAnderson.aspx"><asp:Image runat="server" ID="pamImage" ImageUrl="~/SwimStories/App_Images/PamAnderson_tn.jpg" AlternateText="Pam Anderson - Why I took part" toolTip="Pam Anderson - Why I took part" CssClass="img"/></asp:HyperLink></span>
            <strong>Pam Anderson</strong><br />
            Why I took part<br />&nbsp;

        </p>
        <p class="eventList stories">
            <span class="date"><asp:HyperLink runat="server" ID="HyperLink3" NavigateUrl="~/SwimStories/TonyMellett.aspx"><asp:Image runat="server" ID="tonyImage" ImageUrl="~/SwimStories/App_Images/TonyMellett_tn.jpg" AlternateText="Tony Mellett - Why I took part" ToolTip="Tony Mellett - Why I took part" CssClass="img"/></asp:HyperLink></span>
            <strong>Tony Mellett</strong>
            <br />Why I took part<br />&nbsp;
        </p>
        <p class="eventList stories">
            <span class="date"><asp:HyperLink runat="server" ID="HyperLink4" NavigateUrl="~/SwimStories/ClaireHolt.aspx"><asp:Image runat="server" ID="claireImage" ImageUrl="~/SwimStories/App_Images/ClaireHolt_tn.jpg" AlternateText="Claire Holt - Why I took part" ToolTip="Claire Holt - Why I took part" CssClass="img" /></asp:HyperLink></span>
            <strong>Claire Holt</strong><br />Why I took part<br />&nbsp;
        </p>
    
</div>