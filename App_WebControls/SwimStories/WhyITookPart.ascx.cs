﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using ThirteenStrides.Web;
using GS.Generic.Web;

public partial class App_WebControls_SwimStories_WhyITookPart : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = { new SqlParam("limit", SqlDbType.Int, 4) };

        DataSetResponse res = connection.RunCommand("gs_front_select_insp_stories", paras, "stories");

        if (res.ReturnCode == 0)
        {
            storiesRepeater.DataSource = res.Result.Tables["stories"].DefaultView;
            storiesRepeater.DataBind();
        }
        else
        {

        }
    }
}
