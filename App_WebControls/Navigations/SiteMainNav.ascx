﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteMainNav.ascx.cs" Inherits="App_WebControls_Navigations_SiteMainNav" %>
<div class="nav-collapse collapse">

    <ul class="nav">
    
    	 <li class="dropdown">
                <asp:HyperLink ID="HlEvents" NavigateUrl="/Event" runat="server" class="dropdown-toggle" data-toggle="dropdown">Distances</asp:HyperLink>
                <ul class="dropdown-menu">
                    <li>
                      <ul class="dropdown-col">
                  		    <li><a href="/Distances/13-miles" class="nav-header">13 Miles</a></li>
                  		    <li><a href="/Distances/13-miles"><img src="/img/css/dropdown-event1.jpg" alt="" /></a></li>
                  		    <li><a class="btn" href="/Distances/13-miles">Enter Now »</a></li>
                      </ul>
                      <ul class="dropdown-col">
                  		    <li><a href="/Distances/26-miles" class="nav-header">26 Miles</a></li>
                  		    <li><a href="/Distances/26-miles"><img src="/img/css/dropdown-event2.jpg" alt="" /></a></li>
                  		    <li><a class="btn" href="/Distances/26-miles">Enter Now »</a></li>
                      </ul>
                      <ul class="dropdown-col">
                  		    <li><a href="/Distances/52-miles" class="nav-header">52 Miles</a></li>
                  		    <li><a href="/Distances/52-miles"><img src="/img/css/dropdown-event3.jpg"  alt="" /></a></li>
                 		    <li><a class="btn" href="/Distances/52-miles">Enter Now »</a></li>
                      </ul>
                      <ul class="dropdown-col">
                  		    <li><a href="https://entry.enteronline.org/Login.aspx?StreamID=1018" class="nav-header">Official jersey</a></li>
                  		    <li><a href="https://entry.enteronline.org/Login.aspx?StreamID=1018"><img src="/img/css/dropdown-event4.jpg"  alt="" /></a></li>
                 		    <li><a class="btn" href="https://entry.enteronline.org/Login.aspx?StreamID=1018" target="_blank">Buy Now</a></li>
                      </ul>
                    </li>
                </ul>
              </li>
        <li>
            <asp:HyperLink ID="HlBlog" NavigateUrl="~/Blog" runat="server">Cycle Blog</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HlNews" NavigateUrl="~/News" runat="server">News</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HlCharity" NavigateUrl="~/About/Cycle-for-Charity" runat="server">Cycle for Charity</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HlResults" NavigateUrl="~/Results" runat="server">Results</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HlPhotos" NavigateUrl="~/Photos" runat="server">Photos</asp:HyperLink></li>
			 <li>
            <asp:HyperLink ID="HlMerchandise" NavigateUrl="~/about/merchandise" runat="server">Merchandise</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HlFaq" NavigateUrl="~/About/faq" runat="server">FAQs</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HlContact" runat="server" NavigateUrl="~/Contact" data-toggle="collapse" data-target="#demo">Contact</asp:HyperLink></li>
    </ul>
</div>
<!--/.nav-collapse -->
