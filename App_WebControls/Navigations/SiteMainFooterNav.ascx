﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteMainFooterNav.ascx.cs"
	Inherits="App_WebControls_Navigations_SiteMainFooterNav" %>
<div class="footercol">
	<h4>
		ABOUT</h4>
	<a href="/Event">Distances</a> <a href="/Blog">Cycle Blog</a> <a href="/News">News <a
		href="/Cycle-for-Charity">Cycle for Charity</a></a> <a href="/Results">Results</a>
</div>
<div class="footercol" style="padding-top: 65px;">
	<a href="/Photos">Photos</a> <a href="/about/merchandise">Merchandise</a> <a href="/about/faq">
		FAQs</a> <a href="/Contact">Contact</a>
</div>
<div class="footercol">
	<h4>
		SOCIAL</h4>
	<a href="https://www.facebook.com/greatcycle?fref=ts" target="_blank">Facebook</a>
	<a href="https://twitter.com/Great_Cycle" target="_blank">Twitter</a> <a href="https://plus.google.com/101700984595360626729/posts"
		target="_blank">Google+</a> <a href="https://pinterest.com/greatcycle/" target="_blank">
			Pinterest</a>
</div>
<div class="footercol" style="background: none;">
	<h4>
		EVENTS</h4>
	<a href="http://www.greatrun.org/" target="_blank">Great Run</a> <a href="http://www.greatswim.org/"
		target="_blank">Great Swim</a> <a href="http://www.greatcycle.org/">Great Cycle</a>
	<a href="http://www.greatcitygames.org/" target="_blank">Great CityGames</a> <a href="http://www.greattrailchallenge.org/"
		target="_blank">Great Trail Challenge</a>
</div>
<div class="clearer">
</div>
<p style="background: #333">
	<asp:HyperLink ID="HlGtc" NavigateUrl="/about/Website-Terms-And-Conditions" runat="server">Website Terms and Conditions</asp:HyperLink>
	<asp:HyperLink ID="HlEtc" NavigateUrl="/about/Entry-Terms-And-Conditions" runat="server">Entry Terms and Conditions</asp:HyperLink>
	<asp:HyperLink ID="HlPrivacy" NavigateUrl="/about/Privacy-And-Cookies-Policy" runat="server">Privacy and Cookie Policy</asp:HyperLink>
    <asp:HyperLink ID="HlSitemap" NavigateUrl="/about/Sitemap" runat="server">Sitemap</asp:HyperLink>
</p>