﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class App_WebControls_Navigations_SiteMainNav : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Contains("blog"))
        {
            HlBlog.CssClass = "nav-active";
        }
        else if (Contains("event"))
        {
            HlEvents.CssClass = "nav-active";
        }
        else if (Contains("news"))
        {
            HlNews.CssClass = "nav-active";
        }
        else if (Contains("cycle-for-charity"))
        {
            HlCharity.CssClass = "nav-active";
        }
        else if (Contains("results"))
        {
            HlResults.CssClass = "nav-active";
        }
        else if (Contains("photos"))
        {
            HlPhotos.CssClass = "nav-active";
        }
        else if (Contains("merchandise"))
        {
            HlMerchandise.CssClass = "nav-active";
        }
        else if (Contains("faq"))
        {
            HlFaq.CssClass = "nav-active";
        }
        else if (Contains("contact"))
        {
            HlContact.CssClass = "nav-active";
        }
    }
    private bool Contains(string param)
    {

        return Request.RawUrl.ToLower().Contains("/" + param);
    }
}