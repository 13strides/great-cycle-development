﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactUsForm.ascx.cs" Inherits="App_WebControls_Forms_ContactUsForm" %>
 <h4>Please feel free to get in touch</h4>
<asp:Literal ID="LiteralMessage" runat="server" />
 <label>Name</label><asp:TextBox ID="TbName" runat="server"></asp:TextBox> 
 <label>Email</label><asp:TextBox ID="TbEmail" runat="server"></asp:TextBox> 
 <label>Message</label><asp:TextBox ID="TbMessage" TextMode="MultiLine" runat="server"></asp:TextBox>
<!--"6LcP3t0SAAAAAC3nmJOVA6Ik6M-TYEo3yfB_XoR2", "6LcP3t0SAAAAAGiZ4ccC7UXV_yYJ6Wxm-4IUg2vp" -->
<script type="text/javascript"
       src="http://www.google.com/recaptcha/api/challenge?k=6Leo3t0SAAAAAChyELJSxk4cVLDTjKDww97sRBDW">
    </script>
    <noscript>
       <iframe src="http://www.google.com/recaptcha/api/noscript?k=6Leo3t0SAAAAAChyELJSxk4cVLDTjKDww97sRBDW"
           height="300" width="500" frameborder="0"></iframe><br>
       <textarea name="recaptcha_challenge_field" rows="3" cols="40">
       </textarea>
       <input type="hidden" name="recaptcha_response_field"
           value="manual_challenge">
    </noscript>
<asp:Button cssClass="btn btn-primary" ID="ButtonSend" runat="server" Text="Send" OnClick="ButtonSend_Click" />