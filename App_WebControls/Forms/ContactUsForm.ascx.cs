﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Configuration;
public partial class App_WebControls_Forms_ContactUsForm : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LiteralMessage.Text = string.Empty;
    }
    protected void ButtonSend_Click(object sender, EventArgs e)
    {
        string email = TbEmail.Text;
        string name = TbName.Text;
        string messageText = TbMessage.Text;

            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(email)
                && !string.IsNullOrEmpty(messageText))
            {
                string toAddress = ConfigurationManager.AppSettings["ApplicationEmail"].ToString();
                try
                {
                    //(1) Create the MailMessage instance
                    MailMessage message = new MailMessage(email, toAddress);

                    //(2) Assign the MailMessage's properties
                    message.Subject = "Great Cycle Support";
                    message.Body = messageText;
                    message.IsBodyHtml = true;

                    //(3) Create the SmtpClient object
                    SmtpClient smtp = new SmtpClient();

                    //(4) Send the MailMessage (will use the Web.config settings)
                    smtp.Send(message);

                    //(5) Notify user
                    LiteralMessage.Text = "<p class=\"success\">Message has been send successfully.</p>";
                }
                catch (Exception ex)
                {
                    LiteralMessage.Text = "<p class=\"error\">Couldn't send your message. Please contact the customer services at: <a href=\"mailto:help@greatcycle.org\">help@greatcycle.org</a>.</p>";
                }

            }
            else
            {
                LiteralMessage.Text = "<p class=\"error\">All fields are required.</p>";

            }
        
    }
}