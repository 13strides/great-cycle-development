﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeroSlider.ascx.cs" Inherits="App_WebControls_HeroSlider_HeroSlider" %>
<!-- do we need a non js first slide-->
<div class="hero-unit hero-gms">
	<div id="myCarousel" class="carousel slide">
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<!-- Carousel items -->
		<div class="carousel-inner">
			<asp:Literal runat="server" ID="carouselItemsLiteral" />
			<!--<div class="active item">
				<img class="hero-mainimage" src="img/hero/hero1.jpg">
				<img src="img/hero/hero-overlay.png" class="hero-overlay">
				<h1>
					3 Distances</h1>
				<p style="color: #888;">
					One Amazing Day</p>
				<div class="hero-box">
					Get Out and Pedal over 13 miles</div>
				<div class="hero-box">
					Rule the Roads over 26 miles</div>
				<div class="hero-box">
					Test Yourself over 52 miles</div>
				<p>
					<a href="/Event/" class="btn btn-primary btn-large" style="background: #fff; color: #222;">
						Enter Now »</a></p>
			</div>
			<div class="item">
				<img class="hero-mainimage" src="img/hero/hero2.jpg">
				<img src="img/hero/hero-overlay.png" class="hero-overlay">
				<h1>
					Official Event Jersey</h1>
				<p style="color: #888;">
					Official Event Jersey</p>
				<div class="hero-box">
					Official Event Jersey</div>
				<p>
					<a href="/Merchandise/" class="btn btn-primary btn-large" style="background: #fff;
						color: #222;">View Merchandise »</a></p>
			</div>
			<div class="item">
				<img class="hero-mainimage" src="img/hero/hero3.jpg">
				<img src="img/hero/hero-overlay.png" class="hero-overlay">
				<h1>
					Be prepared.</h1>
				<p style="color: #888;">
					Our cycle blog will talk about everything you need to kow about the big day and
					beyond</p>
				<p>
					<a href="/Blog/" class="btn btn-primary btn-large" style="background: #fff; color: #222;">
						View Blog »</a></p>
			</div>-->
		</div>
		<!-- Carousel nav -->
		<a class="carousel-control left" href="#myCarousel" data-slide="prev">‹</a> <a class="carousel-control right"
			href="#myCarousel" data-slide="next">›</a>
	</div>
</div>