﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;

public partial class App_WebControls_HeroSlider_HeroSlider : System.Web.UI.UserControl {

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			BindCarouselItems();
		}
	}

	private void BindCarouselItems() {
		List<HeroSlider> hs = HeroSlider.GetSlider(3, true);
		StringBuilder sb;
		bool First = true;
		if (hs.Count > 0) {
			foreach (HeroSlider slide in hs) {
				sb = new StringBuilder();
				if (First) {
					sb.Append("<div class=\"active item\">");
					First = false;
				} else {
					sb.Append("<div class=\"item\">");
				}
				sb.Append("<img class=\"hero-mainimage\" src=\"/App_Files/ImageLibrary/Homepage/" + slide.Image + "\">");
				sb.Append("<img src=\"img/hero/hero-overlay.png\" class=\"hero-overlay\">");
				sb.Append(slide.Message);
				sb.Append("<p>");
				sb.Append("<a href=\"" + slide.Link_url + "\" class=\"btn btn-primary btn-large\" style=\"background: #fff; color: #222;\">");
				sb.Append(slide.Link_text + "</a></p>");
				sb.Append("</div>");
				carouselItemsLiteral.Text += sb;
			}
		} else {
			Response.Write("Slideshow failed to initialise");
		}

		/*
		 * <div class="active item">
		<img class="hero-mainimage" src="img/hero/hero1.jpg">
		<img src="img/hero/hero-overlay.png" class="hero-overlay">
		<h1>
			3 Distances</h1>
		<p style="color: #888;">
			One Amazing Day</p>
		<div class="hero-box">
			Get Out and Pedal over 13 miles</div>
		<div class="hero-box">
			Rule the Roads over 26 miles</div>
		<div class="hero-box">
			Test Yourself over 52 miles</div>
		<p>
			<a href="/Event/" class="btn btn-primary btn-large" style="background: #fff; color: #222;">
				Enter Now »</a></p>
	</div>
		 * */
	}
}