﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SecondaryLondon.ascx.cs" Inherits="App_WebControls_SecondaryContent_SecondaryLondon" %>
    <div class="box oneThirds" id="shop">
        <h2 class="box-title">Great Swim Shop</h2>
        
            <p class="highlight"><a href="/store/Default.aspx"><img src="../../App_Images/contentImages/homepanels/store/store-thumb.jpg"
                alt="Great Swim Shop" class="box-thumb" /></a>We have everything you need to get the most out of your swimming right here in the Great Swim Shop, in collaboration with blueseventy.</p>
        <h3 class="box-sub-title"><a href="/store/Default.aspx">Visit Great Swim Shop</a></h3>
        <div class="box-content">
            <ul class="links">
            	<li><a href="/store/Default.aspx"><img src="../../App_Images/contentImages/homepanels/store/store3-list.jpg" alt="Swimming wetsuits available to buy and hire" /> <span>Wetsuit Hire</span></a></li>
                <li><a href="/store/Default.aspx"><img src="../../App_Images/contentImages/homepanels/store/store1-list.jpg" alt="Goggles designed for outdoor swimming" /> <span>Goggles</span></a></li>
            </ul>
                <div class="clearer"></div>
        </div>
    </div>
    <div class="box oneThirds" id="newsletter">
        <h2 class="box-title">Great Swim Newsletter</h2>
        <p class="highlight"><a href="/edm/administration/subscribe/default.aspx"><img src="../../App_Images/contentImages/newsletter-thumb.jpg"
                alt="Great Swim Newsletter" class="box-thumb" /></a>Sign up here for the Great Swim Newsletter, plus find out first about new events,
                additional places becoming available and much more...</p>
        <h3 class="box-sub-title"><a href="/edm/administration/subscribe/default.aspx">Subscribe to Newsletter</a></h3>
        <div class="box-content">

            	<ul class="links">
                	<li><a href="http://www.facebook.com/greatswim"><img src="../../App_Images/contentImages/homepanels/social/facebook.gif" alt="Extended community site for friends of Great Swim" /> <span>Join us on Facebook</span></a></li>
                    <li><a href="http://twitter.com/great_swim"><img src="../../App_Images/contentImages/homepanels/social/twitter.gif" alt="Great Swim on Twitter" /> <span>Follow us on Twitter</span></a></li>
                </ul>
                <div class="clearer"></div>

        </div>
    </div>
    
    <div class="box oneThirds" id="news">
        <h2 class="box-title">Event News</h2>
        <a href="/News/Default.aspx"><img id="Img1" runat="server" src="~/App_Images/contentImages/tracker.jpg" /></a><h3 class="box-sub-title"><a href="/News/Default.aspx">More Great Swim News</a></h3>
        <div class="box-content">
            <asp:Literal runat="server" ID="Literal1"></asp:Literal>
            <asp:Repeater runat="server" ID="myNewsRepeater">
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate>
                <p class="eventNews"><asp:HyperLink runat="server" ID="newsHyperLink" NavigateUrl='<%# "~/News/news_article.aspx?nid=" 
                    + Eval("news_id") + "" %>'><%# Eval("news_title") %></asp:HyperLink></p>
            </ItemTemplate>
        </asp:Repeater>
        </div>
    </div>
    <asp:Panel Visible="false" runat="server" ID="archiveContent">
    <div class="secondaryContentPanel secEventMap" style="min-height: 259px;">
       <h3>Great Swim on Channel 4</h3>
    <span style="margin-left:15px;"><asp:HyperLink runat="server" ID="HyperLink3" NavigateUrl="~/TV/Default.aspx"><img src="/App_Images/secondaryBackgrounds/channel4tv.jpg" border="0" /></span><p><strong>Missed the Great Swim TV programmes on Channel 4? Watch the four-part series online with Great Swim TV</strong></asp:HyperLink></p>
<p class="moreInfo"><asp:HyperLink runat="server" NavigateUrl="~/TV/Default.aspx" ID="moreNewsLink4"><span>More news</span></asp:HyperLink></p></div>

        
    <div class="secondaryContentPanel secEventMap" style="min-height: 259px;">
        <h3>Event News</h3>
        <asp:Literal runat="server" ID="errorLiteral"></asp:Literal>
        
        <p class="moreInfo"><asp:HyperLink runat="server" NavigateUrl="~/News/Default.aspx?filter=3" ID="moreNewsLink"><span>More news</span></asp:HyperLink></p>
    </div>
    <div class="secondaryContentPanel smallContent fixedButton">
        <h3>Swim Shop</h3>
        <p><a href="http://www.thetristore.com/GreatSwim" target="_blank"><asp:Image runat="server" ID="tTTSImage" ImageUrl="~/App_Images/secondaryBackgrounds/shop.jpg" AlternateText="Great Swim Store" />
            New Great Swim store powered by the Tristore

</a></p>
        <p class="moreInfo"><a href="http://www.thetristore.com/GreatSwim" target="_blank"><span>Find out more</span></a></p>
    </div>
    <div class="secondaryContentPanel smallContent fixedButton">
        <h3>Swim Tracker</h3>
        <p><asp:HyperLink runat="server" ID="mapMySwim" NavigateUrl="~/Training/MapTrack/Default.aspx"><asp:Image runat="server" ID="Image1" ImageUrl="~/App_Images/secondaryBackgrounds/MapMySwim.jpg" AlternateText="Map My Swim" />
            Find or share your nearest lake, lock or dock

</asp:HyperLink></p>
        <p class="moreInfo"><asp:HyperLink runat="server" ID="HyperLink2" NavigateUrl="~/Training/MapTrack/Default.aspx"><span>Find out more</span></asp:HyperLink></p>
    </div>
    </asp:Panel>