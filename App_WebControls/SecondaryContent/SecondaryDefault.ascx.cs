﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class App_WebControls_SecondaryContent_SecondaryDefault : System.Web.UI.UserControl
{
    string linkCookie;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if GreatRunLinkCookie is found, append it to the registerlink attributes
            if (Request.Cookies["GreatSwimEmailCookie"] != null)
            {
                linkCookie = Request.Cookies["GreatSwimEmailCookie"].Value;

            }
            CreateRegisterNavLink();
        }
    }

    private void CreateRegisterNavLink()
    {
        if (linkCookie != String.Empty && linkCookie != null)
        {
            //           eventEntryLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GSSStreamID"] + "&" + linkCookie;
            /*London*/
            //GLSEnterLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GLSStreamID"] + "&" + linkCookie;
            /*East*/
            //GESEnterLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GESStreamID"] + "&" + linkCookie;
            /*Scottish*/
            //GSSEnterLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GSSStreamID"] + "&" + linkCookie;
            /*North*/
            //GNSEnterLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GNSStreamID"] + "&" + linkCookie;
        }
        else
        {
            //            eventEntryLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GSSStreamID"];
            /*London*/
            //GLSEnterLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GLSStreamID"];
            /*East*/
            //GESEnterLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GESStreamID"];
            /*Scottish*/
            //GSSEnterLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GSSStreamID"];
            /*North*/
            //GNSEnterLink.NavigateUrl = ConfigurationManager.AppSettings["EntrySystemUrl"] + ConfigurationManager.AppSettings["GNSStreamID"];
        }
    }

}
