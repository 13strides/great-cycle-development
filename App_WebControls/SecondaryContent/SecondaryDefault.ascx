﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SecondaryDefault.ascx.cs" Inherits="App_WebControls_SecondaryContent_SecondaryDefault" %>
    <div class="box oneThirds">
    	<h2 class="box-title">Great Swim Shop</h2>
        
        	<p class="highlight"><a href="/store/Default.aspx"><asp:Image runat="server" ID="storeThumbImage" ImageUrl="~/App_Images/contentImages/homepanels/store/store-thumb.jpg" AlternateText="Great Swim Shop in collaboration with blueseventy" CssClass="box-thumb" /></a>We have everything you need to get the most out of your swimming right here in the Great Swim Shop, in collaboration with blueseventy.</p>
        
        <h3 class="box-sub-title"><a href="/store/Default.aspx" target="_blank">Visit Great Swim Shop</a></h3>
        <div class="box-content">
            <ul class="links">
            	<li><a href="/store/Default.aspx">
            	<asp:Image runat="server" ID="wetsuitsImage" ImageUrl="~/App_Images/contentImages/homepanels/store/store3-list.jpg" AlternateText="Swimming wetsuits available to buy and hire" /> <span>Wetsuit Hire</span></a></li>
                <li><a href="/store/Default.aspx">
                <asp:Image runat="server" ID="gogglesImage" ImageUrl="~/App_Images/contentImages/homepanels/store/store1-list.jpg" alt="Goggles designed for outdoor swimming" /> <span>Goggles</span></a></li>
            </ul>
            <div class="clearer"></div>
        </div>
        
    </div>
    <div class="box oneThirds">
    	<h2 class="box-title">Facebook &amp; Twitter</h2>
        <div class="highlight" id="socialThumbs">
        	<ul class=" jcarousel-skin-tango">
				<li style="text-align:center;"><a href="http://www.facebook.com/album.php?aid=242221&id=124286752281">
				    <asp:Image runat="server" ID="facebookThumb" ImageUrl="~/App_Images/contentImages/homepanels/social/box-img.jpg" style="border:5px solid #FFFFFF !important" AlternateText="Great Swim Galleries on Facebook" /></a></li>
            </ul>
        </div>
        <h3 class="box-sub-title"><a href="http://www.facebook.com/greatswim#!/greatswim?v=photos">View Facebook Galleries</a></h3>
        <div class="box-content">
        	<ul class="links">
            	<li><a href="http://www.facebook.com/greatswim" target="_blank"><asp:Image runat="server" ID="facebookLogoImage" ImageUrl="~/App_Images/contentImages/homepanels/social/facebook.gif" AlternateText="Extended community site for friends of Great Swim" /> <span>Join us on Facebook</span></a></li>
                <li><a href="http://twitter.com/great_swim" target="_blank"><asp:Image runat="server" ID="twitterLogoImage" ImageUrl="~/App_Images/contentImages/homepanels/social/twitter.gif" AlternateText="Great Swim on Twitter" /> <span>Follow us on Twitter</span></a></li>
            </ul>
            <div class="clearer"></div>
        </div>
    </div>
    <div class="box oneThirds">
        <h2 class="box-title">Newsletter</h2>
        <div class="box-content newsletterHeight"><a href="/edm/administration/subscribe/default.aspx"><asp:Image runat="server" ID="newsletterImage" CssClass="box-thumb" ImageUrl="~/App_Images/contentImages/homepanels/social/newletter.gif" AlternateText="Great Swim Newletter" /></a>
            <p><br /><strong>Sign up for the Great Swim Newsletter</strong>,<br />get the very latest info on events, training advice and essential key points for your swim!</p><p><asp:HyperLink runat="server" ID="SubscribeLink" Target="_blank" NavigateUrl="http://www.greatswim.org/edm/administration/subscribe/default.aspx" CssClass="button subscribe"><span>Subscribe</span></asp:HyperLink></p>
            <div class="clearer"></div>
        </div>
    </div>
    <div class="clearer"></div>
    
       

