﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using GS.Generic.Web;
using ThirteenStrides.Database;

public partial class App_WebControls_SecondaryContent_SecondaryManchester : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                BindData(6);
                /*2 GNS
                  3 GLS
                  4 GSS
                  5 GES   
                 6 GMS*/
            }
            catch
            {
                errorLiteral.Text = "Filtered news unsuccessful";
            }
        }

    }
    private void BindData(int _event)
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = { new SqlParam("@event", SqlDbType.Int, _event),
                                new SqlParam("@limit", SqlDbType.Int, 3)};

        DataSetResponse res = connection.RunCommand("gs_front_select_news_limit", paras, "news");

        if (res.ReturnCode == 0)
        {
            newsRepeater.DataSource = res.Result.Tables["news"].DefaultView;
            newsRepeater.DataBind();
        }
        else
        {
            errorLiteral.Text = "The news failed to load" + res.ReturnCode.ToString() + res.ErrorMessage;
        }

    }
}
