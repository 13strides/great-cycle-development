﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatePicker.ascx.cs" Inherits="App_WebControls_DatePicker" %>

<asp:DropDownList ID="dayDropDown" runat="server">
	<asp:ListItem>01</asp:ListItem>
	<asp:ListItem>02</asp:ListItem>
	<asp:ListItem>03</asp:ListItem>
	<asp:ListItem>04</asp:ListItem>
	<asp:ListItem>05</asp:ListItem>
	<asp:ListItem>06</asp:ListItem>
	<asp:ListItem>07</asp:ListItem>
	<asp:ListItem>08</asp:ListItem>
	<asp:ListItem>09</asp:ListItem>
	<asp:ListItem>10</asp:ListItem>
	<asp:ListItem>11</asp:ListItem>
	<asp:ListItem>12</asp:ListItem>
	<asp:ListItem>13</asp:ListItem>
	<asp:ListItem>14</asp:ListItem>
	<asp:ListItem>15</asp:ListItem>
	<asp:ListItem>16</asp:ListItem>
	<asp:ListItem>17</asp:ListItem>
	<asp:ListItem>18</asp:ListItem>
	<asp:ListItem>19</asp:ListItem>
	<asp:ListItem>20</asp:ListItem>
	<asp:ListItem>21</asp:ListItem>
	<asp:ListItem>22</asp:ListItem>
	<asp:ListItem>23</asp:ListItem>
	<asp:ListItem>24</asp:ListItem>
	<asp:ListItem>25</asp:ListItem>
	<asp:ListItem>26</asp:ListItem>
	<asp:ListItem>27</asp:ListItem>
	<asp:ListItem>28</asp:ListItem>
	<asp:ListItem>29</asp:ListItem>
	<asp:ListItem>30</asp:ListItem>
	<asp:ListItem>31</asp:ListItem>
</asp:DropDownList>
<asp:DropDownList ID="monthDropDown" runat="server" OnLoad="AddStartDaysMonthBehaviour">
	<asp:ListItem Value="01">Jan</asp:ListItem>
	<asp:ListItem Value="02">Feb</asp:ListItem>
	<asp:ListItem Value="03">Mar</asp:ListItem>
	<asp:ListItem Value="04">Apr</asp:ListItem>
	<asp:ListItem Value="05">May</asp:ListItem>
	<asp:ListItem Value="06">Jun</asp:ListItem>
	<asp:ListItem Value="07">Jul</asp:ListItem>
	<asp:ListItem Value="08">Aug</asp:ListItem>
	<asp:ListItem Value="09">Sep</asp:ListItem>
	<asp:ListItem Value="10">Oct</asp:ListItem>
	<asp:ListItem Value="11">Nov</asp:ListItem>
	<asp:ListItem Value="12">Dec</asp:ListItem>
</asp:DropDownList>
<asp:DropDownList ID="yearDropDown" runat="server" OnLoad="AddStartDaysYearBehaviour">
</asp:DropDownList>
