﻿function GenerateStartDays(prefix) {
	// getting the selected month
	var monthDrpDwn = document.getElementById(prefix+"monthDropDown");
	
	if (monthDrpDwn != null) {
		var month = monthDrpDwn.options[monthDrpDwn.selectedIndex].value;

		// getting the start day dropdown list
		var daysDrpDwn = document.getElementById(prefix+"dayDropDown");
		
		if (daysDrpDwn != null) {
			// alert ("value: " + daysDrpDwn.options.length);
			if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
				switch (daysDrpDwn.options.length) {
					case 30:
						daysDrpDwn.options[30] = new Option("31", "31");
						break;
					case 29:
						daysDrpDwn.options[29] = new Option("30", "30");
						daysDrpDwn.options[30] = new Option("31", "31");
						break;
					case 28:
						daysDrpDwn.options[28] = new Option("29", "29");
						daysDrpDwn.options[29] = new Option("30", "30");
						daysDrpDwn.options[30] = new Option("31", "31");
						break;
					default:
						break;
				}
			} else {
				daysDrpDwn.options[30] = null;
				
				// February is different!
				if (month == 2) {
					daysDrpDwn.options[29] = null;
					
					// finding selected year
					var yearDrpDwn = document.getElementById(prefix + "yearDropDown");
					
					if (yearDrpDwn != null) {
						var year = yearDrpDwn.options[yearDrpDwn.selectedIndex].value;
						
						// detecting leap year
						if ((year - 2000) % 4 != 0) {
							// not leap year
							daysDrpDwn.options[28] = null;
						} else {
							// leap year
							if (daysDrpDwn.options.length < 29) daysDrpDwn.options[28] = new Option("29", "29");
						}
					} else {
						alert("Couldn't find year list");
					}
				}
			}
		} else {
			alert("Couldn't find day list");
		}
	} else {
		alert("Couldn't find month list");
	}
}