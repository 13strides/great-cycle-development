﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class App_WebControls_DatePicker : System.Web.UI.UserControl {
	int _maxDisplayYears;
	int _startYear;
	DateTime _selectedDate;

	public App_WebControls_DatePicker() {
		_maxDisplayYears = 15;
		_selectedDate = DateTime.Today;
		_startYear = _selectedDate.Year - 5;
	}
	
	protected void Page_Init(object sender, EventArgs e) {
		if (!IsPostBack) {
			// registering the javascript
			Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "DatePicker", Page.ResolveClientUrl("~/App_WebControls/DatePicker/DatePickerJs.js"));
			
			for (int i = _startYear; i < _startYear + _maxDisplayYears; i++) {
				yearDropDown.Items.Add(new ListItem(i.ToString(), i.ToString()));
			}

			try {
				try {
					// removing useless days from the days list (months in 28, 29 or 30 days only)
					DateTime lastDayOfMonth = new DateTime(_selectedDate.Year, _selectedDate.Month, 1).AddDays(-1);
					for (int i = lastDayOfMonth.Day - 1; i < 31; i++) {
						dayDropDown.Items.RemoveAt(i);
					}
				} catch { }

				dayDropDown.SelectedValue = _selectedDate.Day.ToString("00");
				monthDropDown.SelectedValue = _selectedDate.Month.ToString("00");
				yearDropDown.SelectedValue = _selectedDate.Year.ToString();
			} catch { }
		}
	}

	private void BindYearsDropDown() {
		yearDropDown.Items.Clear();
		for (int i = _startYear; i < _startYear + _maxDisplayYears; i++) {
			yearDropDown.Items.Add(new ListItem(i.ToString(), i.ToString()));
		}
	}

	/// <summary>
	/// gets or sets the maximum number of years to display. Defautlt: 15.
	/// </summary>
	public int MaxDisplayYears {
		get { return _maxDisplayYears; }
		set {
			if (value > 0) {
				_maxDisplayYears = value;
				BindYearsDropDown();
			} else {
				throw new Exception("MaxDisplayYears cannot be set to 0 or less.");
			}
		}
	}

	/// <summary>
	/// gets or sets the first year to be displayed
	/// </summary>
	public int StartYear {
		get { return _startYear; }
		set { _startYear = value; }
	}

	/// <summary>
	/// Gets the selected date
	/// </summary>
	[Localizable(true)]
	public DateTime SelectedDate {
		get {
			_selectedDate = new DateTime(Convert.ToInt32(yearDropDown.SelectedValue), Convert.ToInt32(monthDropDown.SelectedValue), Convert.ToInt32(dayDropDown.SelectedValue));
			return _selectedDate;
		}

		set {
			_selectedDate = value;
			if (_startYear > value.Year) {
				_startYear = value.Year;
				BindYearsDropDown();
			}
			try {
				dayDropDown.SelectedValue = _selectedDate.Day.ToString("00");
				monthDropDown.SelectedValue = _selectedDate.Month.ToString("00");
				yearDropDown.SelectedValue = _selectedDate.Year.ToString();
			} catch {
				try {
					_selectedDate = DateTime.Today;
					dayDropDown.SelectedValue = _selectedDate.Day.ToString("00");
					monthDropDown.SelectedValue = _selectedDate.Month.ToString("00");
					yearDropDown.SelectedValue = _selectedDate.Year.ToString();
				} catch { }
			}
		}
	}

	protected void AddStartDaysMonthBehaviour(Object sender, EventArgs e) {
		// find the control's prefix
		DropDownList list = ((DropDownList)sender);

		int idLength = list.UniqueID.Length;
		int nameIndex = list.UniqueID.LastIndexOf("monthDropDown");
		string prefix = "";

		if (nameIndex >= 0) {
			prefix = list.UniqueID.Substring(0, nameIndex).Replace('$', '_');
		}

		list.Attributes.Add("onchange", "javascript: GenerateStartDays('"+ prefix + "')");
	}

	protected void AddStartDaysYearBehaviour(Object sender, EventArgs e) {
		// find the control's prefix
		DropDownList list = ((DropDownList)sender);

		int idLength = list.UniqueID.Length;
		int nameIndex = list.UniqueID.LastIndexOf("yearDropDown");
		string prefix = "";

		if (nameIndex >= 0) {
			prefix = list.UniqueID.Substring(0, nameIndex).Replace('$', '_');
		}

		list.Attributes.Add("onchange", "javascript: GenerateStartDays('" + prefix + "')");
	}
}
