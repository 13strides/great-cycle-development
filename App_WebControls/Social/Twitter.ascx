﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Twitter.ascx.cs" Inherits="App_WebControls_Social_Twitter" %>
<div class="span8">
    <h2>Social</h2>
    <div id="tweet">
        <p>Please wait while my tweets load
            <img src="/images/indicator.gif" /></p>
        <p><a href="http://twitter.com/rem">If you can't wait - check out what I've been twittering</a></p>
    </div>   
</div>
 <div class="span2">
        <h3>Join Our Platforms</h3>
        <a href="https://www.facebook.com/greatcycle?fref=ts" target="_blank"><img src="/img/css/fb.png" /> Facebook</a><br /><br />
        <a href="https://twitter.com/Great_Cycle" target="_blank"><img src="/img/css/tw.png" /> Twitter</a><br /><br />
        <a href="https://plus.google.com/101700984595360626729" rel="publisher" target="_blank"><img src="/img/css/google.png" /> Google +</a><br /><br />
        <a href="https://pinterest.com/greatcycle/" target="_blank"><img src="/img/css/pintrest.png" /> Pinterest</a>
        </div>