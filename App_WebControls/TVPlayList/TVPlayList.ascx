<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TVPlayList.ascx.cs" Inherits="App_WebControls_TVPlayList_TVPlayList" %>
<div class="box fullWidth">
    <h2 class="box-title">
        Latest Videos
    </h2>
    <style>
    .tvVidLink {

    float: left;

    list-style-type: none !important ;

}
    
    </style>
    <div class="box-content" style="height:170px">
        <ul class="mycarousel jcarousel-list jcarousel-list-horizontal">
		<li class="tvVidLink jcarousel-item jcarousel-item-horizontal"><a ID="showipLink" href="http://www.bbc.co.uk/iplayer/episode/b011739w/Great_Salford_Swim/" target="_blank"><asp:Image runat="server" ImageUrl="~/App_Files/Tv_Thumbnails/gss2011-iplayer.jpg" CssClass="img" alternateText="British Gas Great Salford Swim 2011 (BBC iPlayer)" tooltip="British Gas Great Salford Swim 2011 (BBC iPlayer)" Height="95" Width="135"/><p><strong>British Gas Great Salford Swim 2011 (BBC iPlayer)</strong></p></a></li>
		<li class="tvVidLink jcarousel-item jcarousel-item-horizontal"><asp:HyperLink runat="server" ID="show1Link" NavigateUrl="~/TV/Default.aspx?id=1"><asp:Image runat="server" ImageUrl="~/App_Files/Tv_Thumbnails/salford2010.jpg" CssClass="img" alternateText="British Gas Great Salford Swim 2010 Review" tooltip="British Gas Great Salford Swim 2010 Review" Height="95" Width="135"/><p><strong>British Gas Great Salford Swim 2010 Review</strong></p></asp:HyperLink></li>
		<li class="tvVidLink jcarousel-item jcarousel-item-horizontal"><asp:HyperLink runat="server" ID="show2Link" NavigateUrl="~/TV/Default.aspx?id=2"><asp:Image runat="server" ImageUrl="~/App_Files/Tv_Thumbnails/east2010.jpg" CssClass="img" alternateText="British Gas Great East Swim 2010 Review" tooltip="British Gas Great East Swim 2010 Review" Height="95" Width="135"/><p><strong>British Gas Great East Swim 2010 Review</strong></p></asp:HyperLink></li>
		<li class="tvVidLink jcarousel-item jcarousel-item-horizontal"><asp:HyperLink runat="server" ID="show3Link" NavigateUrl="~/TV/Default.aspx?id=3"><asp:Image runat="server" ImageUrl="~/App_Files/Tv_Thumbnails/london2010.jpg" CssClass="img" alternateText="British Gas Great London Swim 2010 Review" tooltip="British Gas Great London Swim 2010 Review" Height="95" Width="135"/><p><strong>British Gas Great London Swim 2010 Review</strong></p></asp:HyperLink></li>
		
		

        </ul>
    </div>
</div>

<script type="text/javascript">
      //  jQuery(document).ready(function() {
       //     jQuery('.mycarousel').jcarousel({
       //       wrap: 'circular'
       //     });

       // });
</script>