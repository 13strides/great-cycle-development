﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlogHeadAndThumbnail.ascx.cs"
	Inherits="App_WebControls_Blog_BlogHeadAndThumbnail" %>
<asp:Repeater ID="Repeater1" runat="server">
	<ItemTemplate>
		<div class="rhc-post">
			<a href="<%# GetDetailsLink(Eval("friendlyUrl")) %>">
				<img src='<%# GetImageFile(Eval("ImageFileName")) %>' alt="" /></a> <a class="rhc-post-title"
					href="<%# GetDetailsLink(Eval("friendlyUrl")) %>">
					<%# TrimString(Eval("Title")) %></a>
			<p class="rhc-description">
				<a href="<%# GetDetailsLink(Eval("friendlyUrl")) %>">
					<%# TrimDescription(Eval("ShortDescription"))  %></a></p>
		</div>
	</ItemTemplate>
</asp:Repeater>
<div class="clearer">
</div>