﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GS.Generic.Web;
using ThirteenStrides.Database;
using System.Configuration;
using System.Data;
using Gr.Model;
public partial class App_WebControls_Blog_BlogTagPanel : System.Web.UI.UserControl
{

    private string _selectedTag;
    private BlogArticleTagList _thisBlogTags;
    public string SelectedTag
    {
        get { return _selectedTag; }
        set { _selectedTag = value; }
    }
    private int _selectedBlog;
    public int SelectedBlog
    {
        get { return _selectedBlog; }
        set { _selectedBlog = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["tagid"] != null)
        {
            SelectedTag = Request.QueryString["tagid"];
        }
        if (Request.QueryString["id"] != null)
        {
            int.TryParse(Request.QueryString["id"].ToString(), out _selectedBlog);
        }
        BindData();
    }
    private void BindData()
    {
        
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = {  };

        SqlDataReaderResponse res = connection.RunCommand("gr_get_blog_tags_with_count", paras);
        if (res.ReturnCode == 0)
        {
                string cssClass = "blog-tag";
                GetTagsOfThisBlog();
                while (res.Result.Read())
                {
                     cssClass = "blog-tag";
                    if((!string.IsNullOrEmpty(_selectedTag) && _selectedTag.Equals(res.Result["TagId"].ToString()))
                        || CheckTagsOfThisBlog(res.Result["TagId"].ToString()))
                    {
                            cssClass = cssClass + " blog-tag-active";
                    }
                    LiteralTags.Text += "<a href='/Blog/" 
                        + Server.UrlEncode(res.Result["Name"].ToString()) + "/"
                        + res.Result["TagId"].ToString()
                        + "' class='" + cssClass + "'>"
                        + res.Result["Name"].ToString() + " ("+ res.Result["Count"].ToString()+ ")</a>";
                }              
            
        }

    }
    private bool CheckTagsOfThisBlog(string tagid)
    {
        bool found = false;
        int tid = 0;
        int.TryParse(tagid, out tid);
        if (tid> 0 && _thisBlogTags != null && _thisBlogTags.Count > 0)
        {
            return _thisBlogTags.Contains(tid);
        }
        return found;
    }
    private void GetTagsOfThisBlog()
    {
        if (_selectedBlog > 0)
        {
            _thisBlogTags = BlogArticleTagList.GetByArticleID(_selectedBlog);            
        }
    }
    protected string TrimString(object obj)
    {
        string str = obj.ToString().Trim();
        if (str.Length > 28)
        {
            str = str.Substring(0, 28) + "...";
        }
        return str;
    }
    protected string GetDetailsLink(object obj)
    {
        return "/Blog/Detail/" + obj.ToString();
    }

}