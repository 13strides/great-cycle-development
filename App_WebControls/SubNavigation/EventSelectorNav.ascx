﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventSelectorNav.ascx.cs" Inherits="App_WebControls_SubNavigation_EventSelectorNav" %>
<div id="subEventStatus">
    <h3>Event Categories</h3>
    <p style="float: left;
width: 240px;">You’re almost ready to roll! Choose your distance and start gearing up for hottest cycle event of the summer. </p>
    <asp:Literal ID="LiteralEvents" runat="server"></asp:Literal>
</div>