﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using System.Configuration;
using System.Data;
using Ni.Events.EventDetails;
public partial class App_WebControls_SubNavigation_EntryStatusNav : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        BindLink();
    }
    protected void BindLink()
    {
        List<EventInfo> list = EventInfo.GetAll();
        if (list != null && list.Count > 0)
        {
            bool thereIsAtLeastOneOpenOrBallotEvent = false;
            
            foreach(EventInfo ef in list)
            {
                if (ef.Status == EventStatus.Open || ef.Status == EventStatus.Ballot)
                {
                    HlEntryStatus.NavigateUrl = "~/Event";
                    HlEntryStatus.Text = "Enter Now &raquo;";
                    thereIsAtLeastOneOpenOrBallotEvent = true;
                    break;
                }
            }
            if (thereIsAtLeastOneOpenOrBallotEvent == false)
            {
                foreach (EventInfo ef in list)
                {
                    if (ef.Status == EventStatus.OpenToCharities )
                    {
                        HlEntryStatus.NavigateUrl = "~/Event";
                        HlEntryStatus.Text = "Find Charities &raquo;";
                        thereIsAtLeastOneOpenOrBallotEvent = true;
                        break;
                    }
                }
            }
            if (thereIsAtLeastOneOpenOrBallotEvent == false)
            {
                EventStatus es = list.ToArray()[0].Status;                
                bool reminder = false;
                // mixture Of Diffrent States;
                foreach (EventInfo ei in list)
                {
                       if (es == EventStatus.ReminderService || 
                            ei.Status == EventStatus.ReminderService)
                        {
                            HlEntryStatus.NavigateUrl = "~/Reminder/" + ei.Ap16Id.ToString();
                            HlEntryStatus.Text = "Register Now &raquo;";
                            reminder = true;
                            break;
                        }
        
                }
                if (reminder == false)
                {
                    HlEntryStatus.Visible = false;
                }
                
 
            }


        }

    }
}