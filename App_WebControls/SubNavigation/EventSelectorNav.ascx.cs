﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using System.Configuration;
using System.Data;
using Ni.Events.EventDetails;
using System.Text;
public partial class App_WebControls_SubNavigation_EventSelectorNav : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindLink();
    }
    protected void BindLink()
    {
        List<EventInfo> list = EventInfo.GetAll();
        StringBuilder sb = new StringBuilder("");
        if (list != null && list.Count > 0)
        {
            string link = "#";
            string text = string.Empty;
            foreach (EventInfo ef in list)
            {
                switch (ef.Status)
                {
                    case EventStatus.Open:
                         link = "/Event/" + ef.EventName + "/"+ ef.Ap16Id.ToString();
                         text = "Enter " + ef.EventName;
                         sb.Append(string.Format(@"<a href=""{0}"">{1}</a><br/>", link, text));
                        break;
                    case EventStatus.Closed:
                        break;
                    case EventStatus.OpenToCharities:
                         link = "/About/Cycle-for-Charity";
                         text = " " + ef.EventName;
                         sb.Append(string.Format(@"<a href=""{0}"">{1}</a><br/>", link, text));
                        break;
                    case EventStatus.ReminderService:
                        link = "/Reminder/" + ef.Ap16Id.ToString();
                        text = "" + ef.EventName;
                        sb.Append(string.Format(@"<a href=""{0}"">{1}</a><br/>", link, text));
                        break;
                    case EventStatus.PriorityEntry:
                        break;
                    case EventStatus.Ballot:
                        link = "/Event/" + ef.EventName + "/"+ ef.Ap16Id.ToString();
                        text = " " + ef.EventName;
                        sb.Append(string.Format(@"<a href=""{0}"">{1}</a><br/>", link, text));
                        break;
                    default:
                        break;
                }
                
            }
            LiteralEvents.Text = sb.ToString();

        }

    }
}