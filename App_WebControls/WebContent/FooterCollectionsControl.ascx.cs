﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventPage;
using NI.Events.Collections;
public partial class App_WebControls_WebContent_Footer : System.Web.UI.UserControl
{
	int _contentPage;
	int _event = 0;
	int _collection = 5;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

	private void BindCollection(int evID, int colID) {
		//int contentId = _contentId;

		//EventPage thisPage = new EventPage(-1, _contentId, true);
		//litContent.Text = thisPage._currentContent;
		EventTypeEventCollection thisCollect = new EventTypeEventCollection(colID, evID);
		string Series = "SPONSOR";

		if (thisCollect != null && thisCollect.ItemList != null && thisCollect.ItemList.Count > 0) {
			foreach (EventCollectionItem row in thisCollect.ItemList) {
				HyperLink newLink = new HyperLink();
				Image newimage = new Image();
				string name = row._partnerName.Replace(" ", "-");
				newimage.ImageUrl = "~/" + row._partnerImage;
				newLink.NavigateUrl = row._partner_url;
				newLink.Target = "_blank";
				newLink.Attributes.Add("onClick", "javascript:pageTracker._trackPageview('/EXIT/" + Series + "/" + name + ".html');");
				Panel1.Controls.Add(newLink);
				newLink.Controls.Add(newimage);
				//litContent.Text += "<div class=\"charityPartner\">" + row._partnerDesc+ "</div>";



				//        britishGasLink.Attributes.Add("onClick", "javascript:pageTracker._trackPageview('/EXIT/SPONSOR/British-Gas.html');");

			}

		}
	}


	#region
	/// <summary>
	/// gets or sets the page to load
	/// </summary>
	public int EventID {
		get { return _event; }
		set {
			_event = value;
			if (CollectionID > 0)
				BindCollection(_event, _collection);

		}
	}


	public int CollectionID {
		get { return _collection; }
		set {
			_collection = value;
			if (EventID>0)
				BindCollection(_event, _collection);
		}
	}

	#endregion
}
