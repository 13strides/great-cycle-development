﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventContent;

public partial class App_WebControls_WebContent_ContentViewer : System.Web.UI.UserControl {
	string _friendlyUrl;
	string _contentPageTitle;
    public string columnOneCss = string.Empty;
	protected void Page_Load(object sender, EventArgs e) {
		BindContentPage(_friendlyUrl);
	}

	private void BindContentPage(string friendlyUrl) {
		EventContent content = new EventContent(friendlyUrl, true);
        if (content != null && content._contentID > 0)
        {
            litContent.Text = content._content;
            this.ContentPageTitle = content._title;
            if (!string.IsNullOrEmpty(content._excerpt))
            {
                SideView.Visible = true;
                columnOneCss = "span7";
                litSummary.Text = content._excerpt;
            }
            else
            {
                columnOneCss = "span11";
                SideView.Visible = false;
            }
        }
		//litHeader.Text = Server.HtmlEncode(content._title);
		//Page.Title = thisPage._currentTitle;
	}

	#region

	/// <summary>
	/// gets or sets the page to load
	/// </summary>
	public string FriendlyUrl {
		get { return _friendlyUrl; }
		set { _friendlyUrl = value; }
	}

	public string ContentPageTitle {
		get { return _contentPageTitle; }
		set { _contentPageTitle = value; }
	}

	#endregion
}