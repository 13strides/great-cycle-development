﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContentViewer.ascx.cs" Inherits="App_WebControls_WebContent_ContentViewer" %>



<div class="<%=columnOneCss %>">
<div id="primary">
    <div class="com75">
        <div class="header">
            <h1><asp:Literal runat="server" ID="litHeader" /></h1>
        </div>
        <div class="content">
            <div class="infoPages">
                <asp:Literal runat="server" ID="litContent" />
            </div>
        </div>
    </div>
</div>
</div>
<asp:Panel ID="SideView" runat="server" Visible="false">
			<div class="span3 rhc">
                <asp:Literal ID="litSummary" runat="server"></asp:Literal>               
			</div>
</asp:Panel>