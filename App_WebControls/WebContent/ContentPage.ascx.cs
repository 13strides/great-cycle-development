﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventPage;
public partial class App_WebControls_WebContent_ContentPage : System.Web.UI.UserControl
{
	int _contentPage;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

	private void BindContentPage(int _contentId) {
		int contentId = _contentId;

		EventPage thisPage = new EventPage(-1, _contentId, true);
		litContent.Text = thisPage._currentContent;
        litHeader.Text = Server.HtmlEncode(thisPage._currentTitle);
        //Page.Title = thisPage._currentTitle;
	}


	#region
	/// <summary>
	/// gets or sets the page to load
	/// </summary>
	public int ContentPage {
		get { return _contentPage; }
		set {
			_contentPage = value;
			BindContentPage(_contentPage);
		}
	}
	#endregion
}
