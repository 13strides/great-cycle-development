﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContentPage.ascx.cs" Inherits="App_WebControls_WebContent_ContentPage" %>


<div id="primary">
    <div class="com75">
        <div class="header">
            <h1><asp:Literal runat="server" ID="litHeader" /></h1>
        </div>
        <div class="content">
            <div class="infoPages">
                <asp:Literal runat="server" ID="litContent" />
            </div>
        </div>
    </div>
</div>
