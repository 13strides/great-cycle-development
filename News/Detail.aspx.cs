﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using Gr.Model;
public partial class News_Detail : System.Web.UI.Page {
	//int newsID = 1;
    public string newsTitle = string.Empty;
    public string metaKey = string.Empty;
    public string metaDescription = string.Empty;
    string friendlyUrl = string.Empty;
	protected void Page_Load(object sender, EventArgs e) {
        LiteralRelatedNewsTitle.Text = string.Empty;
        if (Request.QueryString["furl"] != null)
        {
            friendlyUrl = Request.QueryString["furl"];
            if (!string.IsNullOrEmpty(friendlyUrl))
            {
                BindDataWithFriendly();
            }
            else
            {
                Response.Redirect("~/News");
            }

        }
		
	}

	
    private void BindDataWithFriendly()
    {
        NewsArticle article = NewsArticle.GetByFriendlyUrl(friendlyUrl, false, 1);
        if (article != null && article.ID > 0)
        {
            NewsThumbnail1.ArticleId = article.ID;
            List<NewsArticle> list = new List<NewsArticle>();
            list.Add(article);
            Repeater1.DataSource = list;
            Repeater1.DataBind();

            string metaTitle = article.MetaTitle;
            newsTitle = article.Title;
            metaKey = article.MetaKey;
            metaDescription = article.MetaDescription;

            this.Page.Title = article.Title;
            if (!string.IsNullOrEmpty(metaTitle))
                this.Page.Title = metaTitle;

        }
        else
        {
            Response.Redirect("~/News");
        }
    }

	protected string GetImageFile(object obj) {
		string defaultPath = "~/App_Files/ImageLibrary/NewsFull/";
		string defaultImage = "~/img/css/blog1.jpg";
		string str = obj.ToString().Trim();
		if (str.Length > 0) {
			if (!str.StartsWith("~")) {
				str = defaultPath + str;
				if (!File.Exists(Server.MapPath(str))) {
					str = defaultImage;
				}
			}
		}
		return str;
	}
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (NewsThumbnail1.ItemCount > 0)
        {
            LiteralRelatedNewsTitle.Text = "<h3>" + NewsThumbnail1.NewsType + "</h3>";
        }
    }
}