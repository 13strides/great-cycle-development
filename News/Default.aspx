﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="News_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row pageheading">
        <div class="span6">
            <h1>News</h1>
            <ul class="breadcrumb">
             <strong>You are here:</strong>
                <li><a href="/">Home</a> <span class="divider">/</span></li>
                <li class="active">News</li>
<%--                <li class="active">Current Page</li>--%>
            </ul>
        </div>
        
        <div class="span3 filter">
        <span style="float:left; margin:5px;">Filter By:</span>
         <asp:DropDownList ID="DdlSort" runat="server" AutoPostBack="true">
             <asp:ListItem Text="Newest" Value="False" />
             <asp:ListItem Text="Oldest" Value="True" />
         </asp:DropDownList>
            
        </div>
        
    </div>
    <div class="row">
        <div class="span8 blog-post" style="padding: 0; margin: 0;">
            <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                     <div class="blog-heading">
                    <h2><a href='<%# GetDetailsLink(Eval("FriendlyUrl")) %>'><%# Eval("Title") %></a></h2>
                     <span><%# Eval("PublishDate", "{0:dd MMMM yyyy}") %></span>
                     </div>
                    <asp:Image ID="NewsImg" runat="server" ImageUrl='<%# GetImageFile(Eval("ImageFilename")) %>'/>
<%--                    <span class="img-caption">Lizzie Armitstead Image Caption</span>--%>
                    <div class="para"><%# Eval("ShortDescription") %></div>
                    <div class="read-more"><a href='<%# GetDetailsLink(Eval("FriendlyUrl")) %>'>More</a></div>
                </ItemTemplate>
            </asp:Repeater>



            <div class="pagination">
                <asp:HiddenField ID="HfPage" runat="server" Value="1" />
                <asp:Literal ID="LtPagination" runat="server"></asp:Literal>
            </div>


             

        </div>



        <div class="span3 rhc">
            <h3>Other News</h3>
             <gs:NewsThumbnails ID="NewsThumbnail1" runat="server" LiveOnly="1" TopX="4" />     

        </div>
        <div class="clearer"></div>

        <div class="row social">
        <gs:Twitter ID="TwitterView" runat="server" />        
       
      </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" Runat="Server">
</asp:Content>

