﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="News_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="keywords" content="<%=metaKey %>" />
    <meta name="description" content="<%=metaDescription %>" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <div class="row pageheading">
        <div class="span11">
            <h1 style="font-size:28px; text-transform:none;"><%=newsTitle %></h1>
            <ul class="breadcrumb">
             <strong>You are here:</strong>
                <li><a href="/">Home</a> <span class="divider">/</span></li>
                <li><a href="/News">News</a> <span class="divider">/</span></li>
                <li class="active"><%=newsTitle %></li>
<%--                <li class="active">Current Page</li>--%>
            </ul>
        </div>
        
    </div>
    <div class="row">
        <div class="span8 blog-post" style="padding: 0; margin: 0;">
            <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                     <div class="blog-heading" style="padding-top: 10px;">
                    
                     <span><%# Eval("PublishDate", "{0:dd MMMM yyyy}") %></span>
                     </div>
                    <asp:Image ID="NewsImg" runat="server" ImageUrl='<%# GetImageFile(Eval("ImageFilename")) %>'/>
<%--                    <span class="img-caption">Lizzie Armitstead Image Caption</span>--%>
                    <div class="para"><%# Eval("ArticleContent") %></div>
                  
                </ItemTemplate>
            </asp:Repeater>



            <div class="pagination">
                <asp:HiddenField ID="HfPage" runat="server" Value="1" />
                <asp:Literal ID="LtPagination" runat="server"></asp:Literal>
            </div>

            <div class="article-comment">
                <div id="disqus_thread"></div>
                <script type="text/javascript">
                    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                    var disqus_shortname = 'greatcyclelive'; // required: replace example with your forum shortname

                    /* * * DON'T EDIT BELOW THIS LINE * * */
                    (function () {
                        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
            </div>



        </div>



        <div class="span3 rhc">
            <asp:Literal ID="LiteralRelatedNewsTitle" runat="server"></asp:Literal>
             <gs:NewsThumbnails ID="NewsThumbnail1" runat="server" LiveOnly="1" TopX="4" />     

     
        </div>
        <div class="clearer"></div>

        <div class="row social">
        <gs:Twitter ID="TwitterView" runat="server" />        
       
      </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" Runat="Server">
</asp:Content>

