﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;

public partial class News_Default : System.Web.UI.Page {
	int page = 1;
	int maxPerPage = 3;
	int maxPagesToDisplay = 5;
    bool isAscSort = false;
	protected void Page_Load(object sender, EventArgs e) {
        Form.Action = Request.RawUrl;
        if (Page.IsPostBack)
        {
            isAscSort = Convert.ToBoolean(DdlSort.SelectedValue);
        }
		if (Request.QueryString["id"] != null) {
			int.TryParse(Request.QueryString["id"].ToString(), out page);
			if (page > 0) {
				int start = (page * maxPerPage) - (maxPerPage - 1);
				int end = start + (maxPerPage - 1);
				BindData(1, start, end);
			} else {
				BindData(1, 1, maxPerPage);
			}
		} else {
			BindData(1, 1, maxPerPage);
		}
	}

	private void BindData(int liveOnly, int start, int end) {
		MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

		SqlParam[] paras = { new SqlParam("@liveOnly", SqlDbType.Int, liveOnly), new SqlParam("@start", SqlDbType.Int, start)
                           , new SqlParam("@end", SqlDbType.Int, end), 
                           new SqlParam("@isAscSort", SqlDbType.Bit, isAscSort)};

		DataSetResponse res = connection.RunCommand("gr_get_news_article_paged", paras, "news");
		if (res.ReturnCode == 0) {
			if (res.Result.Tables["news"].Rows.Count > 0) {
				Repeater1.DataSource = res.Result.Tables["news"].DefaultView;
				Repeater1.DataBind();
			}
		}
	}

	private int RowCount(int liveOnly) {
		int count = 0;
		MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

		SqlParam[] paras = { new SqlParam("@liveOnly", SqlDbType.Int, liveOnly) };

		DataSetResponse res = connection.RunCommand("gr_get_news_article_row_count", paras, "count");
		if (res.ReturnCode == 0) {
			if (res.Result.Tables["count"].Rows.Count > 0) {
				count = Convert.ToInt32(res.Result.Tables["count"].Rows[0][0].ToString());
			}
		}
		return count;
	}

	private void BuildPagination() {
		StringBuilder sb = new StringBuilder("");
		int rowCount = RowCount(1);
		int prev = page - 1;
		int next = page + 1;
		if (prev <= 0) prev = 1;
		if (rowCount > maxPerPage) {
			sb.AppendLine("<ul>");
			sb.Append("<li><a href='/News/" + prev.ToString() + "'>Prev</a></li>");
			int display = rowCount / maxPerPage;
			int mod = rowCount % maxPerPage;
			if (mod > 0) display = display + 1;
			if ((maxPerPage * maxPagesToDisplay) > rowCount) {
				for (int p = 1; p <= display; p++) {
					sb.Append("<li><a href='/News/" + p.ToString() + "'>" + p.ToString() + "</a></li>");
				}
			} else {
				for (int p = 1; p <= maxPagesToDisplay; p++) {
					sb.Append("<li><a href='/News/" + p.ToString() + "'>" + p.ToString() + "</a></li>");
				}
			}
			if (next > display) next = display;
			sb.Append("<li><a href='/News/" + next.ToString() + "'>Next</a></li>");
			sb.AppendLine("</ul>");
		}
		LtPagination.Text = sb.ToString();
	}

	protected string GetImageFile(object obj) {
		string defaultPath = "~/App_Files/ImageLibrary/NewsFull/";
		string defaultImage = "~/img/css/blog1.jpg";
		string str = obj.ToString().Trim();
		if (str.Length > 0) {
			if (!str.StartsWith("~")) {
				str = defaultPath + str;
				if (!File.Exists(Server.MapPath(str))) {
					str = defaultImage;
				}
			}
		}
		return str;
	}

	protected string GetDetailsLink(object obj) {
		return "/News/" + obj.ToString();
	}

	protected void Page_PreRender(object sender, EventArgs e) {
		BuildPagination();
		HfPage.Value = Page.ToString();
	}
}