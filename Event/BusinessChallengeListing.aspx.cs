﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessChallengeService;
using Gr.Model;

public partial class Events_BusinessChallengeListing : System.Web.UI.Page {
	BusinessChallengeService.BusinessChallenge bsInfo;
	int eventID = 0;
	Gr.Model.BusinessChallenge bc = null;
	protected Site master;

	protected void Page_Load(object sender, EventArgs e) {
		master = ((Site)this.Master);
		int.TryParse(Request.QueryString["id"], out eventID);
		bc = Gr.Model.BusinessChallenge.GetActiveByEventID(eventID);

		eventNameLiteral.Text = "Great Manchester Cycle";
		tieredListingsPanel.CssClass = "gmc-event";

		bsInfo = new BusinessChallengeService.BusinessChallenge();
		DisplayEventEntrants();
	}

	private void DisplayEventEntrants() {
		int altRow = 0;
		string teamPhotoString = "";
		try {
			GroupInfoResponse bsI = bsInfo.GetGroupInfoForStream(Convert.ToInt32(bc.StreamID), ConfigurationManager.AppSettings["ApplicationGUID"]);
			if (bsI.Successful) {
				//diamondLiteral.Text += "total: " + bsI.GroupList.Length + " stream:" + bc.StreamID;
				foreach (GroupInfoDTO t in bsI.GroupList) {
					if (!String.IsNullOrEmpty(t.HyperLink) && t.ImageName.Length > 1) {
						diamondLiteral.Text += "<div class=\"goldMember\">";
						string localHyperlink = "";
						if (t.HyperLink.Contains("http://")) {
							localHyperlink = t.HyperLink;
						} else {
							localHyperlink = "http://" + t.HyperLink;
						}
						//
						if (t.ImageName.Length > 1) {
							diamondLiteral.Text += "<a href=\"#\" target=\"_blank\" title=\"" + t.Name + "\" class=\"trigger\" id=\"t_" + t.Id + "\">"
								+ "<img src=\"http://groups.greatrun.org/images/groupimages/" + t.ImageName + "\" alt=\"" + t.AltTag + "\" width=\"100\" /></a>";
						}
						if ((!string.IsNullOrEmpty(t.TeamPhotoName)) && (t.TeamPhotoName.Length > 0)) {
							teamPhotoString = "<img src=\"http://groups.greatrun.org/images/teamphotos/" + t.TeamPhotoName + "\" alt=\"\"  />";
						} else {
							teamPhotoString = "";
						}
						diamondLiteral.Text += "<div class=\"charityDescription popup\" id=\"p_t_" + t.Id + "\"><p><strong>" + t.Name + "</strong></p>" + "<p>" + teamPhotoString + t.TeamProfile + " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vel nunc id orci scelerisque imperdiet. Nulla congue interdum aliquam. Aliquam consectetur est ut nisi congue faucibus. Phasellus commodo dolor vitae massa pulvinar gravida. Nunc imperdiet felis in justo pharetra aliquam. Morbi eget mauris eu nunc rutrum feugiat.</p>";
						// ;

						diamondLiteral.Text += "<p><a href=\"" + localHyperlink + "\" target=\"_blank\" title=\"" + t.Name + "\" style=\"color:#0a57c0;\" id=\"t_" + t.Id + "\">Visit the website</a></p>";
						diamondLiteral.Text += "</div></div>";

						if (altRow == 3) {
							altRow = 0;
							diamondLiteral.Text += "<div class=\"clearer\"></div>";
						} else {
							altRow++;
						}
					}
				}
			} else {
				errorMessage.Text += "Error: " + bsI.ErrorMessage + "<br />";
				//errorMessage.Type = Gr.Generic.Web.InfoType.Error;
				errorMessage.Visible = true;
			}
		} catch (Exception e) {
			diamondLiteral.Text += "message: " + e.Message
				+ "<br />Inner Exception: " + e.InnerException;
		}
	}
}