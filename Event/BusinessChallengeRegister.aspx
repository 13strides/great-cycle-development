﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeFile="BusinessChallengeRegister.aspx.cs" Inherits="Events_2012_BusinessChallengeRegister" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
	<!--
		input[type="text"]{width:200px;}
		table tr td, table tr th{font-size:0.8em;}
	-->
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div class="row">
        <div class="span7 abouttheswim">
			<div class="header">
			<h1>
				<asp:Literal runat="server" ID="FormTitle"></asp:Literal></h1>
		</div>


				<asp:Literal runat="server" ID="infoMessage" />
				<div class="clearer">
				</div>
				<asp:Literal ID="LiteralFormDescription" runat="server"></asp:Literal>
				<h3>
					Team Details</h3>
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td style="width: 35%">
							<b>Name of Company
								<asp:RequiredFieldValidator ID="RfvBName" runat="server" ErrorMessage="*" ControlToValidate="TextBoxbName"></asp:RequiredFieldValidator></b>
						</td>
						<td colspan="2">
							<asp:TextBox ID="TextBoxbName" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<b>Address Line 1
								<asp:RequiredFieldValidator ID="RfvAddress1" runat="server" ErrorMessage="*" ControlToValidate="TextBoxAddress1"></asp:RequiredFieldValidator></b>
						</td>
						<td colspan="2">
							<asp:TextBox ID="TextBoxAddress1" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							Address Line 2
						</td>
						<td>
							<asp:TextBox ID="TextBoxAddress2" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<b>Postcode
								<asp:RequiredFieldValidator ID="RfvPostCode" runat="server" ErrorMessage="*" ControlToValidate="TextBoxPostcode"></asp:RequiredFieldValidator>
							</b>
						</td>
						<td colspan="2">
							<asp:TextBox ID="TextBoxPostcode" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<b>City
								<asp:RequiredFieldValidator ID="RfvCity" runat="server" ErrorMessage="*" ControlToValidate="TextBoxCity"></asp:RequiredFieldValidator></b>
						</td>
						<td colspan="2">
							<asp:TextBox ID="TextBoxCity" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<b>Country </b>
						</td>
						<td colspan="2">
							<asp:DropDownList ID="DdlCountry" runat="server">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td>
							<b>Telephone
								<asp:RequiredFieldValidator ID="RfvBTelephone" runat="server" ErrorMessage="*" ControlToValidate="TextBoxbTelephone"></asp:RequiredFieldValidator></b>
						</td>
						<td>
							<asp:TextBox ID="TextBoxbTelephone" runat="server"></asp:TextBox>
						</td>
						<td>
							<asp:RegularExpressionValidator ID="RevBTel" runat="server" ErrorMessage="invalid telephone number"
								ControlToValidate="TextBoxbTelephone" ValidationExpression="[0-9]+"></asp:RegularExpressionValidator>
						</td>
					</tr>
					<tr>
						<td>
							<b>Challenge Distance
								<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
									ControlToValidate="DdlDistance" InitialValue=""></asp:RequiredFieldValidator></b>
						</td>
						<td colspan="2">
							<asp:DropDownList ID="DdlDistance" runat="server">
								<asp:ListItem Text="--distance--" Value="" />
								<asp:ListItem Text="11 Km" Value="11 Km" />
								<asp:ListItem Text="22 Km" Value="22 Km" />
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td>
							<b>No. of Entrants
								<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
									ControlToValidate="TextBoxEntrants"></asp:RequiredFieldValidator></b>
						</td>
						<td>
							<asp:TextBox ID="TextBoxEntrants" runat="server"></asp:TextBox>
						</td>
						<td>
							<asp:RegularExpressionValidator ID="RevEntrants" runat="server" ErrorMessage="number"
								ControlToValidate="TextBoxEntrants" ValidationExpression="(^([0-9]*|\d*\d{1}?\d*)$)"></asp:RegularExpressionValidator>
						</td>
					</tr>
				</table>
				<br />
				<br />
				<h3>
					TEAM CO-ORDINATOR DETAILS</h3>
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td style="width: 35%">
							<b>Name
								<asp:RequiredFieldValidator ID="RfvCName" runat="server" ErrorMessage="*" ControlToValidate="TextBoxcName"></asp:RequiredFieldValidator></b>
						</td>
						<td colspan="2">
							<asp:TextBox ID="TextBoxcName" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<b>Telephone
								<asp:RequiredFieldValidator ID="RfvCTelephone" runat="server" ErrorMessage="*" ControlToValidate="TextBoxcTelephone"></asp:RequiredFieldValidator></b>
						</td>
						<td>
							<asp:TextBox ID="TextBoxcTelephone" runat="server"></asp:TextBox>
						</td>
						<td>
							<asp:RegularExpressionValidator ID="RevCTel" runat="server" ErrorMessage="invalid"
								ControlToValidate="TextBoxcTelephone" ValidationExpression="[0-9]+"></asp:RegularExpressionValidator>
						</td>
					</tr>
					<tr>
						<td>
							<b>Email
								<asp:RequiredFieldValidator ID="RfvCEmail" runat="server" ErrorMessage="*" ControlToValidate="TextBoxcEmail"></asp:RequiredFieldValidator></b>
						</td>
						<td>
							<asp:TextBox ID="TextBoxcEmail" runat="server"></asp:TextBox>
						</td>
						<td>
							<asp:RegularExpressionValidator ID="RevCEmail" runat="server" ErrorMessage="invalid"
								ControlToValidate="TextBoxcEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
						</td>
					</tr>
					<tr>
						<td>
							<b>Confirm Email
								<asp:CompareValidator ID="CvConfrim" runat="server" ErrorMessage="inaccurate" ControlToCompare="TextBoxcEmail"
									ControlToValidate="TextBoxcConfirmEmail"></asp:CompareValidator></b>
						</td>
						<td colspan="2">
							<asp:TextBox ID="TextBoxcConfirmEmail" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td colspan="2">
							<asp:Literal ID="LiteralTerms" runat="server"></asp:Literal>
							<asp:CheckBox ID="CheckBoxAccept" Checked="false" runat="server" Text="Accept" />
						</td>
					</tr>
				</table>
				<asp:Button ID="BtnSubmint" OnClick="BtnSubmit_Click" runat="server" Text="Submit"
					CssClass="button" />
				<div class="clearer">
				</div>
			</div>
		</div>
	
</asp:Content>