﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;

public partial class Events_2012_BusinessChallengeRegister : System.Web.UI.Page {
	int bID = 0;
	int eventID = 0;
	BusinessChallenge bc = null;

	//Events_EventsMasterPage master;
	protected void Page_Load(object sender, EventArgs e) {
		//master = this.Master as Events_EventsMasterPage;
		int.TryParse(Request.QueryString["bid"], out bID);
		int.TryParse(Request.QueryString["id"], out eventID);
		Event evt = new Event();
		if (bID <= 0 || evt.ID != eventID) {
			Response.Redirect("~/Business-Challenges/" + eventID.ToString());
		} else {
			FormTitle.Text = "Register For " + evt.Name + " Business Challenge";
			bc = BusinessChallenge.GetByID(bID);

			if (bc == null || bc.IsOpen == false || bc.LinkActive == false) {
				Response.Redirect("~/Business-Challenge/" + eventID.ToString());
			} else {
				LiteralTerms.Text = "<a href='/Business-Challenge-Terms/"
				+ eventID.ToString() + "/" + bc.TermsID.ToString() +
				"' target='_blank'>" +
				"Terms &amp; Conditions</a>";
				if (!string.IsNullOrEmpty(bc.FormDescription))
					LiteralFormDescription.Text = bc.FormDescription;
			}
		}
	}

	protected void BtnSubmit_Click(object sender, EventArgs e) {
		if (IsValid()) {
			BusinessChallengeForm bf = new BusinessChallengeForm();
			bf.BusinessChallengeID = bID;
			bf.BusinessName = TextBoxbName.Text;
			bf.BusinessAddress1 = TextBoxAddress1.Text;
			bf.BusinessAddress2 = TextBoxAddress2.Text;
			bf.BusinessTelephone = TextBoxbTelephone.Text;
			bf.BusinessCity = TextBoxCity.Text;
			bf.BusinessPostcode = TextBoxPostcode.Text;
			bf.BusinessCountryID = int.Parse(DdlCountry.SelectedValue);
			bf.BusinessSize = BcBusinessSize.Default;
			bf.BusinessChallengeDistance = DdlDistance.SelectedValue;
			bf.NumberOfEntrants = int.Parse(TextBoxEntrants.Text);
			bf.CoordinatorEmail = TextBoxcEmail.Text;
			bf.CoordinatorName = TextBoxcName.Text;
			bf.CoordinatorTelephone = TextBoxcTelephone.Text;
			bf.IsEmailed = SendEmail(bf);

			BusinessChallengeForm resp = BusinessChallengeForm.Save(bf);
			if ((resp != null && resp.ID > 0) || bf.IsEmailed) {
				//ResetForm();
				Response.Redirect("~/Business-Challenges/" + eventID.ToString()+"/true");
			} else {
				infoMessage.Text = "<p class='error'>There was unexpected error whilst saving your registration. Please try again. " + bf.ErrorMessage + "</p>";
			}
		}
	}

	protected void Page_PreRender(object sender, EventArgs e) {
		if (!Page.IsPostBack) {
			BindCountry();
		}
	}

	private void ResetForm() {
		TextBoxbName.Text = string.Empty;
		TextBoxAddress1.Text = string.Empty;
		TextBoxAddress2.Text = string.Empty;
		TextBoxbTelephone.Text = string.Empty;
		TextBoxCity.Text = string.Empty;
		TextBoxEntrants.Text = string.Empty;
		TextBoxPostcode.Text = string.Empty;
		TextBoxcEmail.Text = string.Empty;
		TextBoxcName.Text = string.Empty;
		TextBoxcTelephone.Text = string.Empty;
		BindCountry();
	}

	private bool IsValid() {
		bool valid = false;
		int entrants = 0;
		if (CheckBoxAccept.Checked) {
			if (!string.IsNullOrEmpty(TextBoxbName.Text) && !string.IsNullOrEmpty(TextBoxAddress1.Text)
				&& !string.IsNullOrEmpty(TextBoxCity.Text) && !string.IsNullOrEmpty(TextBoxbTelephone.Text) && !string.IsNullOrEmpty(TextBoxPostcode.Text)
				&& !string.IsNullOrEmpty(TextBoxcName.Text) && !string.IsNullOrEmpty(TextBoxcEmail.Text)
				&& !string.IsNullOrEmpty(TextBoxcTelephone.Text) && int.Parse(DdlCountry.SelectedValue) > 0
				&& !string.IsNullOrEmpty(DdlDistance.SelectedValue) && !string.IsNullOrEmpty(TextBoxEntrants.Text)) {
				int.TryParse(TextBoxEntrants.Text, out entrants);
				if (TextBoxcEmail.Text.ToLower().Equals(TextBoxcConfirmEmail.Text.ToLower())) {
					if (entrants > 0)
						valid = true;
					else {
						infoMessage.Text = "<p class='error'>Number of Entrants must be greater than zero.</p>";
					}
				} else {
					infoMessage.Text = "<p class='error'>Your email confirmation was incorrect.</p>";
				}
			} else {
				infoMessage.Text = "<p class='error'>Please fill in all the requiered fields.</p>";
			}
		} else {
			infoMessage.Text = "<p class='error'>You must accept our terms and conditions to register.</p>";
		}
		return valid;
	}

	private void BindCountry() {
		List<Country> countries = Country.GetAll();
		if (DdlCountry.Items != null && DdlCountry.Items.Count > 0)
			DdlCountry.Items.Clear();
		ListItem li;
		//li = new ListItem("-- country --", "0");
		//DdlCountry.Items.Add(li);
		if (countries != null && countries.Count > 0) {
			foreach (Country country in countries) {
				li = new ListItem(country.Name, country.ID.ToString());
				DdlCountry.Items.Add(li);
			}
			//DdlCountry.Items.FindByValue("104").Selected = true;
		}
	}

	private bool SendEmail(BusinessChallengeForm bf) {
		bool send = false;
		try {
			Event evt = new Event();
			StringBuilder b = new StringBuilder();
			b.AppendLine("Event Name: " + evt.Name + Environment.NewLine);
			b.AppendLine("Business Name: " + bf.BusinessName + Environment.NewLine);
			b.AppendLine("Business Address 1: " + bf.BusinessAddress1 + Environment.NewLine);
			b.AppendLine("Business Address 2: " + bf.BusinessAddress2 + Environment.NewLine);
			b.AppendLine("Business Telephone: " + bf.BusinessTelephone + Environment.NewLine);
			b.AppendLine("Business City: " + bf.BusinessCity + Environment.NewLine);
			b.AppendLine("Business Postcode: " + bf.BusinessPostcode + Environment.NewLine);
			b.AppendLine("Business Country: " + Country.GetByID(bf.BusinessCountryID).Name + Environment.NewLine); ;
			b.AppendLine("Challenge Distance: " + bf.BusinessChallengeDistance + Environment.NewLine);
			b.AppendLine("Number Of Entrants: " + bf.NumberOfEntrants.ToString() + Environment.NewLine);
			b.AppendLine("Coordinator Name: " + bf.CoordinatorName + Environment.NewLine);
			b.AppendLine("Coordinator Email: " + bf.CoordinatorEmail + Environment.NewLine);
			b.AppendLine("Coordinator Telephone: " + bf.CoordinatorTelephone + Environment.NewLine);
			//MailMessage message = new MailMessage("mailsend@greatactivity.net", "businesschallenge@greatrun.org");//"businesschallenge@greatrun.org");
			MailMessage message = new MailMessage("mailsend@greatactivity.net", "kev@13strides.com");
			//MailAddress cc = new MailAddress("kev@13strides.com");
			MailAddress replyTo = new MailAddress("mailsend@greatactivity.net");
			//message.CC.Add(cc);
			message.ReplyTo = replyTo;
			message.Subject = "Business Challenge Registration";
			message.Body = b.ToString();
			SmtpClient sendMail = new SmtpClient();
			sendMail.Send(message);
			send = true;
		} catch (Exception ex) {
			//infoMessage.Text = ex.Message;
			//infoMessage.Type = InfoType.Error;
			send = false;
		}
		return send;
	}
}