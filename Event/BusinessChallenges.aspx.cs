﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;

public partial class Events_2012_BusinessChallenges : System.Web.UI.Page {
	int eventID = 0;
	BusinessChallenge bc = null;

	protected void Page_Load(object sender, EventArgs e) {
		Event evt = new Event();
		int.TryParse(Request.QueryString["id"], out eventID);
		PanelDefaultView.Visible = false;
		PanelListingView.Visible = false;
		if (evt.ID == eventID) //if it is great ireland run
			bc = BusinessChallenge.GetActiveByEventID(eventID);

		if (bc != null && bc.ID > 0 && bc.LinkActive) {
            string linkListings = "<a href='/Business-Challenge-Register/{0}/{1}' class='linkButton Listings'><span>See This Year's Teams</span></a>";

            string link = "<a href='/Business-Challenge-Register/{0}/{1}' class='linkButton'><span>Register</span></a>";
			switch (bc.DisplayMode) {
				case BcDisplayMode.Default:
				case BcDisplayMode.Listing:
					PanelDefaultView.Visible = true;
					if (Request.QueryString["success"] != null) {
						infoMessageDefault.Text = "Your request has been submitted, " +
						   "our Business Challenge coordinator will contact the team " +
						   "coordinator to organise payment for places.";
					}
					PanelDefaultView.CssClass = "gir-event";

					if (bc.IsOpen)
						LiteralDescription.Text = bc.Description;
					LiteralDefaultRegister.Text =
						string.Format(link, eventID.ToString(), bc.ID.ToString());
					if (bc.DisplayMode == BcDisplayMode.Listing)
						ListingsButtonLink.Text = string.Format(linkListings, eventID.ToString(), bc.ID.ToString());
					break;
				case BcDisplayMode.Results:
					Response.Redirect("~/Business-Challenge-Results");
					break;
				default:
					break;
			}
		} else {
			Response.Redirect("~/Event");
		}
	}
}