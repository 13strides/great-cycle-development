﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeFile="BusinessChallenges.aspx.cs" Inherits="Events_2012_BusinessChallenges" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    	<style type="text/css">
		<!--
		a.linkButton{padding:0 5px 0 0; display:block; float:right; font-size:0.7em; color:#223d94; font-weight:bold; margin:10px 5px 8px 0px; border-radius:3px; -webkit-border-radius:3px; -moz-border-radius:3px; background:url(/App_Images/gir-button.jpg) repeat;text-decoration:none;}
		a.linkButton span{font-size:1.5em;padding:3px 15px 5px 10px; display:block; background:url(http://www.greatrun.org/App_Images/buttons/arrow.png) no-repeat right;}
.success {
	background: url(http://www.greatrun.org/Admin/images/icons/success.png) center left no-repeat #cbffd1;
	border: 1px solid #63d872;
	color: #2f7038;
	padding-left:30px;
}

		-->
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div class="row">
        <div class="span7 abouttheswim">
			<asp:Panel ID="PanelDefaultView" runat="server" Visible="false">
				<h3 class="box-title">
					<asp:Literal runat="server" ID="bcDefaultTitle">Business Challenge</asp:Literal>
				</h3>
				<div class="box-content textAdjust">
					<div class="newsArticle infull">
						<asp:Literal runat="server" ID="infoMessageDefault" />
						<asp:Literal ID="LiteralDescription" runat="server"></asp:Literal>
						<asp:Literal ID="ListingsButtonLink" runat="server"></asp:Literal>
						<asp:Literal ID="LiteralDefaultRegister" runat="server"></asp:Literal>
						<div class="clearer">
						</div>
					</div>
				</div>
			</asp:Panel>
			<asp:Panel ID="PanelListingView" runat="server" Visible="false">
				<div class="eventContent charitiesPage">
					<h2 class="box-title">
						<asp:Literal runat="server" ID="bcListingTitle">Business Challenge</asp:Literal></h2>
					<div class="box-content textAdjust">
						<asp:Literal runat="server" ID="infoMessageListing" />
						<!-- add listing view here -->
						<asp:Literal ID="LiteralListingRegister" runat="server"></asp:Literal>
						<div class="clearer">
						</div>
					</div>
				</div>
			</asp:Panel>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" Runat="Server">
</asp:Content>