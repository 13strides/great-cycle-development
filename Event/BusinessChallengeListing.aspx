﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeFile="BusinessChallengeListing.aspx.cs" Inherits="Events_BusinessChallengeListing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        <!--
        .goldMember {
            position: relative;
            width: 121px;
            float: left;
            padding: 0;
            clear: none;
            margin-right: 15px;
            margin-top: 10px;
            background-color: white;
            border: 1px solid #dddddd;
            height: 120px;
            vertical-align: middle;
            text-align: center;
            line-height: 120px;
            -web-kit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }

            .goldMember a {
                height: 100%;
                width: 100%;
                display: block;
            }

                .goldMember a img {
                    float: none;
                    vertical-align: middle;
                    padding: 0;
                }

        .charityDescription {
            position: absolute;
            display: none;
            z-index: 50;
            border-collapse: collapse;
            line-height: 100%;
            margin-left: 0;
            width: 500px;
            height: 165px;
            text-align: left;
            padding: 20px;
            font-size: 1.0em;
            color: #666;
        }

            .charityDescription strong {
                font-weight: bold;
                text-transform: uppercase;
                display: block;
                margin-bottom: 10px;
            }

        #listingsContainer {
            min-height: 680px;
        }

        .charityDescription img {
            float: left;
            margin-right: 10px;
            margin-bottom: 50px;
        }

        .gir-event h4 {
            color: #ffffff;
            padding: 15px;
            margin-bottom: 5px;
            background-color: #008942;
            text-transform: uppercase;
            font-weight: bold;
            font-size: 20px;
        }

        .GSR-event h4 {
        }

        .GBR-event h4 {
        }

        h4 span {
            font-size: 0.8em;
            text-transform: none;
        }

        #listingsPanel {
            padding-left: 10px;
        }

        -->
    </style>
    <!-- Contact Form CSS files -->
    <link type='text/css' href='modalbox/css/basic.css' rel='stylesheet' media='screen' />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

	<div class="box threeQuarters" style="overflow: visible;">
		<h2 class="box-title">
			<asp:Literal runat="server" ID="BusinessTitleLiteral">This Years Teams</asp:Literal></h2>
		<div class="box-content textAdjust" id="listingsContainer">
			<asp:Literal runat="server" ID="errorMessage" />
			<asp:Panel runat="server" ID="tieredListingsPanel">
				<img src="/App_Images/businesschallenge.jpg" alt="">
				<h4>
					<asp:Literal runat="server" ID="eventNameLiteral" />
					2013 Business Challenge Teams</h4>
				<div id="listingsPanel">
					<asp:Literal ID="diamondLiteral" runat="server"></asp:Literal></div>
			</asp:Panel>
		</div>
		<div class="clearer">
		</div>
	</div>
	<script type='text/javascript' src='modalbox/js/jquery.simplemodal.js'></script>
	<script type='text/javascript' src='modalbox/js/basic.js'></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" runat="server">

</asp:Content>