﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;
public partial class Events_2012_BusinessChallengeTerms : System.Web.UI.Page
{
    int eventID = 0;
    int termID = 0;
    BusinessChallengeTerm bct = null;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        
        int.TryParse(Request.QueryString["id"], out eventID);
        int.TryParse(Request.QueryString["tid"], out termID);
        Event evt = new Event();
        if(evt.ID == eventID)
            bct = BusinessChallengeTerm.GetByID(termID);

        if (bct != null && bct.ID > 0)
        {
            LiteralTitle.Text = bct.Title;
            LiteralBody.Text = bct.Body;
            LiteralFooter.Text = bct.Footer;

        }
        else
        {
            if (Request.UrlReferrer != null)
                Response.Redirect(Request.UrlReferrer.AbsoluteUri);
            else
            {
                Response.Redirect("~/");
            }
        }
    }
}