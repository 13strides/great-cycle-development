﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Event_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="row pageheading">
        <div class="span6">
            <h1>Enter Now</h1>
            <ul class="breadcrumb">
            <strong>You are here:</strong>
                <li><a href="/">Home</a> <span class="divider">/</span></li>
                <li class="active">Enter Now</li>
            </ul>
        </div>
		
      </div>
<div class="row">
<span style="display:block; line-height: 25px; border-top:1px solid #ccc; font-size: 24px; padding:30px 20px 30px 20px; text-align:center; border-bottom:1px solid #ccc;">What do you want to enter?</span>
        <div class="span5 entryoption">
        
              
<div id="subEventStatus"><img style="float:left; margin-right:10px;" src="/img/css/entryop.jpg" />
<gs:EventSelectorLink ID="EventSelector1" runat="server" />
</div>
</div>
              <div class="span2 entryoption" id="charitiesAvailable">
              <img src="/img/css/charityentry.jpg" />
                  <h3><a href="/About/Cycle-for-Charity">Charities</a></h3>
                  <p>Find the good cause that best suits you.</p>
              </div>
              <asp:Panel runat="server" ID="BcPanel" Visible="false" >
                  <div class="span2 entryoption"  id="businessChallenge ">
                  <img src="/img/css/entryop.jpg" />
                 <asp:Literal ID="LiteralBc" runat="server" />
                 <p>Get out of office and on the roads</p>
                  </div>
              </asp:Panel>
    <asp:Panel runat="server" ID="JercyPanel" Visible="false">
        <div class="span2 entryoption">

            <a href="/about/merchandise">
                <img src="/img/css/jerseyoffer.jpg" alt="" /></a>
            <h4 style="line-height: 24px;">
                <a href="/about/merchandise">£15 Jersey early bird offer for a limited time only</a>

            </h4>

         </div>
    </asp:Panel>
	  
	  <div class="row">
		<div class="span6">
		<h3>How to enter</h3>
 
<p>The Great Manchester Cycle has 3 different event distances incorporated into the day, 52 miles, 26 miles and 13 miles, for more information on these please go to (detail which page the event distances info is listed). When considering which event you would like to do, remember to factor into your decision the following:
 </p>
<p>How regularly you cycle? – This is particularly important if you are considering the 26 mile or 52 mile event. You should be a regular cyclist, who can comfortably spend several hours in the saddle and maintain a good average speed, please go to (detail which page the event distances info is listed).
 </p>

 

 
<div id="more1" class="collapse out">

<p>Are you comfortable riding alongside other cyclists? – All 3 events are mass participation and the thrill of riding alongside others on closed roads should be experienced by all cyclists, however riding alongside other cyclists is a skill and if you have not done it much before then we advise that  you read our information on “Rider behaviour” please go to (detail which page the rider behaviour info is listed). This is particularly important if you are considering the longer distances of the 26 mile and 52 mile events as riders will tend to travel at faster speeds.
 </p>
 
<p>Can you maintain the recommended average speed for the distance you would like to complete? – All 3 distances will have a recommended average speed that riders should try and maintain, please go to (detail which page the event distances info is listed). The City roads used for the event cannot stay closed indefinitely so in order to allow each distance to have its share of the day on the route riders will need to maintain a recommended average speed to complete the full distance. In the event that riders fall behind the recommended distance you may be required to finish your event before completing the full distance (this applies to the 26 mile and 52 mile events).
 </p>
<p>How much training can you get in before the event? – This may dictate which event you can feasibly do. There really is no substitute to doing some training, its good fun, it gets/keeps you fit and is a really good way to socialize with friends or even join a club. Please go to (detail which page the training info is listed).
 </p>
<p>Once you have made up your mind which event distance is right for you it’s easy to enter, simply go to ENTER HERE and follow the simple entry process.
 </p>
<p>Your entry includes accurate chip timing/results service, a Great Manchester Cycle T-shirt, medal and finishers pack.
 </p>
 </div>
  <a data-toggle="collapse" style="cursor:pointer;" data-target="#more1">Read More...</a>
 
<h3>Event pack and information</h3>
 
<p>Once you are registered you will receive a confirmation email that you are entered, please make sure that you supply us with your correct email address as further emails will be sent on to you with training tips and key event information.
 </p>
<p>Before the event you will be sent an event pack, this will contain your event numbers, one for your handle bars and one for your jersey, please ensure that you attach these prior to the event as failure to show these on site could lead to ejection from the event. The numbers also help us track you on route, are used if you are involved in a medical incident and the photo guys use them to get your event day pics!
 </p>
 <div id="more2" class="collapse out">
<p>Event information this year will be available electronically from the website. A downloadable pdf will detail all the key information for your ride so please make sure you read through it before arriving on site.
</p> 

 
<p>If you have any specific questions and you need to talk with the Great Cycle team please drop us an email, we will get back to you as soon as we can (if we are not out on our bikes)!! Detail email address info@greatcycle.org
 </p>
 </div>
  <a data-toggle="collapse" style="cursor:pointer;" data-target="#more2">Read More...</a>
 
 
		</div>
		<div class="span3">


 
<h3>General Route overview</h3>
<p>The route has been developed to give cyclists the opportunity to enjoy traffic free riding on some of Manchester and Trafford's major trunk roads.</p>

<p>Starting, lapping and finishing at the Etihad Campus each distance will use a 13 mile circuit that begins by heading east on Ashton New Rd before going south on Alan Turing Way, these roads offer great cycling as riders set out on the early part of each lap. 
</p>

 <div id="more3" class="collapse out">
<p>At approximately 1 mile cyclists are heading west on Ashton Old Rd and towards the famous Mancunian Way, this section of the route offers good flat riding but does narrow in places, so be sure to look out for other cyclists around you and keep overtaking to a minimum! 
</p>
<p>Then comes the truly unique experience of cycling on the Mancunian Way, this is usually reserved for vehicles only but at the Great Manchester Cycle it is for cyclists to rule the roads! The rolling ups and downs of the Mancunian way make it an exhilarating ride, be sure to stay left unless overtaking. 
</p>
<p>Just beyond 3 miles the route will take cyclists onto Chester Rd and then Bridgewater Way as you speed towards Trafford, this is where cyclists will complete the outward section of the route by turning near Old Trafford football ground. A feed station will be positioned close to Old Trafford to allow you to rehydrate and rest before heading east back towards the Etihad Campus. The inbound route follows a similar course as the outbound route taking cyclists back to the Etihad Campus. 
</p>
<p>For those cyclists taking on the 26 mile or 52 mile challenges you will need to look out for signage at the Etihad Campus to direct you through your lapping point and onto your next lap, remember to signal when moving across the carriageway so other cyclists behind you know what you are doing. 
</p>
<p>On route cyclists can expect full rider support with moto marshals, mechanical points and roaming medical personnel.</p>
</p>
</div>
 <a data-toggle="collapse" style="cursor:pointer;" data-target="#more3">Read More...</a>
		</div>
	  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" Runat="Server">
</asp:Content>

