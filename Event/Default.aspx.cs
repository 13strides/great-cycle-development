﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;
using System.Configuration;
public partial class Event_Default : System.Web.UI.Page
{
    int eventID = 1;
    int streamID = 0;
    int raceID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["streamid"] != null)
        {
            int.TryParse(Request.QueryString["streamid"].ToString(), out streamID);
            if (streamID > 0)
            {
                string entryUrl = ConfigurationManager.AppSettings["EntrySystemUrl"].ToString();
                Response.Redirect(entryUrl + streamID.ToString());
            }
        }
        if (Request.QueryString["raceid"] != null)
        {
            int.TryParse(Request.QueryString["raceid"].ToString(), out raceID);
            if (raceID > 0)
            {
                string entryUrl = ConfigurationManager.AppSettings["RegistrationUrl"].ToString();
                Response.Redirect(entryUrl + raceID.ToString());
            }

        }
        BusinessChallenge bc = BusinessChallenge.GetActiveByEventID(eventID);
        if (bc != null && bc.ID > 0 && bc.LinkActive)
        {
            BcPanel.Visible = true;
            LiteralBc.Text = "<h3><a href=\"/Business-Challenges\">Business Challenge</a></h3>";
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        JercyPanel.Visible = !BcPanel.Visible;
    }
}