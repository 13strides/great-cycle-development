﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeFile="BusinessChallengeTerms.aspx.cs" Inherits="Events_2012_BusinessChallengeTerms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        <!--
        ol {
            margin: 0;
            padding: 0;
        }

            ol li, p {
                margin: 0 0 15px 10px;
                padding: 0;
                line-height: 15px;
                color: #333;
                font-size: 1.0em;
                line-height: 130%;
            }
        -->
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="row">
        <div class="span7 abouttheswim">
            <div class="header">
                <h1>
                    <asp:Literal runat="server" ID="LiteralTitle"></asp:Literal></h1>
            </div>

            <div class="content">
                <div class="infoPages">
                    <asp:Literal ID="LiteralBody" runat="server"></asp:Literal>
                    <div class="clearer">
                    </div>
                    <asp:Literal ID="LiteralFooter" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" Runat="Server"></asp:Content>