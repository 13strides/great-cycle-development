﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using GS.Generic.Web;
using ThirteenStrides.Database;

public partial class Charities_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            SiteMapNode currentNode = SiteMap.CurrentNode;
            Page.Title = currentNode.Title;
        }
    }
    
}
