﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Charities_Default" Title="Untitled Page" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="row pageheading">
        <div class="span6">
            <h1>Cycle for Charity</h1>
            <ul class="breadcrumb">
                <li><a href="/">Home</a> <span class="divider">/</span></li>
                <li class="active">Cycle for Charity</li>
            </ul>
        </div>
        <a href="/Event/" class="btn btn-primary btn-large">Enter Now »</a>
        
    </div>

      <div class="row">
        <div class="span7 abouttheswim">
                <div class="infoPages" style="min-height:500px;">
                    

<div class="charitiesAdvert">
                    	<h3><a  onclick="javascript:pageTracker._trackPageview('/EXIT/CHARITY/LeukaemiaResearch.html');" href="http://leukaemialymphomaresearch.org.uk" target="_blank">Leukaemia &amp; Lymphoma Research</a></h3><img src="/App_Images/Charities/LeukaemiaResearch.jpg" alt="" />
                        <p>Leukaemia &amp; Lymphoma Research's vision is to beat blood cancers. Every 20 minutes someone in the UK is diagnosed with a blood cancer.<br />

Since our charity began in 1960, survival from childhood leukaemia has risen from 0% to 90%. We now urgently need to achieve the same survival rate for adults, which is on average just 50%.<br />

We can and we will beat blood cancers by understanding their biology, improving early detection and diagnosis, developing new treatments, and by ensuring that patients and health professionals have the information they need.</p>
                        <p><a onclick="javascript:pageTracker._trackPageview('/EXIT/CHARITY/LeukaemiaResearch.html');"  href="http://leukaemialymphomaresearch.org.uk" target="_blank">Find out more</a></p>
                    </div>

<div class="charitiesAdvert">
                    	<h3><a  onclick="javascript:pageTracker._trackPageview('/EXIT/CHARITY/CancerResearch.html');" href="http://www.cancerresearchuk.org/sportchallenges" target="_blank">Cancer Research UK</a></h3><img src="/App_Images/Charities/cancerResearchUK.jpg" alt="" />
                        <p>Cancer Research UK's work on the causes and prevention of cancer has saved millions of lives in the UK and across the world. We are funded entirely by the public, so the support of people like you is vital. We support more than 4000 scientists, doctors and nurses across the country, whose research into all types of cancer has helped survival rates improve significantly over the years. When you cycle for Cancer Research UK, you are becoming part of a unique team dedicated to detecting and beating cancer. In return, we’ll help you as much as we can along the way by providing information, fundraising packs and support on the day. Make your challenge make a difference.</p>
                        <p><a  onclick="javascript:pageTracker._trackPageview('/EXIT/CHARITY/CancerResearch.html');"  href="http://www.cancerresearchuk.org/sportchallenges" target="_blank">Find out more</a></p>
                        
                    </div>
                    
                    
                    
                    <div class="charitiesAdvert">
                    	<h3><a onclick="javascript:pageTracker._trackPageview('/EXIT/CHARITY/Christies.html');"  href="http://www.christies.org/bike" target="_blank">The Christie</a></h3><img src="/App_Images/Charities/TheChristie.gif" alt="" />
                        <p><strong>We Care, We Discover, We Teach. </strong><br />

The Christie is a specialist cancer centre that has been pioneering cancer research breakthroughs for more than 100 years.  Made up of leading experts in cancer treatment, research and education, we are the largest centre of our kind in Europe and treat 40,000 patients each year.  We could not do without the incredible efforts of our fundraisers.  Whether you are pedalling miles across the country, summiting an African peak or holding a coffee morning, every single penny has a direct impact on our patients.  Together we can work towards a future without cancer.
                        </p>
                         <p><a  onclick="javascript:pageTracker._trackPageview('/EXIT/CHARITY/Christies.html');"  href="http://www.christies.org/bike" target="_blank">Find out more</a></p>
                    </div>
           
           			<div class="charitiesAdvert lastCharity">
                    	<h3><a  onclick="javascript:pageTracker._trackPageview('/EXIT/CHARITY/ChildrenWithCancer.html');"   href="http://www.childrenwithcancer.org.uk/Events/the-great-manchester-cycle/25e17fbf-03b0-41a1-b81f-4645d43ec7b5" target="_blank">CHILDREN with CANCER UK</a></h3><img src="/App_Images/Charities/cwc.gif" alt="" />
                        <p>Contact: Yvonne<br />
Tel: 020 7404 0808<br />
Email:  <a href="mailto:yvonne.dugera@childrenwithcancer.org.uk" target="_blank">yvonne.dugera@childrenwithcancer.org.uk</a><br />
We are the leading national charity dedicated to the fight against childhood cancer. Our aim is to determine the causes, find cures and provide care for children with cancer. CHILDREN with LEUKAEMIA was the former working name of CHILDREN with CANCER UK. </p>
						<p><a  onclick="javascript:pageTracker._trackPageview('/EXIT/CHARITY/ChildrenWithCancer.html');"    href="http://www.childrenwithcancer.org.uk/Events/the-great-manchester-cycle/25e17fbf-03b0-41a1-b81f-4645d43ec7b5" target="_blank">Find out more</a></p>
                    </div>
                    
           
 
                </div>
            </div>
            
            <div class="span3 rhc">
          <div class="rhc-post">
          <h3>Cycle For Charity</h3>
          <a href="#"><img src="img/css/charity.jpg"></a>
          <a href="#" class="rhc-post-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</a>

          </div>
<img src="img/css/mpu.gif">
          
          <div class="clearer"></div>
          
   
          
        </div>
            
        </div>
        
        

</asp:Content>
 <asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" Runat="Server">
</asp:Content>