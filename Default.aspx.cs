﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ThirteenStrides.Database;
using System.Configuration;
using System.Data;
using System.Text;
public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            DateTime mainEventDay = Convert.ToDateTime(
                ConfigurationManager.AppSettings["MainEventDay"].ToString());
            TimeSpan timespan = (mainEventDay- DateTime.Now );
            int daysLeft = (int)timespan.TotalDays + 1;
            if (daysLeft >= 0)
            {
                StringBuilder sb = new StringBuilder("");
                sb.Append("<span class='countdown-date'>");
                sb.AppendLine(string.Format("{0:dddd dd MMMM yyyy}", mainEventDay) + "</span>");//Sunday 30 June 2013
                sb.AppendLine("<span class='countdown-time'>");
			    sb.AppendLine("<span class='s1'>"+ (daysLeft + 5).ToString() +" days to go</span>");
                sb.AppendLine("<span class='s2'>" + (daysLeft + 4).ToString() + " days to go</span>");
                sb.AppendLine("<span class='s3'>" + (daysLeft + 3).ToString() + " days to go</span>");
                sb.AppendLine("<span class='s4'>" + (daysLeft + 2).ToString() + " days to go</span>");
                sb.AppendLine("<span class='s5'>" + (daysLeft + 1).ToString() + " days to go</span>");
                sb.AppendLine("<span class='s6'>" + daysLeft.ToString() + " days to go</span></span>");
                LiteralEventDay.Text = sb.ToString();
            }
           
        }
        catch
        { 
                LiteralEventDay.Text += "<span class='countdown-date'>";
                LiteralEventDay.Text += "Sunday 30 June 2013";
                LiteralEventDay.Text += "</span>";
        }
    }
	private void EventStatusContent()
    {
        DateTime showEventClosed = new DateTime(2011, 10, 27, 23, 59, 59);
        if (DateTime.Now > showEventClosed)
        {
            //ballotClosedContent.Visible = true;
        }
        else
        {
            //ballotOpenContent.Visible = true;
        }
        
    }
	private void EventPriceContent()
    {
        DateTime showEventPrice = new DateTime(2012, 3, 26, 8, 30, 00);
        if (DateTime.Now > showEventPrice)
        {
            
        }
        
        
    }
}
