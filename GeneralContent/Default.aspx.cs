﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GeneralContent_Default : System.Web.UI.Page {
	public string contentTitle;

	protected void Page_Load(object sender, EventArgs e) {
		if (Request.QueryString["name"] != null) {
            ContentViewer2.FriendlyUrl = Request.QueryString["name"].ToString();
		}
	}

	protected void Page_PreRender(object sender, EventArgs e) {

            if (Request.QueryString["name"].ToString().ToLower() == "merchandise") {
                EnterOnlineLink.NavigateUrl = "https://entry.enteronline.org/Login.aspx?StreamID=1018";
                EnterOnlineLink.Text = "Buy Now &raquo;";
            }
       
        contentTitle = ContentViewer2.ContentPageTitle;
		if (contentTitle.ToLower().Contains("26 miles") ||
			contentTitle.ToLower().Contains("13 miles") ||
			contentTitle.ToLower().Contains("52 miles")) {
			LitBreadcrumb.Text = "<li><a href='/Event'>Distances</a><span class='divider'>/</span></li><li class='active'>" + contentTitle + "</li>";
		} else {
			LitBreadcrumb.Text = "<li class='active'>" + contentTitle + "</li>";
		}
	}
}