﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeFile="Default.aspx.cs" Inherits="GeneralContent_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="row pageheading">
		<div class="span6">
			<h1>
				<%=contentTitle %></h1>
			<ul class="breadcrumb">
            <strong>You are here:</strong>
				<li><a href="/">Home</a> <span class="divider">/</span></li>
				<asp:Literal ID="LitBreadcrumb" runat="server" />
			</ul>
		</div>
		<asp:HyperLink runat="server" ID="EnterOnlineLink" NavigateUrl="/Event/" CssClass="btn btn-primary btn-large">Enter Now &raquo;</asp:HyperLink>
		<!--<a href="/Event/" class="btn btn-primary btn-large">Enter Now »</a>-->
	</div>
    <div class="row">
        <gs:ContentViewer ID="ContentViewer2" runat="server" />
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" runat="Server">
</asp:Content>