﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GS.Generic.Membership
{
    /// <summary>
    /// designed to hold the user's rights to the different applications
    /// </summary>
    [Serializable]
    public class Rights
    {
        ulong _mask;

        /// <summary>
        /// Classe constructor
        /// </summary>
        /// <param name="mask">unsigned integer containing the rights (binary)</param>
        public Rights(ulong mask)
        {
            _mask = mask;
        }

        /// <summary>
        /// property returns the user's rights in one integer
        /// </summary>
        public ulong Mask
        {
            get
            {
                return _mask;
            }
        }

        /// <summary>
        /// returns true if a user has the access right for the given index, false if not
        /// </summary>
        /// <param name="index">index of the right to be checked for</param>
        /// <returns>yes or no depending on the value of the user's right for the given index</returns>
        public bool this[int index]
        {
            get
            {
                // index starts at 0 and the upper value of signed int is 2^31 - 1
                ulong flag = (ulong)Math.Pow(2, index);
                return ((_mask & flag) > 0);
            }
            set
            {
                // index starts at 0 and the upper value of signed int is 2^31 - 1
                ulong flag = (ulong)Math.Pow(2, index);
                if (value)
                {
                    // set on
                    _mask |= flag;
                }
                else
                {
                    // set off
                    _mask &= ~flag;
                }
            }
        }
    }
}