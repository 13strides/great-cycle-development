﻿using System;
using System.Collections.Generic;
using System.Text;
using GS.Generic;
using System.Configuration;
using ThirteenStrides.Database;
using System.Data;
using System.Web;
using MembershipService;


namespace GS.Generic.Membership
{
    /// <summary>
    /// Summary description for GSUser
    /// </summary>
    public class GSUser
    {
	    string _securityToken;
        Rights _settings;
        UserDetails _details;
        string _status;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public GSUser()
        {
            _settings = new Rights(0);
            _details = new UserDetails();
            _securityToken = _status = "";
        }

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="userId">string userId (36)</param>
        public GSUser(string userId) {
			_securityToken = "";
			
            GetUserDetailsFromWS(userId);

			try {
				GetUserDetailsFromDB();
			} catch {
				// the user wasn't found in the database! Create him
				if (RegisterUser()) {
					GetUserDetailsFromDB();
				}
			}
		}

        /// <summary>
        /// Constructor (registration process)
        /// </summary>
        /// <param name="details"></param>
        /// <param name="securityToken"></param>
        public GSUser(UserDetails details, string securityToken) {
			_details = details;
			_securityToken = securityToken;
			try {
				GetUserDetailsFromDB();
			} catch {
				// the user wasn't found in the database! Create him
				if (RegisterUser()) {
					GetUserDetailsFromDB();
				}
			}
		}

        /// <summary>
		/// Get the user details from the membership web service.
		/// </summary>
		private void GetUserDetailsFromWS(string id) {
			MembershipService.Membership myservice = new MembershipService.Membership();

			UserDetails details = new UserDetails();
			details.Id = id;

			// login as admin first to get a security token
			UserDetailsResponse admin = myservice.AdminLogin(ConfigurationManager.AppSettings["ApplicationGUID"], ConfigurationManager.AppSettings["ApplicationAdminLogin"], ConfigurationManager.AppSettings["ApplicationAdminPassword"]);
			if (admin.Successful) {
				UserDetailsResponse user;
				// login as normal user
				user = myservice.GetUserDetails(ConfigurationManager.AppSettings["ApplicationGUID"], admin.SecurityToken, details);

				if (user.Successful && user.UserDetailsList[0].Id != "00000000-0000-0000-0000-000000000000") {
					// setting Session variables:
					_details = user.UserDetailsList[0];
                    _securityToken = user.SecurityToken;
				} else {
					user = myservice.AdminGetUserDetails(ConfigurationManager.AppSettings["ApplicationGUID"], admin.SecurityToken, details);

					if (user.Successful && user.UserDetailsList[0].Id != "00000000-0000-0000-0000-000000000000") {
						// setting Session variables:
						_details = user.UserDetailsList[0];
					} else {
						throw new Exception("Couldn't find the user in the membership service");
					}
				}
			}
		}

        /// <summary>
        /// Logs the user in
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>null if user isn't allowed to login, a filled user object otherwise</returns>
        public static GSUser Login(string username, string password)
        {
            MembershipService.Membership myservice = new MembershipService.Membership();

            // login as normal user
            UserDetailsResponse user = myservice.Login(ConfigurationManager.AppSettings["ApplicationGUID"], username, password);

            if (user.Successful)
            {
                // setting Session variables:
                GSUser gsUser = new GSUser(user.UserDetailsList[0], user.SecurityToken);
                HttpContext.Current.Session["USER"] = gsUser;
                return gsUser;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Logs a user with administrative rights in
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>null if user isn't allowed to login, a filled user object otherwise</returns>
        public static GSUser AdminLogin(string username, string password)
        {
            MembershipService.Membership myservice = new MembershipService.Membership();

            // login as normal user
            UserDetailsResponse user = myservice.AdminLogin(ConfigurationManager.AppSettings["ApplicationGUID"], username, password);

            if (user.Successful)
            {
                // setting Session variables:
                GSUser gsUser = new GSUser(user.UserDetailsList[0], user.SecurityToken);
                gsUser.UpdateLastLoginDate();
                HttpContext.Current.Session["USER"] = gsUser;
                return gsUser;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// get the user details from the GreatAustralianRun.com.au database
        /// </summary>
        private void GetUserDetailsFromDB()
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

            SqlParam[] paras = { new SqlParam("@id", SqlDbType.VarChar, 36, _details.Id) };

            SqlDataReaderResponse reader = connection.RunCommand("gs_select_user", paras);

            if (reader.ReturnCode == 0)
            {
                if (reader.Result.Read())
                {
                    // gets the user settings
                    _settings = new Rights(Convert.ToUInt64(reader.Result["user_settings"]));
                    //_image = reader.Result["user_image"].ToString();
                    _status = reader.Result["user_status"].ToString();
                    //_privacy = Convert.ToBoolean(reader.Result["user_privacy"]);
                }
                else
                {
                    reader.Result.Close();
                    throw new Exception("The user wasn't found in the database.");
                }
                reader.Result.Close();
            }
            else
            {
                throw new Exception(reader.ErrorMessage);
            }
        }

        /// <summary>
        /// Registers the user into the database
        /// </summary>
        private bool RegisterUser()
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

            SqlParam[] paras = { new SqlParam("@id", SqlDbType.VarChar, 36, _details.Id) };

            Int32Response res = connection.RunInt32Command("gs_insert_user", paras);

            return res.ReturnCode == 0;
        }

        /// <summary>
        /// Updates the last login date to now
        /// </summary>
        private void UpdateLastLoginDate()
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
            SqlParam[] paras = { new SqlParam("@id", SqlDbType.VarChar, 36, _details.Id) };
            Int32Response result = connection.RunInt32Command("gs_update_user_last_login", paras);
        }

        /// <summary>
        /// Saves the Great Run specific info into the Greatrun.org database
        /// </summary>
        /// <returns></returns>
        private bool SaveInDb()
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
            SqlParam[] paras = { new SqlParam("@id", SqlDbType.VarChar, 36, _details.Id),
				new SqlParam("@settings", SqlDbType.BigInt, _settings.Mask),
				new SqlParam("@status", SqlDbType.VarChar, 512, _status)};
				//new SqlParam("@image", SqlDbType.VarChar, 128, _image),
				//new SqlParam("@privacy", SqlDbType.Bit, _privacy) };
            Int32Response result = connection.RunInt32Command("gs_update_user", paras);
            return (result.ReturnCode == 0);
        }

        /// <summary>
        /// Saves the user's details in the membership web service
        /// </summary>
        /// <returns>0 if successful, a different error code otherwise</returns>
        private int SaveInWs()
        {
            MembershipService.Membership myservice = new MembershipService.Membership();
            UserDetailsResponse response;
            try
            {
                response = myservice.UpdateUser(ConfigurationManager.AppSettings["ApplicationGUID"], _securityToken, _details);
            }
            catch
            {
                return -1;
            }

            if (response.Successful)
            {
                _securityToken = response.SecurityToken;
                _details = response.UserDetailsList[0];
                return 0;
            }
            else
            {
                return response.ErrorCode;
            }
        }

        /// <summary>
        /// Saves the entire user profile (in both database and web service)
        /// </summary>
        /// <returns>0 if successful, a different error code otherwise</returns>
        public int Save()
        {
            if (!SaveInDb())
            {
                return -2;
            }
            else
            {
                return SaveInWs();
            }
        }

        #region attributes
        /// <summary>
        /// Gets the user's id
        /// </summary>
        public string Id
        {
            get { return _details.Id; }
        }
        /// <summary>
        /// This is a personalized status that the user can use as a personal message on the public profile.
        /// </summary>
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        /// <summary>
        /// Gets or sets the security token
        /// </summary>
        public string SecurityToken
        {
            get { return _securityToken; }
            set { _securityToken = value; }
        }

        /// <summary>
        /// gets or sets the user's settings (and rights)
        /// </summary>
        public Rights Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }

        /// <summary>
        /// gets or sets user details
        /// </summary>
        public UserDetails Details
        {
            get { return _details; }
            set { _details = value; }
        }

        /// <summary>
        /// returns the basic user object attached to this user
        /// </summary>
        public GSUserBasic BasicUser
        {
            get
            {
                return new GSUserBasic(_details.Id, _details.Firstname, _details.Lastname, _details.Username, _settings);
            }
        }
        #endregion 
    }
}
