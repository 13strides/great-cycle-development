﻿using System;
using System.Collections.Generic;
using System.Text;


namespace GS.Generic.Membership
{

    /// <summary>
    /// Summary description for GSUserBasic
    /// </summary>
    public class GSUserBasic
    {
        string _id, _firstname, _lastname, _username;
        Rights _settings;
        /// <summary>
		/// constructor by values
		/// </summary>
		/// <param name="id">user id</param>
		/// <param name="firstname">user firstname</param>
		/// <param name="lastname">user surname</param>
		/// <param name="username">user username</param>
		/// <param name="settings">user settings</param>
		public GSUserBasic(string id, string firstname, string lastname, string username, Rights settings) {
			_id = id;
			_firstname = firstname;
			_lastname = lastname;
			_username = username;
			_settings = settings;
		}
    }
}
