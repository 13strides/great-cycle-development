﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using GS.Generic.Membership;
using System.Globalization;

namespace GS.Generic
{
    /// <summary>
    /// Summary description for GreatSwim
    /// </summary>
    public class GreatSwim
    {

        public static GSUser CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session["USER"] != null)
                {
                    return (GSUser)HttpContext.Current.Session["USER"];
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
