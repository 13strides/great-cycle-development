﻿using System;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using Microsoft.VisualBasic;
/// <summary>
/// Summary description for StringManuplation
/// </summary>
/// 
namespace Gr.Utils
{
    public class StringManuplation
    {

        public static bool isEmail(string email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(strRegex);
            if (re.IsMatch(email))
                return true;
            else
                return false;
        }
        public static string CleanStringForURL(string str)
        {
            StringBuilder sb = new StringBuilder();
            HttpContext.Current.Response.Write(str + "****");
            // trim...
            str = HttpContext.Current.Server.HtmlDecode(str.Trim());

            // fix ampersand...
            str = str.Replace("&", "and");

            // normalize the Unicode
            str = str.Normalize(NormalizationForm.FormKD);
            for (int i = 0; i < str.Length; i++)
            {
                if (char.IsWhiteSpace(str[i]) ||
                   char.GetUnicodeCategory(str[i]) == UnicodeCategory.DashPunctuation) 
                    sb.Append('-');
                else if (char.GetUnicodeCategory(str[i]) != UnicodeCategory.NonSpacingMark
                    && !char.IsPunctuation(str[i])
                    && !char.IsSymbol(str[i]) 
                    )
                    sb.Append(str[i]);
            }
            HttpContext.Current.Response.Write(sb.ToString());
            return sb.ToString();
        }

        public static string ReplaceUppercase(string str)
        {
            return Regex.Replace(str, @"(?<!_)([A-Z])", "_$1").Remove(0, 1);
        }
         

        public static string StripHTML(string html)
        {
            string TAG_PATTERN = "<.*?>";
            return Regex.Replace
              (html, TAG_PATTERN, string.Empty);
        }
        
    }

}