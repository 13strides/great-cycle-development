﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Gr.Utils
{
    public static class CustomConverter
    {
        public static List<T> EnumToList<T>()
        {

            Type enumType = typeof(T);

            // Check type is enum otherwise throw argument exception

            if (enumType.BaseType != typeof(Enum))

                throw new ArgumentException("Must be of type System.Enum");

            return new List<T>(Enum.GetValues(enumType) as IEnumerable<T>);

        }
    }
}
