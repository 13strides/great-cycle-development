﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GS.Generic.Web
{
    [ToolboxData("<{0}:Info runat=server></{0}:Info>")]
    /// <summary>
    /// Summary description for Info
    /// </summary>
    public class Info :Literal{
        InfoType _type;
		string _cssClass;

		/* protected override void Render(HtmlTextWriter writer) {
			
			base.Render(writer);
		} */

		public Info()
			: base() {
			_type = InfoType.Notice;
			_cssClass = "";
			// base.Text = "<p class=\"" + _type.ToString().ToLower() + "\"></p>";
			base.EnableViewState = false;
		}

		/// <summary>
		/// Gets or sets the text inside the tag
		/// </summary>
		public new string Text {
			get {
				if (base.Text.Length > 0) {
					int endFirstTag = base.Text.IndexOf('>');
					int length = base.Text.Length - 5 - endFirstTag;

					if (endFirstTag > 0) {
						return base.Text.Substring(endFirstTag + 1, length);
					} else {
						return base.Text;
					}
				} else {
					return "";
				}
			}
			set {
				if (value == String.Empty) {
					base.Text = "";
				} else {
					base.Text = "<p class=\"" + _cssClass + " " + _type.ToString().ToLower() + "\">" + value + "</p>";
				}
			}
		}

		/// <summary>
		/// Gets or sets the type of message.
		/// </summary>
		public InfoType Type {
			get {
				return _type;
			}
			set {
				_type = value;
				if (base.Text.Length > 0) {
					int endFirstTag = base.Text.IndexOf('>');
					int length = base.Text.Length - 5 - endFirstTag;
					this.Text = base.Text.Substring(endFirstTag + 1, length);
				}
			}
		}

		/// <summary>
		/// gets or sets the css class for that message
		/// </summary>
		public string CssClass {
			get { return _cssClass; }
			set { _cssClass = value; }
		}
	}
}
