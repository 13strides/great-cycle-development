﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GS.Generic.Web
{
    public enum InfoType
    {
        Error,
        Warning,
        Success,
        Question,
        Notice
    }
}
