﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ni.Generic.Measurement
{
    [Serializable]
    public class Distance
    {
        private double _mm;

        /// <summary>
        /// Constructor by value (Kilometres)
        /// </summary>
        /// <param name="metres">Metres</param>
        public Distance(double metres)
        {
            _mm = metres * 1000;
        }

        /// <summary>
        /// gets the number of Miles represented by the Distance object
        /// </summary>
        public double Miles
        {
            get
            {
                return (_mm / (double)1609344);
            }
        }

        /// <summary>
        /// gets the number of Metres represented by the Distance object
        /// </summary>
        public double Metres
        {
            get
            {
                return (_mm / (double)1000);
            }
        }

        /// <summary>
        /// gets the number of Centimetres represented by the Distance object
        /// </summary>
        public double Centimetres
        {
            get
            {
                return (_mm / (double)10);
            }
        }

        /// <summary>
        /// gets the number of Kilometres represented by the Distance object
        /// </summary>
        public double Kilometers
        {
            get
            {
                return (_mm / (double)1000000);
            }
        }

        /// <summary>
        /// gets the number of Millimeters represented by the Distance object
        /// </summary>
        public double Millimeters
        {
            get
            {
                return (_mm);
            }
        }

        /// <summary>
        /// gets the number of Feet represented by the Distance object
        /// </summary>
        public double Feet
        {
            get
            {
                return (_mm / (double)304.8);
            }
        }

        /// <summary>
        /// gets the number of Inches represented by the Distance object
        /// </summary>
        public double Inches
        {
            get
            {
                return (_mm / (double)25.4);
            }
        }

        /// <summary>
        /// adds two distance objects together
        /// </summary>
        /// <param name="distance1"></param>
        /// <param name="distance2"></param>
        /// <returns></returns>
        public static Distance operator +(Distance distance1, Distance distance2)
        {
            return new Distance(distance1.Metres + distance2.Metres);
        }

        /// <summary>
        /// substracts two distance objects
        /// </summary>
        /// <param name="distance1"></param>
        /// <param name="distance2"></param>
        /// <returns></returns>
        public static Distance operator -(Distance distance1, Distance distance2)
        {
            return new Distance(distance1.Metres - distance2.Metres);
        }
    }
}
