﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ni.Generic.Measurement
{
    [Serializable]
    public class Weight
    {
        int _grams;

        /// <summary>
        /// Constructor by value
        /// </summary>
        /// <param name="grams">grams</param>
        public Weight(int grams)
        {
            _grams = grams;
        }

        /// <summary>
        /// Constructor by value
        /// </summary>
        /// <param name="kg">Kilograms</param>
        public Weight(double kg)
        {
            _grams = (int)Math.Round(kg * 1000);
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Weight()
        {
            _grams = 0;
        }

        /// <summary>
        /// gets or sets the number of grams represented by the Weight object
        /// </summary>
        public int Grams
        {
            get
            {
                return _grams;
            }
            set
            {
                _grams = value;
            }
        }

        /// <summary>
        /// gets or sets the number of kilograms represented by the Weight object
        /// </summary>
        public double Kilograms
        {
            get
            {
                return ((double)_grams / (double)1000);
            }
            set
            {
                _grams = (int)Math.Round(value * 1000);
            }
        }

        /// <summary>
        /// gets or sets the number of stones represented by the Weight object
        /// </summary>
        public double Stones
        {
            get
            {
                return ((double)_grams / (double)6350.29318);
            }
            set
            {
                _grams = (int)Math.Round(value * 6350.29318);
            }
        }

        /// <summary>
        /// gets or sets the number of pounds represented by the Weight object
        /// </summary>
        public double Pounds
        {
            get
            {
                return ((double)_grams / (double)453.59237);
            }
            set
            {
                _grams = (int)Math.Round(value * 453.59237);
            }
        }

        /// <summary>
        /// gets or sets the number of ounces represented by the Weight object
        /// </summary>
        public double Ounces
        {
            get
            {
                return ((double)_grams / (double)28.3495231);
            }
            set
            {
                _grams = (int)Math.Round(value * 28.3495231);
            }
        }

        /// <summary>
        /// adds the given number of pounds to the Weight object
        /// </summary>
        public Weight AddPounds(double pounds)
        {
            return new Weight(_grams + (int)Math.Round(pounds * 453.59237));
        }

        /// <summary>
        /// adds the given number of ounces to the Weight object
        /// </summary>
        public Weight AddOunces(double ounces)
        {
            return new Weight(_grams + (int)Math.Round(ounces * 28.3495231));
        }

        /// <summary>
        /// adds the given number of stones to the Weight object
        /// </summary>
        public Weight AddStones(double stones)
        {
            return new Weight(_grams + (int)Math.Round(stones * 6350.29318));
        }

        /// <summary>
        /// adds the given number of grams to the Weight object
        /// </summary>
        public Weight AddGrams(int grams)
        {
            return new Weight(_grams + grams);
        }

        /// <summary>
        /// adds the given number of kilograms to the Weight object
        /// </summary>
        public Weight AddKilograms(double kgs)
        {
            return new Weight(_grams + (int)Math.Round(kgs * 1000));
        }

        /// <summary>
        /// Clones the given Weight object
        /// </summary>
        public static Weight Clone(Weight w)
        {
            return new Weight(w.Kilograms);
        }

        /// <summary>
        /// clones the current Weight object
        /// </summary>
        public Weight Clone()
        {
            return new Weight(this._grams);
        }

        /// <summary>
        /// adds two weights together
        /// </summary>
        /// <param name="weight1"></param>
        /// <param name="weight2"></param>
        /// <returns></returns>
        public static Weight operator +(Weight weight1, Weight weight2)
        {
            return new Weight(weight1._grams + weight2._grams);
        }

        /// <summary>
        /// substracts two weigth objects from each other
        /// </summary>
        /// <param name="weight1"></param>
        /// <param name="weight2"></param>
        /// <returns></returns>
        public static Weight operator -(Weight weight1, Weight weight2)
        {
            return new Weight(weight1._grams - weight2._grams);
        }
    }
}
