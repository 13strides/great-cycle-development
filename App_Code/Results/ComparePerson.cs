﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for ComparePerson
/// </summary>
public class ComparePerson
{
    private int _bib;
    private string _fullName;
    private int _raceID;
    public int Bib
    {
        get { return _bib; }
        set { _bib = value; }
    }
    public int RaceID
    {
        get { return _raceID; }
        set { _raceID = value; }
    }
    public string FullName
    {
        get { return _fullName; }
        set { _fullName = value; }
    }
	public ComparePerson()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public ComparePerson(int bib, string fullName, int raceID)
    {
        _bib = bib;
        _fullName = fullName;
        _raceID = raceID;
    }
}
