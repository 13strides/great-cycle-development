﻿using System;
using System.Collections.Generic;
using System.Web;

public class RaceBib : IComparable<RaceBib>
{
    private int _raceID;
    private double _distance;
    private int _bib;

    public static Comparison<RaceBib> BibComparison = delegate(RaceBib rb1, RaceBib rb2)
    {
        return rb1._bib.CompareTo(rb2._bib);
    };

    public static Comparison<RaceBib> RaceIDComparison = delegate(RaceBib rb1, RaceBib rb2)
    {
        return rb1._raceID.CompareTo(rb2._raceID);
    };

    public int RaceID
    {
        get { return _raceID; }
        set { _raceID = value; }
    }

    public double Distance
    {
        get { return _distance; }
        set { _distance = value; }
    }

    public int Bib
    {
        get { return _bib; }
        set { _bib = value; }
    }

    public RaceBib(int raceID, double distance, int bib)
    {
        this._raceID = raceID;
        this._distance = distance;
        this._bib = bib;
    }

    #region IComparable<RaceBib> Members

    public int CompareTo(RaceBib other)
    {
        return RaceID.CompareTo(other.RaceID);
    }

    #endregion

    public override string ToString()
    {
        return string.Format("RaceID: {0} Distance: {1} Bib: {2}", _raceID, _distance, _bib);
    }
}