using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
//using MySql.Data.MySqlClient;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DataOperations
/// </summary>
/// 
namespace GCResults
{
    public class DataOperations
    {
        private string _connString = ConfigurationManager.ConnectionStrings["SqlServerConnection"].ConnectionString;
        private int _pageSize = 10;
        public int PAGE_SIZE { get { return _pageSize; } set { _pageSize = value; } }

        public DataOperations()
        {
            //
            // TODO: Add constructor logic here
            //
            PAGE_SIZE = 10;
        }

        /// <summary>
        /// Gets a list of Races from the stgnova.race table, populated into a drop down list
        /// </summary>
        /// <returns>DataSet of Race Information</returns>
        public DataSet GetRaces()
        {
            DataSet raceData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    SqlDataAdapter raceDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    //cmd.CommandText = "select description, idrace, raceyear, racedate from (select '-- Please Select a Race --' as description ,0 as idrace,'' as raceyear,'2020-01-01' as racedate ) as t1  union  select description, idrace, cast(year(racedate) as varchar) as raceyear, racedate from race where quickresults=1 and racedate <= getdate() order by racedate desc";
                    cmd.CommandText = "select description, idrace, raceyear, racedate from (select '-- Please Select a Race --' as description ,0 as idrace,'' "
                        + "as raceyear,'2020-01-01' as racedate ) as t1  union "
                        + "select description, idrace, cast(year(racedate) as varchar) as raceyear, racedate "
                        + "from race r inner join [dbo].[event] e on "
                        + "r.Event_idEvent = e.idEvent inner join eventtype et on e.idEventType = et.idEventType "
                        + "where et.idEventType = 9 and quickresults=1 and racedate <= getdate() order by racedate desc";

                    conn.Open();
                    raceDataAdapter.SelectCommand = cmd;
                    raceDataAdapter.Fill(raceData);
                }
            }

            return raceData;

        }

        /// <summary>
        /// Function to get information on a particular race from the database
        /// </summary>
        /// <param name="raceId"></param>
        /// <returns>DataSet containing a Race Id and description</returns>
        public DataSet GetRace(string raceId)
        {
            DataSet raceData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter raceDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    //cmd.CommandText = "select r.Description,r.idRace,isnull(np.NikePlusUrl,'') as NikePlusUrl,isnull(np.Active,'2') as Active, e.idEventType, r.PhotosRaceId as PhotosRaceId, r.PhotosRaceName as PhotosRaceName, Year(r.RaceDate) as RaceYear from race r left outer join nikeplus np on r.idrace=np.idrace left outer join event e on r.event_idevent = e.idEvent where r.idRace=@IDRACE";
                    cmd.CommandText = "select r.Description,r.idRace,isnull(np.NikePlusUrl,'') as NikePlusUrl,isnull(np.Active,'2') as Active, e.idEventType, r.PhotosRaceId as PhotosRaceId, Year(r.RaceDate) as RaceYear, r.distance as Distance from race r left outer join nikeplus np on r.idrace=np.idrace left outer join event e on r.event_idevent = e.idEvent where r.idRace=@IDRACE";
                    cmd.Parameters.AddWithValue("@IDRACE", raceId);

                    conn.Open();
                    raceDataAdapter.SelectCommand = cmd;
                    raceDataAdapter.Fill(raceData);
                }
            }

            return raceData;

        }

        /// <summary>
        /// Function to return race categories for a given race
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <returns>Dataset containing race category information</returns>
        public DataSet GetRaceCategories(string raceId)
        {
            DataSet raceCategoryData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter raceCategoryDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText = "select rc.idracecategory, rg.display from racecategory rc, registrationcategory rg with (nolock) where rc.idregcategory = rg.idregcategory and rc.idrace=@IDRACE and rg.idregcategory <> 104 order by displayorder";
                    cmd.Parameters.AddWithValue("@IDRACE", raceId);

                    conn.Open();
                    raceCategoryDataAdapter.SelectCommand = cmd;
                    raceCategoryDataAdapter.Fill(raceCategoryData);
                }
            }
            return raceCategoryData;
        }

        ///// <summary>
        ///// Function to return a dataset of athletics clubs from the database
        ///// </summary>
        ///// <returns>DataSet of clubs</returns>
        //public DataSet GetAthleticsClubs()
        //{
        //    DataSet clubData = new DataSet();
        //    MySqlConnection conn = new MySqlConnection(_connString);
        //    MySqlCommand cmd = new MySqlCommand();
        //    MySqlDataAdapter clubDataAdapter = new MySqlDataAdapter();
        //    cmd.Connection = conn;
        //    cmd.CommandText = "select Name, Name from stgnova.RunningClub order by Name asc";

        //    using (conn)
        //    {
        //        conn.Open();
        //        clubDataAdapter.SelectCommand = cmd;
        //        clubDataAdapter.Fill(clubData);
        //    }

        //    return clubData;

        //}


        ///// <summary>
        ///// Function to return a dataset of countries from the database
        ///// </summary>
        ///// <returns>DataSet of countries</returns>
        //public DataSet GetCountries()
        //{
        //    DataSet countryData = new DataSet();
        //    MySqlConnection conn = new MySqlConnection(_connString);
        //    MySqlCommand cmd = new MySqlCommand();
        //    MySqlDataAdapter countryDataAdapter = new MySqlDataAdapter();
        //    cmd.Connection = conn;
        //    cmd.CommandText = "select Name,idCountry from country order by Name asc";

        //    using (conn)
        //    {
        //        conn.Open();
        //        countryDataAdapter.SelectCommand = cmd;
        //        countryDataAdapter.Fill(countryData);
        //    }

        //    return countryData;

        //}

        /// <summary>
        /// Helper function to calculate number of pages of results
        /// </summary>
        /// <param name="totalRows"></param>
        /// <returns>Number of pages as integer</returns>
        private int CalculateTotalPages(double totalRows)
        {
            int totalPages = (int)Math.Ceiling(totalRows / PAGE_SIZE);
            return totalPages;
        }

        /// <summary>
        /// Function to return results from the database, based on search criteria passed to the
        /// function
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <param name="raceNumber">Individual Race Number</param>
        /// <param name="surname">Surname</param>
        /// <param name="firstName">First Name(s)</param>
        /// <param name="gender">Gender (1 male, 2 female)</param>
        /// <param name="postcode">Postcode</param>
        /// <param name="country">Country</param>
        /// <param name="raceCategory">Race Category</param>
        /// <param name="ageGroup">Age Range Group</param>
        /// <param name="athleticsClub">Athletics Club</param>
        /// <param name="page">Page of results to return</param>
        /// <param name="sortColumn">Column to sort results on</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <param name="nikePlusUrl">Url for Nike+ (if appropriate)</param>
        /// <param name="active">If Nike+ is active yet</param>
        /// <returns>DataSet of Results</returns>
        public DataSet GetResults(string raceId, string raceNumber, string surname, string firstName, string gender, string postcode, string country, string raceCategory, string ageGroup, string athleticsClub, int page, string sortColumn, string sortOrder, string nikePlusUrl, string active, string juniorRace, string photosRaceId, string photosRaceName, string raceYear, string gunChip, string eventType)
        {
            DataSet resultsData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;

                    cmd.CommandText = "select resultsdata.* from (SELECT ROW_NUMBER() OVER (";

                    //Sort Column
                    if (sortColumn == "")
                    {
                        cmd.CommandText += "order by TimeFinish, r.CategoryPosition ";
                    }
                    else
                    {
                        cmd.CommandText += "order by " + sortColumn + " ";
                    }

                    //Sort Order
                    if (sortOrder == "")
                    {
                        cmd.CommandText += "ASC ";
                    }
                    else
                    {
                        cmd.CommandText += sortOrder + " ";
                    }

                    cmd.CommandText += ") as Row, ";

                    cmd.CommandText += " r.idRaceRegistration, r.idRace, isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name, r.RaceNumber as 'Race Number', r.AthleticsClubName as 'Club', r.CategoryPosition as 'Overall Position',r.AgeCatPos as 'Age Group Position',r.GenderPos as 'Gender Position',r.AgeGenderPos as 'Age Group and Gender Position', r.TimeFinish as 'Finish Time',isnull(TimeInt1,'NA') as 'Split 1',isnull(TimeInt2,'NA') as 'Split 2',isnull(TimeInt3,'NA') as 'Split 3',isnull(TimeInt4,'NA') as 'Split 4',isnull(TimeInt5,'NA') as 'Split 5',isnull(TimeInt6,'NA') as 'Split 6',isnull(TimeInt7,'NA') as 'Split 7',isnull(TimeInt8,'NA') as 'Split 8',isnull(TimeInt9,'NA') as 'Split 9',isnull(TimeInt10,'NA') as 'Split 10', r.GunChip as 'Gun / Chip' ";

                    if (eventType == "6")
                    {
                        cmd.CommandText += ", r.wetsuit as Wetsuit ";
                    }
                    cmd.CommandText += " from results r with (nolock) left outer join raceregistration rr with (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p with (nolock) on  rr.idperson=p.idperson left outer join racecategory rc with (nolock)  on rc.idracecategory = r.idracecategory where r.idrace= @IDRACE ";
                    cmd.Parameters.AddWithValue("@IDRACE", raceId);

                    if (raceNumber.Trim() == "" || raceNumber.Trim().Equals("0")) //extend it to || raceNumber.Trim().equals("0")
                    {

                        if (firstName.Trim() != "")
                        {
                            cmd.CommandText += "AND R.FORENAMES LIKE @FIRSTNAME ";
                            cmd.Parameters.AddWithValue("@FIRSTNAME", firstName + "%");
                        }

                        if (surname.Trim() != "")
                        {
                            cmd.CommandText += "AND R.SURNAME LIKE @SURNAME ";
                            cmd.Parameters.AddWithValue("@SURNAME", surname + "%");
                        }

                        if (gender != "0")
                        {
                            cmd.CommandText += "AND R.GENDER = @GENDER ";
                            cmd.Parameters.AddWithValue("@GENDER", gender);
                        }

                        if (postcode.Trim() != "")
                        {
                            cmd.CommandText += "AND P.POSTCODE LIKE @POSTCODE ";
                            cmd.Parameters.AddWithValue("@POSTCODE", "%" + postcode + "%");
                        }

                        if (country != "0")
                        {
                            cmd.CommandText += "AND P.IDCOUNTRY = @COUNTRY ";
                            cmd.Parameters.AddWithValue("@COUNTRY", country);
                        }

                        if (raceCategory != "0")
                        {
                            if (raceCategory != "1")
                            {
                                cmd.CommandText += "AND R.IDRACECATEGORY = @RACECATEGORY ";
                                cmd.Parameters.AddWithValue("@RACECATEGORY", raceCategory);
                            }
                            else
                            {
                                cmd.CommandText += "AND RC.IDREGCATEGORY NOT IN (3,8,65) ";
                            }
                        }

                        if (ageGroup != "0")
                        {
                            int MinAge;
                            int MaxAge;

                            switch (ageGroup)
                            {
                                case "34 and under":
                                    MinAge = 0;
                                    MaxAge = 34;
                                    break;

                                case "35-39":
                                    MinAge = 35;
                                    MaxAge = 39;
                                    break;

                                case "40-44":
                                    MinAge = 40;
                                    MaxAge = 44;
                                    break;

                                case "45-49":
                                    MinAge = 45;
                                    MaxAge = 49;
                                    break;

                                case "50-54":
                                    MinAge = 50;
                                    MaxAge = 54;
                                    break;

                                case "55-59":
                                    MinAge = 55;
                                    MaxAge = 59;
                                    break;

                                case "60-64":
                                    MinAge = 60;
                                    MaxAge = 64;
                                    break;

                                case "65-69":
                                    MinAge = 65;
                                    MaxAge = 69;
                                    break;

                                case "70-74":
                                    MinAge = 70;
                                    MaxAge = 74;
                                    break;

                                case "75-79":
                                    MinAge = 75;
                                    MaxAge = 79;
                                    break;

                                case "80+":
                                    MinAge = 80;
                                    MaxAge = 120;
                                    break;

                                default:
                                    MinAge = 0;
                                    MaxAge = 120;
                                    break;
                            }

                            cmd.CommandText += "AND R.AGE >= @MINAGE AND R.AGE <= @MAXAGE ";

                            cmd.Parameters.AddWithValue("@MINAGE", MinAge);
                            cmd.Parameters.AddWithValue("@MAXAGE", MaxAge);
                        }

                        if (juniorRace != "")
                        {
                            cmd.CommandText += TidyJuniorRaceSql(juniorRace) + " ";
                        }

                        if (athleticsClub.Trim() != "")
                        {
                            cmd.CommandText += "AND R.ATHLETICSCLUBNAME LIKE @ATHLETICSCLUB ";
                            cmd.Parameters.AddWithValue("@ATHLETICSCLUB", athleticsClub + "%");
                        }

                        if (gunChip.Trim() != "")
                        {
                            cmd.CommandText += "AND R.GUNCHIP = @GUNCHIP ";
                            cmd.Parameters.AddWithValue("@GUNCHIP", gunChip);
                        }

                        cmd.CommandText += "AND R.TIMEFINISH IS NOT NULL ";

                    }
                    else
                    {
                        //Its a Race Number Search
                        cmd.CommandText += "AND R.RACENUMBER = @RACENUMBER AND R.TIMEFINISH IS NOT NULL ";
                        cmd.Parameters.AddWithValue("@RACENUMBER", raceNumber);
                    }

                    int PageStartRecord = (page * PAGE_SIZE) + 1;
                    int PageEndRecord = ((page * PAGE_SIZE)) + (PAGE_SIZE);

                    cmd.CommandText += ") as resultsdata WHERE Row >= " + PageStartRecord + " AND Row <= " + PageEndRecord + " order by Row";

                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }

            int NumberOfSplits = CountSplitNames(raceId);

            //Strip out invalid Split Times
            if (NumberOfSplits < 1)
            {
                resultsData.Tables[0].Columns.Remove("Split 1");
            }

            if (NumberOfSplits < 2)
            {
                resultsData.Tables[0].Columns.Remove("Split 2");
            }

            if (NumberOfSplits < 3)
            {
                resultsData.Tables[0].Columns.Remove("Split 3");
            }

            if (NumberOfSplits < 4)
            {
                resultsData.Tables[0].Columns.Remove("Split 4");
            }

            if (NumberOfSplits < 5)
            {
                resultsData.Tables[0].Columns.Remove("Split 5");
            }

            if (NumberOfSplits < 6)
            {
                resultsData.Tables[0].Columns.Remove("Split 6");
            }

            if (NumberOfSplits < 7)
            {
                resultsData.Tables[0].Columns.Remove("Split 7");
            }

            if (NumberOfSplits < 8)
            {
                resultsData.Tables[0].Columns.Remove("Split 8");
            }

            if (NumberOfSplits < 9)
            {
                resultsData.Tables[0].Columns.Remove("Split 9");
            }

            if (NumberOfSplits < 10)
            {
                resultsData.Tables[0].Columns.Remove("Split 10");
            }

            //Gun/Chip Time Processing
            foreach (DataRow row in resultsData.Tables[0].Rows)
            {
                if (row["Gun / Chip"].ToString() == "G")
                {
                    row["Gun / Chip"] = "<a href=\"#\" title=\"Gun Time\"> <img src=\"images\\gun.gif\" border=\"0\" alt=\"Gun Time\"></a>";
                }
                else
                {
                    row["Gun / Chip"] = "<a href=\"#\" title=\"Chip Time\"><img src=\"images\\chip.gif\" border=\"0\" alt=\"Chip Time\"></a>";
                }

                if (row.Table.Columns.Contains("Wetsuit"))
                {
                    if (row["Wetsuit"].ToString() == "W")
                    {
                        row["Wetsuit"] = "<a href=\"#\" title=\"Wetsuit\"><img src=\"images\\wetsuit.gif\" border=\"0\" alt=\"Wetsuit\"></a>";
                    }
                    else
                    {
                        row["Wetsuit"] = "&nbsp;";
                    }
                }
            }

            //Nike+ Column Processing
            if (active == "1")
            {
                //Add Nike+ Column
                resultsData.Tables[0].Columns.Add("Nike+");

                foreach (DataRow row in resultsData.Tables[0].Rows)
                {
                    row["Nike+"] = "<a href=\"" + ConfigurationManager.AppSettings["NikeUrl"].ToString() + "runner=" + row["idRaceRegistration"] + "&rid=" + row["idrace"] + "\"  target=\"_blank\"><img src=\"images\\GIR_buttonSeeRun.gif\" border=\"0\" alt=\"See your run powered by Nike+\"></a>";
                }
            }

            //Photo Column Processing
            if (photosRaceId != "")
            {
                //Add Photos Column
                resultsData.Tables[0].Columns.Add("Race Photos");

                foreach (DataRow row in resultsData.Tables[0].Rows)
                {
                    row["Race Photos"] = "<a href=\"" + ConfigurationManager.AppSettings["4rsPhotosUrl"].ToString() + "page=search3&st&race_id=" + photosRaceId.ToString() + "&runner_no=" + row["Race Number"] + "\"  target=\"_blank\"><img src=\"images\\buttonSeeYourPhotos.gif\" border=\"0\" alt=\"See your photos\"></a>";
                }

            }
            //  else if (photosRaceName != "")
            //{
            //    //Add Photos Column
            //    resultsData.Tables[0].Columns.Add("Race Photos");

            //    foreach (DataRow row in resultsData.Tables[0].Rows)
            //    {
            //        row["Race Photos"] = "<a href=\"" + ConfigurationManager.AppSettings["MarathonPhotosUrl"].ToString() + raceYear + "/" + photosRaceName.ToString() + "&bib=" + row["Race Number"] + "\"  target=\"_blank\"><img src=\"images\\buttonSeeYourPhotos.gif\" border=\"0\" alt=\"See your photos\"></a>";
            //    }

            //}

            resultsData.Tables[0].Columns.Remove("Row");
            resultsData.Tables[0].Columns.Remove("idRaceRegistration");
            resultsData.Tables[0].Columns.Remove("idRace");
            //resultsData.Tables[0].Columns.Remove("Gender Position");
            return resultsData;
        }

        /// <summary>
        /// Function to return the total count of records based on search criteria.
        /// This is used to determine total number of pages of results for a search
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <param name="raceNumber">Individual Race Number</param>
        /// <param name="surname">Surname</param>
        /// <param name="firstName">Forename(s)</param>
        /// <param name="gender">Gender 1=male, 2=female</param>
        /// <param name="postcode">Postcode</param>
        /// <param name="country">Country</param>
        /// <param name="raceCategory">Race Category</param>
        /// <param name="ageGroup">Age Range Group</param>
        /// <param name="athleticsClub">Athletics Club</param>
        /// <returns>Integer representing number of results of search</returns>
        public int GetResultsCount(string raceId, string raceNumber, string surname, string firstName, string gender, string postcode, string country, string raceCategory, string ageGroup, string athleticsClub, string juniorRace, string gunChip)
        {
            DataSet resultsData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;

                    cmd.CommandText = "select count(*) from results r with (nolock) left outer join raceregistration rr with (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p with (nolock) on  rr.idperson=p.idperson left outer join racecategory rc with (nolock)  on rc.idracecategory = r.idracecategory where r.idrace= @IDRACE ";
                    cmd.Parameters.AddWithValue("@IDRACE", raceId);

                    if (raceNumber.Trim() == "" || raceNumber.Trim().Equals("0"))
                    {

                        if (firstName.Trim() != "")
                        {
                            cmd.CommandText += "AND R.FORENAMES LIKE @FIRSTNAME ";
                            cmd.Parameters.AddWithValue("@FIRSTNAME", firstName + "%");
                        }

                        if (surname.Trim() != "")
                        {
                            cmd.CommandText += "AND R.SURNAME LIKE @SURNAME ";
                            cmd.Parameters.AddWithValue("@SURNAME", surname + "%");
                        }

                        if (gender != "0")
                        {
                            cmd.CommandText += "AND R.GENDER = @GENDER ";
                            cmd.Parameters.AddWithValue("@GENDER", gender);
                        }

                        if (postcode.Trim() != "")
                        {
                            cmd.CommandText += "AND P.POSTCODE LIKE @POSTCODE ";
                            cmd.Parameters.AddWithValue("@POSTCODE", "%" + postcode + "%");
                        }

                        if (country != "0")
                        {
                            cmd.CommandText += "AND P.IDCOUNTRY = @COUNTRY ";
                            cmd.Parameters.AddWithValue("@COUNTRY", country);
                        }

                        if (raceCategory != "0")
                        {
                            if (raceCategory != "1")
                            {
                                cmd.CommandText += "AND R.IDRACECATEGORY = @RACECATEGORY ";
                                cmd.Parameters.AddWithValue("@RACECATEGORY", raceCategory);
                            }
                            else
                            {
                                cmd.CommandText += "AND RC.IDREGCATEGORY NOT IN (3,8,65) ";
                            }
                        }

                        if (ageGroup != "0")
                        {
                            int MinAge;
                            int MaxAge;

                            switch (ageGroup)
                            {
                                case "34 and under":
                                    MinAge = 0;
                                    MaxAge = 34;
                                    break;

                                case "35-39":
                                    MinAge = 35;
                                    MaxAge = 39;
                                    break;

                                case "40-44":
                                    MinAge = 40;
                                    MaxAge = 44;
                                    break;

                                case "45-49":
                                    MinAge = 45;
                                    MaxAge = 49;
                                    break;

                                case "50-54":
                                    MinAge = 50;
                                    MaxAge = 54;
                                    break;

                                case "55-59":
                                    MinAge = 55;
                                    MaxAge = 59;
                                    break;

                                case "60-64":
                                    MinAge = 60;
                                    MaxAge = 64;
                                    break;

                                case "65-69":
                                    MinAge = 65;
                                    MaxAge = 69;
                                    break;

                                case "70-74":
                                    MinAge = 70;
                                    MaxAge = 74;
                                    break;

                                case "75-79":
                                    MinAge = 75;
                                    MaxAge = 79;
                                    break;

                                case "80+":
                                    MinAge = 80;
                                    MaxAge = 120;
                                    break;

                                default:
                                    MinAge = 0;
                                    MaxAge = 120;
                                    break;
                            }

                            cmd.CommandText += "AND R.AGE >= @MINAGE AND R.AGE <= @MAXAGE ";
                            cmd.Parameters.AddWithValue("@MINAGE", MinAge);
                            cmd.Parameters.AddWithValue("@MAXAGE", MaxAge);
                        }

                        if (juniorRace != "")
                        {
                            cmd.CommandText += TidyJuniorRaceSql(juniorRace) + " ";
                        }

                        if (athleticsClub.Trim() != "")
                        {
                            cmd.CommandText += "AND R.ATHLETICSCLUBNAME LIKE @ATHLETICSCLUB ";
                            cmd.Parameters.AddWithValue("@ATHLETICSCLUB", athleticsClub + "%");
                        }

                        if (gunChip.Trim() != "")
                        {
                            cmd.CommandText += "AND R.GUNCHIP = @GUNCHIP ";
                            cmd.Parameters.AddWithValue("@GUNCHIP", gunChip);
                        }
                    }
                    else
                    {
                        //Its a Race Number Search
                        cmd.CommandText += "AND R.RACENUMBER = @RACENUMBER ";
                        cmd.Parameters.AddWithValue("@RACENUMBER", raceNumber);
                    }

                    //cmd.CommandText += "AND R.TIMEFINISH IS NOT NULL ";

                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }

            return Convert.ToInt32(resultsData.Tables[0].Rows[0][0]);
        }

        public DataSet GetWebServiceResults(string raceId, string raceNumbers)
        {
            DataSet resultsData = new DataSet();

            //Race Number Validation (validates that it is a comma separated integer list, with or without an additional space char)
            //Regex IsValidRaceNumberList = new Regex(@"^((\d?)|(([-+]?\d+\.?\d*)|([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d+\.?\d*))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d+\.?\d*)))$");

            //Race Number Validation (validates that it is a comma separated integer list)
            Regex IsValidRaceNumberList = new Regex(@"^([1-9]{1}[0-9]{0,6})+((,[1-9]{1}[0-9]{0,6}){0,1})+$");

            bool RaceNumberOk = IsValidRaceNumberList.IsMatch(raceNumbers);

            if (RaceNumberOk)
            {
                using (SqlConnection conn = new SqlConnection(_connString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                        cmd.Connection = conn;

                        cmd.CommandText = "select resultsdata.* from (SELECT ROW_NUMBER() OVER (";
                        cmd.CommandText += "order by TimeFinish, r.CategoryPosition ASC";
                        cmd.CommandText += ") as Row, ";
                        cmd.CommandText += " r.forenames + ' ' + r.surname as Name, r.RaceNumber as 'RaceNumber', r.AthleticsClubName as 'Club', r.CategoryPosition as 'OverallPosition',r.AgeCatPos as 'AgeGroupPosition',r.GenderPos as 'GenderPosition',r.AgeGenderPos as 'AgeGroupAndGenderPosition', r.TimeFinish as 'FinishTime',isnull(TimeInt1,'NA') as 'Split1',isnull(TimeInt2,'NA') as 'Split2',isnull(TimeInt3,'NA') as 'Split3',isnull(TimeInt4,'NA') as 'Split4',isnull(TimeInt5,'NA') as 'Split5',isnull(TimeInt6,'NA') as 'Split6',isnull(TimeInt7,'NA') as 'Split7',isnull(TimeInt8,'NA') as 'Split8',isnull(TimeInt9,'NA') as 'Split9',isnull(TimeInt10,'NA') as 'Split10' from results r with (nolock), raceregistration rr with (nolock), person p with (nolock), racecategory rc with (nolock) where r.idraceregistration=rr.idraceregistration and rr.idperson=p.idperson and r.idrace= @IDRACE and rc.idracecategory = r.idracecategory ";

                        cmd.Parameters.AddWithValue("@IDRACE", raceId);

                        cmd.CommandText += "AND R.RACENUMBER IN ( " + raceNumbers + " )";
                        cmd.CommandText += " AND R.TIMEFINISH IS NOT NULL ";
                        //cmd.Parameters.AddWithValue("@RACENUMBERS", raceNumbers);
                        cmd.CommandText += ") as resultsdata";

                        conn.Open();
                        resultsDataAdapter.SelectCommand = cmd;
                        resultsDataAdapter.Fill(resultsData);
                    }
                }

                int NumberOfSplits = CountSplitNames(raceId);

                //Strip out invalid Split Times
                if (NumberOfSplits < 1)
                {
                    resultsData.Tables[0].Columns.Remove("Split1");
                }

                if (NumberOfSplits < 2)
                {
                    resultsData.Tables[0].Columns.Remove("Split2");
                }

                if (NumberOfSplits < 3)
                {
                    resultsData.Tables[0].Columns.Remove("Split3");
                }

                if (NumberOfSplits < 4)
                {
                    resultsData.Tables[0].Columns.Remove("Split4");
                }

                if (NumberOfSplits < 5)
                {
                    resultsData.Tables[0].Columns.Remove("Split5");
                }

                if (NumberOfSplits < 6)
                {
                    resultsData.Tables[0].Columns.Remove("Split6");
                }

                if (NumberOfSplits < 7)
                {
                    resultsData.Tables[0].Columns.Remove("Split7");
                }

                if (NumberOfSplits < 8)
                {
                    resultsData.Tables[0].Columns.Remove("Split8");
                }

                if (NumberOfSplits < 9)
                {
                    resultsData.Tables[0].Columns.Remove("Split9");
                }

                if (NumberOfSplits < 10)
                {
                    resultsData.Tables[0].Columns.Remove("Split10");
                }

                resultsData.Tables[0].Columns.Remove("Row");
            }
            return resultsData;
        }

        public DataSet GetWebserviceAllResultsForPerson(string personId)
        {
            DataSet raceResults = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter raceResultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText = "select race.Description, res.TimeFinish, res.TimeInt1, res.TimeInt2, res.TimeInt3, res.TimeInt4, res.TimeInt5, res.TimeInt6, res.TimeInt7, res.TimeInt8, res.TimeInt9, res.TimeInt10  from results res with (nolock), raceregistration rr with (nolock), person p with (nolock), race with (nolock) where rr.idPerson = @PERSONID and res.idRaceRegistration = rr.idRaceRegistration and rr.idPerson = p.idperson and rr.idRace=race.idrace order by race.RaceDate asc";
                    cmd.Parameters.AddWithValue("@PERSONID", personId);

                    conn.Open();
                    raceResultsDataAdapter.SelectCommand = cmd;
                    raceResultsDataAdapter.Fill(raceResults);
                }
            }
            return raceResults;
        }

        /// <summary>
        /// Function to count the number of Split Names for a particular race
        /// </summary>
        /// <param name="raceId">The Race Id of the race</param>
        /// <returns>integer detailing number of splits</returns>
        private int CountSplitNames(string raceId)
        {
            DataSet splitData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {

                    SqlDataAdapter splitDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText = "select count(*) from racetimes with (nolock) where idrace=@IDRACE and display=1";
                    cmd.Parameters.AddWithValue("@IDRACE", raceId);

                    conn.Open();
                    splitDataAdapter.SelectCommand = cmd;
                    splitDataAdapter.Fill(splitData);
                }
            }

            return Convert.ToInt32(splitData.Tables[0].Rows[0][0]);
        }

        /// <summary>
        /// Function to return the names of splits for a given race
        /// </summary>
        /// <param name="raceId">The Race Id of the race</param>
        /// <returns>Dataset containing the split names</returns>
        public DataSet GetSplitNames(string raceId)
        {
            DataSet splitData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    SqlDataAdapter splitDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText = "select TimeName from racetimes with (nolock) where idrace=@IDRACE and display=1 order by RaceOrder";
                    cmd.Parameters.AddWithValue("@IDRACE", raceId);

                    conn.Open();
                    splitDataAdapter.SelectCommand = cmd;
                    splitDataAdapter.Fill(splitData);
                }
            }

            return splitData;

        }

        /// <summary>
        /// Function to return race waves for a given junior race
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <returns>Dataset containing race waves information</returns>
        public DataSet GetRaceWaves(string raceId)
        {
            DataSet raceWavesData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter raceWavesDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText = "select title, clause from racewaves with (nolock) where idrace=@IDRACE";
                    cmd.Parameters.AddWithValue("@IDRACE", raceId);

                    conn.Open();
                    raceWavesDataAdapter.SelectCommand = cmd;
                    raceWavesDataAdapter.Fill(raceWavesData);
                }
            }
            return raceWavesData;
        }

        private string TidyJuniorRaceSql(string juniorRace)
        {
            juniorRace = juniorRace.Replace("age", "r.age");
            juniorRace = juniorRace.Replace("gender", "r.gender");
            juniorRace = juniorRace.Replace("RaceNumber", "r.RaceNumber");

            return juniorRace;
        }


        /**************EXTENDED VERSION2 *****************/
        /// <summary>
        /// Function to return results from the database, based on search criteria passed to the
        /// function
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <param name="raceNumber">Individual Race Number</param>
        /// <param name="surname">Surname</param>
        /// <param name="firstName">First Name(s)</param>
        /// <param name="gender">Gender (1 male, 2 female)</param>
        /// <param name="postcode">Postcode</param>
        /// <param name="country">Country</param>
        /// <param name="raceCategory">Race Category</param>
        /// <param name="ageGroup">Age Range Group</param>
        /// <param name="athleticsClub">Athletics Club</param>
        /// <param name="page">Page of results to return</param>
        /// <param name="sortColumn">Column to sort results on</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <param name="nikePlusUrl">Url for Nike+ (if appropriate)</param>
        /// <param name="active">If Nike+ is active yet</param>
        /// <returns>DataSet of Results</returns>
        public DataSet GetResults2(string raceId, string raceNumber, string surname, string firstName, string gender, string postcode, string country, string raceCategory, string ageGroup, string athleticsClub, int page, string sortColumn, string sortOrder, string nikePlusUrl, string active, string juniorRace, string photosRaceId, string photosRaceName, string raceYear, string gunChip, string eventType)
        {
            DataSet resultsData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;

                    cmd.CommandText = "select resultsdata.* from (SELECT ROW_NUMBER() OVER (";

                    //Sort Column
                    if (sortColumn == "")
                    {
                        //cmd.CommandText += "order by TimeFinish, r.CategoryPosition ";
                        cmd.CommandText += "order by r.surname, r.forenames ";
                    }
                    else
                    {
                        cmd.CommandText += "order by " + sortColumn + " ";
                    }

                    //Sort Order
                    if (sortOrder == "")
                    {
                        cmd.CommandText += "ASC ";
                    }
                    else
                    {
                        cmd.CommandText += sortOrder + " ";
                    }

                    cmd.CommandText += ") as Row, ";

                    cmd.CommandText += " r.idRaceRegistration, r.idRace, isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name, r.RaceNumber as 'BIB', r.AthleticsClubName as 'Club', r.CategoryPosition as 'Pos', r.TimeFinish as 'Finish Time', r.GunChip as 'Gun/Chip' ";

                    if (eventType == "6")
                    {
                        cmd.CommandText += ", r.wetsuit as Wetsuit ";
                    }
                    cmd.CommandText += " from results r with (nolock) left outer join raceregistration rr with (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p with (nolock) on  rr.idperson=p.idperson left outer join racecategory rc with (nolock)  on rc.idracecategory = r.idracecategory where r.idrace= @IDRACE ";
                    cmd.Parameters.AddWithValue("@IDRACE", raceId);

                    if (raceNumber.Trim() == "" || raceNumber.Trim().Equals("0")) //extend it to || raceNumber.Trim().equals("0")
                    {

                        if (firstName.Trim() != "")
                        {
                            cmd.CommandText += "AND R.FORENAMES LIKE @FIRSTNAME ";
                            cmd.Parameters.AddWithValue("@FIRSTNAME", firstName + "%");
                        }

                        if (surname.Trim() != "")
                        {
                            cmd.CommandText += "AND R.SURNAME LIKE @SURNAME ";
                            cmd.Parameters.AddWithValue("@SURNAME", surname + "%");
                        }

                        if (gender != "0")
                        {
                            cmd.CommandText += "AND R.GENDER = @GENDER ";
                            cmd.Parameters.AddWithValue("@GENDER", gender);
                        }

                        if (postcode.Trim() != "")
                        {
                            cmd.CommandText += "AND P.POSTCODE LIKE @POSTCODE ";
                            cmd.Parameters.AddWithValue("@POSTCODE", "%" + postcode + "%");
                        }

                        if (country != "0")
                        {
                            cmd.CommandText += "AND P.IDCOUNTRY = @COUNTRY ";
                            cmd.Parameters.AddWithValue("@COUNTRY", country);
                        }

                        if (raceCategory != "0")
                        {
                            if (raceCategory != "1")
                            {
                                cmd.CommandText += "AND R.IDRACECATEGORY = @RACECATEGORY ";
                                cmd.Parameters.AddWithValue("@RACECATEGORY", raceCategory);
                            }
                            else
                            {
                                cmd.CommandText += "AND RC.IDREGCATEGORY NOT IN (3,8,65) ";
                            }
                        }

                        if (ageGroup != "0")
                        {
                            int MinAge;
                            int MaxAge;

                            switch (ageGroup)
                            {
                                case "34 and under":
                                    MinAge = 0;
                                    MaxAge = 34;
                                    break;

                                case "35-39":
                                    MinAge = 35;
                                    MaxAge = 39;
                                    break;

                                case "40-44":
                                    MinAge = 40;
                                    MaxAge = 44;
                                    break;

                                case "45-49":
                                    MinAge = 45;
                                    MaxAge = 49;
                                    break;

                                case "50-54":
                                    MinAge = 50;
                                    MaxAge = 54;
                                    break;

                                case "55-59":
                                    MinAge = 55;
                                    MaxAge = 59;
                                    break;

                                case "60-64":
                                    MinAge = 60;
                                    MaxAge = 64;
                                    break;

                                case "65-69":
                                    MinAge = 65;
                                    MaxAge = 69;
                                    break;

                                case "70-74":
                                    MinAge = 70;
                                    MaxAge = 74;
                                    break;

                                case "75-79":
                                    MinAge = 75;
                                    MaxAge = 79;
                                    break;

                                case "80+":
                                    MinAge = 80;
                                    MaxAge = 120;
                                    break;

                                default:
                                    MinAge = 0;
                                    MaxAge = 120;
                                    break;
                            }

                            cmd.CommandText += "AND R.AGE >= @MINAGE AND R.AGE <= @MAXAGE ";

                            cmd.Parameters.AddWithValue("@MINAGE", MinAge);
                            cmd.Parameters.AddWithValue("@MAXAGE", MaxAge);
                        }

                        if (juniorRace != "")
                        {
                            cmd.CommandText += TidyJuniorRaceSql(juniorRace) + " ";
                        }

                        if (athleticsClub.Trim() != "")
                        {
                            cmd.CommandText += "AND R.ATHLETICSCLUBNAME LIKE @ATHLETICSCLUB ";
                            cmd.Parameters.AddWithValue("@ATHLETICSCLUB", athleticsClub + "%");
                        }

                        if (gunChip.Trim() != "")
                        {
                            cmd.CommandText += "AND R.GUNCHIP = @GUNCHIP ";
                            cmd.Parameters.AddWithValue("@GUNCHIP", gunChip);
                        }

                       // cmd.CommandText += "AND R.TIMEFINISH IS NOT NULL ";

                    }
                    else
                    {
                        //Its a Race Number Search
                        //cmd.CommandText += "AND R.RACENUMBER = @RACENUMBER AND R.TIMEFINISH IS NOT NULL ";
                        cmd.CommandText += "AND R.RACENUMBER = @RACENUMBER ";
                        cmd.Parameters.AddWithValue("@RACENUMBER", raceNumber);
                    }

                    int PageStartRecord = (page * PAGE_SIZE) + 1;
                    int PageEndRecord = ((page * PAGE_SIZE)) + (PAGE_SIZE);

                    cmd.CommandText += ") as resultsdata WHERE Row >= " + PageStartRecord + " AND Row <= " + PageEndRecord + " order by Row";

                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }

            int NumberOfSplits = CountSplitNames(raceId);


            //Gun/Chip Time Processing
            ReplaceData(ref resultsData);

            //Nike+ Column Processing
            //if (active == "1")
            //{
            //    //Add Nike+ Column
            //    resultsData.Tables[0].Columns.Add("Nike+");

            //    foreach (DataRow row in resultsData.Tables[0].Rows)
            //    {
            //        row["Nike+"] = "<a href=\"" + ConfigurationManager.AppSettings["NikeUrl"].ToString() + "runner=" + row["idRaceRegistration"] + "&rid=" + row["idrace"] + "\"  target=\"_blank\"><img src=\"images\\GIR_buttonSeeRun.gif\" border=\"0\" alt=\"See your run powered by Nike+\"></a>";
            //    }
            //}

            //Add Average Speed Column
            resultsData.Tables[0].Columns.Add("Speed mph");
            foreach (DataRow row in resultsData.Tables[0].Rows)
            {
                row["Speed mph"] = "average";
            }


            //Photo Column Processing
            if (photosRaceId != "")
            {
                //Add Photos Column
                resultsData.Tables[0].Columns.Add("Photos");

                foreach (DataRow row in resultsData.Tables[0].Rows)
                {
                    row["Photos"] = "photos";
                }

            }
            //  else if (photosRaceName != "")
            //{
            //    //Add Photos Column
            //    resultsData.Tables[0].Columns.Add("Race Photos");

            //    foreach (DataRow row in resultsData.Tables[0].Rows)
            //    {
            //        row["Race Photos"] = "<a href=\"" + ConfigurationManager.AppSettings["MarathonPhotosUrl"].ToString() + raceYear + "/" + photosRaceName.ToString() + "&bib=" + row["Race Number"] + "\"  target=\"_blank\"><img src=\"images\\buttonSeeYourPhotos.gif\" border=\"0\" alt=\"See your photos\"></a>";
            //    }

            //}

           
            //add to compare
            resultsData.Tables[0].Columns.Add("Compare");

            foreach (DataRow row in resultsData.Tables[0].Rows)
            {
                if(!string.IsNullOrEmpty(row["Pos"].ToString()))
                row["Compare"] = "<a href=\"javascript:addToCompare('" + row["Name"] + "'," + row["BIB"] + ");\" title=\"add to compare\"><img src=\"\\Results\\images\\add.png\" border=\"0\" alt=\"add to compare\"></a>";
            }

            resultsData.Tables[0].Columns.Remove("Row");
            resultsData.Tables[0].Columns.Remove("idRaceRegistration");
            resultsData.Tables[0].Columns.Remove("idRace");
            resultsData.Tables[0].Columns.Remove("Gun/Chip");
            resultsData.Tables[0].Columns.Remove("Club");
            resultsData.Tables[0].Columns.Remove("Pos");
            return resultsData;
        }

        public DataSet GetFullResults(string raceId, string sortColumn, string sortOrder, string eventType, int page, string raceCategory)
        {
            DataSet resultsData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;

                    cmd.CommandText = "select resultsdata.* from (SELECT ROW_NUMBER() OVER (";

                    //Sort Column
                    if (sortColumn == "")
                    {
                        //cmd.CommandText += "order by TimeFinish, r.CategoryPosition ";
                        cmd.CommandText += "order by r.surname, r.forenames ";
                    }
                    else
                    {
                        cmd.CommandText += "order by " + sortColumn + " ";
                    }

                    //Sort Order
                    if (sortOrder == "")
                    {
                        cmd.CommandText += "ASC ";
                    }
                    else
                    {
                        cmd.CommandText += sortOrder + " ";
                    }

                    cmd.CommandText += ") as Row, ";

                    cmd.CommandText += " r.idRaceRegistration, r.idRace, r.CategoryPosition as 'Pos', r.Gender as 'Gender', r.RaceNumber as 'BIB', isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name, r.AthleticsClubName as 'Club', r.TimeFinish as 'Finish Time', r.GunChip as 'Gun/Chip' ";

                    if (eventType == "6")
                    {
                        cmd.CommandText += ", r.wetsuit as Wetsuit ";
                    }
                    cmd.CommandText += " from results r with (nolock) left outer join raceregistration rr with (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p with (nolock) on  rr.idperson=p.idperson left outer join racecategory rc with (nolock)  on rc.idracecategory = r.idracecategory where r.idrace= @IDRACE ";
                    cmd.Parameters.AddWithValue("@IDRACE", raceId);

                    if (raceCategory != "0")
                    {
                        if (raceCategory != "1")
                        {
                            cmd.CommandText += "AND R.IDRACECATEGORY = @RACECATEGORY ";
                            cmd.Parameters.AddWithValue("@RACECATEGORY", raceCategory);
                        }
                        else
                        {
                            cmd.CommandText += "AND RC.IDREGCATEGORY NOT IN (3,8,65) ";
                        }
                    }

                    //Its a Race Number Search
                   // cmd.CommandText += "AND R.TIMEFINISH IS NOT NULL ";


                    int PageStartRecord = (page * PAGE_SIZE) + 1;
                    int PageEndRecord = ((page * PAGE_SIZE)) + (PAGE_SIZE);

                    cmd.CommandText += ") as resultsdata WHERE Row >= " + PageStartRecord + " AND Row <= " + PageEndRecord + " order by Row";

                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }

            int NumberOfSplits = CountSplitNames(raceId);

            //Add Average Speed Column
            resultsData.Tables[0].Columns.Add("Speed mph");
            foreach (DataRow row in resultsData.Tables[0].Rows)
            {
                row["Speed mph"] = "average";
            }

            //Gun/Chip Time Processing
            ReplaceData(ref resultsData);

            resultsData.Tables[0].Columns.Remove("Row");
            resultsData.Tables[0].Columns.Remove("idRaceRegistration");
            resultsData.Tables[0].Columns.Remove("idRace");
            resultsData.Tables[0].Columns.Remove("Gun/Chip");
            resultsData.Tables[0].Columns.Remove("Club");
            resultsData.Tables[0].Columns.Remove("Pos");
            return resultsData;
        }


        /// <summary>
        /// Function to return results from the database, based on search criteria passed to the
        /// function
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <param name="raceNumber">Race Number</param>
        /// <returns>DataSet of Results</returns>
        public DataSet GetResultsByRaceNumber(string raceId, string raceNumber)
        {
            DataSet resultsData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    int bib = 0;
                    string tolerance = "5";
                    bool isNum = int.TryParse(raceNumber, out bib);
                    if (isNum && bib < 400) tolerance = "2";
                    

                    cmd.CommandText += "DECLARE @thisPosition int ";
                    cmd.CommandText += "DECLARE @posPointer int ";
                    cmd.CommandText += "SELECT @thisPosition = r.CategoryPosition from results r ";
                    cmd.CommandText += "WHERE r.idrace = @idrace ";
                    cmd.CommandText += "AND r.RACENUMBER = @racenumber AND r.TIMEFINISH IS NOT NULL ";

                    cmd.CommandText += "IF (@thisPosition IS NOT NULL) ";
                    cmd.CommandText += "BEGIN ";
                    cmd.CommandText += "SET @posPointer = @thisPosition - " + tolerance + " ";

                    cmd.CommandText += "SET RowCount 11 ";
                    cmd.CommandText += "SELECT  r.idRaceRegistration, r.idRace, r.CategoryPosition as 'Pos', r.RaceNumber as 'BIB', isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name," +
                        " r.TimeFinish as 'Finish Time', r.GunChip as 'Gun/Chip' from results r with (nolock) left outer join raceregistration rr  ";
                    cmd.CommandText += "WITH (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p  ";
                    cmd.CommandText += "WITH (nolock) on  rr.idperson=p.idperson left outer join racecategory rc  ";
                    cmd.CommandText += "WITH (nolock)  on rc.idracecategory = r.idracecategory where r.idrace= @idrace ";
                    cmd.CommandText += "AND r.CategoryPosition >= @posPointer AND r.TIMEFINISH IS NOT NULL order by r.CategoryPosition ";
                    cmd.CommandText += "END ";
                    cmd.CommandText += "ELSE ";
                    cmd.CommandText += "BEGIN ";
                    cmd.CommandText += "SELECT  r.idRaceRegistration, r.idRace, r.CategoryPosition as 'Pos', r.RaceNumber as 'BIB', isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name, ";
                    cmd.CommandText += "r.TimeFinish as 'Finish Time', r.GunChip as 'Gun/Chip' from results r with (nolock) left outer join raceregistration rr ";
                    cmd.CommandText += "WITH (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p ";
                    cmd.CommandText += "WITH (nolock) on  rr.idperson=p.idperson left outer join racecategory rc ";
                    cmd.CommandText += "WITH (nolock)  on rc.idracecategory = r.idracecategory where r.idrace = @idrace AND r.RaceNumber = @racenumber ";
                    cmd.CommandText += "END";
                    cmd.Parameters.AddWithValue("@idrace", raceId);
                    cmd.Parameters.AddWithValue("@racenumber", raceNumber);
                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }


            //Gun/Chip Time Processing
            if (resultsData != null && resultsData.Tables.Count > 0)
            {
                ReplaceData(ref resultsData);
                resultsData.Tables[0].Columns.Remove("idRaceRegistration");
                resultsData.Tables[0].Columns.Remove("idRace");
                resultsData.Tables[0].Columns.Remove("Gun/Chip");
            }
            return resultsData;
        }

        public DataSet GetResultsByGenderGroup(string raceId, string raceNumber)
        {
            DataSet resultsData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    int bib = 0;
                    string tolerance = "2";
                    bool isNum = int.TryParse(raceNumber, out bib);
                    if (isNum && bib < 400) tolerance = "1";
                    cmd.CommandText += "DECLARE @thisPosition int ";
                    cmd.CommandText += "DECLARE @posPointer int ";
                    cmd.CommandText += "DECLARE @gender smallint ";
                    cmd.CommandText += "SELECT @thisPosition = r.GenderPos, @gender = r.Gender from results r ";
                    cmd.CommandText += "WHERE r.idrace = @idrace ";
                    cmd.CommandText += "AND r.RACENUMBER = @racenumber AND r.TIMEFINISH IS NOT NULL ";

                    cmd.CommandText += "IF (@thisPosition IS NOT NULL) ";
                    cmd.CommandText += "BEGIN ";
                    cmd.CommandText += "SET @posPointer = @thisPosition - " + tolerance + " ";

                    cmd.CommandText += "SET RowCount 5 ";
                    cmd.CommandText += "SELECT r.idRaceRegistration, r.idRace, r.GenderPos as 'Pos', r.RaceNumber as 'BIB', isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name,";
                    cmd.CommandText += " r.TimeFinish as 'Finish Time' from results r with (nolock) left outer join raceregistration rr";
                    cmd.CommandText += " with (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p";
                    cmd.CommandText += " with (nolock) on  rr.idperson=p.idperson left outer join racecategory rc";
                    cmd.CommandText += " with (nolock)  on rc.idracecategory = r.idracecategory where r.idrace= @idrace";
                    cmd.CommandText += " AND r.GenderPos >= @posPointer AND r.TIMEFINISH IS NOT NULL";
                    cmd.CommandText += " AND r.Gender = @gender";
                    cmd.CommandText += " order by r.GenderPos";
                    cmd.CommandText += " END";
                    cmd.Parameters.AddWithValue("@idrace", raceId);
                    cmd.Parameters.AddWithValue("@racenumber", raceNumber);
                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }


            //Gun/Chip Time Processing
            if (resultsData != null && resultsData.Tables.Count > 0)
            {
                //ReplaceData(ref resultsData);
                resultsData.Tables[0].Columns.Remove("idRaceRegistration");
                resultsData.Tables[0].Columns.Remove("idRace");
            }
            return resultsData;
        }
        /// <summary>
        /// Function to return results from the database, based on search criteria passed to the
        /// function
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <param name="raceNumber">Race Number</param>
        /// <returns>DataSet of Results</returns>
        public DataSet CompareByRaceNumber(string raceId, string raceNumber)
        {
            DataSet resultsData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;

                    cmd.CommandText += "SELECT r.idRaceRegistration, r.idRace, r.CategoryPosition as 'Pos', r.RaceNumber as 'BIB', isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name, ";
                    cmd.CommandText += " r.TimeFinish as 'Finish Time', r.GunChip as 'Gun/Chip' from results r with (nolock) left outer join raceregistration rr ";
                    cmd.CommandText += "WITH (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p ";
                    cmd.CommandText += "WITH (nolock) on  rr.idperson=p.idperson left outer join racecategory rc  ";
                    cmd.CommandText += "WITH (nolock)  on rc.idracecategory = r.idracecategory where r.idrace= @idrace ";
                    cmd.CommandText += "AND r.RaceNumber IN (" + raceNumber + ") ";
                    cmd.CommandText += "AND r.TIMEFINISH IS NOT NULL order by r.CategoryPosition ";
                    cmd.Parameters.AddWithValue("@idrace", raceId);
                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }


            //Gun/Chip Time Processing
            if (resultsData != null && resultsData.Tables.Count > 0)
            {
                ReplaceData(ref resultsData);
                resultsData.Tables[0].Columns.Remove("idRaceRegistration");
                resultsData.Tables[0].Columns.Remove("idRace");
                resultsData.Tables[0].Columns.Remove("Gun/Chip");
                //resultsData.Tables[0].Columns.Remove("Pos");
            }
            return resultsData;
        }

        /// <summary>
        /// Function to return results from the database, based on search criteria passed to the
        /// function
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <param name="raceNumber">Race Number</param>
        /// <returns>DataSet of Results</returns>
        public DataSet GetTimeSplits(string raceId, string raceNumber)
        {
            DataSet resultsData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;

                    cmd.CommandText += "SELECT isnull(TimeInt1,'NA') as 'Split 1',isnull(TimeInt2,'NA') as 'Split 2',isnull(TimeInt3,'NA') as 'Split 3',isnull(TimeInt4,'NA') as 'Split 4',isnull(TimeInt5,'NA') as 'Split 5', ";
                    cmd.CommandText += "isnull(TimeInt6,'NA') as 'Split 6',isnull(TimeInt7,'NA') as 'Split 7',isnull(TimeInt8,'NA') as 'Split 8',isnull(TimeInt9,'NA') as 'Split 9',isnull(TimeInt10,'NA') as 'Split 10' ";
                    cmd.CommandText += "from results r with (nolock) left outer join raceregistration rr ";
                    cmd.CommandText += "WITH (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p ";
                    cmd.CommandText += "WITH (nolock) on  rr.idperson=p.idperson left outer join racecategory rc  ";
                    cmd.CommandText += "WITH (nolock)  on rc.idracecategory = r.idracecategory where r.idrace= @idrace ";
                    cmd.CommandText += "AND r.RaceNumber = " + raceNumber + " ";
                    //cmd.CommandText += "AND r.TIMEFINISH IS NOT NULL ";
                    cmd.Parameters.AddWithValue("@idrace", raceId);
                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }

            //Gun/Chip Time Processing
            if (resultsData != null && resultsData.Tables.Count > 0)
            {
                int NumberOfSplits = CountSplitNames(raceId);

                //Strip out invalid Split Times
                if (NumberOfSplits < 1)
                {
                    resultsData.Tables[0].Columns.Remove("Split 1");
                }

                if (NumberOfSplits < 2)
                {
                    resultsData.Tables[0].Columns.Remove("Split 2");
                }

                if (NumberOfSplits < 3)
                {
                    resultsData.Tables[0].Columns.Remove("Split 3");
                }

                if (NumberOfSplits < 4)
                {
                    resultsData.Tables[0].Columns.Remove("Split 4");
                }

                if (NumberOfSplits < 5)
                {
                    resultsData.Tables[0].Columns.Remove("Split 5");
                }

                if (NumberOfSplits < 6)
                {
                    resultsData.Tables[0].Columns.Remove("Split 6");
                }

                if (NumberOfSplits < 7)
                {
                    resultsData.Tables[0].Columns.Remove("Split 7");
                }

                if (NumberOfSplits < 8)
                {
                    resultsData.Tables[0].Columns.Remove("Split 8");
                }

                if (NumberOfSplits < 9)
                {
                    resultsData.Tables[0].Columns.Remove("Split 9");
                }

                if (NumberOfSplits < 10)
                {
                    resultsData.Tables[0].Columns.Remove("Split 10");
                }


            }
            return resultsData;
        }

        /// <summary>
        /// Function to return results from the database, based on search criteria passed to the
        /// function
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <param name="raceNumber">Race Number</param>
        /// <returns>DataSet of Results</returns>
        public DataSet GetResultsByAgeGroup(string raceId, string raceNumber)
        {
            DataSet resultsData = new DataSet();
            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    int bib = 0;
                    string tolerance = "2";
                    bool isNum = int.TryParse(raceNumber, out bib);
                    if (isNum && bib < 400) tolerance = "1";
                    cmd.CommandText += "DECLARE @thisPosition int ";
                    cmd.CommandText += "DECLARE @posPointer int ";
                    cmd.CommandText += "DECLARE @age int ";
                    cmd.CommandText += "DECLARE @maxAge int ";
                    cmd.CommandText += "DECLARE @minAge int ";
                    cmd.CommandText += "SELECT @thisPosition = r.AgeCatPos, @age = r.Age FROM results r  ";
                    cmd.CommandText += "WHERE r.idrace= @idrace ";
                    cmd.CommandText += "AND r.RACENUMBER = @racenumber AND r.TIMEFINISH IS NOT NULL ";
                    cmd.CommandText += "IF (@thisPosition IS NOT NULL AND @age IS NOT NULL) ";
                    cmd.CommandText += "BEGIN ";
                    cmd.CommandText += "IF (@age <=34) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 0 ";
                    cmd.CommandText += "		SET @maxAge = 34 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=35 AND @age <= 39) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 35 ";
                    cmd.CommandText += "		SET @maxAge = 39 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=40 AND @age <= 44) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 40 ";
                    cmd.CommandText += "		SET @maxAge = 44 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=45 AND @age <= 49) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 45 ";
                    cmd.CommandText += "		SET @maxAge = 49 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=50 AND @age <= 54) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 50 ";
                    cmd.CommandText += "		SET @maxAge = 54 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=35 AND @age <= 39) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 35 ";
                    cmd.CommandText += "		SET @maxAge = 39 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=55 AND @age <= 59) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 55 ";
                    cmd.CommandText += "		SET @maxAge = 59 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=60 AND @age <= 64) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 60 ";
                    cmd.CommandText += "		SET @maxAge = 64 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=65 AND @age <= 69) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 65 ";
                    cmd.CommandText += "		SET @maxAge = 69 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=70 AND @age <= 74) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 70 ";
                    cmd.CommandText += "		SET @maxAge = 74 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=75 AND @age <= 79) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 75 ";
                    cmd.CommandText += "		SET @maxAge = 79 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "ELSE IF (@age >=80 AND @age <= 120) ";
                    cmd.CommandText += "	BEGIN ";
                    cmd.CommandText += "		SET @minAge = 80 ";
                    cmd.CommandText += "		SET @maxAge = 120 ";
                    cmd.CommandText += "	END ";
                    cmd.CommandText += "SET @posPointer = @thisPosition - " + tolerance + " ";
                    cmd.CommandText += "SET RowCount 5 ";
                    cmd.CommandText += "SELECT  r.idRaceRegistration, r.idRace, r.AgeCatPos AS 'Pos', r.RaceNumber AS 'BIB', isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name,"
                        + " r.TimeFinish AS 'Finish Time', cast(@minAge as varchar) + ' - ' + cast(@maxAge as varchar) as 'Age Group' FROM results r WITH (nolock) left outer join raceregistration rr  ";
                    cmd.CommandText += "WITH (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p ";
                    cmd.CommandText += "WITH (nolock) on  rr.idperson=p.idperson left outer join racecategory rc  ";
                    cmd.CommandText += "WITH (nolock)  on rc.idracecategory = r.idracecategory WHERE r.idrace= @idrace ";
                    cmd.CommandText += "AND r.AgeCatPos >= @posPointer AND r.TIMEFINISH IS NOT NULL  ";
                    cmd.CommandText += "AND r.Age between @minAge AND @maxAge ";
                    cmd.CommandText += "ORDER BY r.AgeCatPos ";
                    cmd.CommandText += "END ";
                    cmd.Parameters.AddWithValue("@idrace", raceId);
                    cmd.Parameters.AddWithValue("@racenumber", raceNumber);
                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }


            //Gun/Chip Time Processing
            if (resultsData != null && resultsData.Tables.Count > 0)
            {
                // ReplaceData(ref resultsData);
                resultsData.Tables[0].Columns.Remove("idRaceRegistration");
                resultsData.Tables[0].Columns.Remove("idRace");
                // resultsData.Tables[0].Columns.Remove("Gun/Chip");
            }
            return resultsData;
        }
        /// <summary>
        /// Function to return results from the database, based on search criteria passed to the
        /// function
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <param name="raceNumber">Race Number</param>
        /// <returns>DataSet of Results</returns>
        public DataSet CompareByAgeGroup(string raceId, string raceNumber, bool getGunChip)
        {
            DataSet resultsData = new DataSet();
            string gunChip = string.Empty;
            if (getGunChip) gunChip = ", r.GunChip as 'Gun/Chip'";
            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText += "SELECT r.idRaceRegistration, r.idRace, r.AgeCatPos 'Pos', r.RaceNumber AS 'BIB', isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name, r.TimeFinish AS 'Finish Time'" + gunChip + " FROM results r WITH (nolock) left outer join raceregistration rr ";
                    cmd.CommandText += "WITH (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p  ";
                    cmd.CommandText += "WITH (nolock) on  rr.idperson=p.idperson left outer join racecategory rc  ";
                    cmd.CommandText += "WITH (nolock)  on rc.idracecategory = r.idracecategory WHERE r.idrace = @idrace ";
                    cmd.CommandText += "AND r.RaceNumber IN (" + raceNumber + ") ";
                    cmd.CommandText += "ORDER BY r.AgeCatPos ";
                    cmd.Parameters.AddWithValue("@idrace", raceId);
                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }


            if (resultsData != null && resultsData.Tables.Count > 0)
            {
                if (getGunChip)
                {
                    ReplaceData(ref resultsData);
                }
                resultsData.Tables[0].Columns.Remove("idRaceRegistration");
                resultsData.Tables[0].Columns.Remove("idRace");
                resultsData.Tables[0].Columns.Remove("Gun/Chip");
            }
            return resultsData;
        }
        public DataSet CompareByGenderGroup(string raceId, string raceNumber)
        {
            DataSet resultsData = new DataSet();
            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText += "SELECT r.idRaceRegistration, r.idRace, r.RaceNumber AS 'BIB', isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name, r.GenderPos 'Pos', r.TimeFinish AS 'Finish Time' FROM results r WITH (nolock) left outer join raceregistration rr ";
                    cmd.CommandText += "WITH (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p  ";
                    cmd.CommandText += "WITH (nolock) on  rr.idperson=p.idperson left outer join racecategory rc  ";
                    cmd.CommandText += "WITH (nolock)  on rc.idracecategory = r.idracecategory WHERE r.idrace = @idrace ";
                    cmd.CommandText += "AND r.RaceNumber IN (" + raceNumber + ") ";
                    cmd.CommandText += "ORDER BY r.GenderPos ";
                    cmd.Parameters.AddWithValue("@idrace", raceId);
                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                }
            }


            if (resultsData != null && resultsData.Tables.Count > 0)
            {
                // ReplaceData(ref resultsData);
                resultsData.Tables[0].Columns.Remove("idRaceRegistration");
                resultsData.Tables[0].Columns.Remove("idRace");
                //resultsData.Tables[0].Columns.Remove("Gun/Chip");
            }
            return resultsData;
        }
        /// <summary>
        /// Function to return results from the database, based on search criteria passed to the
        /// function
        /// </summary>
        /// <param name="raceId">Race Id</param>
        /// <param name="raceId">Top Number of Records</param>
        /// <param name="raceId">Gendar</param>
        /// <returns>DataSet of Results</returns>
        public DataSet GetTopFinishers(string raceId, string top, string gender)
        {
            DataSet resultsData = new DataSet();
            if (string.IsNullOrEmpty(top)) top = "5";
            if (string.IsNullOrEmpty(gender)) gender = "0";
            using (SqlConnection conn = new SqlConnection(_connString))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter resultsDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText += "SELECT TOP " + top + " r.idRaceRegistration, r.idRace, r.CategoryPosition 'Pos', isnull(r.forenames,'') + ' ' + isnull(r.surname,'') as Name, r.RaceNumber AS 'BIB', r.TimeFinish AS 'Finish Time', r.GunChip AS 'Gun/Chip' FROM results r WITH (nolock) left outer join raceregistration rr ";
                    cmd.CommandText += "WITH (nolock) on r.idraceregistration=rr.idraceregistration left outer join person p  ";
                    cmd.CommandText += "WITH (nolock) on  rr.idperson=p.idperson left outer join racecategory rc  ";
                    cmd.CommandText += "WITH (nolock)  on rc.idracecategory = r.idracecategory WHERE r.idrace = @idrace ";
                    cmd.CommandText += "AND r.Gender = @gender AND r.TimeFinish IS NOT NULL AND r.CategoryPosition IS NOT NULL AND r.GunChip IS NOT NULL ";
                    cmd.CommandText += "ORDER BY r.CategoryPosition ";
                    cmd.Parameters.AddWithValue("@idrace", raceId);
                    cmd.Parameters.AddWithValue("@gender", gender);
                    conn.Open();
                    resultsDataAdapter.SelectCommand = cmd;
                    resultsDataAdapter.Fill(resultsData);
                    //for legacy data with gun/chips always be null, try again
                    if (resultsData != null && resultsData.Tables.Count > 0 && resultsData.Tables[0].Rows.Count == 0)
                    {
                        cmd.CommandText = cmd.CommandText.Replace("AND r.GunChip IS NOT NULL", "");
                        resultsDataAdapter.SelectCommand = cmd;
                        resultsDataAdapter.Fill(resultsData);
                    }
                }
            }


            //Gun/Chip Time Processing
            if (resultsData != null && resultsData.Tables.Count > 0)
            {
                ReplaceData(ref resultsData);
                resultsData.Tables[0].Columns.Remove("idRaceRegistration");
                resultsData.Tables[0].Columns.Remove("idRace");
            }
            return resultsData;
        }

        public string GetRaceName(int raceId)
        {
            string name = string.Empty;

            DataSet raceData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    SqlDataAdapter raceDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText = "select description, idrace from race where idrace = @raceId";
                    cmd.Parameters.AddWithValue("@raceId", raceId);
                    conn.Open();
                    raceDataAdapter.SelectCommand = cmd;
                    raceDataAdapter.Fill(raceData);
                }
            }
            if (raceData != null && raceData.Tables.Count > 0 && raceData.Tables[0].Rows.Count > 0)
            {
                name = raceData.Tables[0].Rows[0]["description"].ToString();
            }
            return name;
        }
        private void ReplaceData(ref DataSet ds)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row["Gun/Chip"].ToString() == "G")
                {
                    row["Gun/Chip"] = "<a href=\"#\" title=\"Gun Time\"><img src=\"images\\gun.gif\" border=\"0\" alt=\"Gun Time\" /></a>";
                }
                else
                {
                    row["Gun/Chip"] = "<a href=\"#\" title=\"Chip Time\"><img src=\"images\\chip.gif\" border=\"0\" alt=\"Chip Time\" /></a>";
                }

                if (row.Table.Columns.Contains("Wetsuit"))
                {
                    if (row["Wetsuit"].ToString() == "W")
                    {
                        row["Wetsuit"] = "<a href=\"#\" title=\"Wetsuit\"><img src=\"images\\wetsuit.gif\" border=\"0\" alt=\"Wetsuit\" /></a>";
                    }
                    else
                    {
                        row["Wetsuit"] = "&nbsp;";
                    }
                }
            }
        }
    }
}
