﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Helper
/// </summary>
public static class Helper
{
    public class RaceInfo
    {
        private double _distance;
        private string _name;
        public double Distance { get { return _distance; } set { _distance = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        public RaceInfo() { }
        public RaceInfo(string name, double distance) { _name = name; _distance = distance; }
    }
    /// <summary>
    /// Checks whether a search paremeter string contains bad characters or phrases
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static bool IsSearchContainsBadPhrases(string str)
    {
        bool contains = (str != HttpUtility.HtmlEncode(str));
        if (contains == false)
        {
            if (str.ToLower().Contains("insert") || str.ToLower().Contains("update")
                || str.ToLower().Contains("delete") || str.ToLower().Contains("--"))
            {
                contains = true;
            }
        }
        return contains;
    }
    public static string GetEventName(int EventID)
    {
        string name = string.Empty;

        return name;
    }
    public static string ComputeAverageSpeed(string time, string distance)
    {
        string avgSpeed = string.Empty;
        try
        {
            int dist = GetDistance(distance);
            string[] t1Array = time.Split(":".ToCharArray());
            if (t1Array.Length > 0 && dist > 0)
            {
                decimal hours = Convert.ToDecimal(t1Array[0]);
                decimal munites = Convert.ToDecimal(t1Array[1]);
                decimal totalMunites = (hours * 60) + munites;
                decimal speed = (60 * dist) / totalMunites;
                avgSpeed = Math.Round(speed, 2).ToString();

            }
        }
        catch
        {
            avgSpeed = string.Empty;
        }

        return avgSpeed;
    }
    public static int GetDistance(string strDistance)
    {
        char[] chars = strDistance.ToCharArray();
        string filtered = string.Empty;
        int response = 0;
        foreach (char c in chars)
        {
            if (char.IsDigit(c))
            {
                filtered += c;
            }
        }
        if (!string.IsNullOrEmpty(filtered))
            response = Convert.ToInt32(filtered);
        return response;
    }
    public static string GenerateFlotGraph(string dynamicStr, string renderElementID, string graphTitle, string xAxisTitle, string yAxisTitle)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder("");

        if (!string.IsNullOrEmpty(dynamicStr))
        {
            if (dynamicStr.EndsWith(","))
                dynamicStr = dynamicStr.Remove(dynamicStr.LastIndexOf(","), 1);
            sb.AppendLine("<script type=\"text/javascript\">");
           
                sb.AppendLine("var fh_data = [" + dynamicStr + "];");

                sb.AppendLine("var options = {");
                sb.AppendLine("lines: { show: true, lineWidth: 3 },");
                sb.AppendLine("points: { show: true },");
                sb.AppendLine("legend: { noColumns: 2, position: \"se\"/*, container: '#flot-legend'*/ },");
                //sb.AppendLine("yaxis: { min: -25, max: 25 },");
                sb.AppendLine("xaxis: {  },");
                sb.AppendLine("selection: { mode: \"x\" },");
                sb.AppendLine("grid: { color: \"#fff\", hoverable: true},");
                sb.AppendLine("colors: [\"#EFC1FF\"]");
                sb.AppendLine("};");

            sb.AppendLine("$(document).ready(function() {");

            sb.AppendLine("/* setup navigation, content boxes, etc... */");
            //sb.AppendLine("Administry.setup();");

            //andhere
            
            sb.AppendLine("function showTooltip(x, y, contents) {");
            //sb.AppendLine("alert(contents);");
            sb.AppendLine("$('<div id=\"hovertip\">Average Speed: <br />' + contents + '</div>').css({");
            sb.AppendLine("position: 'absolute',");
            sb.AppendLine("display: 'none',");
            sb.AppendLine("top: y + 5,");
            sb.AppendLine("left: x + 15,");
            sb.AppendLine("border: '2px solid #666',");
            sb.AppendLine("padding: '4px',");
            sb.AppendLine("'background-color': '#fff',");
            sb.AppendLine("opacity: 0.9,");
            sb.AppendLine("color: '#666',");
            sb.AppendLine("fontSize: '13px'");
            sb.AppendLine("}).appendTo(\"body\").fadeIn('fast');");
            sb.AppendLine("}");

            //here
            

            sb.AppendLine("var plot = $.plot($(\"#" + renderElementID + "\"), fh_data, options);");

            sb.AppendLine("$(\"#" + renderElementID + "\").bind(\"selected\", function(event, area) {");
            sb.AppendLine("plot = $.plot($(\"#" + renderElementID + "\"), fh_data,");
            sb.AppendLine("$.extend(true, {}, options, {");
            sb.AppendLine("xaxis: { min: area.x1, max: area.x2 }");
            sb.AppendLine("}));");
            sb.AppendLine("$('#clearSelection').show();");
            sb.AppendLine("});");
            sb.AppendLine("var previousPoint = null;");
            sb.AppendLine("$(\"#" + renderElementID + "\").bind(\"plothover\", function(event, pos, item) {");
            sb.AppendLine("if (item) {");
            sb.AppendLine("if (previousPoint != item.datapoint) {");
            sb.AppendLine("previousPoint = item.datapoint;");

            sb.AppendLine("$(\"#hovertip\").remove();");
            sb.AppendLine("var y = item.datapoint[1];");

            sb.AppendLine("showTooltip(item.pageX, item.pageY, y + ' " + yAxisTitle + "');");
            sb.AppendLine("}");
            sb.AppendLine("}");
            sb.AppendLine("else {");
            sb.AppendLine("$(\"#hovertip\").remove();");
            sb.AppendLine("previousPoint = null;");
            sb.AppendLine("}");
            sb.AppendLine("});");
            sb.AppendLine("$(\"#clearSelection\").click(function() {");
            sb.AppendLine("$.plot($(\"#" + renderElementID + "\"), fh_data, options);");
            sb.AppendLine("$('#clearSelection').hide();");
            sb.AppendLine("});");

            sb.AppendLine("});");

            sb.AppendLine("</script>");

        }
        return sb.ToString();

    }

}
