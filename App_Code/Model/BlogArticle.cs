﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using GS.Generic.Web;
using ThirteenStrides.Database;

/// <summary>
/// Summary description for NewsArticle
/// </summary>

namespace Gr.Model {

	public class BlogArticle {
		#region private properties

		private int _id;
		private string _title;
		private string _shortDescription;
		private string _imageFilename;
		private string _article_content;
		private string _author;
        private string _metaTitle;
        private string _metaKey;
        private string _metaDescription;
        private string _friendlyUrl;
		private string _last_edited;
		private DateTime _publish_date;
		private DateTime _expiry_date;
		private NewsStatus _blog_status;

		private bool _isOnRunning;
		private bool _isJunior;
		private bool _allowComments;

		private BlogArticleTagList _tags;
		private string _errorMessage;

		#endregion private properties

		#region public properties

		public int ID { get { return _id; } set { _id = value; } }

		public string Title { get { return _title; } set { _title = value; } }

		public string ShortDescription { get { return _shortDescription; } set { _shortDescription = value; } }

		public string ImageFileName { get { return _imageFilename; } set { _imageFilename = value; } }

		public string ArticleContent { get { return _article_content; } set { _article_content = value; } }

		public string Author { get { return _author; } set { _author = value; } }
        public string MetaTitle { get { return _metaTitle; } set { _metaTitle = value; } }
        public string MetaKey { get { return _metaKey; } set { _metaKey = value; } }
        public string MetaDescription { get { return _metaDescription; } set { _metaDescription = value; } }
        public string FriendlyUrl { get { return _friendlyUrl; } set { _friendlyUrl = value; } }

		public string LastEdited { get { return _last_edited; } set { _last_edited = value; } }

		public DateTime PublishDate { get { return _publish_date; } set { _publish_date = value; } }

		public DateTime ExpiryDate { get { return _expiry_date; } set { _expiry_date = value; } }

		public NewsStatus BlogStatus { get { return _blog_status; } set { _blog_status = value; } }

		public BlogArticleTagList Tags { get { return _tags; } set { _tags = value; } }

		public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

		public bool IsOnRunning { get { return _isOnRunning; } set { _isOnRunning = value; } }

		public bool IsJunior { get { return _isJunior; } set { _isJunior = value; } }

		public bool AllowComments { get { return _allowComments; } set { _allowComments = value; } }

		#endregion public properties

		#region Object Initializer

		public BlogArticle() {
			BlogStatus = NewsStatus.Draft;
			Tags = new BlogArticleTagList();
			PublishDate = DateTime.Now;
			ExpiryDate = DateTime.Now.AddYears(1);
		}

		#endregion Object Initializer

		#region Static Method

		public static BlogArticle GetByID(int id, bool withChild, int liveOnly) {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			BlogArticle ba = new BlogArticle();

			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id),
									new SqlParam("@liveOnly", SqlDbType.Int,liveOnly)};

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_blog_article_by_id", paras);
			if (reader.ReturnCode == 0) {
				if (reader.Result.Read()) {
					ba.ID = Convert.ToInt32(reader.Result["id"].ToString());
					ba.Title = reader.Result["Title"].ToString();
					ba.ShortDescription = reader.Result["ShortDescription"].ToString();
					ba.ImageFileName = reader.Result["ImageFilename"].ToString();
					ba.ArticleContent = reader.Result["ArticleContent"].ToString();
					ba.Author = reader.Result["Author"].ToString();
                    ba.MetaTitle = reader.Result["MetaTitle"].ToString();
                    ba.MetaKey = reader.Result["MetaKey"].ToString();
                    ba.MetaDescription = reader.Result["MetaDescription"].ToString();
                    ba.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
					ba.LastEdited = reader.Result["LastEdited"].ToString();
					ba.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
					ba.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
					ba.BlogStatus = (NewsStatus)Convert.ToInt32(reader.Result["BlogStatus"]);
					ba.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
					ba.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
					ba.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());
					if (withChild) {
						ba.Tags = BlogArticleTagList.GetByArticleID(ba.ID);
					}
				} else {
					if (reader.Result.IsClosed == false) reader.Result.Close();
					ba.ErrorMessage = "Blog Article wasn't found in the database.";
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			} else {
				ba.ErrorMessage = reader.ErrorMessage;
			}
			return ba;
		}
        public static BlogArticle GetByFriendlyUrl(string friendlyUrl, bool withChild, int liveOnly)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            BlogArticle ba = new BlogArticle();

            SqlParam[] paras = { new SqlParam("@friendlyUrl", SqlDbType.NVarChar, friendlyUrl),
									new SqlParam("@liveOnly", SqlDbType.Int,liveOnly)};

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_blog_article_by_friendly_url", paras);
            if (reader.ReturnCode == 0)
            {
                if (reader.Result.Read())
                {
                    ba.ID = Convert.ToInt32(reader.Result["id"].ToString());
                    ba.Title = reader.Result["Title"].ToString();
                    ba.ShortDescription = reader.Result["ShortDescription"].ToString();
                    ba.ImageFileName = reader.Result["ImageFilename"].ToString();
                    ba.ArticleContent = reader.Result["ArticleContent"].ToString();
                    ba.Author = reader.Result["Author"].ToString();
                    ba.MetaTitle = reader.Result["MetaTitle"].ToString();
                    ba.MetaKey = reader.Result["MetaKey"].ToString();
                    ba.MetaDescription = reader.Result["MetaDescription"].ToString();
                    ba.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
                    ba.LastEdited = reader.Result["LastEdited"].ToString();
                    ba.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
                    ba.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
                    ba.BlogStatus = (NewsStatus)Convert.ToInt32(reader.Result["BlogStatus"]);
                    ba.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
                    ba.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
                    ba.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());
                    if (withChild)
                    {
                        ba.Tags = BlogArticleTagList.GetByArticleID(ba.ID);
                    }
                }
                else
                {
                    if (reader.Result.IsClosed == false) reader.Result.Close();
                    ba.ErrorMessage = "Blog Article wasn't found in the database.";
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }
            else
            {
                ba.ErrorMessage = reader.ErrorMessage;
            }
            return ba;
        }
		public static List<BlogArticle> GetBlogs(int limit, bool withChild, bool liveOnly) {
			int recordsetLimit;
			int liveFilter;
			if (limit > 0) {
				recordsetLimit = limit;
			} else {
				recordsetLimit = 100;
			}
			if (liveOnly) {
				liveFilter = 1;
			} else {
				liveFilter = 0;
			}
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			List<BlogArticle> list = new List<BlogArticle>();
			SqlParam[] paras = { new SqlParam("@limit", SqlDbType.Int, recordsetLimit),
							    new SqlParam("@liveOnly", SqlDbType.Int, liveFilter)};

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_blog_articles_with_limit", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					BlogArticle ba = new BlogArticle();
					ba.ID = Convert.ToInt32(reader.Result["id"].ToString());
					ba.Title = reader.Result["Title"].ToString();
					ba.ShortDescription = reader.Result["ShortDescription"].ToString();
					ba.ImageFileName = reader.Result["ImageFilename"].ToString();
					ba.ArticleContent = reader.Result["ArticleContent"].ToString();
					ba.Author = reader.Result["Author"].ToString();
                    ba.MetaTitle = reader.Result["MetaTitle"].ToString();
                    ba.MetaKey = reader.Result["MetaKey"].ToString();
                    ba.MetaDescription = reader.Result["MetaDescription"].ToString();
                    ba.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
					ba.LastEdited = reader.Result["LastEdited"].ToString();
					ba.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
					ba.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
					ba.BlogStatus = (NewsStatus)Convert.ToInt32(reader.Result["BlogStatus"]);
					ba.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
					ba.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
					ba.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());

					if (withChild) {
						ba.Tags = BlogArticleTagList.GetByArticleID(ba.ID);
					}

					list.Add(ba);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}

			return list;
		}

		public static BlogArticle GetLatestArticle() {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();

			BlogArticle ba = new BlogArticle();
			SqlParam[] paras = { };

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_blog_articles_latest", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					ba.ID = Convert.ToInt32(reader.Result["id"].ToString());
					ba.Title = reader.Result["Title"].ToString();
					ba.ShortDescription = reader.Result["ShortDescription"].ToString();
					ba.ImageFileName = reader.Result["ImageFilename"].ToString();
					ba.ArticleContent = reader.Result["ArticleContent"].ToString();
					ba.Author = reader.Result["Author"].ToString();
                    ba.MetaTitle = reader.Result["MetaTitle"].ToString();
                    ba.MetaKey = reader.Result["MetaKey"].ToString();
                    ba.MetaDescription = reader.Result["MetaDescription"].ToString();
                    ba.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
					ba.LastEdited = reader.Result["LastEdited"].ToString();
					ba.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
					ba.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
					ba.BlogStatus = (NewsStatus)Convert.ToInt32(reader.Result["BlogStatus"]);
					ba.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
					ba.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
					ba.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}

			return ba;
		}

		public static List<BlogArticle> GetByTag(int tagId, int limit) {
			int recordsetLimit;
			if (limit > 0) {
				recordsetLimit = limit;
			} else {
				recordsetLimit = -1;
			}
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			List<BlogArticle> list = new List<BlogArticle>();
			SqlParam[] paras = { new SqlParam("@tagId", SqlDbType.Int, tagId),
							   new SqlParam("@limit", SqlDbType.Int, recordsetLimit)};

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_blog_articles_by_tag_id", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					BlogArticle ba = new BlogArticle();
					ba.ID = Convert.ToInt32(reader.Result["id"].ToString());
					ba.Title = reader.Result["Title"].ToString();
					ba.ShortDescription = reader.Result["ShortDescription"].ToString();
					ba.ImageFileName = reader.Result["ImageFilename"].ToString();
					ba.ArticleContent = reader.Result["ArticleContent"].ToString();
					ba.Author = reader.Result["Author"].ToString();
                    ba.MetaTitle = reader.Result["MetaTitle"].ToString();
                    ba.MetaKey = reader.Result["MetaKey"].ToString();
                    ba.MetaDescription = reader.Result["MetaDescription"].ToString();
                    ba.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
					ba.LastEdited = reader.Result["LastEdited"].ToString();
					ba.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
					ba.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
					ba.BlogStatus = (NewsStatus)Convert.ToInt32(reader.Result["BlogStatus"]);
					ba.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
					ba.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
					ba.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());

					list.Add(ba);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}

			return list;
		}

		public static BlogArticle Save(BlogArticle obj) {
			obj.ErrorMessage = string.Empty;

			try {
				MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
				SqlParam[] paras = {  new SqlParam("@id", SqlDbType.Int, obj.ID),
                new SqlParam("@title", SqlDbType.VarChar,128, obj.Title),
                new SqlParam("@shortDescription", SqlDbType.VarChar,256, obj.ShortDescription),
                new SqlParam("@imageFilename", SqlDbType.VarChar,128, obj.ImageFileName),
                new SqlParam("@articleContent", SqlDbType.Text, obj.ArticleContent),
				new SqlParam("@author", SqlDbType.VarChar,50, obj.Author),
                new SqlParam("@metaTitle", SqlDbType.NVarChar,128, obj.MetaTitle),
                new SqlParam("@metaKey", SqlDbType.NVarChar,250, obj.MetaKey),
                new SqlParam("@metaDescription", SqlDbType.NVarChar,250, obj.MetaDescription),
                new SqlParam("@friendlyUrl", SqlDbType.NVarChar,128, obj.FriendlyUrl),
				new SqlParam("@lastEdited", SqlDbType.VarChar,128, obj.LastEdited),
				new SqlParam("@publishDate", SqlDbType.DateTime, obj.PublishDate),
				new SqlParam("@expiryDate", SqlDbType.DateTime, obj.ExpiryDate),
				new SqlParam("@blogStatus", SqlDbType.SmallInt, obj.BlogStatus),
								   new SqlParam("@isOnRunning", SqlDbType.Bit, obj.IsOnRunning),
								   new SqlParam("@isJunior", SqlDbType.Bit, obj.IsJunior),
								   new SqlParam("@allowComments", SqlDbType.Bit, obj.AllowComments) };
				Int32Response result = connection.RunInt32Command("gr_save_blog_article", paras);
				if (result.ReturnCode >= 0) {
					BlogArticle response = BlogArticle.GetByID(result.ReturnCode, false, 0);
					if (response != null && response.ID > 0 && obj.Tags.Count > 0) {
						/*foreach (BlogArticleTag tag in obj.Tags) {
							HttpContext.Current.Response.Write("TAG " + tag.Name);
						}*/

						//obj.Categories.ArticleId = obj.ID;
						obj.Tags.Save(obj.Tags);
					}
					return BlogArticle.GetByID(result.ReturnCode, true, 0);
				} else {
					obj.ErrorMessage = "Error: " + result.ErrorMessage;
					return obj;
				}
			} catch (Exception ex) {
				obj.ErrorMessage = ex.Message;
				return obj;
			}
		}

		public static bool Delete(int id) {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
			Int32Response iRes = connection.RunInt32Command("gr_delete_blog_article", paras);
			if (iRes.ReturnCode == 0)
				return true;
			else
				return false;
		}

		#endregion Static Method
	}
}