﻿using System;
using System.Collections.Generic;
using System.Web;
using ThirteenStrides.Database;
using System.Data;

/// <summary>
/// Summary description for BusinessChallenge 
/// </summary>
/// 
namespace Gr.Model
{
    public enum BcDisplayMode
    {
        Default,
        Listing,
        Results
    }
    public class BusinessChallenge
    {
        #region private properties
        private int _id;
        private string _name;
        private string _description;
        private int _eventID;
        private int _eventYear;
        private bool _linkActive;
        private bool _isOpen;
        private BcDisplayMode _displayMode;
        private int _termsID;
        private string _formDescription;
        private string _steamID;
        private string _errorMessage;

        #endregion private properties
        #region Public Properties
        public int ID { get { return _id; } set { _id = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        public string Description { get { return _description; } set { _description = value; } }
        public int EventID { get { return _eventID; } set { _eventID = value; } }
        public int EventYear { get { return _eventYear; } set { _eventYear = value; } }
        public bool LinkActive { get { return _linkActive; } set { _linkActive = value; } }
        public bool IsOpen { get { return _isOpen; } set { _isOpen = value; } }
        public BcDisplayMode DisplayMode { get { return _displayMode; } set { _displayMode = value; } }
        public int TermsID { get { return _termsID; } set { _termsID = value; } }
        public string FormDescription { get { return _formDescription; } set { _formDescription = value; } }
        public string StreamID { get { return _steamID; } set { _steamID = value; } }
        public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

        #endregion Public Properties

        #region Object Initializer
        public BusinessChallenge() { }
        #endregion

        #region Static Method
        public static BusinessChallenge GetByID(int id)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            BusinessChallenge bc = new BusinessChallenge();
            SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_business_challenge_by_id", paras);
            if (reader.ReturnCode == 0)
            {
                if (reader.Result.Read())
                {
                    bc.ID = Convert.ToInt32(reader.Result["ID"].ToString());
                    bc.Name = reader.Result["Name"].ToString();
                    bc.Description = reader.Result["Description"].ToString();
                    bc.EventID = Convert.ToInt32(reader.Result["EventID"].ToString());
                    bc.EventYear = Convert.ToInt32(reader.Result["EventYear"].ToString());
                    bc.LinkActive = Convert.ToBoolean(reader.Result["LinkActive"].ToString());
                    bc.IsOpen = Convert.ToBoolean(reader.Result["IsOpen"].ToString());
                    bc.DisplayMode = (BcDisplayMode)Enum.Parse(typeof(BcDisplayMode), reader.Result["DisplayMode"].ToString(), true);
                    if (reader.Result["TermsID"] != DBNull.Value)
                        bc.TermsID = Convert.ToInt32(reader.Result["TermsID"].ToString());
                    bc.FormDescription = reader.Result["FormDescription"].ToString();
                    bc.StreamID = reader.Result["StreamID"].ToString();
                }
                else
                {
                    if (reader.Result.IsClosed == false) reader.Result.Close();
                    bc.ErrorMessage = "Business challenge wasn't found in the database.";
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }
            else
            {
                bc.ErrorMessage = reader.ErrorMessage;
            }
            return bc;
        }
        public static List<BusinessChallenge> GetByEventID(int eventID)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            List<BusinessChallenge> list = new List<BusinessChallenge>();
            SqlParam[] paras = { new SqlParam("@eventID", SqlDbType.Int, eventID) };

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_business_challenge_by_event_id", paras);
            if (reader.ReturnCode == 0)
            {
                while (reader.Result.Read())
                {
                    BusinessChallenge obj = new BusinessChallenge();
                    obj.ID = Convert.ToInt32(reader.Result["ID"].ToString());
                    obj.Name = reader.Result["Name"].ToString();
                    obj.Description = reader.Result["Description"].ToString();
                    obj.EventID = Convert.ToInt32(reader.Result["EventID"].ToString());
                    obj.EventYear = Convert.ToInt32(reader.Result["EventYear"].ToString());
                    obj.LinkActive = Convert.ToBoolean(reader.Result["LinkActive"].ToString());
                    obj.IsOpen = Convert.ToBoolean(reader.Result["IsOpen"].ToString());
                    obj.DisplayMode = (BcDisplayMode)Enum.Parse(typeof(BcDisplayMode), reader.Result["DisplayMode"].ToString(), true);
                    if (reader.Result["TermsID"] != DBNull.Value)
                        obj.TermsID = Convert.ToInt32(reader.Result["TermsID"].ToString());
                    obj.FormDescription = reader.Result["FormDescription"].ToString();
                    obj.StreamID = reader.Result["StreamID"].ToString();
                    list.Add(obj);
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }

            return list;
        }
        public static BusinessChallenge GetActiveByEventID(int eventID)
        {
            BusinessChallenge obj = new BusinessChallenge();
            List<BusinessChallenge> list = GetByEventID(eventID);
            if (list != null && list.Count > 0)
            {
                foreach (BusinessChallenge bc in list)
                {
                    if (bc.LinkActive)
                    {
                        obj = bc;
                        break;
                    }
                }
            }
            return obj;
        }
        public static List<BusinessChallenge> GetAll()
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            List<BusinessChallenge> list = new List<BusinessChallenge>();
            SqlParam[] paras = { };

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_business_challenge_all", paras);
            if (reader.ReturnCode == 0)
            {
                while (reader.Result.Read())
                {
                    BusinessChallenge obj = new BusinessChallenge();
                    obj.ID = Convert.ToInt32(reader.Result["ID"].ToString());
                    obj.Name = reader.Result["Name"].ToString();
                    obj.Description = reader.Result["Description"].ToString();
                    obj.EventID = Convert.ToInt32(reader.Result["EventID"].ToString());
                    obj.EventYear = Convert.ToInt32(reader.Result["EventYear"].ToString());
                    obj.LinkActive = Convert.ToBoolean(reader.Result["LinkActive"].ToString());
                    obj.IsOpen = Convert.ToBoolean(reader.Result["IsOpen"].ToString());
                    obj.DisplayMode = (BcDisplayMode)Enum.Parse(typeof(BcDisplayMode), reader.Result["DisplayMode"].ToString(), true);
                    if (reader.Result["TermsID"] != DBNull.Value)
                        obj.TermsID = Convert.ToInt32(reader.Result["TermsID"].ToString());
                    obj.FormDescription = reader.Result["FormDescription"].ToString();
                    obj.StreamID = reader.Result["StreamID"].ToString();
                    list.Add(obj);
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }

            return list;
        }
        public static BusinessChallenge Save(BusinessChallenge obj)
        {
            obj.ErrorMessage = string.Empty;

            try
            {
                object termsID = DBNull.Value;
                object description = DBNull.Value;
                object formDescription = DBNull.Value;
                object streamID = DBNull.Value;
                if (obj.TermsID > 0)
                    termsID = obj.TermsID;
                if (!string.IsNullOrEmpty(obj.Description))
                    description = obj.Description;
                if (!string.IsNullOrEmpty(obj.FormDescription))
                    formDescription = obj.FormDescription;
                if (!string.IsNullOrEmpty(obj.StreamID))
                    streamID = obj.StreamID;
                MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
                SqlParam[] paras = {  new SqlParam("@id", SqlDbType.Int, obj.ID),
                new SqlParam("@name", SqlDbType.NVarChar, obj.Name),
                new SqlParam("@description", SqlDbType.NVarChar, description),
                new SqlParam("@eventID", SqlDbType.Int, obj.EventID),
                new SqlParam("@eventYear", SqlDbType.Int, obj.EventYear),
                new SqlParam("@linkActive", SqlDbType.Bit, obj.LinkActive),
                new SqlParam("@isOpen", SqlDbType.Bit, obj.IsOpen),
                new SqlParam("@displayMode", SqlDbType.NVarChar, obj.DisplayMode.ToString()),
                new SqlParam("@termsID", SqlDbType.Int, termsID), 
                new SqlParam("@formDescription", SqlDbType.NVarChar, formDescription),
                new SqlParam("@streamID", SqlDbType.NVarChar, streamID)};
                Int32Response result = connection.RunInt32Command("gr_save_business_challenge", paras);
                if (result.ReturnCode >= 0)
                {
                    return BusinessChallenge.GetByID(result.ReturnCode);
                }
                else
                {
                    obj.ErrorMessage = "Error: " + result.ErrorMessage;
                    return obj;
                }
            }
            catch (Exception ex)
            {
                obj.ErrorMessage = ex.Message;
                return obj;
            }
        }
        public static bool Delete(int id)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
            Int32Response iRes = connection.RunInt32Command("gr_delete_business_challenge", paras);
            if (iRes.ReturnCode == 0)
                return true;
            else
                return false;

        }
        #endregion Static Method

    }
}