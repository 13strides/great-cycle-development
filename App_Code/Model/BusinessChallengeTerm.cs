﻿using System;
using System.Collections.Generic;
using System.Web;
using ThirteenStrides.Database;
using System.Data;

/// <summary>
/// Summary description for BusinessChallengeTerm
/// </summary>
/// 
namespace Gr.Model
{
    public class BusinessChallengeTerm
    {
        #region private properties
        private int _id;
        private string _name;
        private string _title;
        private string _body;
        private string _footer;
        private string _errorMessage;

        #endregion private properties
        #region Public Properties
        public int ID { get { return _id; } set { _id = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        public string Title { get { return _title; } set { _title = value; } }
        public string Body { get { return _body; } set { _body = value; } }
        public string Footer {get {return _footer;} set {_footer = value;}}
        public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

        #endregion Public Properties

        #region Object Initializer
        public BusinessChallengeTerm() { }
        #endregion

        #region Static Method
        public static BusinessChallengeTerm GetByID(int id)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            BusinessChallengeTerm term = new BusinessChallengeTerm();
            SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_business_challenge_term_by_id", paras);
            if (reader.ReturnCode == 0)
            {
                if (reader.Result.Read())
                {
                    term.ID = Convert.ToInt32(reader.Result["ID"].ToString());
                    term.Name = reader.Result["Name"].ToString();
                    term.Title = reader.Result["Title"].ToString();
                    term.Body = reader.Result["Body"].ToString();
                    term.Footer = reader.Result["Footer"].ToString();
                }
                else
                {
                    if (reader.Result.IsClosed == false) reader.Result.Close();
                    term.ErrorMessage = "Business challenge term wasn't found in the database.";
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }
            else
            {
                term.ErrorMessage = reader.ErrorMessage;
            }
            return term;
        }
        public static List<BusinessChallengeTerm> GetAll()
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            List<BusinessChallengeTerm> list = new List<BusinessChallengeTerm>();
            SqlParam[] paras = { };

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_business_challenge_term_all", paras);
            if (reader.ReturnCode == 0)
            {
                while (reader.Result.Read())
                {
                    BusinessChallengeTerm term = new BusinessChallengeTerm();
                    term.ID = Convert.ToInt32(reader.Result["ID"].ToString());
                    term.Name = reader.Result["Name"].ToString();
                    term.Title = reader.Result["Title"].ToString();
                    term.Body = reader.Result["Body"].ToString();
                    term.Footer = reader.Result["Footer"].ToString();

                    list.Add(term);
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }

            return list;
        }  
        public static BusinessChallengeTerm Save(BusinessChallengeTerm obj)
        {
            obj.ErrorMessage = string.Empty;
            
            try
            {
                object footer = DBNull.Value;
                if (!string.IsNullOrEmpty(obj.Footer))
                    footer = obj.Footer;
                MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
                SqlParam[] paras = {  new SqlParam("@id", SqlDbType.Int, obj.ID),
                new SqlParam("@name", SqlDbType.NVarChar, obj.Name),
                new SqlParam("@title", SqlDbType.NVarChar, obj.Title),
                new SqlParam("@body", SqlDbType.NVarChar, obj.Body),
                new SqlParam("@footer", SqlDbType.NVarChar, footer) };
                Int32Response result = connection.RunInt32Command("gr_save_business_challenge_term", paras);
                if (result.ReturnCode >= 0)
                {
                    return BusinessChallengeTerm.GetByID(result.ReturnCode);
                }
                else
                {
                    obj.ErrorMessage = "Error: " + result.ErrorMessage;
                    return obj;
                }
            }
            catch (Exception ex)
            {
                obj.ErrorMessage = ex.Message;
                return obj;
            }
        }
        public static bool Delete(int id)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
            Int32Response iRes = connection.RunInt32Command("gr_delete_business_challenge_term", paras);
            if (iRes.ReturnCode == 0)
                return true;
            else
                return false;

        }
        #endregion Static Method

    }
}