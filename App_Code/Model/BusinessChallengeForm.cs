﻿using System;
using System.Collections.Generic;
using System.Web;
using ThirteenStrides.Database;
using System.Data;

/// <summary>
/// Summary description for BusinessChallengeForm
/// </summary>
/// 
namespace Gr.Model
{
   
    public enum BcBusinessSize
    {
        Default
    }
    public class BusinessChallengeForm
    {
        #region private properties
        private int _id;
        private string _businessName;
        private string _businessAddress1;
        private string _businessAddress2;
        private string _businessCity;
        private int _businessCountryID;
        private string _businessPostcode;
        private string _businessTelephone;
        private BcBusinessSize _businessSize;
        private string _businessChallengeDistance;
        private int _numberOfEntrants;
        private string _coordinatorName;
        private string _coordinatorTelephone;
        private string _coordinatorEmail;
        private int _businessChallengeID;
        private bool _isEmailed;
        private string _errorMessage;

        #endregion private properties
        #region Public Properties
        public int ID {get {return _id;} set{_id = value;}}
        public string BusinessName {get {return _businessName;} set{_businessName = value;}}
        public string BusinessAddress1 {get {return _businessAddress1;} set{_businessAddress1 = value;}}
        public string BusinessAddress2 {get {return _businessAddress2;} set{_businessAddress2 = value;}}
        public string BusinessCity {get {return _businessCity;} set{_businessCity = value;}}
        public int BusinessCountryID {get {return _businessCountryID;} set{_businessCountryID = value;}}
        public string BusinessPostcode {get {return _businessPostcode;} set{_businessPostcode = value;}}
        public string BusinessTelephone {get {return _businessTelephone;} set{_businessTelephone = value;}}
        public BcBusinessSize BusinessSize {get {return _businessSize;} set{_businessSize = value;}}
        public string BusinessChallengeDistance { get { return _businessChallengeDistance; } set { _businessChallengeDistance = value; } }
        public int NumberOfEntrants { get { return _numberOfEntrants; } set { _numberOfEntrants = value; } }
        public string CoordinatorName {get {return _coordinatorName;} set{_coordinatorName = value;}}
        public string CoordinatorTelephone {get {return _coordinatorTelephone;} set{_coordinatorTelephone = value;}}
        public string CoordinatorEmail {get {return _coordinatorEmail;} set{_coordinatorEmail = value;}}
        public int BusinessChallengeID {get {return _businessChallengeID;} set{_businessChallengeID = value;}}
        public bool IsEmailed {get {return _isEmailed;} set{_isEmailed = value;}}

        public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

        #endregion Public Properties

        #region Object Initializer
        public BusinessChallengeForm() { }
        #endregion

        #region Static Method
        public static BusinessChallengeForm GetByID(int id)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            BusinessChallengeForm bcf = new BusinessChallengeForm();
            SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_business_challenge_entry_by_id", paras);
            if (reader.ReturnCode == 0)
            {
                if (reader.Result.Read())
                {
                      bcf.ID = Convert.ToInt32(reader.Result["ID"].ToString());
                      bcf.BusinessName = reader.Result["BusinessName"].ToString();
                      bcf.BusinessAddress1 = reader.Result["BusinessAddress1"].ToString();
                      if (reader.Result["BusinessAddress2"] != DBNull.Value)
                        bcf.BusinessAddress2 = reader.Result["BusinessAddress2"].ToString();
                      bcf.BusinessCity = reader.Result["BusinessCity"].ToString();
                      bcf.BusinessCountryID = Convert.ToInt32(reader.Result["BusinessCountryID"].ToString());
                      if (reader.Result["BusinessPostcode"] != DBNull.Value)
                      bcf.BusinessPostcode = reader.Result["BusinessPostcode"].ToString();
                      bcf.BusinessTelephone = reader.Result["BusinessTelephone"].ToString();
                      bcf.BusinessSize = (BcBusinessSize)Enum.Parse(typeof(BcBusinessSize), reader.Result["BusinessSize"].ToString(), true);
                      bcf._businessChallengeDistance = reader.Result["BusinessChallengeDistance"].ToString();
                      bcf.NumberOfEntrants = Convert.ToInt32(reader.Result["NumberOfEntrants"].ToString());
                      bcf.CoordinatorName = reader.Result["CoordinatorName"].ToString();
                      bcf.CoordinatorTelephone = reader.Result["CoordinatorTelephone"].ToString();
                      bcf.CoordinatorEmail = reader.Result["CoordinatorEmail"].ToString();
                      bcf.BusinessChallengeID = Convert.ToInt32(reader.Result["BusinessChallengeID"].ToString());
                      bcf.IsEmailed = Convert.ToBoolean(reader.Result["IsEmailed"].ToString());

                }
                else
                {
                    if (reader.Result.IsClosed == false) reader.Result.Close();
                    bcf.ErrorMessage = "Business challenge form wasn't found in the database.";
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }
            else
            {
                bcf.ErrorMessage = reader.ErrorMessage;
            }
            return bcf;
        }
        public static List<BusinessChallengeForm> GetByBusinessChallengeID(int businessChallengeID)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            List<BusinessChallengeForm> list = new List<BusinessChallengeForm>();
            SqlParam[] paras = { new SqlParam("@businessChallengeID", SqlDbType.Int, businessChallengeID) };

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_business_challenge_entry_by_challenge_id", paras);
               if (reader.ReturnCode == 0)
                {
                    while (reader.Result.Read())
                    {
                        BusinessChallengeForm bcf = new BusinessChallengeForm();
                        bcf.ID = Convert.ToInt32(reader.Result["ID"].ToString());
                        bcf.BusinessName = reader.Result["BusinessName"].ToString();
                        bcf.BusinessAddress1 = reader.Result["BusinessAddress1"].ToString();
                        if (reader.Result["BusinessAddress2"] != DBNull.Value)
                            bcf.BusinessAddress2 = reader.Result["BusinessAddress2"].ToString();
                        bcf.BusinessCity = reader.Result["BusinessCity"].ToString();
                        bcf.BusinessCountryID = Convert.ToInt32(reader.Result["BusinessCountryID"].ToString());
                        if (reader.Result["BusinessPostcode"] != DBNull.Value)
                            bcf.BusinessPostcode = reader.Result["BusinessPostcode"].ToString();
                        bcf.BusinessTelephone = reader.Result["BusinessTelephone"].ToString();
                        bcf.BusinessSize = (BcBusinessSize)Enum.Parse(typeof(BcBusinessSize), reader.Result["BusinessSize"].ToString(), true);
                        bcf._businessChallengeDistance = reader.Result["BusinessChallengeDistance"].ToString();
                        bcf.NumberOfEntrants = Convert.ToInt32(reader.Result["NumberOfEntrants"].ToString());
                        bcf.CoordinatorName = reader.Result["CoordinatorName"].ToString();
                        bcf.CoordinatorTelephone = reader.Result["CoordinatorTelephone"].ToString();
                        bcf.CoordinatorEmail = reader.Result["CoordinatorEmail"].ToString();
                        bcf.BusinessChallengeID = Convert.ToInt32(reader.Result["BusinessChallengeID"].ToString());
                        bcf.IsEmailed = Convert.ToBoolean(reader.Result["IsEmailed"].ToString());
                        list.Add(bcf);
                    }
                    if (reader.Result.IsClosed == false) reader.Result.Close();
                }

               return list;
        }
        public static BusinessChallengeForm Save(BusinessChallengeForm obj)
        {
            obj.ErrorMessage = string.Empty;
            
            try
            {
                object address2 = DBNull.Value;
                object postcode = DBNull.Value;
                if (!string.IsNullOrEmpty(obj.BusinessAddress2))
                    address2 = obj.BusinessAddress2;
                if (!string.IsNullOrEmpty(obj.BusinessPostcode))
                    postcode = obj.BusinessPostcode;
                MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
                SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, obj.ID),
                new SqlParam("@businessName", SqlDbType.NVarChar, obj.BusinessName),
                new SqlParam("@businessAddress1", SqlDbType.NVarChar, obj.BusinessAddress1),
                new SqlParam("@businessAddress2", SqlDbType.NVarChar, address2),
                new SqlParam("@businessCity", SqlDbType.NVarChar, obj.BusinessCity),
                new SqlParam("@businessCountryID", SqlDbType.Int, obj.BusinessCountryID),
                new SqlParam("@businessPostcode", SqlDbType.NVarChar, postcode),
                new SqlParam("@businessTelephone", SqlDbType.NVarChar, obj.BusinessTelephone),
                new SqlParam("@businessSize", SqlDbType.NVarChar, obj.BusinessSize.ToString()),
                new SqlParam("@businessChallengeDistance", SqlDbType.NVarChar, obj.BusinessChallengeDistance),
                new SqlParam("@numberOfEntrants", SqlDbType.Int, obj.NumberOfEntrants),
                new SqlParam("@coordinatorName", SqlDbType.NVarChar, obj.CoordinatorName),
                new SqlParam("@coordinatorTelephone", SqlDbType.NVarChar, obj.CoordinatorTelephone),
                new SqlParam("@coordinatorEmail", SqlDbType.NVarChar, obj.CoordinatorEmail),
                new SqlParam("@businessChallengeID", SqlDbType.Int, obj.BusinessChallengeID),
                new SqlParam("@isEmailed", SqlDbType.Bit, obj.IsEmailed) };
                Int32Response result = connection.RunInt32Command("gr_save_business_challenge_entry", paras);
                if (result.ReturnCode >= 0)
                {
                    return BusinessChallengeForm.GetByID(result.ReturnCode);
                }
                else
                {
                    obj.ErrorMessage = "Error: " + result.ErrorMessage;
                    return obj;
                }
            }
            catch (Exception ex)
            {
                obj.ErrorMessage = ex.Message;
                return obj;
            }
        }
        public static bool Delete(int id)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
            Int32Response iRes = connection.RunInt32Command("gr_delete_business_challenge_entry", paras);
            if (iRes.ReturnCode == 0)
                return true;
            else
                return false;

        }
        #endregion Static Method

    }
}