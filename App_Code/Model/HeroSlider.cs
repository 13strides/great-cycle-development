﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using GS.Generic.Web;
using ThirteenStrides.Database;

/// <summary>
/// Summary description for HeroSlider
/// </summary>

namespace Gr.Model {

	public class HeroSlider {
		#region private properties

		private int _id;
		private string _image;
		private string _message;
		private string _link_text;
		private string _link_url;
		private DateTime _pub_date;
		private int _priority;
		private NewsStatus _banner_status;
		private string _errorMessage;

		#endregion private properties

		#region public properties

		public int ID { get { return _id; } set { _id = value; } }

		public string Image { get { return _image; } set { _image = value; } }

		public string Message { get { return _message; } set { _message = value; } }

		public string Link_text { get { return _link_text; } set { _link_text = value; } }

		public string Link_url { get { return _link_url; } set { _link_url = value; } }

		public DateTime Pub_date { get { return _pub_date; } set { _pub_date = value; } }

		public int Priority { get { return _priority; } set { _priority = value; } }

		public NewsStatus Banner_status { get { return _banner_status; } set { _banner_status = value; } }

		public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

		#endregion public properties

		#region Object Initializer

		public HeroSlider() {
			//
			// TODO: Add constructor logic here
			//
			Banner_status = NewsStatus.Draft;
			Pub_date = DateTime.Now;
		}

		#endregion Object Initializer

		#region Static Method

		public static HeroSlider GetByID(int id, int liveOnly) {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();

			HeroSlider hs = new HeroSlider();

			SqlParam[] paras = {new SqlParam("@id", SqlDbType.Int, id),
								new SqlParam("@liveOnly", SqlDbType.Int, liveOnly)};

			SqlDataReaderResponse reader = connection.RunCommand("ni_get_hero_slider_by_id", paras);
			if (reader.ReturnCode == 0) {
				if (reader.Result.Read()) {
					hs.ID = Convert.ToInt32(reader.Result["id"].ToString());
					hs.Image = reader.Result["image"].ToString();
					hs.Message = reader.Result["message"].ToString();
					hs.Link_text = reader.Result["link_text"].ToString();
					hs.Link_url = reader.Result["link_url"].ToString();
					hs.Pub_date = Convert.ToDateTime(reader.Result["pub_date"].ToString());
					hs.Priority = Convert.ToInt32(reader.Result["priority"].ToString());
					hs.Banner_status = (NewsStatus)Convert.ToInt32(reader.Result["banner_status"]);
				} else {
					if (reader.Result.IsClosed == false) reader.Result.Close();
					hs.ErrorMessage = "Blog Article wasn't found in the database.";
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			} else {
				hs.ErrorMessage = reader.ErrorMessage;
			}
			return hs;
		}

		public static List<HeroSlider> GetSlider(int limit, bool liveOnly) {
			int recordsetLimit;
			int liveFilter;
			if (limit > 0) {
				recordsetLimit = limit;
			} else {
				recordsetLimit = 100;
			}
			if (liveOnly) {
				liveFilter = 1;
			} else {
				liveFilter = 0;
			}
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			List<HeroSlider> list = new List<HeroSlider>();
			SqlParam[] paras = { new SqlParam("@limit", SqlDbType.Int, recordsetLimit),
							    new SqlParam("@liveOnly", SqlDbType.Int, liveFilter)};

			SqlDataReaderResponse reader = connection.RunCommand("ni_get_hero_slider", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					HeroSlider hs = new HeroSlider();
					hs.ID = Convert.ToInt32(reader.Result["id"].ToString());
					hs.Image = reader.Result["image"].ToString();
					hs.Message = reader.Result["message"].ToString();
					hs.Link_text = reader.Result["link_text"].ToString();
					hs.Link_url = reader.Result["link_url"].ToString();
					hs.Pub_date = Convert.ToDateTime(reader.Result["pub_date"].ToString());
					hs.Priority = Convert.ToInt32(reader.Result["priority"].ToString());
					hs.Banner_status = (NewsStatus)Convert.ToInt32(reader.Result["banner_status"]);

					list.Add(hs);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}

			return list;
		}

		public static HeroSlider Save(HeroSlider obj) {
			obj.ErrorMessage = string.Empty;

			try {
				MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
				SqlParam[] paras = {  new SqlParam("@id", SqlDbType.Int, obj.ID),
                new SqlParam("@image", SqlDbType.VarChar,128, obj.Image),
                new SqlParam("@message", SqlDbType.VarChar,512, obj.Message),
                new SqlParam("@link_text", SqlDbType.VarChar,128, obj.Link_text),
                new SqlParam("@link_url", SqlDbType.VarChar, 128, obj.Link_url),
				new SqlParam("@pub_date", SqlDbType.DateTime, obj.Pub_date),
				new SqlParam("@priority", SqlDbType.Int, obj.Priority),
				new SqlParam("@banner_status", SqlDbType.SmallInt, obj.Banner_status)};

				Int32Response result = connection.RunInt32Command("ni_save_hero_slider", paras);
				if (result.ReturnCode >= 0) {
					HeroSlider response = HeroSlider.GetByID(result.ReturnCode, 0);

					return HeroSlider.GetByID(result.ReturnCode, 0);
				} else {
					obj.ErrorMessage = "Error: " + result.ErrorMessage;
					return obj;
				}
			} catch (Exception ex) {
				obj.ErrorMessage = ex.Message;
				return obj;
			}
		}

		public static bool Delete(int id) {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
			Int32Response iRes = connection.RunInt32Command("ni_delete_hero_slider", paras);
			if (iRes.ReturnCode == 0)
				return true;
			else
				return false;
		}

		#endregion Static Method
	}
}