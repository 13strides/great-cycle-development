﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using ThirteenStrides.Database;

/// <summary>
/// Summary description for BlogArticleTagList
/// </summary>

namespace Gr.Model {

	public class BlogArticleTagList : CollectionBase {
		#region private properties

		int _articleId;

		#endregion private properties

		#region public properties

		public int ArticleId { get { return _articleId; } set { _articleId = value; } }

		#endregion public properties

		#region object initialiser

		public BlogArticleTagList() {
			//
			// TODO: Add constructor logic here
			//
		}

		#endregion object initialiser

		#region static methods

		public static BlogArticleTagList GetByArticleID(int articleId) {
			//_id = articleId;

			BlogArticleTagList list = new BlogArticleTagList();
			list.ArticleId = articleId;
			BlogArticleTag blogTag = new BlogArticleTag();
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@articleId", SqlDbType.Int, list.ArticleId) };

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_blog_tag_list_by_article_by_id", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					blogTag = new BlogArticleTag();
					blogTag.ID = Convert.ToInt32(reader.Result["id"]);
					blogTag.Name = reader.Result["Name"].ToString();
					blogTag.IsTopLevel = Convert.ToBoolean(reader.Result["isTopLevel"]);
					blogTag.IsActive = Convert.ToBoolean(reader.Result["isActive"].ToString());
					blogTag.IsVisible = Convert.ToBoolean(reader.Result["isVisible"].ToString());

					list.InnerList.Add(blogTag);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			} else {
				//error
			}

			return list;
		}

		public void Add(BlogArticleTag tag) {
			this.InnerList.Add(tag);
		}

		public void Remove(BlogArticleTag tag) {
			this.InnerList.Remove(tag);
		}

		public bool Contains(int tagId) {
			foreach (BlogArticleTag _tag in this.InnerList) {
				if (tagId == _tag.ID) {
					return true;
				}
			}
			return false;
		}

		public void Save(BlogArticleTagList obj) {
			//HttpContext.Current.Response.Write("HELLO " + obj.ArticleId + " " + obj.InnerList.Count);
			if (DeleteAllBlogTagsFromDB(obj.ArticleId)) {
				HttpContext.Current.Response.Write("- Deleted " + obj.InnerList.Count);
				HttpContext.Current.Response.Write("<br />" + obj.ArticleId.ToString() + "<br />");
				MsSqlConnection connection = DataAccess.DbConnection.GetConnection();

				foreach (BlogArticleTag tag in obj.InnerList) {
					SqlParam[] paras = { new SqlParam("@articleId", SqlDbType.Int, obj.ArticleId),
					new SqlParam("@tagName", SqlDbType.NVarChar,128, tag.Name)};

					//HttpContext.Current.Response.Write("cat =" + cat.ID + " artID = " + obj.ArticleId);

					Int32Response result = connection.RunInt32Command("gr_save_blog_article_tags", paras);
					/*if (result.ReturnCode >= 0) {
						return BusinessChallenge.GetByID(result.ReturnCode);
					} else {
						obj.ErrorMessage = "Error: " + result.ErrorMessage;
						return obj;
					}*/
					//HttpContext.Current.Response.Write(result.ErrorMessage);
					//HttpContext.Current.Response.Write("<br />T: " + tag.Name + " A: " + obj.ArticleId.ToString() + "<br />");
				}
			} else {
				//success = false;
			}

			//return success;
			return;
		}

		private bool DeleteAllBlogTagsFromDB(int delArtID) {
			/*MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, delArtID) };
			SqlDataReaderResponse reader = connection.RunCommand("gr_delete_all_news_categories", paras);

			HttpContext.Current.Response.Write("- Deleted " + reader.ReturnCode);*/
			bool deleted = true;
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, delArtID) };
			Int32Response iRes = connection.RunInt32Command("gr_delete_blog_tags_all", paras);
			if (iRes.ReturnCode >= 0)
				deleted = true;
			else
				deleted = false;
			return deleted;
			//return true; // (reader.ReturnCode >= 0);
		}

		#endregion static methods
	}
}