﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using GS.Generic.Web;
using ThirteenStrides.Database;

/// <summary>
/// Summary description for NewsArticle
/// </summary>

namespace Gr.Model {

	public class NewsArticle {
		#region private properties

		private int _id;
		private string _title;
		private string _shortDescription;
		private string _imageFilename;
		private string _article_content;
		private string _author;
        private string _metaTitle;
        private string _metaKey;
        private string _metaDescription;
        private string _friendlyUrl;
		private string _last_edited;
		private DateTime _publish_date;
		private DateTime _expiry_date;
		private NewsStatus _news_status;

		private bool _isOnRunning;
		private bool _isJunior;
		private bool _allowComments;

		private NewsArticleCategoryList _categories;
		private string _errorMessage;

		#endregion private properties

		#region public properties

		public int ID { get { return _id; } set { _id = value; } }

		public string Title { get { return _title; } set { _title = value; } }

		public string ShortDescription { get { return _shortDescription; } set { _shortDescription = value; } }

		public string ImageFileName { get { return _imageFilename; } set { _imageFilename = value; } }

		public string ArticleContent { get { return _article_content; } set { _article_content = value; } }

		public string Author { get { return _author; } set { _author = value; } }
        public string MetaTitle { get { return _metaTitle; } set { _metaTitle = value;} }
        public string MetaKey { get { return _metaKey;} set { _metaKey = value;} }
        public string MetaDescription { get { return _metaDescription;} set { _metaDescription = value;} }
        public string FriendlyUrl { get { return _friendlyUrl;} set { _friendlyUrl = value;} }
		public string LastEdited { get { return _last_edited; } set { _last_edited = value; } }

		public DateTime PublishDate { get { return _publish_date; } set { _publish_date = value; } }

		public DateTime ExpiryDate { get { return _expiry_date; } set { _expiry_date = value; } }

		public NewsStatus NewsStatus { get { return _news_status; } set { _news_status = value; } }

		public NewsArticleCategoryList Categories { get { return _categories; } set { _categories = value; } }

		public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

		public bool IsOnRunning { get { return _isOnRunning; } set { _isOnRunning = value; } }

		public bool IsJunior { get { return _isJunior; } set { _isJunior = value; } }

		public bool AllowComments { get { return _allowComments; } set { _allowComments = value; } }

		#endregion public properties

		#region Object Initializer

		public NewsArticle() {
			NewsStatus = NewsStatus.Draft;
			Categories = new NewsArticleCategoryList();
			PublishDate = DateTime.Now;
			ExpiryDate = DateTime.Now.AddYears(1);
		}

		#endregion Object Initializer

		#region Static Method

		public static NewsArticle GetByID(int id, bool withChild, int liveOnly) {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			NewsArticle na = new NewsArticle();

			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id),
									new SqlParam("@liveOnly", SqlDbType.Int,liveOnly)};

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_news_article_by_id", paras);
			if (reader.ReturnCode == 0) {
				if (reader.Result.Read()) {
					na.ID = Convert.ToInt32(reader.Result["id"].ToString());
					na.Title = reader.Result["Title"].ToString();
					na.ShortDescription = reader.Result["ShortDescription"].ToString();
					na.ImageFileName = reader.Result["ImageFilename"].ToString();
					na.ArticleContent = reader.Result["ArticleContent"].ToString();
					na.Author = reader.Result["Author"].ToString();
                    na.MetaTitle = reader.Result["MetaTitle"].ToString();
                    na.MetaKey = reader.Result["MetaKey"].ToString();
                    na.MetaDescription = reader.Result["MetaDescription"].ToString();
                    na.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
					na.LastEdited = reader.Result["LastEdited"].ToString();
					na.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
					na.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
					na.NewsStatus = (NewsStatus)Convert.ToInt32(reader.Result["NewsStatus"]);
					na.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
					na.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
					na.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());
					if (withChild) {
						na.Categories = NewsArticleCategoryList.GetByArticleID(na.ID);
					}
				} else {
					if (reader.Result.IsClosed == false) reader.Result.Close();
					na.ErrorMessage = "News Article wasn't found in the database.";
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			} else {
				na.ErrorMessage = reader.ErrorMessage;
			}
			return na;
		}
        public static NewsArticle GetByFriendlyUrl(string friendlyUrl, bool withChild, int liveOnly)
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            NewsArticle na = new NewsArticle();

            SqlParam[] paras = { new SqlParam("@friendlyUrl", SqlDbType.NVarChar, friendlyUrl),
									new SqlParam("@liveOnly", SqlDbType.Int,liveOnly)};

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_news_article_by_friendly_url", paras);
            if (reader.ReturnCode == 0)
            {
                if (reader.Result.Read())
                {
                    na.ID = Convert.ToInt32(reader.Result["id"].ToString());
                    na.Title = reader.Result["Title"].ToString();
                    na.ShortDescription = reader.Result["ShortDescription"].ToString();
                    na.ImageFileName = reader.Result["ImageFilename"].ToString();
                    na.ArticleContent = reader.Result["ArticleContent"].ToString();
                    na.Author = reader.Result["Author"].ToString();
                    na.MetaTitle = reader.Result["MetaTitle"].ToString();
                    na.MetaKey = reader.Result["MetaKey"].ToString();
                    na.MetaDescription = reader.Result["MetaDescription"].ToString();
                    na.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
                    na.LastEdited = reader.Result["LastEdited"].ToString();
                    na.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
                    na.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
                    na.NewsStatus = (NewsStatus)Convert.ToInt32(reader.Result["NewsStatus"]);
                    na.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
                    na.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
                    na.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());
                    if (withChild)
                    {
                        na.Categories = NewsArticleCategoryList.GetByArticleID(na.ID);
                    }
                }
                else
                {
                    if (reader.Result.IsClosed == false) reader.Result.Close();
                    na.ErrorMessage = "News Article wasn't found in the database.";
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }
            else
            {
                na.ErrorMessage = reader.ErrorMessage;
            }
            return na;
        }
		public static List<NewsArticle> GetNews(int limit, bool withChild, bool liveOnly) {
			int recordsetLimit;
			int liveFilter;
			if (limit > 0) {
				recordsetLimit = limit;
			} else {
				recordsetLimit = 100;
			}
			if (liveOnly) {
				liveFilter = 1;
			} else {
				liveFilter = 0;
			}
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			List<NewsArticle> list = new List<NewsArticle>();
			SqlParam[] paras = { new SqlParam("@limit", SqlDbType.Int, recordsetLimit),
							    new SqlParam("@liveOnly", SqlDbType.Int, liveFilter)};

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_news_articles_with_limit", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					NewsArticle na = new NewsArticle();
					na.ID = Convert.ToInt32(reader.Result["id"].ToString());
					na.Title = reader.Result["Title"].ToString();
					na.ShortDescription = reader.Result["ShortDescription"].ToString();
					na.ImageFileName = reader.Result["ImageFilename"].ToString();
					na.ArticleContent = reader.Result["ArticleContent"].ToString();
					na.Author = reader.Result["Author"].ToString();
                    na.MetaTitle = reader.Result["MetaTitle"].ToString();
                    na.MetaKey = reader.Result["MetaKey"].ToString();
                    na.MetaDescription = reader.Result["MetaDescription"].ToString();
                    na.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
                    na.LastEdited = reader.Result["LastEdited"].ToString();
					na.LastEdited = reader.Result["LastEdited"].ToString();
					na.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
					na.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
					na.NewsStatus = (NewsStatus)Convert.ToInt32(reader.Result["NewsStatus"]);
					na.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
					na.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
					na.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());

					if (withChild) {
						na.Categories = NewsArticleCategoryList.GetByArticleID(na.ID);
					}

					list.Add(na);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}

			return list;
		}

		public static NewsArticle GetLatestArticle() {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();

			NewsArticle na = new NewsArticle();
			SqlParam[] paras = { };

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_news_articles_latest", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					na.ID = Convert.ToInt32(reader.Result["id"].ToString());
					na.Title = reader.Result["Title"].ToString();
					na.ShortDescription = reader.Result["ShortDescription"].ToString();
					na.ImageFileName = reader.Result["ImageFilename"].ToString();
					na.ArticleContent = reader.Result["ArticleContent"].ToString();
					na.Author = reader.Result["Author"].ToString();
                    na.MetaTitle = reader.Result["MetaTitle"].ToString();
                    na.MetaKey = reader.Result["MetaKey"].ToString();
                    na.MetaDescription = reader.Result["MetaDescription"].ToString();
                    na.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
                    na.LastEdited = reader.Result["LastEdited"].ToString();
					na.LastEdited = reader.Result["LastEdited"].ToString();
					na.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
					na.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
					na.NewsStatus = (NewsStatus)Convert.ToInt32(reader.Result["NewsStatus"]);
					na.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
					na.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
					na.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}

			return na;
		}

		public static List<NewsArticle> GetByCategory(int categoryId, int limit) {
			int recordsetLimit;
			if (limit > 0) {
				recordsetLimit = limit;
			} else {
				recordsetLimit = -1;
			}
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			List<NewsArticle> list = new List<NewsArticle>();
			SqlParam[] paras = { new SqlParam("@categoryId", SqlDbType.Int, categoryId),
							   new SqlParam("@limit", SqlDbType.Int, recordsetLimit)};

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_news_articles_by_category_id", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					NewsArticle na = new NewsArticle();
					na.ID = Convert.ToInt32(reader.Result["id"].ToString());
					na.Title = reader.Result["Title"].ToString();
					na.ShortDescription = reader.Result["ShortDescription"].ToString();
					na.ImageFileName = reader.Result["ImageFilename"].ToString();
					na.ArticleContent = reader.Result["ArticleContent"].ToString();
					na.Author = reader.Result["Author"].ToString();
                    na.MetaTitle = reader.Result["MetaTitle"].ToString();
                    na.MetaKey = reader.Result["MetaKey"].ToString();
                    na.MetaDescription = reader.Result["MetaDescription"].ToString();
                    na.FriendlyUrl = reader.Result["FriendlyUrl"].ToString();
                    na.LastEdited = reader.Result["LastEdited"].ToString();
					na.LastEdited = reader.Result["LastEdited"].ToString();
					na.PublishDate = Convert.ToDateTime(reader.Result["PublishDate"].ToString());
					na.ExpiryDate = Convert.ToDateTime(reader.Result["ExpiryDate"].ToString());
					na.NewsStatus = (NewsStatus)Convert.ToInt32(reader.Result["NewsStatus"]);
					na.IsOnRunning = Convert.ToBoolean(reader.Result["IsOnRunning"].ToString());
					na.IsJunior = Convert.ToBoolean(reader.Result["IsJunior"].ToString());
					na.AllowComments = Convert.ToBoolean(reader.Result["AllowComments"].ToString());

					list.Add(na);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}

			return list;
		}

		public static NewsArticle Save(NewsArticle obj) {
			obj.ErrorMessage = string.Empty;

			try {
				MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
				SqlParam[] paras = {  new SqlParam("@id", SqlDbType.Int, obj.ID),
                new SqlParam("@title", SqlDbType.VarChar,128, obj.Title),
                new SqlParam("@shortDescription", SqlDbType.VarChar,256, obj.ShortDescription),
                new SqlParam("@imageFilename", SqlDbType.VarChar,128, obj.ImageFileName),
                new SqlParam("@articleContent", SqlDbType.Text, obj.ArticleContent),
				new SqlParam("@author", SqlDbType.VarChar,50, obj.Author),
                new SqlParam("@metaTitle", SqlDbType.NVarChar,128, obj.MetaTitle),
                new SqlParam("@metaKey", SqlDbType.NVarChar,250, obj.MetaKey),
                new SqlParam("@metaDescription", SqlDbType.NVarChar,250, obj.MetaDescription),
                new SqlParam("@friendlyUrl", SqlDbType.NVarChar,128, obj.FriendlyUrl),
				new SqlParam("@lastEdited", SqlDbType.VarChar,128, obj.LastEdited),
				new SqlParam("@publishDate", SqlDbType.DateTime, obj.PublishDate),
				new SqlParam("@expiryDate", SqlDbType.DateTime, obj.ExpiryDate),
				new SqlParam("@newsStatus", SqlDbType.SmallInt, obj.NewsStatus),
								   new SqlParam("@isOnRunning", SqlDbType.Bit, obj.IsOnRunning),
								   new SqlParam("@isJunior", SqlDbType.Bit, obj.IsJunior),
								   new SqlParam("@allowComments", SqlDbType.Bit, obj.AllowComments) };
				Int32Response result = connection.RunInt32Command("gr_save_news_article", paras);
				if (result.ReturnCode >= 0) {
					NewsArticle response = NewsArticle.GetByID(result.ReturnCode, false, 0);
					if (response != null && response.ID > 0 && obj.Categories.Count > 0) {
						//obj.Categories.ArticleId = obj.ID;
						obj.Categories.Save(obj.Categories);
					}
					return NewsArticle.GetByID(result.ReturnCode, true, 0);
				} else {
					obj.ErrorMessage = "Error: " + result.ErrorMessage;
					return obj;
				}
			} catch (Exception ex) {
				obj.ErrorMessage = ex.Message;
				return obj;
			}
		}

		public static bool Delete(int id) {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
			Int32Response iRes = connection.RunInt32Command("gr_delete_news_article", paras);
			if (iRes.ReturnCode == 0)
				return true;
			else
				return false;
		}

		#endregion Static Method
	}
}