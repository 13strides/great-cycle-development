﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Event
/// </summary>
///
namespace Gr.Model {

	public class Event {
		#region private properties

		private int _id;
		private string _name;
		private string _code;
		private string _errorMessage;

		#endregion private properties

		#region Public Properties

		public int ID { get { return _id; } set { _id = value; } }

		public string Name { get { return _name; } set { _name = value; } }

		public string Code { get { return _code; } set { _code = value; } }

		public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

		#endregion Public Properties

		public Event() {
			ID = 1;
			Name = "Great Trail Challenge";
			Code = "GTC";
		}
	}
}