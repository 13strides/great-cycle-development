﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using ThirteenStrides.Database;

/// <summary>
/// Summary description for BlogArticleTag
/// </summary>
namespace Gr.Model {

	public class BlogArticleTag {
		#region private properties

		private string _name;
		private int _id;
		private bool _isTopLevel;
		private string _errorMessage;
		private bool _isActive;
		private bool _isVisible;

		#endregion private properties

		#region public properties

		public string Name { get { return _name; } set { _name = value; } }

		public int ID { get { return _id; } set { _id = value; } }

		public bool IsTopLevel { get { return _isTopLevel; } set { _isTopLevel = value; } }

		public bool IsActive { get { return _isActive; } set { _isActive = value; } }

		public bool IsVisible { get { return _isVisible; } set { _isVisible = value; } }

		public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

		#endregion public properties

		#region object initializer

        public BlogArticleTag() { }

		#endregion object initializer

		#region static methods

        public static BlogArticleTag GetByID(int id)
        {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            BlogArticleTag nac = new BlogArticleTag();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_blog_article_tag_by_id", paras);
			if (reader.ReturnCode == 0) {
				if (reader.Result.Read()) {
					nac.ID = Convert.ToInt32(reader.Result["ID"].ToString());
					nac.Name = reader.Result["Name"].ToString();
					nac.IsTopLevel = Convert.ToBoolean(reader.Result["isTopLevel"].ToString());
					nac.IsActive = Convert.ToBoolean(reader.Result["isActive"].ToString());
					nac.IsVisible = Convert.ToBoolean(reader.Result["isVisible"].ToString());
				} else {
					if (reader.Result.IsClosed == false) reader.Result.Close();
					nac.ErrorMessage = "blog article tag wasn't found in the database.";
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			} else {
				nac.ErrorMessage = reader.ErrorMessage;
			}

			return nac;
		}

        public static BlogArticleTag Save(BlogArticleTag obj)
        {
			obj.ErrorMessage = string.Empty;

			try {
				MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
				SqlParam[] paras = {  new SqlParam("@id", SqlDbType.Int, obj.ID),
                new SqlParam("@name", SqlDbType.NVarChar, obj.Name),
                new SqlParam("@isTopLevel", SqlDbType.Bit, obj.IsTopLevel) };
				Int32Response result = connection.RunInt32Command("gr_save_blog_article_tag", paras);
				if (result.ReturnCode >= 0) {
                    return BlogArticleTag.GetByID(result.ReturnCode);
				} else {
					obj.ErrorMessage = "Error: " + result.ErrorMessage;
					return obj;
				}
			} catch (Exception ex) {
				obj.ErrorMessage = ex.Message;
				return obj;
			}
		}

		public static bool Delete(int id) {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
			Int32Response iRes = connection.RunInt32Command("gr_delete_blog_article_tag", paras);
			if (iRes.ReturnCode == 0)
				return true;
			else
				return false;
		}

        public static List<BlogArticleTag> GetBlogTags()
        {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            List<BlogArticleTag> list = new List<BlogArticleTag>();
			SqlParam[] paras = { };

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_blog_tags", paras);

			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
                    BlogArticleTag blogTag = new BlogArticleTag();
                    blogTag.ID = Convert.ToInt32(reader.Result["ID"].ToString());
                    blogTag.Name = reader.Result["Name"].ToString();
                    blogTag.IsTopLevel = Convert.ToBoolean(reader.Result["isTopLevel"].ToString());
                    blogTag.IsActive = Convert.ToBoolean(reader.Result["isActive"].ToString());
                    blogTag.IsVisible = Convert.ToBoolean(reader.Result["isVisible"].ToString());

                    list.Add(blogTag);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}
			return list;
		}
        public static List<BlogArticleTag> GetTagsTaggedAndLive()
        {
            MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
            List<BlogArticleTag> list = new List<BlogArticleTag>();
            SqlParam[] paras = { };

            SqlDataReaderResponse reader = connection.RunCommand("gr_get_blog_tags_tagged_in_article", paras);

            if (reader.ReturnCode == 0)
            {
                while (reader.Result.Read())
                {
                    BlogArticleTag blogTag = new BlogArticleTag();
                    blogTag.ID = Convert.ToInt32(reader.Result["ID"].ToString());
                    blogTag.Name = reader.Result["Name"].ToString();
                    blogTag.IsTopLevel = Convert.ToBoolean(reader.Result["isTopLevel"].ToString());
                    blogTag.IsActive = Convert.ToBoolean(reader.Result["isActive"].ToString());
                    blogTag.IsVisible = Convert.ToBoolean(reader.Result["isVisible"].ToString());

                    list.Add(blogTag);
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }
            return list;
        }
		public static bool SetActiveTab(int id) {
			try {
				MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
				SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
				Int32Response result = connection.RunInt32Command("gr_save_blog_active_tab", paras);
				if (result.ReturnCode >= 0) {
					return true;
				} else {
					return false;
				}
			} catch {
				return false;
			}
		}

		#endregion static methods
	}
}