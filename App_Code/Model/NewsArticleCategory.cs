﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using ThirteenStrides.Database;

/// <summary>
/// Summary description for NewsArticleCategory
/// </summary>
namespace Gr.Model {

	public class NewsArticleCategory {
		#region private properties

		private string _name;
		private int _id;
		private int _parent;
		private bool _isTopLevel;
		private string _errorMessage;
		private bool _isActive;
		private bool _isVisible;
		private int _order;

		#endregion private properties

		#region public properties

		public string Name { get { return _name; } set { _name = value; } }

		public int ID { get { return _id; } set { _id = value; } }

		public int Parent { get { return _parent; } set { _parent = value; } }

		public bool IsTopLevel { get { return _isTopLevel; } set { _isTopLevel = value; } }

		public bool IsActive { get { return _isActive; } set { _isActive = value; } }

		public bool IsVisible { get { return _isVisible; } set { _isVisible = value; } }

		public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

		public int Order { get { return _order; } set { _order = value; } }

		#endregion public properties

		#region object initializer

		public NewsArticleCategory() { }

		#endregion object initializer

		#region static methods

		public static NewsArticleCategory GetByID(int id) {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			NewsArticleCategory nac = new NewsArticleCategory();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_news_article_category_by_id", paras);
			if (reader.ReturnCode == 0) {
				if (reader.Result.Read()) {
					nac.ID = Convert.ToInt32(reader.Result["ID"].ToString());
					nac.Name = reader.Result["Name"].ToString();
					if (reader.Result["parent"] != DBNull.Value) {
						nac.Parent = Convert.ToInt32(reader.Result["parent"]);
					}
					nac.IsTopLevel = Convert.ToBoolean(reader.Result["isTopLevel"].ToString());
					nac.IsActive = Convert.ToBoolean(reader.Result["isActive"].ToString());
					nac.IsVisible = Convert.ToBoolean(reader.Result["isVisible"].ToString());
					nac._order = Convert.ToInt32(reader.Result["order"].ToString());
				} else {
					if (reader.Result.IsClosed == false) reader.Result.Close();
					nac.ErrorMessage = "news article cateogry wasn't found in the database.";
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			} else {
				nac.ErrorMessage = reader.ErrorMessage;
			}

			return nac;
		}

		public static NewsArticleCategory Save(NewsArticleCategory obj) {
			obj.ErrorMessage = string.Empty;

			try {
				MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
				SqlParam[] paras = {  new SqlParam("@id", SqlDbType.Int, obj.ID),
                new SqlParam("@name", SqlDbType.NVarChar, obj.Name),
                new SqlParam("@parent", SqlDbType.Int, obj.Parent),
                new SqlParam("@isTopLevel", SqlDbType.Bit, obj.IsTopLevel),
				new SqlParam("@isVisible", SqlDbType.Bit, obj.IsVisible),
				new SqlParam("@isActive", SqlDbType.Bit, obj.IsActive),
				new SqlParam("@order", SqlDbType.Int, obj.Order)};
				Int32Response result = connection.RunInt32Command("gr_save_news_article_category", paras);
				if (result.ReturnCode >= 0) {
					return NewsArticleCategory.GetByID(result.ReturnCode);
				} else {
					obj.ErrorMessage = "Error: " + result.ErrorMessage;
					return obj;
				}
			} catch (Exception ex) {
				obj.ErrorMessage = ex.Message;
				return obj;
			}
		}

		public static bool Delete(int id) {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
			Int32Response iRes = connection.RunInt32Command("gr_delete_news_article_category", paras);
			if (iRes.ReturnCode == 0)
				return true;
			else
				return false;
		}

		public static List<NewsArticleCategory> GetNewsCategories() {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			List<NewsArticleCategory> list = new List<NewsArticleCategory>();
			SqlParam[] paras = { };

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_news_categories", paras);

			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					NewsArticleCategory NewsCat = new NewsArticleCategory();
					NewsCat.ID = Convert.ToInt32(reader.Result["ID"].ToString());
					NewsCat.Name = reader.Result["Name"].ToString();
					if (reader.Result["parent"] != DBNull.Value) {
						NewsCat.Parent = Convert.ToInt32(reader.Result["parent"]);
					}
					NewsCat.IsTopLevel = Convert.ToBoolean(reader.Result["isTopLevel"].ToString());
					NewsCat.IsActive = Convert.ToBoolean(reader.Result["isActive"].ToString());
					NewsCat.IsVisible = Convert.ToBoolean(reader.Result["isVisible"].ToString());
					NewsCat.Order = Convert.ToInt32(reader.Result["order"].ToString());
					list.Add(NewsCat);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}
			return list;
		}

		public static bool SetActiveTab(int id) {
			try {
				MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
				SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
				Int32Response result = connection.RunInt32Command("gr_save_news_active_tab", paras);
				if (result.ReturnCode >= 0) {
					return true;
				} else {
					return false;
				}
			} catch {
				return false;
			}
		}

		#endregion static methods
	}
}