﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using ThirteenStrides.Database;

/// <summary>
/// Summary description for NewsArticleCategoryList
/// </summary>

namespace Gr.Model {

	public class NewsArticleCategoryList : CollectionBase {
		#region private properties

		int _articleId;

		#endregion private properties

		#region public properties

		public int ArticleId { get { return _articleId; } set { _articleId = value; } }

		#endregion public properties

		#region object initialiser

		public NewsArticleCategoryList() {
			//
			// TODO: Add constructor logic here
			//
		}

		#endregion object initialiser

		#region static methods

		public static NewsArticleCategoryList GetByArticleID(int articleId) {
			//_id = articleId;

			NewsArticleCategoryList list = new NewsArticleCategoryList();
			list.ArticleId = articleId;
			NewsArticleCategory newsCat = new NewsArticleCategory();
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@articleId", SqlDbType.Int, list.ArticleId) };

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_news_category_list_by_article_by_id", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					newsCat = new NewsArticleCategory();
					newsCat.ID = Convert.ToInt32(reader.Result["id"]);
					newsCat.Name = reader.Result["Name"].ToString();
					newsCat.IsTopLevel = Convert.ToBoolean(reader.Result["isTopLevel"]);
					newsCat.IsActive = Convert.ToBoolean(reader.Result["isActive"].ToString());
					newsCat.IsVisible = Convert.ToBoolean(reader.Result["isVisible"].ToString());
					newsCat.Order = Convert.ToInt32(reader.Result["order"].ToString());
					if (reader.Result["parent"] != DBNull.Value) {
						newsCat.Parent = Convert.ToInt32(reader.Result["parent"]);
					}
					list.InnerList.Add(newsCat);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			} else {
				//error
			}

			return list;
		}

		public void Add(NewsArticleCategory category) {
			this.InnerList.Add(category);
		}

		public void Remove(NewsArticleCategory category) {
			this.InnerList.Remove(category);
		}

		public bool Contains(int categoryId) {
			foreach (NewsArticleCategory _cat in this.InnerList) {
				if (categoryId == _cat.ID) {
					return true;
				}
			}
			return false;
		}

		public void Save(NewsArticleCategoryList obj) {
			//HttpContext.Current.Response.Write("HELLO " + obj.ArticleId + " " + obj.InnerList.Count);
			if (DeleteAllNewsCategoriesFromDB(obj.ArticleId)) {
				HttpContext.Current.Response.Write("- Deleted " + obj.InnerList.Count);
				MsSqlConnection connection = DataAccess.DbConnection.GetConnection();

				foreach (NewsArticleCategory cat in obj.InnerList) {
					SqlParam[] paras = { new SqlParam("@articleId", SqlDbType.Int, obj.ArticleId),
					new SqlParam("@categoryId", SqlDbType.Int, cat.ID)};

					//HttpContext.Current.Response.Write("cat =" + cat.ID + " artID = " + obj.ArticleId);

					Int32Response result = connection.RunInt32Command("gr_save_news_article_categories", paras);
					/*if (result.ReturnCode >= 0) {
						return BusinessChallenge.GetByID(result.ReturnCode);
					} else {
						obj.ErrorMessage = "Error: " + result.ErrorMessage;
						return obj;
					}*/
				}
			} else {
				//success = false;
			}

			//return success;
			return;
		}

		private bool DeleteAllNewsCategoriesFromDB(int delArtID) {
			/*MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, delArtID) };
			SqlDataReaderResponse reader = connection.RunCommand("gr_delete_all_news_categories", paras);

			HttpContext.Current.Response.Write("- Deleted " + reader.ReturnCode);*/
			bool deleted = true;
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, delArtID) };
			Int32Response iRes = connection.RunInt32Command("gr_delete_news_categories_all", paras);
			if (iRes.ReturnCode >= 0)
				deleted = true;
			else
				deleted = false;
			return deleted;
			//return true; // (reader.ReturnCode >= 0);
		}

		#endregion static methods
	}
}