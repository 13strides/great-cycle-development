﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using GS.Generic.Membership;
using ThirteenStrides.Database;

/// <summary>
/// Summary description for Country
/// </summary>
///
namespace Gr.Model {

	public class Country {
		#region private properties

		private int _id;
		private int _order;
		private string _name;
		private string _code;
		private string _errorMessage;

		#endregion private properties

		#region Public Properties

		public int ID { get { return _id; } set { _id = value; } }

		public int Order { get { return _order; } set { _order = value; } }

		public string Name { get { return _name; } set { _name = value; } }

		public string Code { get { return _code; } set { _code = value; } }

		public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; } }

		#endregion Public Properties

		#region Object Initializer

		public Country() { }

		#endregion Object Initializer

		#region Static Methods

		public static Country GetByID(int id) {
			MsSqlConnection connection = Gr.DataAccess.DbConnection.GetConnection();
			Country obj = new Country();
			SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
			SqlDataReaderResponse reader = connection.RunCommand("gr_get_country_by_id", paras);

			if (reader != null && reader.ReturnCode == 0) {
				if (reader.Result.Read()) {
					obj.ID = Convert.ToInt32(reader.Result["ID"].ToString());
					obj.Order = Convert.ToInt32(reader.Result["DisplayOrder"].ToString());
					obj.Name = reader.Result["Name"].ToString();
					obj.Code = reader.Result["Code"].ToString().ToLower();
				} else {
					if (reader.Result.IsClosed == false) reader.Result.Close();
					obj.ErrorMessage = "Country wasn't found in the database.";
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			} else {
				obj.ErrorMessage = reader.ErrorMessage;
			}
			return obj;
		}

		public static List<Country> GetAll() {
			MsSqlConnection connection = DataAccess.DbConnection.GetConnection();
			List<Country> list = new List<Country>();
			SqlParam[] paras = { };

			SqlDataReaderResponse reader = connection.RunCommand("gr_get_country_all", paras);
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					Country obj = new Country();
					obj.ID = Convert.ToInt32(reader.Result["ID"].ToString());
					obj.Order = Convert.ToInt32(reader.Result["DisplayOrder"].ToString());
					obj.Name = reader.Result["Name"].ToString();
					obj.Code = reader.Result["Code"].ToString().ToLower();

					list.Add(obj);
				}
				if (reader.Result.IsClosed == false) reader.Result.Close();
			}

			return list;
		}

		#endregion Static Methods
	}
}