﻿using System;
using System.Configuration;
using ThirteenStrides.Database;

/// <summary>
/// Summary description for DbConnection
/// </summary>
///
namespace Gr.DataAccess {

	public static class DbConnection {

		public static MsSqlConnection GetConnection() {
			return new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
		}
	}
}