﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using ThirteenStrides.Database;
using System.Data;
using System.Configuration;

namespace GS.Generic.Events
{

    /// <summary>
    /// Summary description for EventList
    /// </summary>
    public class EventList : CollectionBase
    {
        public EventList(){
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

			SqlParam[] paras = { };

			SqlDataReaderResponse reader = connection.RunCommand("gs_select_events", paras);
			
			if (reader.ReturnCode == 0) {
				while (reader.Result.Read()) {
					this.InnerList.Add(new Event(Convert.ToInt32(reader.Result["event_id"]), reader.Result["event_name"].ToString()));
				}
				reader.Result.Close();
			} else {
				throw new Exception(reader.ErrorMessage);
			}
		}

		/// <summary>
		/// this constructor does nothing, it can be used by derived classes to avoid loading all the events at runtime
		/// </summary>
		/// <param name="o">anything (this was designed to avoid having a duplicate function signature)</param>
		protected EventList(object o) {

		}

		/// <summary>
		/// adds an event to the end of the list
		/// </summary>
		/// <param name="evt">event to be added</param>
		public void Add(Event evt) {
			this.InnerList.Add(evt);
		}

		/// <summary>
		/// removes an event from the list
		/// </summary>
		/// <param name="evt">event to be removed</param>
		public void Remove(Event evt) {
			this.InnerList.Remove(evt);
		}

		/// <summary>
		/// gets or sets the event at the given index
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public Event this[int index] {
			get { return (Event)this.InnerList[index]; }
			set { this.InnerList[index] = value; }
		}

		/// <summary>
		/// removes the event at the given index
		/// </summary>
		/// <param name="index"></param>
		public void RemoveAt(int index) {
			Event evt = (Event)this.InnerList[index];
			this.Remove(evt);
		}

		/// <summary>
		/// Empties the list
		/// </summary>
		public void Clear() {
			this.InnerList.Clear();
		}
	}
}
