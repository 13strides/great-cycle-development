﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using ThirteenStrides.Database;

namespace GS.Generic.Events
{
    /// <summary>
    /// Summary description for Event
    /// </summary>
    public class Event
    {
        protected int _id;
        protected string _name, _code;

        /// <summary>
        /// Default constructor
        /// </summary>
        public Event()
        {
            _id = -1;
            _name = "";
            _code = "";
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">The event's id</param>
        public Event(int id)
        {
            _id = id;
            GetEventDetailsFromDB();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="code">The event's code</param>
        public Event(string code)
        {
            _id = -1;
            _code = code;
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

            SqlParam[] paras = { new SqlParam("@code", SqlDbType.VarChar, 5, _code) };

            SqlDataReaderResponse reader = connection.RunCommand("gs_select_event_by_code", paras);

            if (reader.ReturnCode == 0)
            {
                if (reader.Result.Read())
                {
                    // gets the user settings
                    _id = Convert.ToInt32(reader.Result["event_id"]);
                    _name = reader.Result["event_name"].ToString();
                }
                else
                {
                    reader.Result.Close();
                    throw new Exception("The event wasn't found in the database.");
                }
                reader.Result.Close();
            }
            else
            {
                throw new Exception(reader.ErrorMessage);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public Event(int id, string name)
        {
            _id = id;
            _name = name;
        }

        /// <summary>
        /// gets the event id
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        /// <summary>
        /// gets or sets the event name
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// gets or sets the event name
        /// </summary>
        public string Code
        {
            get { return _code; }
        }

        /// <summary>
        /// saves the event into the database
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
            SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, _id),
				new SqlParam("@name", SqlDbType.VarChar, 50, _name),
				new SqlParam("@code", SqlDbType.VarChar, 5, _code) };

            Int32Response result = connection.RunInt32Command("gs_save_event", paras);

            if (result.ReturnCode >= 0)
            {
                _id = result.ReturnCode;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// get the event details from the GreatRun.org database
        /// </summary>
        private void GetEventDetailsFromDB()
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

            SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, _id) };

            SqlDataReaderResponse reader = connection.RunCommand("gs_select_event", paras);

            if (reader.ReturnCode == 0)
            {
                if (reader.Result.Read())
                {
                    // gets the user settings
                    _name = reader.Result["event_name"].ToString();
                    _code = reader.Result["event_code"].ToString();
                }
                else
                {
                    reader.Result.Close();
                    throw new Exception("The event wasn't found in the database.");
                }
                reader.Result.Close();
            }
            else
            {
                throw new Exception(reader.ErrorMessage);
            }
        }
    }
}
