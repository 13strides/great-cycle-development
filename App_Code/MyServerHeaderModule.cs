﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for MyHttp
/// </summary>
/// 
namespace NationalLot.CustomModule
{
    public class MyServerHeaderModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += OnPreSendRequestHeaders;
        }

        public void Dispose()
        { }

        void OnPreSendRequestHeaders(object sender, EventArgs e)
        {
            //if(HttpContext.Current != null)
            //HttpContext.Current.Response.Headers.Set("Server", "National Lottery Webserver");
            HttpApplication objApp = (HttpApplication)sender;
            HttpContext objContext = (HttpContext)objApp.Context;
            HttpContext.Current.Response.Headers.Set("Server", "National Lottery Webserver");
        }
    }
}
