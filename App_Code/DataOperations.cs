using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace SwimWaveChange
{
    /// <summary>
    /// Summary description for DataOperations
    /// </summary>
        public class DataOperations
        {
            private string _connString = ConfigurationManager.ConnectionStrings["SqlServerConnection"].ConnectionString;
            private string _questionIds = ConfigurationManager.AppSettings["WaveQuestionIds"].ToString();
            private string _raceIds = ConfigurationManager.AppSettings["SwimRaceIds"].ToString();

            /// <summary>
            /// Gets a list of Races from the stgnova.race table, populated into a drop down list
            /// Created 24/11/2009 by Peter Harrison
            /// Last Updated 24/11/2009 by Peter Harrison
            /// </summary>
            /// <returns>DataSet of Race Information</returns>
            public DataSet GetRaces()
            {
                DataSet raceData = new DataSet();

                using (SqlConnection conn = new SqlConnection(_connString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        SqlDataAdapter raceDataAdapter = new SqlDataAdapter();
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT Description,idRace FROM Race with (nolock) WHERE RaceDate > GETDATE() and idrace in (" + _raceIds + ") ORDER BY RaceDate ASC";
                        conn.Open();
                        raceDataAdapter.SelectCommand = cmd;
                        raceDataAdapter.Fill(raceData);
                    }
                }

                return raceData;

            }

            /// <summary>
            /// Function to return race registration details for an individual race registration
            /// RaceId and RaceRegistrationId are passed more as a security measure (as race registration id is unique)
            /// Created 24/11/2009 by Peter Harrison
            /// Last Updated 26/11/2009 by Peter Harrison
            /// </summary>
            /// <param name="idRaceRegistration">The Race Registration Id of the participant</param>
            /// <param name="idRace">The Race they are registered in</param>
            /// <returns>A dataset containing the person / race registration details</returns>
            public DataSet GetRaceRegistrationDetails(int idRaceRegistration, int idRace)
            {
                DataSet registrationData = new DataSet();

                using (SqlConnection conn = new SqlConnection(_connString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        SqlDataAdapter registrationDataAdapter = new SqlDataAdapter();
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT * from person p with (nolock), raceregistration rr with (nolock) where rr.idraceregistration=@IDRACEREGISTRATION and rr.idperson = p.idperson and rr.registrationstatus=10 and rr.paymentstatus=2 and rr.idrace=@IDRACE";
                        cmd.Parameters.AddWithValue("@IDRACEREGISTRATION", idRaceRegistration);
                        cmd.Parameters.AddWithValue("@IDRACE", idRace);
                        conn.Open();
                        registrationDataAdapter.SelectCommand = cmd;
                        registrationDataAdapter.Fill(registrationData);
                    }
                }

                return registrationData;
            }

            /// <summary>
            /// Function to return the current wave assigned to a swim registration
            /// Created 24/11/2009 by Peter Harrison
            /// Last Updated 24/11/2009 by Peter Harrison
            /// </summary>
            /// <param name="idRaceRegistration">The Race Registration Id</param>
            /// <returns>A dataset containing the current wave</returns>
            public DataSet GetCurrentWave(int idRaceRegistration)
            {
                DataSet currentWaveData = new DataSet();

                using (SqlConnection conn = new SqlConnection(_connString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        SqlDataAdapter waveDataAdapter = new SqlDataAdapter();
                        cmd.Connection = conn;
                        cmd.CommandText = "select a.questionid, w.wavename, rr.idrace, w.idRaceWave, a.id from raceregistration rr with (nolock), eventmanagement.dbo.answers a with (nolock), waveallocation w with (nolock) where a.questionid in (" + _questionIds + ") and rr.ExternalRegistrationId = a.registrationid and CAST(a.answerstring as varchar) = w.idRaceWave and rr.registrationstatus=10 and rr.paymentstatus=2 and rr.idRaceRegistration=@IDRACEREGISTRATION";
                        cmd.Parameters.AddWithValue("@IDRACEREGISTRATION", idRaceRegistration);
                        conn.Open();
                        waveDataAdapter.SelectCommand = cmd;
                        waveDataAdapter.Fill(currentWaveData);
                    }
                }

                return currentWaveData;
            }

            /// <summary>
            /// Function to return all available waves for a race / entry type (being Online / Token / etc.)
            /// Created 26/11/2009 by Peter Harrison
            /// Last Updated 26/11/2009 by Peter Harrison
            /// </summary>
            /// <param name="idRace">The Race Id</param>
            /// <param name="entryType">The Entry Type (Online, Token, etc.)</param>
            /// <returns>A dataset of the available waves</returns>
            public DataSet GetAvailableWaves(string idRace, string entryType)
            {
                DataSet availableWaveData = new DataSet();

                using (SqlConnection conn = new SqlConnection(_connString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        SqlDataAdapter availableWaveDataAdapter = new SqlDataAdapter();
                        cmd.Connection = conn;
                        cmd.CommandText = "select idRaceWave, wavename from waveallocation with (nolock) where idrace=@IDRACE and FreeSpaces > 0 and EntryType=@ENTRYTYPE order by DisplayOrder";
                        cmd.Parameters.AddWithValue("@IDRACE", idRace);
                        cmd.Parameters.AddWithValue("@ENTRYTYPE", entryType);
                        conn.Open();
                        availableWaveDataAdapter.SelectCommand = cmd;
                        availableWaveDataAdapter.Fill(availableWaveData);
                    }
                }

                return availableWaveData;
            }
            
            /// <summary>
            /// A function to update the wave of a registration
            /// Created by Peter Harrison 26/11/2009
            /// Last Updated by Peter Harrison 26/11/2009
            /// </summary>
            /// <param name="answerId">The unique id of the answer row</param>
            /// <param name="idRaceWave">The wave to be changed to</param>
            /// <returns>true if updated successfully</returns>
            public bool UpdateWaveAnswer(string answerId, string idRaceWave)
            {
                using (SqlConnection conn = new SqlConnection(_connString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "UPDATE eventmanagement.dbo.answers set answerstring = @IDRACEWAVE where id = @ANSWERID";
                        cmd.Parameters.AddWithValue("@IDRACEWAVE", idRaceWave);
                        cmd.Parameters.AddWithValue("@ANSWERID", answerId);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                    }
                }

                return true;
            }

        }
    }
