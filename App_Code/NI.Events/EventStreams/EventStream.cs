﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Ni.Generic.Measurement;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;

namespace Ni.Events.StreamManagement
{
	public enum SwimEventStatus {
		Open = 1,
		Closed = 2,
		OpenToCharities = 3,
		ReminderService = 4,
		PriorityEntry = 5,
		Ballot = 6
	}


    public class EventStream
    {

		public int _event_info_id;
        public int _sub_Event_ID;
		public string _event_description = "";
		public int _event_info_stream_id;
		public DateTime _event_info_publication_date;
		public int _event_info_status;
		public int _event_info_ap16_id=0;
		public string _event_info_skyscraper_id = "";
		public string _event_info_banner_id = "";
		public string _event_info_std_price = "";


		public DateTime PublicationDate { get { return _event_info_publication_date; } }
		public int StreamID { get { return _event_info_stream_id; } }
		public int Status { get { return _event_info_status; } }
		public int eventInfoID { get { return _event_info_id; } }
		public EventStream() {

		}


		// LOAD based on the _event_info_id
		public EventStream(int info_id) {


			if (info_id != -1) {
				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
				SqlParam[] paras = { 
                   new SqlParam("@info_id", SqlDbType.Int, info_id)
                           };

				SqlDataReaderResponse reader = connection.RunCommand("ni_select_event_information_stream", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.Read()) {
						_event_info_id = info_id;
						_sub_Event_ID = int.Parse(reader.Result["event_info_event_id"].ToString());
						_event_description = reader.Result["subevent_desc"].ToString();
						_event_info_stream_id = int.Parse(reader.Result["event_info_stream_id"].ToString());
						_event_info_publication_date = Convert.ToDateTime(reader.Result["event_info_publication_date"].ToString());
						_event_info_status = int.Parse(reader.Result["event_info_status"].ToString());
						_event_info_ap16_id =int.Parse(reader.Result["event_info_ap16_id"].ToString());
						_event_info_skyscraper_id = reader.Result["event_info_skyscraper_id"].ToString();
						_event_info_banner_id = reader.Result["event_info_banner_id"].ToString();
						_event_info_std_price = reader.Result["event_info_std_price"].ToString();
					} else {
						reader.Result.Close();
						throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {
				//_pageID = pageid;
				//_contentID = -1;
			}






		}

		// CREATE BASED on sub event ID 
		public EventStream(int minusone, int eventID) {
			_event_info_id = -1;
			_sub_Event_ID = eventID;
			if (eventID != -1) {
			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras = { 
                   new SqlParam("@info_id", SqlDbType.Int, eventID)
                           };

			SqlDataReaderResponse reader = connection.RunCommand("ni_select_event_information_stream", paras);
			if (reader.ReturnCode == 0) {
				if (reader.Result.Read()) {
					_event_description = reader.Result["subevent_desc"].ToString();
				} else {
						reader.Result.Close();
						throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {
				//_pageID = pageid;
				//_contentID = -1;
			}

		}

		public bool  Save() {
			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			/*
			 */

			SqlParam[] paras = { 
                   new SqlParam("@event_info_id", SqlDbType.Int, _event_info_id),
                   new SqlParam("@sub_Event_ID", SqlDbType.Int, _sub_Event_ID),
new SqlParam("@event_info_stream_id", SqlDbType.Int, _event_info_stream_id),
new SqlParam("@event_info_publication_date", SqlDbType.DateTime, _event_info_publication_date),
new SqlParam("@event_info_status", SqlDbType.SmallInt, _event_info_status),
new SqlParam("@event_info_ap16_id", SqlDbType.Int, _event_info_ap16_id),
new SqlParam("@event_info_skyscraper_id", SqlDbType.VarChar,20, _event_info_skyscraper_id),
new SqlParam("@event_info_banner_id", SqlDbType.VarChar,20, _event_info_banner_id),
new SqlParam("@event_info_std_price", SqlDbType.VarChar,20, _event_info_std_price)
                           };

			Int32Response result = connection.RunInt32Command("ni_save_event_streamInfo", paras);
			if (result.ReturnCode >= 0) {
				_event_info_id = result.ReturnCode;
				return true;
			} else {
				return false;
			}
















		}

		public void Delete() {
		}

    }
}