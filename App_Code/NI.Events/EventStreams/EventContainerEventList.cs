﻿using System;
using System.Collections.Generic;
using System.Web;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for EventContainerEventList
/// </summary>
namespace Ni.Events.StreamManagement
{


    public class EventContainerEventList
    {
        public List<int> subevents;

        public EventContainerEventList(int containerID)
        {

            subevents = new List<int>();
            if (containerID != -1)
            {
                SqlParam[] paras = { 
                   new SqlParam("@containerID", SqlDbType.Int, containerID)
                           };
                MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

                SqlDataReaderResponse reader = connection.RunCommand("ni_select_subevents_in_container", paras);
                if (reader.ReturnCode == 0)
                {
                    if (reader.Result.Read())
                    {
                        this.subevents.Add(int.Parse(reader.Result["eventID"].ToString()));

                        while (reader.Result.Read())
                        {
                            this.subevents.Add(int.Parse(reader.Result["eventID"].ToString()));
                        }
                    }
                    else
                    {
                        reader.Result.Close();
                        //throw new Exception("The was no information found in the database.");
                    }
                    reader.Result.Close();
                }
                else
                {
                    throw new Exception(reader.ErrorMessage);
                }
            }
            else
            {

            }

        }
    }
}