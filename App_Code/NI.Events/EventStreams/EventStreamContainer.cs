﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Ni.Generic.Measurement;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;
using System.Collections.Generic;

namespace Ni.Events.StreamManagement
{

    public class EventStreamContainer
    {
		public List<EventStream> EventStreams{get { return GetEventStreamList();}}
		public EventStream currentStream;
		public int currentStreamid;
		public int _eventID;
		public int _topeventId;
		public string _event_name = "";


		public DateTime event_info_event_date;
		public int event_info_distance=0;
		public string event_info_location_string="";
		public string event_info_location_ref = "";
		public string event_info_distance_title = "";
		public string event_info_start_time2 = "";
		public int event_info_ap16_id = 0;
		public int event_info_start_time = 0;
		public string event_info_skyscraper_id = "";
		public string event_info_banner_id = "";
		public string event_info_std_price = "";
		public float event_info_temp=0;



		public EventStreamContainer() {
		}


        //Create New Event
        public EventStreamContainer(int minusone, int eventContainer)
        {
            _topeventId = eventContainer;
            _eventID = -1;
        }

        public bool Save()
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);


            SqlParam[] paras = { 
                   new SqlParam("@eventContainer", SqlDbType.Int, _topeventId),
                   new SqlParam("@subeventId", SqlDbType.Int, _eventID),
new SqlParam("@subeventName", SqlDbType.VarChar,200, _event_name),

new SqlParam("@event_info_event_date", SqlDbType.DateTime, event_info_event_date),
new SqlParam("@event_info_distance", SqlDbType.Int, event_info_distance),
new SqlParam("@event_info_location_string", SqlDbType.VarChar,200, event_info_location_string),

new SqlParam("@event_info_location_ref", SqlDbType.VarChar,200, event_info_location_ref),
new SqlParam("@event_info_distance_title", SqlDbType.VarChar,20, event_info_distance_title),
new SqlParam("@event_info_start_time2", SqlDbType.VarChar,20, event_info_start_time2),

new SqlParam("@event_info_start_time", SqlDbType.Int, event_info_start_time),
new SqlParam("@event_info_temp", SqlDbType.Float, event_info_temp),
new SqlParam("@event_info_ap16_id", SqlDbType.Int, event_info_ap16_id),

new SqlParam("@event_info_skyscraper_id", SqlDbType.VarChar,20, event_info_skyscraper_id),
new SqlParam("@event_info_banner_id", SqlDbType.VarChar,20, event_info_banner_id),
new SqlParam("@event_info_std_price", SqlDbType.VarChar,20, event_info_std_price)



                           };

            Int32Response result = connection.RunInt32Command("ni_save_subevent", paras);
            if (result.ReturnCode >= 0)
            {
                _eventID = result.ReturnCode;
                return true;
            }
            else
            {
                return false;
            }
        }




		public List<EventStream> GetEventStreamList()
		{

			List<EventStream> testthis	= new List<EventStream>();
			
			if (_eventID != -1) {
				SqlParam[] paras = { 
                   new SqlParam("@eventID", SqlDbType.Int, _eventID)
                           };
				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

				SqlDataReaderResponse reader = connection.RunCommand("ni_select_event_streams_for_event_list", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.Read()) {
						testthis.Add(new EventStream(int.Parse(reader.Result["event_info_id"].ToString())));

						while (reader.Result.Read()) {
							testthis.Add(new EventStream(int.Parse(reader.Result["event_info_id"].ToString())));
						}
					} else {
						reader.Result.Close();
						//throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {

			}
			return testthis;
		}


		public EventStreamContainer(int eventID) {
			_eventID = eventID;


			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras2 = { 
                   new SqlParam("@eventID", SqlDbType.Int, eventID)
                           };
			SqlDataReaderResponse reader2 = connection.RunCommand("ni_select_event_information_stream_mini", paras2);
			if (reader2.ReturnCode == 0) {
					if (reader2.Result.Read()) {
						_event_name = reader2.Result["subevent_desc"].ToString();
						_topeventId = int.Parse(reader2.Result["event_id"].ToString());




						event_info_event_date = Convert.ToDateTime(reader2.Result["event_info_event_date"].ToString());
						event_info_distance = int.Parse(reader2.Result["event_info_distance"].ToString());
						event_info_location_string = reader2.Result["event_info_location_string"].ToString();
						event_info_location_ref = reader2.Result["event_info_location_ref"].ToString();
						event_info_distance_title = reader2.Result["event_info_distance_title"].ToString();
						event_info_start_time2 = reader2.Result["event_info_start_time2"].ToString();
						event_info_start_time = int.Parse(reader2.Result["event_info_start_time2"].ToString());
						event_info_temp = float.Parse(reader2.Result["event_info_temp"].ToString());
						event_info_ap16_id = int.Parse(reader2.Result["event_info_ap16_id"].ToString());
						event_info_skyscraper_id = reader2.Result["event_info_skyscraper_id"].ToString();
						event_info_banner_id = reader2.Result["event_info_banner_id"].ToString();
						event_info_std_price = reader2.Result["event_info_std_price"].ToString();
						currentStreamid = int.Parse(reader2.Result["current_stream_id"].ToString());
						if (currentStreamid > 0) {
							currentStream = new EventStream(currentStreamid);
						}
					}
					 else {
						reader2.Result.Close();
						//throw new Exception("The was no information found in the database.");
					}
					reader2.Result.Close();
				} else {
					throw new Exception(reader2.ErrorMessage);
				}
			





				/*		
			EventStreams = new List<EventStream>();
			if (eventID != -1) {
				SqlParam[] paras = { 
                   new SqlParam("@eventID", SqlDbType.Int, eventID)
                           };

				SqlDataReaderResponse reader = connection.RunCommand("ni_select_event_streams_for_event_list", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.Read()) {
						this.EventStreams.Add(new EventStream(int.Parse(reader.Result["event_info_id"].ToString())));

						while (reader.Result.Read()) {
							this.EventStreams.Add(new EventStream(int.Parse(reader.Result["event_info_id"].ToString())));
						}
					} else {
						reader.Result.Close();
						//throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {

			}*/



		}

    }
}