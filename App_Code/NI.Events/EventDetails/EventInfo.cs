﻿using System;
using System.Collections.Generic;
using System.Web;
using Ni.Generic.Measurement;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;

namespace Ni.Events.EventDetails
{
    public enum EventStatus
    {
        Open = 1,
        Closed = 2,
        OpenToCharities = 3,
        ReminderService = 4,
        PriorityEntry = 5,
        Ballot = 6
    }
   
    /// <summary>
    /// Summary description for EventInfo
    /// </summary>
    public class EventInfo
    {
        int _id;
        int _event_id;
        string _eventName;
        DateTime _publicationDate;
        DateTime _eventDate;
        Distance _distance;
        string _startTime;
        string _locationString;
        string _locationRef;
        string _distanceTitle;
        string _stdPrice;
        string _mpuAd;
        string _leaderboard;
        int _streamId;
        EventStatus _status;
        int _ap16EventId;



        /// <summary>
        /// Default constructor with empty values
        /// </summary>
        public EventInfo()
        {
            _id = -1;
            _event_id = -1;
            _streamId = -1;
            _ap16EventId = -1;

            _eventName = "";
            _locationRef = "";
            _locationString = "";
            _startTime = "";
            _stdPrice = "";
            _mpuAd = "";
            _leaderboard = "";

            _publicationDate = new DateTime();
            _eventDate = new DateTime();

            _distance = new Distance(-1);
            _status = EventStatus.Closed;
        }

        /// <summary>
        /// returns the current active information for the given event
        /// </summary>
        /// <param name="evt">pass in a base event class and get the active details</param>
        /// <returns></returns>
        public static EventInfo GetActive(Event evt)
        {
            EventInfo result;

            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
            SqlParam[] paras = { 
                   new SqlParam("@id", SqlDbType.Int, evt.Id),
                   new SqlParam("@date", SqlDbType.DateTime, DateTime.Today.Date)
                           };
            SqlDataReaderResponse reader = connection.RunCommand("ni_select_active_event_details", paras);
            if (reader.ReturnCode == 0)
            {
                if (reader.Result.Read())
                {
                    result = new EventInfo();
                    result._id = Convert.ToInt32(reader.Result["event_info_id"]);
                    result._event_id = Convert.ToInt32(reader.Result["event_info_event_id"]);
                    result._ap16EventId = Convert.ToInt32(reader.Result["event_info_ap16_id"]);
                    result._streamId = Convert.ToInt32(reader.Result["event_info_stream_id"]);
                    result._eventName = reader.Result["event_info_event_name"].ToString();
                    result._locationRef = reader.Result["event_info_location_ref"].ToString();
                    result._locationString = reader.Result["event_info_location_string"].ToString();
                    result._publicationDate = Convert.ToDateTime(reader.Result["event_info_publication_date"]);
                    result._eventDate = Convert.ToDateTime(reader.Result["event_info_event_date"]);
                    result._startTime = reader.Result["event_info_start_time"].ToString();
                    result._distanceTitle = reader.Result["event_info_distance_title"].ToString();
                    result._status = (EventStatus)Convert.ToInt32(reader.Result["event_info_status"]);
                    result._distance = new Distance(Convert.ToDouble(reader.Result["event_info_distance"]));
                    result._stdPrice = reader.Result["event_info_std_price"].ToString();
                    result._leaderboard = reader.Result["event_info_banner_id"].ToString();
                    result._mpuAd = reader.Result["event_info_skyscraper_id"].ToString();
                }
                else
                {
                    reader.Result.Close();
                    throw new Exception("The was no information found in the database.");
                }
                reader.Result.Close();
            }
            else
            {
                throw new Exception(reader.ErrorMessage);
            }
            return result;
        }

        /// <summary>
        /// returns all event informaion (Assumes there is only one main event)
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        public static List<EventInfo> GetAll()
        {
            List<EventInfo> list = new List<EventInfo>();
            

            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
            SqlParam[] paras = {   };
            SqlDataReaderResponse reader = connection.RunCommand("gs_get_event_information_all", paras);
            if (reader.ReturnCode == 0)
            {
                while (reader.Result.Read())
                {
                   EventInfo result = new EventInfo();
                    result._id = Convert.ToInt32(reader.Result["event_info_id"]);
                    result._event_id = Convert.ToInt32(reader.Result["event_info_event_id"]);
                    result._ap16EventId = Convert.ToInt32(reader.Result["event_info_ap16_id"]);
                    result._streamId = Convert.ToInt32(reader.Result["event_info_stream_id"]);
                    result._eventName = reader.Result["event_info_event_name"].ToString();
                    result._locationRef = reader.Result["event_info_location_ref"].ToString();
                    result._locationString = reader.Result["event_info_location_string"].ToString();
                    result._publicationDate = Convert.ToDateTime(reader.Result["event_info_publication_date"]);
                    result._eventDate = Convert.ToDateTime(reader.Result["event_info_event_date"]);
                    result._startTime = reader.Result["event_info_start_time"].ToString();
                    result._distanceTitle = reader.Result["event_info_distance_title"].ToString();
                    result._status = (EventStatus)Convert.ToInt32(reader.Result["event_info_status"]);
                    result._distance = new Distance(Convert.ToDouble(reader.Result["event_info_distance"]));
                    result._stdPrice = reader.Result["event_info_std_price"].ToString();
                    result._leaderboard = reader.Result["event_info_banner_id"].ToString();
                    result._mpuAd = reader.Result["event_info_skyscraper_id"].ToString();
                    list.Add(result);
                }
                if (reader.Result.IsClosed == false) reader.Result.Close();
            }
           
            return list;
        }
        #region Properties
        /// <summary>
        /// gets the eventInformation id
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        /// <summary>
        /// gets the event id
        /// </summary>
        public int EventId
        {
            get { return _event_id; }
            set { _event_id = value; }
        }

        /// <summary>
        /// gets/sets the publication date
        /// </summary>
        public DateTime PublicationDate
        {
            get { return _publicationDate; }
            set { _publicationDate = value; }
        }

        /// <summary>
        /// gets/sets the event name
        /// </summary>
        public string EventName
        {
            get { return _eventName; }
            set { _eventName = value; }
        }

        /// <summary>
        /// gets/sets the event date
        /// </summary>
        public DateTime EventDate
        {
            get { return _eventDate; }
            set { _eventDate = value; }
        }

        /// <summary>
        /// gets/sets the race length
        /// </summary>
        public Distance RaceLength
        {
            get { return _distance; }
            set { _distance = value; }
        }

        /// <summary>
        /// gets/sets the race location string
        /// </summary>
        public string RaceLocation
        {
            get { return _locationString; }
            set { _locationString = value; }
        }

        /// <summary>
        /// gets/sets the race location ref
        /// </summary>
        public string LocationRef
        {
            get { return _locationRef; }
            set { _locationRef = value; }
        }

        /// <summary>
        /// gets/sets the human readable distance title (e.g. 10K, Marathon...)
        /// </summary>
        public string DistanceTitle
        {
            get { return _distanceTitle; }
            set { _distanceTitle = value; }
        }
        /// <summary>
        /// gets/sets the human readable start time (e.g. 9:00am)
        /// </summary>
        public string StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }


        /// <summary>
        /// gets/sets the main entry stream id
        /// </summary>
        public int StreamId
        {
            get { return _streamId; }
            set { _streamId = value; }
        }
        /// <summary>
        /// gets/sets the AP16 event id - initially required for charity tiered listings
        /// </summary>
        public int Ap16Id
        {
            get { return _ap16EventId; }
            set { _ap16EventId = value; }
        }

        /// <summary>
        /// gets or sets the event status
        /// </summary>
        public EventStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        /// <summary>
        /// standard price
        /// </summary>
        public string StandardPrice
        {
            get { return _stdPrice; }
            set { _stdPrice = value; }
        }

        /// <summary>
        /// MPU Banner
        /// </summary>
        public string MpuAd
        {
            get { return _mpuAd; }
            set { _mpuAd = value; }
        }

        /// <summary>
        /// leaderboard
        /// </summary>
        public string Leaderboard
        {
            get { return _leaderboard; }
            set { _leaderboard = value; }
        }

        #endregion

    }
}
