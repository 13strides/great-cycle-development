﻿using System;
using System.Collections.Generic;
using System.Web;
using Ni.Generic.Measurement;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for EventContent
/// </summary>
namespace Ni.Events.EventContent
{
	public class EventContent
	{




		//public int _eventID;
		 public int _pageID;
		 public int _contentID;
		 public DateTime _publicationDate;
		 public DateTime _expirationDate;
		public string _title;
		public string _content;
		public string _excerpt;
        private string _friendlyUrl;




		public int order;
		string language ="";
		public string userid = "";

		public string lastedit = "";
		public int tempid;
		public string thumbnail = "";
		public int status;
		public bool pinned;






		public DateTime PubDate {
			get { return _publicationDate; }
		}

		public int ContentID {
			get { return _contentID; }

		}
		public int PageID {
			get { return _pageID; }

		}
		public int Status {
			get { return status; }

		}


        public string FriendlyUrl {
            get { return _friendlyUrl; }
            set { _friendlyUrl = value; }
        }


		public EventContent() {
			//_eventID = -1;
			_pageID = -1;
			_contentID = -1;


		}

		//Get currentcontent for contentID
		public EventContent(int pageid,int contentID) {

			if (contentID != -1) {
				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
				SqlParam[] paras = { 
                   new SqlParam("@contentID", SqlDbType.Int, contentID)
                           };

				SqlDataReaderResponse reader = connection.RunCommand("ni_select_event_page_content", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.Read()) {
						_contentID = contentID;
						_pageID = int.Parse(reader.Result["content_page_id"].ToString());
						_title = reader.Result["content_title"].ToString();
						_content = reader.Result["content_article"].ToString();
						_excerpt = reader.Result["content_excerpt_content"].ToString();
						_publicationDate = Convert.ToDateTime(reader.Result["content_publication_date"].ToString());
						_expirationDate = Convert.ToDateTime(reader.Result["content_expiration_date"].ToString());
						language = reader.Result["content_language"].ToString();
						status = int.Parse(reader.Result["content_status"].ToString());
                        _friendlyUrl = reader.Result["content_friendly_url"].ToString();
					} else {
						reader.Result.Close();
						throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {
				_pageID = pageid;
				_contentID = -1;
			}



		}

        public EventContent(string friendlyUrl, bool currentOnly)
        {

            if (!string.IsNullOrEmpty(friendlyUrl))
            {
                MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
                SqlParam[] paras = { 
                   new SqlParam("@content_friendly_url", SqlDbType.NVarChar, friendlyUrl)
                           };
                string procedureName = "get_ni_event_page_content_by_friendly";
                if (currentOnly)
                {
                    procedureName = "get_ni_event_page_content_current_by_friendly";
                }
                SqlDataReaderResponse reader = connection.RunCommand(procedureName, paras);
                if (reader.ReturnCode == 0)
                {
                    if (reader.Result.Read())
                    {
                        _contentID = int.Parse(reader.Result["content_id"].ToString());
                        _pageID = int.Parse(reader.Result["content_page_id"].ToString());
                        _title = reader.Result["content_title"].ToString();
                        _content = reader.Result["content_article"].ToString();
                        _excerpt = reader.Result["content_excerpt_content"].ToString();
                        _publicationDate = Convert.ToDateTime(reader.Result["content_publication_date"].ToString());
                        _expirationDate = Convert.ToDateTime(reader.Result["content_expiration_date"].ToString());
                        language = reader.Result["content_language"].ToString();
                        status = int.Parse(reader.Result["content_status"].ToString());
                        _friendlyUrl = reader.Result["content_friendly_url"].ToString();
                        reader.Result.Close();
     				} 
                }
                else
                {
                    throw new Exception(reader.ErrorMessage);
                }
            }
            else
            {
                _contentID = -1;
                _friendlyUrl = friendlyUrl;
            }



        }
		public EventContent(int pageid) {


			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras = { 
                   new SqlParam("@contentID", SqlDbType.Int, pageid)
                           };

			SqlDataReaderResponse reader = connection.RunCommand("ni_select_event_page_content_current", paras);
			if (reader.ReturnCode == 0) {
				if (reader.Result.Read()) {
					//_eventID = eventID;
					_contentID= int.Parse(reader.Result["content_id"].ToString());
					_pageID = pageid;
					_title = reader.Result["content_title"].ToString();
					_content = reader.Result["content_article"].ToString();
				} else {
					reader.Result.Close();
					throw new Exception("The was no information found in the database.");
				}
				reader.Result.Close();
			} else {
				throw new Exception(reader.ErrorMessage);
			}




		}

		//Get specific content from a pageid and content id



		public bool Save() {

			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras = { 
                   new SqlParam("@id", SqlDbType.Int, _contentID),
                   new SqlParam("@pageID", SqlDbType.Int, _pageID),

				   new SqlParam("@order", SqlDbType.Int, order),
new SqlParam("@language", SqlDbType.VarChar,10, language),
new SqlParam("@publicationDate", SqlDbType.DateTime, _publicationDate),
new SqlParam("@expirationDate", SqlDbType.DateTime, _expirationDate),
new SqlParam("@userId", SqlDbType.VarChar,256, userid),
new SqlParam("@article", SqlDbType.Text, _content),
new SqlParam("@lastEdit", SqlDbType.VarChar,128, lastedit),
new SqlParam("@templateId", SqlDbType.Int, tempid),
new SqlParam("@title", SqlDbType.VarChar,256, _title),
new SqlParam("@excerptContent", SqlDbType.Text, _excerpt),
new SqlParam("@thumbnail", SqlDbType.VarChar,128, thumbnail),
new SqlParam("@pinned", SqlDbType.Bit, pinned),
new SqlParam("@status", SqlDbType.Int, status),
new SqlParam("@friendlyUrl", SqlDbType.NVarChar,128, _friendlyUrl)



                           };


			Int32Response result = connection.RunInt32Command("ni_save_event_content", paras);
			if (result.ReturnCode >= 0) {
				_contentID = result.ReturnCode;
				return true;
			} else {
				return false;
			}
		}
	}
}
