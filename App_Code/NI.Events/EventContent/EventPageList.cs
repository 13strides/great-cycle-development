﻿using System;
using System.Collections.Generic;
using System.Web;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;
/// <summary>
/// Summary description for EventPageList
/// </summary>
/// 
namespace Ni.Events.EventPage {
	public class EventPageList {

		public List<EventPage> Pages;

		public EventPageList(int EventID) {

			if (EventID != -1) {
				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
				SqlParam[] paras = { 
                   new SqlParam("@eventID", SqlDbType.Int, EventID)
                           };
				Pages = new List<EventPage>();
				SqlDataReaderResponse reader = connection.RunCommand("ni_select_event_pages_for_event", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.Read()) {
						this.Pages.Add(new EventPage(EventID, int.Parse(reader.Result["pageId"].ToString()), false));

						while (reader.Result.Read()) {
							this.Pages.Add(new EventPage(EventID, int.Parse(reader.Result["pageId"].ToString()),false));
						}
					} else {
						reader.Result.Close();
						throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {

			}



		}
	}


}
