﻿using System;
using System.Collections.Generic;
using System.Web;
using Ni.Generic.Measurement;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for EventContent
/// </summary>
namespace Ni.Events.EventContentList
{
	public class EventContentList
	{

		public List<EventContent.EventContent> pageVersions;

		public EventContentList(int pageID) {
			if (pageID != -1) {
				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
				SqlParam[] paras = { 
                   new SqlParam("@pageID", SqlDbType.Int, pageID)
                           };

				SqlDataReaderResponse reader = connection.RunCommand("ni_select_event_page_versions_for_page_list", paras);
				if (reader.ReturnCode == 0) {
					pageVersions = new List<EventContent.EventContent>();
					if (reader.Result.Read()) {
						this.pageVersions.Add(new EventContent.EventContent(pageID, int.Parse(reader.Result["content_id"].ToString())));

						while (reader.Result.Read()) {
							this.pageVersions.Add(new EventContent.EventContent(pageID, int.Parse(reader.Result["content_id"].ToString())));
						}
					} else {
						reader.Result.Close();
						//throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {

			}



		}
	}
}
