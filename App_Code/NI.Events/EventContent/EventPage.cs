﻿using System;
using System.Collections.Generic;
using System.Web;
using Ni.Generic.Measurement;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for EventContent
/// </summary>
namespace Ni.Events.EventPage {
	public class EventPage {

		public int _eventID;
		 public int _pageID;
		 public string _pageName;


		 public string PageName {
			 get { return _pageName; }

		 }
		 public int PageID {
			 get { return _pageID; }

		 }

		public string _currentContent;
		public string _currentTitle;
		public int _sortorder;
		public int _contentid;

		

		public EventPage() {
			_eventID = -1;
			_pageID = -1;
			_pageName = "";
			_sortorder = 0;

		}

		//Create New page for the given event
		public EventPage(int eventID) {
			_eventID = eventID;
			_pageID = -1;
			_pageName = "";
			_sortorder = 0;
		}


		//Get existing page-- option for getting the front end content (short cut approach for quick db response)
		public EventPage(int eventID, int pageID, bool getCurrentContent) {

			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras = { 
                   new SqlParam("@eventID", SqlDbType.Int, eventID),
                   new SqlParam("@pageID", SqlDbType.Int, pageID),
                   new SqlParam("@forFrontEnd", SqlDbType.Bit, getCurrentContent)
                           };

			SqlDataReaderResponse reader = connection.RunCommand("ni_select_event_page", paras);
			if (reader.ReturnCode == 0) {
				if (reader.Result.Read()) {
					_eventID = eventID;
					_pageID = pageID;
					_pageName = reader.Result["page_name"].ToString();
					_currentContent = reader.Result["CurrentContent"].ToString();
					 _currentTitle = reader.Result["Title"].ToString();
					_contentid = int.Parse( reader.Result["content_id"].ToString());
				} else {
					reader.Result.Close();
					throw new Exception("The was no information found in the database.");
				}
				reader.Result.Close();
			} else {
				throw new Exception(reader.ErrorMessage);
			}
		}


		public bool Save() {

			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras = { 
                   new SqlParam("@eventID", SqlDbType.Int, _eventID),
                   new SqlParam("@pageID", SqlDbType.Int, _pageID),
                   new SqlParam("@pageName", SqlDbType.NVarChar,50, _pageName),
                   new SqlParam("@sortorder", SqlDbType.Int, _sortorder)
                           };


			Int32Response result = connection.RunInt32Command("ni_save_event_page", paras);
			if (result.ReturnCode >= 0) {
				_pageID = result.ReturnCode;
				return true;
			} else {
				return false;
			}
		}


	}
}
