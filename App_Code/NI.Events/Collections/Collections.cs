﻿using System;
using System.Collections.Generic;
using System.Web;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;
/// <summary>
/// Summary description for Collections
/// </summary>
/// 
namespace NI.Events.Collections {
	public enum EventCollectionType {
		Celeb,
		Sponsor,
		Partner,
		Elite,
		FooterSponsor
	}


	public class EventCollectionItemBind {

		public EventCollectionItemBind(int eventId, int itemId,int order) {

				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
				SqlParam[] paras = { 
                   new SqlParam("@itemID", SqlDbType.Int, itemId),
				   new SqlParam("@eventID", SqlDbType.Int, eventId),
				   new SqlParam("@order", SqlDbType.Int, order)
				 
                           };

				SqlDataReaderResponse reader = connection.RunCommand("gs_save_event_collection_bind", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.Read()) {

					} else {
						reader.Result.Close();
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}

		}
	
	}

	public class EventCollectionItemUnBind {

		public EventCollectionItemUnBind(int eventId, int itemId) {
			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras = { 
                   new SqlParam("@itemID", SqlDbType.Int, itemId),
				   new SqlParam("@eventID", SqlDbType.Int, eventId)
                           };

			SqlDataReaderResponse reader = connection.RunCommand("gs_unbind_event_collection_bind", paras);
			if (reader.ReturnCode == 0) {
				if (reader.Result.Read()) {

				} else {
					reader.Result.Close();
				}
				reader.Result.Close();
			} else {
				throw new Exception(reader.ErrorMessage);
			}

		}

	}



	public class EventCollectionItem {

		public int _partnerId;
		public string _partnerName;
		public string _partnerDesc;
		public string _partnerImage;
		public EventCollectionType _partnerType;
		public bool _partnerActive;
		public string _partner_url;

		public string PartnerName{get {return _partnerName;}}
		public int PartnerID { get { return _partnerId; } }



		public EventCollectionItem(int itemId) {
			if (itemId != -1) {
				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
				SqlParam[] paras = { 
                   new SqlParam("@itemID", SqlDbType.Int, itemId)
                           };

				SqlDataReaderResponse reader = connection.RunCommand("gs_get_event_collection_item", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.Read()) {
						_partnerId = itemId;
						_partnerName = reader.Result["partner_name"].ToString();
						_partnerDesc = reader.Result["partner_desc"].ToString();
						_partnerImage = reader.Result["partner_image"].ToString();
						_partner_url = reader.Result["partner_link"].ToString();
						//event_code
						//_eventcode = reader.Result["event_code"].ToString();

						_partnerType = (EventCollectionType)int.Parse(reader.Result["partner_type"].ToString());
						_partnerActive = Convert.ToBoolean(reader.Result["active"].ToString());

					} else {
						reader.Result.Close();
						throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {

				_partnerId = -1;
			}












		}


	}
	public class TypeEventCollection {
		public List<EventCollectionItem> ItemList;

		public TypeEventCollection(int typeID) {
			if (typeID != -1) {
				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
				SqlParam[] paras = { 
                   new SqlParam("@typeID", SqlDbType.Int, typeID)
                           };

				SqlDataReaderResponse reader = connection.RunCommand("gs_get_event_collection_byType", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.HasRows) {
						ItemList = new List<EventCollectionItem>();
						while (reader.Result.Read()) {
							this.ItemList.Add(new EventCollectionItem(int.Parse(reader.Result["partnerid"].ToString())));
						}


					} else {
						reader.Result.Close();
						throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {

			}

		}
	}


	public class EventEventCollection {
		public List<EventCollectionItem> ItemList;

		public EventEventCollection(int eventId) {
			if (eventId != -1) {
				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
				SqlParam[] paras = { 
                   new SqlParam("@eventId", SqlDbType.Int, eventId)
                           };

				SqlDataReaderResponse reader = connection.RunCommand("gs_get_event_collection_byEvent", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.HasRows) {
						ItemList = new List<EventCollectionItem>();
						while (reader.Result.Read()) {
							this.ItemList.Add(new EventCollectionItem(int.Parse(reader.Result["partnerid"].ToString())));
						}


					} else {
						reader.Result.Close();
						throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {

			}
		}
	}

	public class EventTypeEventCollection {
		public List<EventCollectionItem> ItemList;
		public int _typeId;
		public int _eventId;
		public string _eventcode;

		public EventTypeEventCollection(int typeID, int eventId) {
			if (typeID != -1 || eventId != -1) {
				MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
				SqlParam[] paras = { 
                   new SqlParam("@typeID", SqlDbType.Int, typeID),
				   new SqlParam("@eventId", SqlDbType.Int, eventId)
                           };
				_typeId = typeID;
				_eventId  =eventId;
				SqlDataReaderResponse reader = connection.RunCommand("gs_get_event_collection_byEventType", paras);
				if (reader.ReturnCode == 0) {
					if (reader.Result.HasRows) {
						ItemList = new List<EventCollectionItem>();
						while (reader.Result.Read()) {
							this.ItemList.Add(new EventCollectionItem(int.Parse(reader.Result["partnerid"].ToString())));
							_eventcode = reader.Result["event_code"].ToString();

						}


					} else {
						reader.Result.Close();
						//throw new Exception("The was no information found in the database.");
					}
					reader.Result.Close();
				} else {
					throw new Exception(reader.ErrorMessage);
				}
			} else {

			}

		}
	}


}
