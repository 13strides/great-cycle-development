﻿using System;
using System.Collections.Generic;
using System.Web;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;
/// <summary>
/// Summary description for HomePageRow
/// </summary>
/// 
namespace Gs.Generic.HomePage {
	public class HomePageRowsAvailable {
		public List<HomePageRow> AllValidRowTypes;
		public HomePageRowsAvailable() {
			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras2 = {  };
			SqlDataReaderResponse reader = connection.RunCommand("ni_select_Available_Row_Controls", paras2);
			if (reader.ReturnCode == 0) {
				if (reader.Result.HasRows) {
					AllValidRowTypes = new List<HomePageRow>();

					while (reader.Result.Read()) {
						this.AllValidRowTypes.Add(new HomePageRow(int.Parse(reader.Result["controlID"].ToString())));
					}
				} else {
					reader.Result.Close();
					//throw new Exception("The was no information found in the database.");
				}
				reader.Result.Close();
			} else {
				throw new Exception(reader.ErrorMessage);
			}



		}

	}


	public class HomePageRow {


		public int _controlId;
		public string _controlPath;
		public string _controlName;
		public string RowDescription { get { return _controlName; } }


		public HomePageRow(int controlID ) {
			//
			// TODO: Add constructor logic here
			//

			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras2 = { 
						 new SqlParam("@controlID", SqlDbType.Int, controlID)

                           };
			SqlDataReaderResponse reader2 = connection.RunCommand("ni_select_rowControl", paras2);
			if (reader2.ReturnCode == 0) {
				if (reader2.Result.Read()) {
					_controlId = int.Parse(reader2.Result["controlId"].ToString());
					//_publicationDate = Convert.ToDateTime(reader2.Result["publishDate"].ToString());
					_controlPath = reader2.Result["WebControlPath"].ToString();
					_controlName = reader2.Result["Description"].ToString();
				} else {
					reader2.Result.Close();
					//throw new Exception("The was no information found in the database.");
				}
				reader2.Result.Close();
			} else {
				throw new Exception(reader2.ErrorMessage);
			}





		}
	}
}