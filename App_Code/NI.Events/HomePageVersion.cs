﻿using System;
using System.Collections.Generic;
using System.Web;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Configuration;
using System.Data;
/// <summary>
/// Summary description for HomePageVersion
/// </summary>
/// 
namespace Gs.Generic.HomePage {



	public class AllHomePageVersions {
		public List<HomePageVersion> hpVersions;


		public AllHomePageVersions() {
			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras2 = { 
						

                           };
			SqlDataReaderResponse reader = connection.RunCommand("ni_select_GetHomePageVersions", paras2);
			if (reader.ReturnCode == 0) {
				if (reader.Result.HasRows) {
					hpVersions = new List<HomePageVersion>();

					while (reader.Result.Read()) {
						this.hpVersions.Add(new HomePageVersion(int.Parse(reader.Result["versionId"].ToString())));
					}
				} else {
					reader.Result.Close();
					//throw new Exception("The was no information found in the database.");
				}
				reader.Result.Close();
			} else {
				throw new Exception(reader.ErrorMessage);
			}
		}
	}




	public class HomePageVersion {
		
		public List<HomePageRow> pageRows;
		public int _versionId;
		public DateTime _publicationDate;
		public int _status;
		public string _description;


		public string Description { get { return _description; } }
		public int Version { get { return _versionId; } }
		public DateTime PublicationDate { get { return _publicationDate; } }
		public int Status { get { return _status; } }


		/*SP List
		 *  [ni_select_HomePageVersion_rowList] //List Of Controls in version  @versionId
		 *  [ni_select_HomePageVersion_info] // Home page info @versionId
		 * ni_select_HomePageVersion_info_current // Home page info 
		 * 
		 * 
		 * 
		 * */

		public HomePageVersion() {
			//
			// TODO: Add constructor logic here
			//Get Current Version
			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras2 = { 
                           };
			SqlDataReaderResponse reader2 = connection.RunCommand("ni_select_HomePageVersion_info_current", paras2);
			if (reader2.ReturnCode == 0) {
				if (reader2.Result.Read()) {
				_versionId = int.Parse(reader2.Result["versionId"].ToString());
				_publicationDate = Convert.ToDateTime(reader2.Result["publishDate"].ToString());
				_status= int.Parse(reader2.Result["homepageStatus"].ToString());
				_description= reader2.Result["Description"].ToString();
				} else {
					reader2.Result.Close();
					//throw new Exception("The was no information found in the database.");
				}
				reader2.Result.Close();
			} else {
				throw new Exception(reader2.ErrorMessage);
			}
			GetRowList();
		}


		public HomePageVersion(int versionId) {
			//
			// TODO: Add constructor logic here
			//Get specific version Version

			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras2 = { 
						 new SqlParam("@versionId", SqlDbType.Int, versionId)

                           };
			SqlDataReaderResponse reader2 = connection.RunCommand("ni_select_HomePageVersion_info", paras2);
			if (reader2.ReturnCode == 0) {
				if (reader2.Result.Read()) {
					_versionId = int.Parse(reader2.Result["versionId"].ToString());
					_publicationDate = Convert.ToDateTime(reader2.Result["publishDate"].ToString());
					_status = int.Parse(reader2.Result["homepageStatus"].ToString());
					_description = reader2.Result["Description"].ToString();
				} else {
					reader2.Result.Close();
					//throw new Exception("The was no information found in the database.");
				}
				reader2.Result.Close();
			} else {
				throw new Exception(reader2.ErrorMessage);
			}
			GetRowList();
		}

		protected void GetRowList() {
			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
			SqlParam[] paras2 = { 
						 new SqlParam("@versionId", SqlDbType.Int, _versionId)

                           };
			SqlDataReaderResponse reader = connection.RunCommand("ni_select_HomePageVersion_rowList", paras2);
			if (reader.ReturnCode == 0) {
				if (reader.Result.HasRows) {
					pageRows = new List<HomePageRow>();

					while (reader.Result.Read()) {
						this.pageRows.Add(new HomePageRow(int.Parse(reader.Result["controlID"].ToString())));
					}
				} else {
					reader.Result.Close();
					//throw new Exception("The was no information found in the database.");
				}
				reader.Result.Close();
			} else {
				throw new Exception(reader.ErrorMessage);
			}








		}



	}
}