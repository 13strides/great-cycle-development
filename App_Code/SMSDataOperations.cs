﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace SMSRegistration
{
    /// <summary>
    /// Summary description for DataOperations
    /// </summary>
    public class SMSDataOperations
    {
        private string _connString = ConfigurationManager.ConnectionStrings["SqlServerConnection"].ConnectionString;

        /// <summary>
        /// Gets a list of Races from the stgnova.race table, populated into a drop down list
        /// </summary>
        /// <returns>DataSet of Race Information</returns>
        public DataSet GetRaces()
        {
            DataSet raceData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    SqlDataAdapter raceDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    //cmd.CommandText = "SELECT Description,idRace FROM Race r with (nolock),event e with (nolock), eventtype et with (nolock)  WHERE r.Event_idEvent = e.idEvent and e.idEventType = et.idEventType and et.TypeName like '" + ConfigurationManager.AppSettings["EventType"].ToString() + "%' and RaceDate > GETDATE() and RunnerSMS > 0 ORDER BY RaceDate ASC";
		    cmd.CommandText = "SELECT Description,idRace FROM Race r with (nolock),event e with (nolock), eventtype et with (nolock)  WHERE r.Event_idEvent = e.idEvent and e.idEventType = et.idEventType and et.TypeName like '" + ConfigurationManager.AppSettings["EventType"].ToString() + "%' and RaceDate > GETDATE() and runnersms > 0 ORDER BY RaceDate ASC";
                    conn.Open();
                    raceDataAdapter.SelectCommand = cmd;
                    raceDataAdapter.Fill(raceData);
                }
            }

            return raceData;

        }

        public DataSet GetRaceRegistrationDetails(int idRaceRegistration, int idRace)
        {
            DataSet registrationData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    SqlDataAdapter registrationDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * from person p with (nolock), raceregistration rr with (nolock) where rr.idraceregistration=@IDRACEREGISTRATION and rr.idperson = p.idperson and rr.registrationstatus=10 and rr.paymentstatus=2 and rr.idrace=@IDRACE";
                    cmd.Parameters.AddWithValue("@IDRACEREGISTRATION", idRaceRegistration);
                    cmd.Parameters.AddWithValue("@IDRACE", idRace);
                    conn.Open();
                    registrationDataAdapter.SelectCommand = cmd;
                    registrationDataAdapter.Fill(registrationData);
                }
            }

            return registrationData;

        }

        public DataSet GetStoredNumbers(int idRaceRegistration)
        {
            DataSet numberData = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    SqlDataAdapter numberDataAdapter = new SqlDataAdapter();
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT idRaceSMS, MobileNo FROM RaceSMS with (nolock) WHERE idRaceRegistration = @IDRACEREGISTRATION order by idracesms";
                    cmd.Parameters.AddWithValue("@IDRACEREGISTRATION", idRaceRegistration);
                    conn.Open();
                    numberDataAdapter.SelectCommand = cmd;
                    numberDataAdapter.Fill(numberData);
                }
            }

            return numberData;
        }

        public bool UpdateMobileNumber(int idRaceSms, string mobileNumber)
        {
            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "UPDATE RaceSms SET idPaymentStatus=2, smsStatus=0, MobileNo=@MOBILENO WHERE idRaceSms = @IDRACESMS";
                    cmd.Parameters.AddWithValue("@IDRACESMS", idRaceSms);
                    cmd.Parameters.AddWithValue("@MOBILENO", mobileNumber);
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    return true;
                }
            }

        }

        public bool AddMobileNumber(int idRaceRegistration, string mobileNumber)
        {
            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "INSERT INTO racesms (idRaceRegistration, mobileNo, idPaymentStatus, smsStatus, sendCount) VALUES (@IDRACEREGISTRATION, @MOBILENO, 2, 0, 0)";
                    cmd.Parameters.AddWithValue("@IDRACEREGISTRATION", idRaceRegistration);
                    cmd.Parameters.AddWithValue("@MOBILENO", mobileNumber);
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    return true;
                }
            }

        }

        public bool ClearMobileNumber(int idRaceSms)
        {
            using (SqlConnection conn = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "UPDATE RaceSms SET smsStatus=2, MobileNo='', Description='' WHERE idRaceSms = @IDRACESMS";
                    cmd.Parameters.AddWithValue("@IDRACESMS", idRaceSms);
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    return true;
                }
            }

        }


    }

}