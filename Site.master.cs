﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using GS.Generic.Events;
using System.Web.UI.HtmlControls;

public partial class Site : System.Web.UI.MasterPage
{
    string tknCookie;
    protected void Page_Init(Object senfer, EventArgs e)
    {
        Page.Header.DataBind();  
        if (Request.Cookies["GreatCycleCookiesAccepted"] != null)
        {
            //cookie exists check value and enable other cookies
            if (Request.Cookies["GreatCycleCookiesAccepted"].Value == "true")
            {
                //cookies accepted add analytics and check for link cookie 
                if (Request.QueryString["tkn"] != null)
                {
                    string tknCookie = Request.QueryString["tkn"].ToString();
                    CreateCookie(tknCookie);
                }

            }
        }
        else
        {
            //accepted cookie doesn't exist
            //show the panel to request permission

            string homepage = Request.ServerVariables.Get("PATH_INFO").ToLower();
            if (homepage == "/default.aspx" || homepage == "/")
            {
                cookiePanel.Visible = true;
                //htmlTag.Attributes.Add("style", "background: #013485 url(../App_Images/CssImages/gradient-home.jpg) no-repeat center 52px;");
            }

        }


    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                addMeta(SiteMap.CurrentNode.Title, SiteMap.CurrentNode.Description, SiteMap.CurrentNode.ResourceKey, SiteMap.CurrentNode.ParentNode.Title);
            }
            catch
            {
                addMeta(SiteMap.CurrentNode.Title, SiteMap.CurrentNode.Description, SiteMap.CurrentNode.ResourceKey, "");
            }
            string linkadd = "";
            if (tknCookie != String.Empty && tknCookie != null)
            {
                linkadd = "&" + tknCookie;
            }

        }

    }
    private bool IsDescendantOf(SiteMapNode node, string title)
    {
        if (node.ParentNode != null)
        {
            if (node.ParentNode.Title == title)
            {
                return true;
            }
            else
            {
                return IsDescendantOf(node.ParentNode, title);
            }
        }
        else
        {
            return false;
        }
    }
    private void addMeta(string _title, string _description, string _keywords, string _optSubTitle)
    {
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();

        if (_optSubTitle == "EVENT")
        {
            Page.Title = Server.HtmlEncode(_title) + " | The Event | 13.1 Miles, 26.2 Miles &amp; 52 Miles cycling event in Manchester | Great Manchester Cycle ";
            keywords.Name = "keywords";
            keywords.Content = _keywords;

            description.Name = "description";
            description.Content = _description;

            Page.Header.Controls.Add(description);
            Page.Header.Controls.Add(keywords);
        }
        else
        {
           // Page.Title = Server.HtmlEncode(_title) + " | 13.1 Miles, 26.2 Miles &amp; 52 Miles cycling event in Manchester | Great Manchester Cycle ";
        }
        // h1Literal.Text = "The National Lottery Olympic Park Run";

       
    }
    private void CreateCookie(string cookieValue)
    {
        HttpCookie linkCookie = new HttpCookie("CycleCookie");
        linkCookie.Expires = DateTime.Now.AddDays(1);
        linkCookie.Values.Add("tkn", cookieValue);
        Response.Cookies.Add(linkCookie);
    }
    protected void AcceptedClicked(Object sender, EventArgs e)
    {
        if (AcceptCookiesBox.Checked)
        {
            CookiePolicy();

            cookiePanel.Visible = false;

            string homepage = Request.ServerVariables.Get("PATH_INFO").ToLower();
            if (homepage == "/default.aspx" || homepage == "/")
            {
                //htmlTag.Attributes.Remove("style");
            }
        }
    }
    private void CookiePolicy()
    {
        HttpCookie CookiePolicy = new HttpCookie("GreatCycleCookiesAccepted", "true");
        CookiePolicy.Expires = DateTime.Now.AddYears(2);
        //CookiePolicy.Values.Add("Accepted", "true");
        Response.Cookies.Add(CookiePolicy);
    }


    private void RegisterTrackCodes()
    {
        /*  
          facebookLink.Attributes.Add("onClick", "javascript:pageTracker._trackPageview('/EXIT/SPONSOR/Facebook.html');");*/
    }
  
   
}
