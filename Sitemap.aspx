﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Sitemap.aspx.cs" Inherits="Sitemap" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="row pageheading">
		<div class="span6">
			<h1>Sitemap</h1>
			<ul class="breadcrumb">
				<li><a href="/">Home</a> <span class="divider">/</span></li>
				Sitemap
			</ul>
		</div>
		<asp:HyperLink runat="server" ID="EnterOnlineLink" NavigateUrl="/Event/" CssClass="btn btn-primary btn-large">Enter Now &raquo;</asp:HyperLink>
		<!--<a href="/Event/" class="btn btn-primary btn-large">Enter Now »</a>-->
	</div>
    
    <div class="row">
    <div class="span11">
    <ul style="list-style-type:none;">
    	<li><a href="/Event"><h2>Distances</h2></a></li>
        <li><a href="/Blog"><h2>Cycle Blog</h2></a></li>
        <li><a href="/News"><h2>News</h2></a></li>
        <li><a href="/Charities"><h2>Cycle for Charity</h2></a></li>
        <li><a href="/Results"><h2>Results</h2></a></li>
        <li><a href="/Photos"><h2>Photos</h2></a></li>
        <li><a href="/Merchandise"><h2>Merchandise</h2></a></li>
        <li><a href="/FAQs"><h2>FAQs</h2></a></li>
        <li><a href="/Contact"><h2>Contact</h2></a></li>
        </ul>
        </div>
    </div>

     <ol style="display:none;">
        <li class="first"><asp:HyperLink runat="server" ID="breadcrumb1Link" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></li>
        <li><strong>Sitemap</strong></li>
    </ol>
    <div id="sitemapLayout" style="display:none;">
        <h1>Sitemap</h1>
        <asp:Literal runat="server" ID="siteMapLiteral" />
        <div class="clearboth"></div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" Runat="Server">
</asp:Content>
