﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventDetails;
using ThirteenStrides.Database;
using Ni.Generic.Measurement;
using GS.Generic.Events;
using Ni.Events.EventContent;
using Ni.Events.EventContentList;
using Ni.Events.EventPage;
using Ni.Events.StreamManagement;
public partial class Admin_Events_EventInformation : System.Web.UI.Page
{
    EventInfo _activeInfo;
    Event _currentEvent;
	EventPageList _pageList;

    protected void Page_Load(object sender, EventArgs e)
    {

            try
            {
                _currentEvent = new Event(7);

                //addLink.NavigateUrl="~/Admin/Events/information_new.aspx?evnt=" + _currentEvent.Id.ToString();
				hypNewPageLink.NavigateUrl = "page_new.aspx?id=7";
				_pageList = new EventPageList(_currentEvent.Id);
                _activeInfo = EventInfo.GetActive(_currentEvent);
				
            }
            catch(Exception ex2)
            {

            }

        if (!IsPostBack)
        {
            BindEventDetails();
            BindEventSubStreams();
        }
    }

    protected void BindEventSubStreams()
    {
       /* EventContainerEventList eventlist = new EventContainerEventList(_currentEvent.Id);
        
        foreach (int eventId in eventlist.subevents)
        {
			EventStreamContainer thisSubEvent = new EventStreamContainer(eventId);

			Literal thisLit = new Literal();
			thisLit.Text ="<a href=\"ViewStreamInfo.aspx?id="+eventId.ToString()+"\">"+thisSubEvent._event_name+"</a>";
			if (thisSubEvent.currentStream != null) {
				thisLit.Text += " " + thisSubEvent.currentStream.StreamID.ToString() + " " + (SwimEventStatus)thisSubEvent.currentStream.Status;
			} else {
				thisLit.Text += " No Stream Information"; 
			}
			thisLit.Text += "<br />";
			//thisSubEvent._event_name;


			/*  Admin_Admin_controls_EventStreamList newList = new Admin_Admin_controls_EventStreamList();
				 newList.SubEventID = eventId;*/
		//	pnlEventStreamLists.Controls.Add(thisLit);
			
        //}*/
    }
	protected string FormatCurrentPage(object id) {
		int pageID = int.Parse(id.ToString());

		try {
			EventPage pageContent = new EventPage(_currentEvent.Id, pageID, true);

			if (pageContent._contentid != null && pageContent._contentid > 0) {
				return "<a href=\"Content_edit.aspx?cid=" + pageContent._contentid.ToString() + "\">" + pageContent._pageName + "</a>";
			} else {

				return pageContent._pageName;
			}
		} catch {
			return "Issue";
		}

	}

	protected string FormatContentList(object id) {
		int pageID = int.Parse(id.ToString());
		EventContentList pageContentList = new EventContentList(pageID);
		string returner = "";
		foreach (EventContent ec in pageContentList.pageVersions)
		{
			//returner += "<tr style=\"display:table-row;\"><td></td><td></td><td><a href=\"Content_edit.aspx?cid=" + ec.ContentID.ToString() + "\">";
			
			// kev edited to try to shorten the page length
			
			if (ec.PubDate >= DateTime.Now) {
				returner += "<a href=\"Content_edit.aspx?cid=" + ec.ContentID.ToString() + "\" class=\"pageVersion future\">";
				returner += "<span class=\"month\">" + ec.PubDate.Date.ToString("MMM") + "</span><span class=\"day\">" 
					+ ec.PubDate.Date.Day.ToString() + "</span><span class=\"year\">" 
					+ ec.PubDate.Date.Year.ToString() + "</span>";
				//returner += ec.PubDate.Date.ToString();
				returner += "</a>";
			} else {
				returner += "<a href=\"Content_edit.aspx?cid=" + ec.ContentID.ToString() + "\" class=\"pageVersion past\">";
				returner += "<span class=\"month\">" + ec.PubDate.Date.ToString("MMM") + "</span><span class=\"day\">"
					+ ec.PubDate.Date.Day.ToString() + "</span><span class=\"year\">"
					+ ec.PubDate.Date.Year.ToString() + "</span>";
				//returner += ec.PubDate.Date.ToString();
				returner += "</a>";
			}
			
			
			//returner += "</a></td></tr>";
		}
		return returner;
	}



    
    /// this function displays the event information but the fields are disabled
    /// 


    private void BindEventDetails()
    {
        try
        {
			pageList.DataSource = _pageList.Pages;
			pageList.DataBind();
			/*
            nameTextbox.Text = _activeInfo.EventName;
            dateTextbox.Text = _activeInfo.EventDate.Date.ToString();
            statusTextbox.Text = _activeInfo.Status.ToString();
            distanceTextbox.Text = _activeInfo.RaceLength.Metres.ToString();
            distanceDesTextbox.Text = _activeInfo.DistanceTitle;
            locationTextbox.Text = _activeInfo.RaceLocation;
            locationRefTextbox.Text = _activeInfo.LocationRef;
            stdAdultPriceTextbox.Text = _activeInfo.StandardPrice;
            startTimeTextbox.Text = _activeInfo.StartTime;
            ap16StreamIdTextbox.Text = _activeInfo.Ap16Id.ToString();
            entryStreamTextbox.Text = _activeInfo.StreamId.ToString();
            mpuTextbox.Text = _activeInfo.MpuAd;
            leaderboardTextbox.Text = _activeInfo.Leaderboard;
		*/
	
        }
        catch(Exception ex)
        {

        }

    }
}
