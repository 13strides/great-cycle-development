﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventDetails;
using ThirteenStrides.Database;
using Ni.Generic.Measurement;
using GS.Generic.Events;
using Ni.Events.EventContent;
using Ni.Events.EventContentList;
using Ni.Events.EventPage;
using Ni.Events.StreamManagement;


public partial class Admin_Events_information_new : System.Web.UI.Page
{

	public EventStreamContainer eventCont;
    protected void Page_Load(object sender, EventArgs e)
    {
	//	EventStreamContainer eventCont;
		if ((Request.QueryString["id"] != null) && (Request.QueryString["id"] != string.Empty)) {
			try {
				//_currentEvent = new Event(Convert.ToInt32(Request.QueryString["id"]));
				//_currentStream = new EventStream(-1,Convert.ToInt32(Request.QueryString["id"]));
				//_currentStreamContainer = new EventStreamContainer(Convert.ToInt32(Request.QueryString["id"]));
				//addLink.NavigateUrl = "~/Admin/Events/information_new.aspx?evnt=" + _currentEvent.Id.ToString();
				//_pageList = new EventPageList(_currentEvent.Id);
				//_activeInfo = EventInfo.GetActive(_currentEvent);

				eventCont = new EventStreamContainer(int.Parse(Request.QueryString["id"]));
				lblEventName.Text = eventCont._event_name;
				streamList.SubEventID = int.Parse(Request.QueryString["id"]);
			} catch (Exception ex2) {
				lblEventName.Text = ex2.Message;
			}
		} else {
			Response.Redirect("Default.aspx");
		}
		if (!IsPostBack) {
			BindEventInfo();
		}
    }

	protected void BindEventInfo() {
		/*
nameTextbox.Text = _activeInfo.EventName;
dateTextbox.Text = _activeInfo.EventDate.Date.ToString();
statusTextbox.Text = _activeInfo.Status.ToString();
distanceTextbox.Text = _activeInfo.RaceLength.Metres.ToString();
distanceDesTextbox.Text = _activeInfo.DistanceTitle;
locationTextbox.Text = _activeInfo.RaceLocation;
locationRefTextbox.Text = _activeInfo.LocationRef;
stdAdultPriceTextbox.Text = _activeInfo.StandardPrice;
startTimeTextbox.Text = _activeInfo.StartTime;
ap16StreamIdTextbox.Text = _activeInfo.Ap16Id.ToString();
entryStreamTextbox.Text = _activeInfo.StreamId.ToString();
mpuTextbox.Text = _activeInfo.MpuAd;
leaderboardTextbox.Text = _activeInfo.Leaderboard;
*/

		txtEventName.Text = eventCont._event_name;
		txtEventLocation.Text = eventCont.event_info_location_string;
		txtEventDistance.Text = eventCont.event_info_distance.ToString();
		txtEventLocation.Text = eventCont.event_info_location_string;
		txtEventStatTime.Text = eventCont.event_info_start_time.ToString();
		txtEventTemp.Text = eventCont.event_info_temp.ToString();
		txtSkyScraperID.Text = eventCont.event_info_skyscraper_id;
		txtAP16ID.Text = eventCont.event_info_ap16_id.ToString();
		txtBannerId.Text = eventCont.event_info_banner_id;
		txtEvemtPrice.Text = eventCont.event_info_std_price;

	}

	protected void Button1_Click1(object sender, EventArgs e) {
		 eventCont._event_name = txtEventName.Text ;
		 eventCont.event_info_location_string = txtEventLocation.Text ;
		 eventCont.event_info_distance= int.Parse(txtEventDistance.Text);
		 eventCont.event_info_location_string = txtEventLocation.Text;
		 eventCont.event_info_start_time = int.Parse(txtEventStatTime.Text);
		 eventCont.event_info_temp = int.Parse(txtEventTemp.Text);
		 eventCont.event_info_skyscraper_id = txtSkyScraperID.Text;
		 eventCont.event_info_ap16_id = int.Parse(txtAP16ID.Text);
		 eventCont.event_info_banner_id = txtBannerId.Text;
		 eventCont.event_info_std_price = txtEvemtPrice.Text;

		 eventCont.Save();
	}
}
