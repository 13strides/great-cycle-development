﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="______Default.aspx.cs" Inherits="Admin_Events_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    
    <div class="content-box">
        <div class="content-box-header">
            <h3>Choose an event</h3>
        </div>
        
        <div class="content-box-content">
        <asp:Literal runat="server" ID="messageLiteral" /></asp:Literal>
            <asp:Repeater runat="server" ID="eventslist">
                <ItemTemplate>
                 <asp:HyperLink runat="server" ID="EventHyperlink" NavigateUrl='<%# "~/Admin/Events/EventInformation.aspx?id=" + Eval("event_id") %>'
                  style="display:block;background:url(../images/logoBackground.gif) no-repeat top left;padding:23px 25px;float:left;">
                <img src="../Admin_images/EventLogos/GreatSwim/<%# Eval("event_code") %>.png" alt="<%# Eval("Event_name") %>" title="<%# Eval("Event_name") %>" /></asp:HyperLink>
                </ItemTemplate>
            </asp:Repeater>
            
                
                <div class="clear"></div>
        </div>
    </div>

</asp:Content>

