﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="CollectionInformation.aspx.cs" Inherits="Admin_Events_EventInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    <div class="content-box column-left main">
        <div class="content-box-header">
            <h3>Event Collection</h3>
        </div>
        <div class="content-box-content">  
             <table class="">
                <thead>
                    <tr>
						<th>Type</th>
                        <th>Count</th>
                    </tr>
                </thead>
             
              
             
                    
                    <tr>
						<td>
							<asp:HyperLink ID="HyperLink1" NavigateUrl="ManageCollection.aspx?type=1&id=" runat="server">Celebrity</asp:HyperLink></td>
                        <td>
							<asp:Label ID="lblCelebCount" runat="server" Text="Label"></asp:Label></td>
                    </tr>
					
					            
                    <tr>
						<td><asp:HyperLink ID="HyperLink2" NavigateUrl="ManageCollection.aspx?type=4&id=" runat="server">Elite</asp:HyperLink></td>
                        <td><asp:Label ID="lblEliteCount" runat="server" Text="Label"></asp:Label></td>
                    </tr>
					
					
					            
                    <tr>
						<td><asp:HyperLink ID="HyperLink3" NavigateUrl="ManageCollection.aspx?type=2&id=" runat="server">Sponsor</asp:HyperLink></td>
                        <td><asp:Label ID="lblSponsorCount" runat="server" Text="Label"></asp:Label></td>
                    </tr>
					
					
					            
                    <tr>
						<td><asp:HyperLink ID="HyperLink4" NavigateUrl="ManageCollection.aspx?type=3&id=" runat="server">Partner</asp:HyperLink></td>
                        <td><asp:Label ID="lblPartnerCount" runat="server" Text="Label"></asp:Label></td>
                    </tr>
					                    <tr>
						<td><asp:HyperLink ID="HyperLink5" NavigateUrl="ManageCollection.aspx?type=5&id=" runat="server">Footer</asp:HyperLink></td>
                        <td><asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></td>
                    </tr>
					
					
                
             
         
                               
            </table>
            
        
        



            
        </div>
    </div>
    <div class="content-box column-right sidebar">
  
        
    </div>
</asp:Content>

