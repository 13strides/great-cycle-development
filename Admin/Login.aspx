﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Admin_Login" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <!--<h2><img src="images/icons/user_32.png" alt="Login" />Login</h2>-->
			
	<div id="login">
		
		<div class="content-box">
			<div class="content-box-header">
				<h3>Login</h3>
			</div>
		
			<div class="content-box-content">
			    <asp:Login runat="server" ID="login" Visible="true" OnAuthenticate="OnLoginClk" RememberMeText="remember"></asp:Login>
				<asp:Button runat="server" ID="loginBtn" OnClick="LoginClk" Text="LOGIN" Visible="false" />
			
			</div>
		</div><!-- end .content-box -->
	</div><!-- end #login -->

    
	
</asp:Content>

