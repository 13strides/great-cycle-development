﻿<%@ Page Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_News_Default" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    
    <div class="content-box">
        <div class="content-box-header">
            <h3>News</h3>
            
            
            
        </div>
        
        <div class="content-box-content"><ul>
                <li><asp:HyperLink runat="server" ID="AddNewsLink" NavigateUrl="~/admin/News/AddNews.aspx">Add News</asp:HyperLink></li>
            </ul><gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
            <asp:Repeater runat="server" ID="newsListRepeater" OnItemCommand="ItemCommandHandler" OnItemDataBound="AddDeleteConfirmation">
            <HeaderTemplate>
                <table cellpadding="0" class="pagination" rel="10">
                    <thead>
                    <tr>
                        <th>#id</th>
                        <th>Title</th>
                        <th>Event</th>
                        <th>Publication Date</th>
                        <th>Expiration Date</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr></thead>
                    <tfoot>
                        <tr>
							<td colspan="7">									</td>
						</tr>
                    </tfoot>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="std-row">
                    <th><%# Eval("news_id") %></th>
                    <td><%# Eval("news_title") %></td>
                    <td><%# Eval("event_name") %></td>
                    <%-- <td><%# FormatStatus(Eval("news_Status")) %></td>--%>
                    <td><%# Eval("news_publication_date", "{0:dd/MM/yyyy}") %></td>
                    <td><%# Eval("news_expiration_date", "{0:dd/MM/yyyy}") %></td>
                    <td><asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# Eval("news_id") %>' CommandName="edit" Text="edit" /></td>
				    <td><asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("news_id") %>' CommandName="delete" Text="delete" /></td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt-row">
                    <th><%# Eval("news_id") %></th>
                    <td><%# Eval("news_title") %></td>
                    <td><%# Eval("event_name") %></td>
                    <%-- <td><%# FormatStatus(Eval("news_Status")) %></td>--%>
                    <td><%# Eval("news_publication_date", "{0:dd/MM/yyyy}") %></td>
                    <td><%# Eval("news_expiration_date", "{0:dd/MM/yyyy}") %></td>
                    <td><asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# Eval("news_id") %>' CommandName="edit" Text="edit" /></td>
				    <td><asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("news_id") %>' CommandName="delete" Text="delete" /></td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                
            </table>
            </FooterTemplate>
        </asp:Repeater>
        <gr:RepeaterPager runat="server" ID="pager" PageSize="50" CssClass="pager" CurrentPageLinkCssClass="current-page-link" LinksCssClass="page-link" NextPreviousCssClass="next-prev-link" NextPreviousDisabledCssClass="next-prev-disabled"></gr:RepeaterPager>

        </div>
    </div>
    
    
    </asp:Content>

