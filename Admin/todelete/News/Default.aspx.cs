﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using GS.Generic.Membership;
using ThirteenStrides.Database;
using ThirteenStrides.Web;
using GS.Generic.Web;

public partial class admin_News_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindData();
    }
    protected string HightlightNew(Object count)
    {
        if (count != DBNull.Value)
        {
            int c = (int)count;
            if (c > 0)
            {
                return "<span style=\"color: red; font-weight: bold\">" + c.ToString() + "</span>";
            }
            else
            {
                return c.ToString();
            }
        }
        else
        {
            return "0";
        }
    }

    protected void AddDeleteConfirmation(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {
            // adds a confirmation javascript to ensure that deleting the template is really what the user wants.
            ((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this news?')";
        }
    }

    private void BindData()
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = {  };

        DataSetResponse res = connection.RunCommand("gs_select_news", paras, "news");

        if (res.ReturnCode == 0)
        {
            pager.ControlToPage = newsListRepeater;
            pager.DataSource = res.Result.Tables["news"].DefaultView;
            pager.DataBind();
        }
        else
        {
            infoMessage.Text = "There was an error looking up for news in the database.";
            infoMessage.Type = InfoType.Error;
        }
    }

    protected string FormatStatus(Object status)
    {
        NewsStatus s = (NewsStatus)Convert.ToInt32(status);
        switch (s)
        {
            case NewsStatus.Draft:
                return "<span class=\"status-draft\">" + s.ToString() + "</span>";
                break;
            case NewsStatus.Live:
                return "<span class=\"status-live\">" + s.ToString() + "</span>";
                break;
            default:
            case NewsStatus.New:
                return "<span class=\"status-new\">" + s.ToString() + "</span>";
                break;
        }
    }

    protected void ItemCommandHandler(Object sender, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "edit":
                Response.Redirect("EditNews.aspx?nid=" + e.CommandArgument.ToString());
                break;
            case "delete":
                DeleteNews(Convert.ToInt32(e.CommandArgument));
                break;
        }
    }

  

    private void DeleteNews(int id)
    {
        if (id != -1)
        {
            try
            {
                MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
                SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
                Int32Response result = connection.RunInt32Command("gs_delete_news", paras);

                if (result.ReturnCode != 0)
                {
                    throw new Exception(result.ErrorMessage);
                }
            }
            catch
            {
                infoMessage.Text = "News Deleted";
                infoMessage.Type = InfoType.Success;
                BindData();
            }
        }
        else
        {
#if DEBUG
				throw new Exception("The news was never saved into the database.");
#endif
            infoMessage.Text = "there was an error and the news hasn't been deleted.";
            infoMessage.Type = InfoType.Error;

        }
        BindData();
    }
}
