﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gs.Generic.HomePage;



public partial class Admin_Homepage_Preview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

		int contentID;

		if (!IsPostBack) {
			if (Request.QueryString["cid"] == null || Request.QueryString["cid"].ToString() == "") {
				Response.Redirect("Default.aspx");

			} else {
				contentID = int.Parse(Request.QueryString["cid"].ToString());



				HomePageVersion ThisVersion = new HomePageVersion(contentID);
				foreach (HomePageRow row in ThisVersion.pageRows) {

					UserControl newControl = (UserControl)LoadControl("~" + row._controlPath);
					Panel1.Controls.Add(newControl);
				}
			}



		}




    }
}
