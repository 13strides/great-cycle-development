﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventContent;
using GS.Generic.Web;
using GS.Generic.Membership;
using GS.Generic.Events;

public partial class Admin_Events_Content_edit : System.Web.UI.Page
{
	static EventContent thisContent;
    protected void Page_Load(object sender, EventArgs e)
    {
		int contentID;

		if (!IsPostBack) {
			if (Request.QueryString["cid"] == null || Request.QueryString["cid"].ToString() == "") {
				Response.Redirect("Default.aspx");

			} else {
				contentID = int.Parse(Request.QueryString["cid"].ToString());
				//try {
				thisContent = new EventContent(0, contentID);
				contentTextBox.Text = thisContent._content;
				titleTextBox.Text = thisContent._title;
				summaryTextBox.Text = thisContent._excerpt;
				ddlStatus.SelectedValue = thisContent.status.ToString();
				//ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(thisContent.status.ToString())); 
				publicationDate.SelectedDate = thisContent._publicationDate;
				expirationDate.SelectedDate = thisContent._expirationDate;
				
				//} catch {
				//Response.Redirect("Default.aspx");
				//	}
			}
		}

    }
	protected void btnSave_Click(object sender, EventArgs e) {
		thisContent._content = contentTextBox.Text;
		thisContent._excerpt = summaryTextBox.Text;
		thisContent._publicationDate = publicationDate.SelectedDate;
		thisContent._expirationDate = expirationDate.SelectedDate;
		thisContent.status = int.Parse(ddlStatus.SelectedValue);
		thisContent.Save();
	}
}
