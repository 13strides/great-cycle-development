﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Admin_Events_EventInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    <div class="content-box  main">
        <div class="content-box-header">
            <h3>Event Content</h3>
        </div>
        <div class="content-box-content">
        
             <asp:Repeater runat="server" ID="pageList">
             
             <HeaderTemplate>
             <table class="">
                <thead>
                    <tr>
						<th>Page ID</th>
                        <th>Title</th>
                        <th>Page Version Starting</th>
                    </tr>
                </thead>
             
              
             </HeaderTemplate>
                <ItemTemplate>
                    
                    
                    
                    <tr>
						<td><%#  DataBinder.Eval(Container.DataItem,"PageID") %></td>
                        <td><%# FormatCurrentPage(DataBinder.Eval(Container.DataItem, "PageID"))%>
							</td>
                        <td><%# FormatContentList(DataBinder.Eval(Container.DataItem, "PageID"))%><a title="Create New Scheduled version" href='Content_new.aspx?pid=<%#  DataBinder.Eval(Container.DataItem,"PageID") %>'>
								<img src="../images/icons/plus.png" alt="Create New Scheduled version" /></a></td>
                    </tr>
					
                
             
                </ItemTemplate>
                <FooterTemplate>
                               
            </table>
                </FooterTemplate>
            </asp:Repeater>
            
        
        



             <img src="../images/icons/plus.png" /> 
             <asp:HyperLink runat="server" ID="hypNewPageLink" NavigateUrl="~/Admin/Events/page_new.aspx">New page</asp:HyperLink>
            
        </div>
    </div>
    
    
    
       <div class="content-box main">
        <div class="content-box-header">
            <h3>Page Rows</h3>
        </div>
        <div class="content-box-content">
        <ni:HomePageRowVersions runat="server" id="hpversion" />
        </div>
       </div>
       

</asp:Content>

