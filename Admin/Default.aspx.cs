﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Gr.Model;
using GS.Generic.Web;

public partial class Admin_Default : System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			BindCarouselRepeater();
		}
	}

	private void BindCarouselRepeater() {
		List<HeroSlider> hs = HeroSlider.GetSlider(0, false);
		if (hs != null && hs.Count > 0) {
			heroSlides.DataSource = hs;
			heroSlides.DataBind();
		} else {
			heroSlides.DataSource = null;
			heroSlides.DataBind();
			//infoMessage.Text = "There are no forms for this business challenge.";
			//infoMessage.Type = InfoType.Error;
		}
	}

	protected void ItemCommandHandler(Object sender, RepeaterCommandEventArgs e) {
		switch (e.CommandName) {
			case "edit":
				RedirectForEdition(e.CommandArgument.ToString());
				break;
			case "delete":
				//DeleteNews(Convert.ToInt32(e.CommandArgument));
				break;
		}
	}

	protected void AddDeleteConfirmation(Object sender, RepeaterItemEventArgs e) {
		if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
			// adds a confirmation javascript to ensure that deleting the template is really what the user wants.
			((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this news?')";
		}
	}

	protected string FormatStatus(Object status) {
		NewsStatus s = (NewsStatus)Convert.ToInt32(status);
		switch (s) {
			case NewsStatus.Draft:
				return "<span class=\"status-draft\">" + s.ToString() + "</span>";
				break;
			case NewsStatus.Live:
				return "<span class=\"status-live\">" + s.ToString() + "</span>";
				break;
			default:
			case NewsStatus.New:
				return "<span class=\"status-new\">" + s.ToString() + "</span>";
				break;
		}
	}

	protected string GetImagePath(Object path) {
		string imageName = path.ToString();

		return "<img src=\"/App_Files/ImageLibrary/NewsMedium/" + imageName + "\" alt=\"\" width=\"200\" />";
	}

	private void RedirectForEdition(string evt) {
		string[] info = evt.Split('#');
		if (info.Length == 2) {
			Response.Redirect("/Admin/HeroSlider/edit_slide.aspx?sid=" + info[1]);
		} else {
			//infoMessage.Text = "There was an error locating the news.";
			//infoMessage.Type = InfoType.Error;
		}
	}
}