﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventDetails;
using ThirteenStrides.Database;
using Ni.Generic.Measurement;
using GS.Generic.Events;
using Ni.Events.EventContent;
using Ni.Events.EventContentList;
using Ni.Events.EventPage;

public partial class Admin_Events_information_new : System.Web.UI.Page
{

	EventInfo _activeInfo;
	Event _currentEvent;
	EventPageList _pageList;


    protected void Page_Load(object sender, EventArgs e)
    {
		
				_currentEvent = new Event(7);

				//addLink.NavigateUrl = "~/Admin/Events/information_new.aspx?evnt=" + _currentEvent.Id.ToString();
				//_pageList = new EventPageList(_currentEvent.Id);
				//_activeInfo = EventInfo.GetActive(_currentEvent);

		if (!IsPostBack) {
			//lblEventName.Text = _currentEvent.Name;
		}
    }
	protected void Button1_Click(object sender, EventArgs e) {
		EventPage _newPage = new EventPage(_currentEvent.Id);
		_newPage._pageName = Textbox1.Text;
		_newPage._sortorder = int.Parse(Textbox2.Text);
		
		if (_newPage.Save()) {
			Response.Redirect("Default.aspx");
		}
	}
}
