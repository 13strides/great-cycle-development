﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="old_Default.aspx.cs" Inherits="Admin_HomePage_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="content-box column-left main">
        <div class="content-box-header">
            <h3>Event Content</h3>
        </div>
        <div class="content-box-content">
        
             <asp:Repeater runat="server" ID="pageList">
             
             <HeaderTemplate>
             <table class="">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Page Version Starting</th>
                    </tr>
                </thead>
             
              
             </HeaderTemplate>
                <ItemTemplate>
                    
                    
                    
                    <tr>
                        <td><%# DataBinder.Eval(Container.DataItem, "PageName")%></td>
                        <td><a href='Content_new.aspx?pid=<%#  DataBinder.Eval(Container.DataItem,"PageID") %>'>Create New Content </a></td>
                    </tr>
					<%# FormatContentList(DataBinder.Eval(Container.DataItem, "PageID"))%>
                
             
                </ItemTemplate>
                <FooterTemplate>
                               
            </table>
                </FooterTemplate>
            </asp:Repeater>
            
        
        



             <img src="../images/icons/plus.png" /> 
             <asp:HyperLink runat="server" ID="hypNewPageLink" NavigateUrl="~/Admin/Events/page_new.aspx">New page</asp:HyperLink>
            
        </div>
    </div>
    <div class="content-box column-right sidebar">
        <div class="content-box-header">
            <h3>Event Config</h3>
        </div>
        
        <div class="content-box-content">
            <fieldset>
								<p>
									<label>Event Name:</label>
									<asp:Textbox ID="nameTextbox" runat="server" CssClass="" disabled="disabled" />
<span class="notification"></span>
									<label>Event Date:</label>
								  <asp:Textbox ID="dateTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
									<span class="notification"></span>
								
									<label>Status:</label>
									<asp:Textbox ID="statusTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
									<span class="notification"></span>
									<label>Event Distance:</label>
									<asp:Textbox ID="distanceTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
								
									<label>Distance Title:</label>
									<asp:Textbox ID="distanceDesTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
								<span class="notification"></span>
									<label>Location:</label>
									<asp:Textbox ID="locationTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
								<span class="notification"></span>
									<label>Location Ref:</label>
									<asp:Textbox ID="locationRefTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
								<span class="notification"></span>
									<label>Adult Price:</label>
									<asp:Textbox ID="stdAdultPriceTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>Start Time:</label>
									<asp:Textbox ID="startTimeTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>AP16 stream id:</label>
									<asp:Textbox ID="ap16StreamIdTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>Entry Stream:</label>
									<asp:Textbox ID="entryStreamTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>MPU Ad:</label>
									<asp:Textbox ID="mpuTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>Leaderboard Ad:</label>
									<asp:Textbox ID="leaderboardTextbox" runat="server" CssClass="datepicker" disabled="disabled" /><span class="notification"></span>
								</p>
                            </fieldset>
            <div class="clear"></div>
            <img src="../images/icons/plus.png" /> <asp:HyperLink runat="server" ID="addLink" NavigateUrl="~/Admin/Events/information_new.aspx">Schedule status change</asp:HyperLink>
        </div>
        
    </div>
</asp:Content>

