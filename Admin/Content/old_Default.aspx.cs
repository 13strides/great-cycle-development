﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventDetails;
using ThirteenStrides.Database;
using Ni.Generic.Measurement;
using GS.Generic.Events;
using Ni.Events.EventContent;
using Ni.Events.EventContentList;
using Ni.Events.EventPage;
public partial class Admin_HomePage_Default : System.Web.UI.Page
{
    EventInfo _activeInfo;
    Event _currentEvent;
	EventPageList _pageList;

    protected void Page_Load(object sender, EventArgs e)
    {

            try
            {
                _currentEvent = new Event(1);

               // addLink.NavigateUrl="~/Admin/Events/information_new.aspx?evnt=" + _currentEvent.Id.ToString();
				hypNewPageLink.NavigateUrl = "~/Admin/Homepage/page_new.aspx?id=" + _currentEvent.Id.ToString();
				_pageList = new EventPageList(_currentEvent.Id);
                _activeInfo = EventInfo.GetActive(_currentEvent);
				
            }
            catch(Exception ex2)
            {

            }
       
        if (!IsPostBack)
        {
            BindEventDetails();
        }
    }





	protected string FormatContentList(object id) {
		int pageID = int.Parse(id.ToString());
		EventContentList pageContentList = new EventContentList(pageID);
		string returner = "";
		foreach (EventContent ec in pageContentList.pageVersions)
		{
			returner += "<tr style=\"display:table-row;\"><td></td><td><a href=\"Content_edit.aspx?cid="+ ec.ContentID.ToString() + "\">";
			returner += ec.PubDate.Date.ToString();
			returner += "</a></td></tr>";
		}
		return returner;
	}



    
    /// this function displays the event information but the fields are disabled
    /// 


    private void BindEventDetails()
    {
        try
        {
			pageList.DataSource = _pageList.Pages;
			pageList.DataBind();
				
	
        }
        catch(Exception ex)
        {

        }

    }
}
