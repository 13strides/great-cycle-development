﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="ViewRowSet.aspx.cs" Inherits="Admin_Events_row_view" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
   <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
   
    <div class="content-box">
        <div class="content-box-header"><h3>New stream page - 
			<asp:Label ID="lblEventName" runat="server" Text="Label"></asp:Label></h3></div>
        <div class="content-box-content">
            <table id="editForm" class="">
                <thead>
                    <tr>
                        <th width="15%">Setting</th>
                        <th width="70%">Value</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tr>
                    <td>Description</td>
                    <td><asp:Label ID="Textbox1" runat="server" CssClass="" />
</td>
                </tr>
                <tr>
                    <td>Start Date:</td>
                    <td><asp:Label ID="publicationDate" runat="server" /></td>
                </tr>
                <tr>
                    <td>Rows:</td>
                    <td>
</td>
                </tr>
				<asp:Repeater ID="Repeater1" runat="server">
				<HeaderTemplate></HeaderTemplate>
				<ItemTemplate>
				<tr>
				<td></td>
				<td><%# DataBinder.Eval(Container.DataItem, "RowDescription") %></td>
				</tr>
				
				</ItemTemplate>
				<FooterTemplate></FooterTemplate>
				</asp:Repeater>
                
                
            </table>
	
            

            
            
			<asp:Button ID="Button1" runat="server" Text="Back" PostBackUrl="~/Admin/Homepage/Default.aspx"/>
        
								
        </div>
    </div>

</asp:Content>

