﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Utils;
using GS.Generic.Events;
using GS.Generic.Membership;
using GS.Generic.Web;
using Ni.Events.EventContent;

public partial class Admin_Events_Content_edit : System.Web.UI.Page {
	static EventContent thisContent;

	protected void Page_Load(object sender, EventArgs e) {
		int contentID;

	
			if (Request.QueryString["cid"] == null || Request.QueryString["cid"].ToString() == "") {
				Response.Redirect("Default.aspx");
			} else {
				contentID = int.Parse(Request.QueryString["cid"].ToString());
				//try {
				thisContent = new EventContent(0, contentID);
                if(!Page.IsPostBack)
                {
				    contentTextBox.Text = thisContent._content;
				    titleTextBox.Text = thisContent._title;
				    //titleTextBox.Text = thisContent._currentTitle;
				    summaryTextBox.Text = thisContent._excerpt;
				    ddlStatus.SelectedValue = thisContent.status.ToString();
				    //ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(thisContent.status.ToString()));
				    publicationDate.SelectedDate = thisContent._publicationDate;
				    expirationDate.SelectedDate = thisContent._expirationDate;
				    friendlyTextBox.Text = thisContent.FriendlyUrl;
				}
			}
		
	}

	protected void btnSave_Click(object sender, EventArgs e) {
        try
        {
            thisContent._title = titleTextBox.Text;
            thisContent._content = contentTextBox.Text;
            thisContent._excerpt = summaryTextBox.Text;
            thisContent._publicationDate = publicationDate.SelectedDate;
            thisContent._expirationDate = expirationDate.SelectedDate;
            thisContent.userid = GS.Generic.GreatSwim.CurrentUser.Id;
            thisContent.status = int.Parse(ddlStatus.SelectedValue);
            if (!string.IsNullOrEmpty(friendlyTextBox.Text))
                thisContent.FriendlyUrl = StringManuplation.CleanStringForURL(friendlyTextBox.Text);
            else
                thisContent.FriendlyUrl = StringManuplation.CleanStringForURL(titleTextBox.Text);
            if (thisContent.Save())
            {
                editPanel.Visible = false;
                redirector.Visible = true;
            }
            else
            {
                infoMessage.Text = "The content didn't save";
                infoMessage.Type = InfoType.Error;
            }
        }
        catch (Exception ex)
        {
           
        }
	}
}