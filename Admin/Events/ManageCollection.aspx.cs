﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventDetails;
using ThirteenStrides.Database;
using Ni.Generic.Measurement;
using GS.Generic.Events;
using Ni.Events.EventContent;
using Ni.Events.EventContentList;
using Ni.Events.EventPage;
using Ni.Events.StreamManagement;
using NI.Events.Collections;
public partial class Admin_Events_EventInformation : System.Web.UI.Page
{
    EventInfo _activeInfo;
    Event _currentEvent;
	EventPageList _pageList;
	EventTypeEventCollection _eveCol;
	TypeEventCollection _typeCol;
	int _typeID;
    protected void Page_Load(object sender, EventArgs e)
    {
		if ((Request.QueryString["id"] != null) && (Request.QueryString["id"] != string.Empty)&&(Request.QueryString["type"] != null) && (Request.QueryString["type"] != string.Empty))
        {
            try
            {
                _currentEvent = new Event(Convert.ToInt32(Request.QueryString["id"]));

				try{
				_eveCol = new EventTypeEventCollection(Convert.ToInt32(Request.QueryString["type"]), _currentEvent.Id);
				}
				catch{}
				try{
					_typeID = Convert.ToInt32(Request.QueryString["type"]);
					_typeCol = new TypeEventCollection(_typeID);
					

				}
				catch{}
                //addLink.NavigateUrl="~/Admin/Events/information_new.aspx?evnt=" + _currentEvent.Id.ToString();
				//hypNewPageLink.NavigateUrl = "~/Admin/Events/page_new.aspx?id=" + _currentEvent.Id.ToString();
				//_pageList = new EventPageList(_currentEvent.Id);
              //  _activeInfo = EventInfo.GetActive(_currentEvent);

				
				
            }
            catch(Exception ex2)
            {

            }
        }
        else
        {
            Response.Redirect("Default.aspx");
        }
        if (!IsPostBack) {
			if (_eveCol != null) {
				GridView1.DataSource = _eveCol.ItemList;
				GridView1.DataBind();
			}
			DropDownList1.Items.Clear();
			if (_typeCol != null) {
				foreach (EventCollectionItem evi in _typeCol.ItemList) {
					DropDownList1.Items.Add(new ListItem(evi._partnerName, evi._partnerId.ToString()));

				}
			}
         //   BindEventDetails();
         //   BindEventSubStreams();
        }
    }

    protected void BindEventSubStreams()
    {
        EventContainerEventList eventlist = new EventContainerEventList(_currentEvent.Id);
        
        foreach (int eventId in eventlist.subevents)
        {
			EventStreamContainer thisSubEvent = new EventStreamContainer(eventId);

			Literal thisLit = new Literal();
			thisLit.Text ="<a href=\"ViewStreamInfo.aspx?id="+eventId.ToString()+"\">"+thisSubEvent._event_name+"</a>";
			if (thisSubEvent.currentStream != null) {
				thisLit.Text += " " + thisSubEvent.currentStream.StreamID.ToString() + " " + (SwimEventStatus)thisSubEvent.currentStream.Status;
			} else {
				thisLit.Text += " No Stream Information"; 
			}
			thisLit.Text += "<br />";
			//thisSubEvent._event_name;


			/*  Admin_Admin_controls_EventStreamList newList = new Admin_Admin_controls_EventStreamList();
				 newList.SubEventID = eventId;*/
		//	pnlEventStreamLists.Controls.Add(thisLit);
			
        }
    }
	protected string FormatCurrentPage(object id) {
		int pageID = int.Parse(id.ToString());

		try {
			EventPage pageContent = new EventPage(_currentEvent.Id, pageID, true);

			if (pageContent._contentid != null && pageContent._contentid > 0) {
				return "<a href=\"Content_edit.aspx?cid=" + pageContent._contentid.ToString() + "\">" + pageContent._pageName + "</a>";
			} else {

				return pageContent._pageName;
			}
		} catch {
			return "Issue";
		}

	}

	protected string FormatContentList(object id) {
		int pageID = int.Parse(id.ToString());
		EventContentList pageContentList = new EventContentList(pageID);
		string returner = "";
		foreach (EventContent ec in pageContentList.pageVersions)
		{
			//returner += "<tr style=\"display:table-row;\"><td></td><td></td><td><a href=\"Content_edit.aspx?cid=" + ec.ContentID.ToString() + "\">";
			
			// kev edited to try to shorten the page length
			
			if (ec.PubDate >= DateTime.Now) {
				returner += "<a href=\"Content_edit.aspx?cid=" + ec.ContentID.ToString() + "\" class=\"pageVersion future\">";
				returner += "<span class=\"month\">" + ec.PubDate.Date.ToString("MMM") + "</span><span class=\"day\">" 
					+ ec.PubDate.Date.Day.ToString() + "</span><span class=\"year\">" 
					+ ec.PubDate.Date.Year.ToString() + "</span>";
				//returner += ec.PubDate.Date.ToString();
				returner += "</a>";
			} else {
				returner += "<a href=\"Content_edit.aspx?cid=" + ec.ContentID.ToString() + "\" class=\"pageVersion past\">";
				returner += "<span class=\"month\">" + ec.PubDate.Date.ToString("MMM") + "</span><span class=\"day\">"
					+ ec.PubDate.Date.Day.ToString() + "</span><span class=\"year\">"
					+ ec.PubDate.Date.Year.ToString() + "</span>";
				//returner += ec.PubDate.Date.ToString();
				returner += "</a>";
			}
			
			
			//returner += "</a></td></tr>";
		}
		return returner;
	}



    
    /// this function displays the event information but the fields are disabled
    /// 


    private void BindEventDetails()
    {
        try
        {
		//	pageList.DataSource = _pageList.Pages;
		//	pageList.DataBind();
			/*
            nameTextbox.Text = _activeInfo.EventName;
            dateTextbox.Text = _activeInfo.EventDate.Date.ToString();
            statusTextbox.Text = _activeInfo.Status.ToString();
            distanceTextbox.Text = _activeInfo.RaceLength.Metres.ToString();
            distanceDesTextbox.Text = _activeInfo.DistanceTitle;
            locationTextbox.Text = _activeInfo.RaceLocation;
            locationRefTextbox.Text = _activeInfo.LocationRef;
            stdAdultPriceTextbox.Text = _activeInfo.StandardPrice;
            startTimeTextbox.Text = _activeInfo.StartTime;
            ap16StreamIdTextbox.Text = _activeInfo.Ap16Id.ToString();
            entryStreamTextbox.Text = _activeInfo.StreamId.ToString();
            mpuTextbox.Text = _activeInfo.MpuAd;
            leaderboardTextbox.Text = _activeInfo.Leaderboard;
		*/
	
        }
        catch(Exception ex)
        {

        }

    }
	protected void Button1_Click(object sender, EventArgs e) {
		EventCollectionItemBind evi = new EventCollectionItemBind(_currentEvent.Id, int.Parse(DropDownList1.SelectedValue), int.Parse(TextBox1.Text));
		Response.Redirect("ManageCollection.aspx?type=" + _typeID.ToString() + "&id=" + _currentEvent.Id.ToString());

	}

	protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e) {
		//Label1.Text = GridView1.DataKeys[e.RowIndex].Values[0].ToString();

		EventCollectionItemUnBind evi = new EventCollectionItemUnBind(_currentEvent.Id, int.Parse(GridView1.DataKeys[e.RowIndex].Values[0].ToString()));
		Response.Redirect("ManageCollection.aspx?type=" + _typeID.ToString()+ "&id=" + _currentEvent.Id.ToString());
	}
}
