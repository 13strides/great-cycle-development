﻿<%@ Page Title="" Language="C#" ValidateRequest="false" MasterPageFile="~/Admin/Admin.master"
	AutoEventWireup="true" CodeFile="BusinessChallengeFormView.aspx.cs" Inherits="Admin_Events_BusinessChallengeFormView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		<!--
		th{text-align:left !important;}
		-->
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				Edit Business Challenge Form</h3>
		</div>
		<div class="content-box-content">
			<table>
				<tr>
					<td>
						<asp:Literal ID="AddNewLink" runat="server" />
					</td>
				</tr>
			</table>
			<gs:Info runat="server" ID="infoMessage" CssClass="info" />
			<table>
				<tr>
					<th>
						Business Challenge:
					</th>
					<td>
						<asp:TextBox ID="businessChallengeTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Business Name:
					</th>
					<td>
						<asp:TextBox ID="businessNameTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Business Address 1:
					</th>
					<td>
						<asp:TextBox ID="businessAddress1TextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Business Address 2:
					</th>
					<td>
						<asp:TextBox ID="businessAddress2TextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Business Telephone:
					</th>
					<td>
						<asp:TextBox ID="businessTelephoneTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Business City:
					</th>
					<td>
						<asp:TextBox ID="businessCityTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Business Postcode:
					</th>
					<td>
						<asp:TextBox ID="businessPostcodeTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Business Country:
					</th>
					<td>
						<asp:TextBox ID="businessCountryTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Challenge Distance:
					</th>
					<td>
						<asp:TextBox ID="businessDistanceTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Number of Entrants:
					</th>
					<td>
						<asp:TextBox ID="entrantsTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Coordinator Name:
					</th>
					<td>
						<asp:TextBox ID="coordinatorNameTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Coordinator Telephone:
					</th>
					<td>
						<asp:TextBox ID="coordinatorTelephoneTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Coordinator Email:
					</th>
					<td>
						<asp:TextBox ID="coordinatorEmailTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Is Emailed:
					</th>
					<td>
						<asp:TextBox ID="isEmailedTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<asp:Button ID="BtnCancel" runat="server" OnClick="BtnCancel_Click" Text="Back" CssClass="button" />
					</td>
				</tr>
			</table>
		</div>
	</div>
</asp:Content>