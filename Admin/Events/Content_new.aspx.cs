﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventContent;
using GS.Generic.Web;
using GS.Generic.Membership;
using GS.Generic.Events;
using Gr.Utils;
public partial class Admin_Events_Content_new : System.Web.UI.Page
{


	static EventContent thisNewContent;
    protected void Page_Load(object sender, EventArgs e)
    {
		int pageID;

		if (!IsPostBack) {
			if (Request.QueryString["pid"] == null || Request.QueryString["pid"].ToString() == "") {
				Response.Redirect("Default.aspx");

			} else {
				pageID = int.Parse(Request.QueryString["pid"].ToString());
				//try {
				thisNewContent = new EventContent(pageID, -1);
				contentTextBox.Text = thisNewContent._content;
				titleTextBox.Text = thisNewContent._title;
				summaryTextBox.Text = thisNewContent._excerpt;
				publicationDate.SelectedDate = DateTime.Now;
				expirationDate.SelectedDate = DateTime.Now.AddYears(1);
				
				//} catch {
				//Response.Redirect("Default.aspx");
				//	}
			}
		}

    }
	protected void btnSave_Click(object sender, EventArgs e) {
		thisNewContent._content = contentTextBox.Text;
		thisNewContent._title = titleTextBox.Text;
		thisNewContent._excerpt = summaryTextBox.Text;
		thisNewContent._publicationDate = publicationDate.SelectedDate;
		thisNewContent._expirationDate = expirationDate.SelectedDate;
		thisNewContent.status = int.Parse(ddlStatus.SelectedValue);
        if (!string.IsNullOrEmpty(friendlyTextBox.Text))
            thisNewContent.FriendlyUrl = StringManuplation.CleanStringForURL(friendlyTextBox.Text);
        else
            thisNewContent.FriendlyUrl = StringManuplation.CleanStringForURL(titleTextBox.Text);   

		if (thisNewContent.Save())
		{
			Response.Redirect("Content_edit.aspx?cid=" + thisNewContent._contentID.ToString());
		}
	}
}
