﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="ManageCollection.aspx.cs" Inherits="Admin_Events_EventInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    <div class="content-box column-left main">
        <div class="content-box-header">
            <h3>Event Content</h3>
        </div>
        <div class="content-box-content">
        
			<asp:GridView DataKeyNames="PartnerID" ID="GridView1" AutoGenerateColumns="false" runat="server" onrowdeleting="GridView1_RowDeleting" 
				 >
				<Columns>
					<asp:BoundField HeaderText="ID" DataField="PartnerID" />
				    <asp:BoundField HeaderText="Name" DataField="PartnerName" />
					<asp:CommandField   ShowDeleteButton="True" />
					
				</Columns>
			</asp:GridView>
            
        
			<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>



             <img src="../images/icons/plus.png" /> 
             <asp:HyperLink runat="server" ID="hypNewPageLink" NavigateUrl="~/Admin/Events/page_new.aspx">New page</asp:HyperLink>
            
        </div>
    </div>
    <div class="content-box column-right sidebar">
        <div class="content-box-header">
            <h3>Event Information</h3>
        </div>
        
        <div class="content-box-content">
			<asp:DropDownList ID="DropDownList1" runat="server">
			</asp:DropDownList>
			<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
			<asp:Button ID="Button1" runat="server" Text="Button" onclick="Button1_Click" />

        </div>
        
    </div>
</asp:Content>

