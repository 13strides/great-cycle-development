﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;
using GS.Generic.Web;

public partial class Admin_Events_BusinessChallengeTerms : System.Web.UI.Page {
	int eventID = 0;
	int tID = 0;

	protected void Page_Load(object sender, EventArgs e) {
		infoMessage.Text = string.Empty;
		int.TryParse(Request.QueryString["id"], out eventID);
		int.TryParse(Request.QueryString["tid"], out tID);
	}

	protected void BtnSave_Click(Object sender, EventArgs e) {
		BusinessChallengeTerm bct = new BusinessChallengeTerm();
		if (IsValid()) {
			if (tID > 0) {
				//save edit
				bct = BusinessChallengeTerm.GetByID(tID);
				if (bct != null && bct.ID > 0) {
					bct.Name = nameTextBox.Text;
					bct.Title = titleTextBox.Text;
					bct.Body = bodyTextBox.Text;
					bct.Footer = footerTextBox.Text;
				}
			} else if (eventID > 0) {
				//save new
				bct.Name = nameTextBox.Text;
				bct.Title = titleTextBox.Text;
				bct.Body = bodyTextBox.Text;
				bct.Footer = footerTextBox.Text;
			}
			if (bct != null) {
				BusinessChallengeTerm resp = BusinessChallengeTerm.Save(bct);
				if (resp != null && resp.ID > 0) {
					infoMessage.Text = "Successfully Saved.";
					infoMessage.Type = InfoType.Success;
				} else {
					infoMessage.Text = "There was an error whilst saving Business Challenge Term.";
					infoMessage.Type = InfoType.Error;
				}
			}
		}
	}

	protected void BtnCancel_Click(Object sender, EventArgs e) {
		if (eventID > 0)
			Response.Redirect("EventBuisnessChallenge.aspx?id=" + eventID.ToString());
		else
			Response.Redirect("Default.aspx");
	}

	private bool IsValid() {
		bool valid = false;
		int year = 0;
		if (!string.IsNullOrEmpty(nameTextBox.Text) &&
			!string.IsNullOrEmpty(titleTextBox.Text)
			&& !string.IsNullOrEmpty(bodyTextBox.Text)) {
			valid = true;
		} else {
			infoMessage.Text = "Name, Title and Body are required.";
			infoMessage.Type = InfoType.Error;
		}
		return valid;
	}

	protected void Page_PreRender(object sender, EventArgs e) {
		if (!Page.IsPostBack) {
			if (tID > 0) {
				BindEdit();
			} else if (eventID > 0) {
				BindNew();
			}
		}
	}

	private void BindEdit() {
		BusinessChallengeTerm bct = BusinessChallengeTerm.GetByID(tID);
		if (bct != null && bct.ID > 0) {
			nameTextBox.Text = bct.Name;
			titleTextBox.Text = Server.HtmlDecode(bct.Title);
			bodyTextBox.Text = Server.HtmlDecode(bct.Body);
			footerTextBox.Text = Server.HtmlDecode(bct.Footer);
		} else {
			infoMessage.Text = "Business Challenge Trem was not found.";
			infoMessage.Type = InfoType.Error;
		}
	}

	private void BindNew() {
	}
}