﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventDetails;
using ThirteenStrides.Database;
using Ni.Generic.Measurement;
using GS.Generic.Events;
using Ni.Events.EventContent;
using Ni.Events.EventContentList;
using Ni.Events.EventPage;

public partial class Admin_Events_information_new : System.Web.UI.Page
{

	EventInfo _activeInfo;
	Event _currentEvent;
	EventPageList _pageList;


    protected void Page_Load(object sender, EventArgs e)
    {
		if ((Request.QueryString["id"] != null) && (Request.QueryString["id"] != string.Empty)) {
			try {
				_currentEvent = new Event(Convert.ToInt32(Request.QueryString["id"]));

				//addLink.NavigateUrl = "~/Admin/Events/information_new.aspx?evnt=" + _currentEvent.Id.ToString();
				//_pageList = new EventPageList(_currentEvent.Id);
				//_activeInfo = EventInfo.GetActive(_currentEvent);

			} catch (Exception ex2) {

			}
		} else {
			Response.Redirect("Default.aspx");
		}
		if (!IsPostBack) {
			lblEventName.Text = _currentEvent.Name;
		}
    }
	protected void Button1_Click(object sender, EventArgs e) {
		EventPage _newPage = new EventPage(_currentEvent.Id);
		_newPage._pageName = Textbox1.Text;
		_newPage._sortorder = int.Parse(Textbox2.Text);
		
		if (_newPage.Save()) {
			Response.Redirect("EventInformation.aspx?id=" + _currentEvent.Id.ToString());
		}
	}
}
