﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="EditMobileContent.aspx.cs" Inherits="Admin_Events_EditMobileContent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="content-box column-left main">
        <div class="content-box-header"><h3>Main Elements</h3></div>
        <div class="content-box-content">
            <table class="pagination" rel="30">
                <tr>
                    <td>Title:</td>
                    <td><asp:TextBox runat="server" ID="titleTextBox" CssClass="large" /></td>
                </tr>
                <tr>
                    <td>Friendly URL:</td>
                    <td><asp:TextBox runat="server" ID="friendlyTextBox" CssClass="large" /></td>
                </tr>
                <tr>
                    <td colspan="2">Content:</td>
                </tr>
                <tr>
                    <td colspan="2"><asp:TextBox runat="server" TextMode="MultiLine" Rows="30" ID="articleTextBox" CssClass="full wysiwyg" /></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="content-box column-right sidebar">
        <div class="content-box-header"><h3>Config</h3></div>
        <div class="content-box-content">
            <table class="pagination" rel="10">
                <tr>
                    <td>Publish:</td>
                    <td><gs:DatePicker runat="server" ID="publishDate" /></td>
                </tr>
                <tr>
                    <td>Expires:</td>
                    <td><gs:DatePicker runat="server" ID="expiresDate" /></td>
                </tr>
                <tr>
                    <td>Order:</td>
                    <td><asp:TextBox runat="server" ID="orderTextbox" /></td>
                </tr>
                <tr>
                    <td colspan="2"><asp:Button runat="server" ID="saveButton" OnClick="SaveButtonClk" Text="Save" CssClass="button" /><br /><a href="#" OnClick="javascript:history.go(-2)" CssClass="button" value="cancel" CssClass="button"> cancel</a>
                    <asp:HiddenField runat="server" ID="ownerIdHidden" /><asp:HiddenField runat="server" ID="evtHidden" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="clear"></div>
</asp:Content>

