﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="EventBuisnessChallenge.aspx.cs" Inherits="Admin_Events_EventBuisnessChallenge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				Business Challenge</h3>
		</div>
		<div class="content-box-content">
			<table>
				<tr>
					<td>
						<asp:Literal ID="AddNewLink" runat="server" />
					</td>
					<td>
						<asp:Literal ID="AddNewTermLink" runat="server" />
					</td>
				</tr>
			</table>
			<gs:Info runat="server" ID="infoMessage" CssClass="info" />
			<asp:Repeater runat="server" ID="RepeaterBc" OnItemCommand="ItemCommandHandler" OnItemDataBound="AddDeleteConfirmation">
				<HeaderTemplate>
					<table cellpadding="0" cellspacing="0" class="pagination" rel="8">
						<thead>
							<tr>
								<th style="width: 3%;">
									ID
								</th>
								<th>
									Name
								</th>
								<th>
									Event Year
								</th>
								<th style="width: 10%">
									Is Active
								</th>
								<th style="width: 10%">
									Is Open
								</th>
								<th>
									Display Mode
								</th>
								<th>
									Entries
								</th>
								<th>
									Terms
								</th>
								<th style="width: 7%">
									Edit
								</th>
								<th style="width: 7%;">
									delete
								</th>
							</tr>
						</thead>
				</HeaderTemplate>
				<ItemTemplate>
					<tr class="std-row">
						<td>
							<%# Eval("ID") %>
						</td>
						<td>
							<%# Eval("Name") %>
						</td>
						<td>
							<%# Eval("EventYear") %>
						</td>
						<td>
							<%# Eval("LinkActive") %>
						</td>
						<td>
							<%# Eval("IsOpen") %>
						</td>
						<td>
							<%# Eval("DisplayMode") %>
						</td>
						<td>
							<asp:HyperLink runat="server" ID="entryForm" NavigateUrl='<%# "BusinessChallengeForms.aspx?bid=" + Eval("ID")+ "&id="+ Eval("EventID") %>'
								Text='<%# GetCount(Eval("ID")) %>' />
						</td>
						<td>
							<asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl='<%# "BusinessChallengeTerms.aspx?tid=" + Eval("TermsID")+ "&id="+ Eval("EventID") %>'
								Text="terms" />
						</td>
						<td>
							<asp:HyperLink runat="server" ID="editButton" NavigateUrl='<%# "EditBusinessChallenge.aspx?bid=" + Eval("ID") + "&id="+ Eval("EventID") %>'
								Text="edit" />
						</td>
						<td>
							<asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("ID") %>'
								CommandName="delete" Text="delete" />
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					</table></FooterTemplate>
			</asp:Repeater>
		</div>
	</div>
</asp:Content>