﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="EventInformation.aspx.cs" Inherits="Admin_Events_EventInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    <div class="content-box column-left main">
        <div class="content-box-header">
            <h3>
                Event Content</h3>
        </div>
        <div class="content-box-content">
            <asp:Repeater runat="server" ID="pageList">
                <HeaderTemplate>
                    <table class="">
                        <thead>
                            <tr>
                                <th>
                                    Page ID
                                </th>
                                <th>
                                    Title
                                </th>

                                <th>
                                    Page Version Starting
                                </th>
                            </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%#  DataBinder.Eval(Container.DataItem,"PageID") %>
                        </td>
                        <td>
                            <%# FormatCurrentPage(DataBinder.Eval(Container.DataItem, "PageID"))%>
                        </td>

                        <td>
                            <%# FormatContentList(DataBinder.Eval(Container.DataItem, "PageID"))%><a title="Create New Scheduled version"
                                href='Content_new.aspx?pid=<%#  DataBinder.Eval(Container.DataItem,"PageID") %>'>
                                <img src="../images/icons/plus.png" alt="Create New Scheduled version" /></a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <img src="../images/icons/plus.png" />
            <asp:HyperLink runat="server" ID="hypNewPageLink" NavigateUrl="~/Admin/Events/page_new.aspx">New page</asp:HyperLink>
        </div>
    </div>
    <div class="content-box column-right sidebar">
        <div class="content-box-header">
            <h3>
                Event Information</h3>
        </div>
        <div class="content-box-content">
            <table>
                <thead>
                    <tr>
                        <th>
                            Distance
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <asp:Panel ID="pnlEventStreamLists" runat="server">
                </asp:Panel>
            </table>
        </div>
        <div class="clearer"></div>
        
    </div>
    <div class="clearer">
    </div>
    <div class="content-box column-right sidebar">
        <div class="content-box-header">
            <h3>Business Challenge</h3>
        </div>
        
        <div class="content-box-content">
           
                <asp:HyperLink ID="HyperLinkBc" runat="server" NavigateUrl="~/Admin/events/EventBuisnessChallenge.aspx?id=1">Manage Business Challenge</asp:HyperLink>

        </div>
        
    </div>
    <!-- suggest hosting the mobile content here
   <div class="content-box column-left main closed">
    <div class="content-box-header">
        <h3>Mobile Content</h3>
    </div>
    <div class="content-box-content">
        <asp:Literal runat="server" ID="errorMessage" />
        <p><img src="../Images/icons/plus.png" /><asp:HyperLink NavigateUrl="~/Admin/Events/CreateMobileContent.aspx" ID="addMobileLink" runat="server"> Add a mobile page</asp:HyperLink></p>
        <asp:Repeater ID="mobileContentRepeater" runat="server" OnItemCommand="MobileItemCommandHandler" OnItemDataBound="MobileAddDeleteConfirmation">
            <HeaderTemplate>
			<table cellspacing="0" class="pagination" rel="15">
			    <thead>
			    <tr>
				    <th>Title</th>
				    <th style="width: 5%;">Order</th>
				    <th style="width: 5%;">Publication</th>
				    <th style="width: 5%;">Expiration</th>
				    <th style="width: 15%;">Last Edit</th>
				    <th style="width: 5%;">Edit</th>
				    <th style="width: 5%;">Delete</th>
			    </tr></thead></HeaderTemplate>
		<ItemTemplate>
		    <tr class="std-row">
				<td><%# Eval("m_content_title") %></td>
				<td><%# Eval("m_content_order")%></td>
				<td><%# Eval("m_content_pub", "{0:dd/MM/yyyy}")%></td>
				<td><%# Eval("m_content_exp", "{0:dd/MM/yyyy}")%></td>
				<td><%# Eval("m_content_last_edit")%></td>
				<td><asp:LinkButton runat="server" ID="cssButton" CommandArgument='<%# Eval("m_content_id") %>' CommandName="edit" Text="Edit" /></td>
				<td><asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("m_content_id") %>' CommandName="delete" Text="Delete" /></td>
			</tr>
		</ItemTemplate>
            <FooterTemplate>
            </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
   </div>-->
</asp:Content>
