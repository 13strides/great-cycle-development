﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;
using GS.Generic.Web;

public partial class Admin_Events_BusinessChallengeForms : System.Web.UI.Page {
	public int eventID = 0;
	int bcID = 0;

	protected void Page_Load(object sender, EventArgs e) {
		infoMessage.Text = string.Empty;
		int.TryParse(Request.QueryString["id"], out eventID);
		int.TryParse(Request.QueryString["bid"], out bcID);
	}

	protected void Page_PreRender(object sender, EventArgs e) {
		List<BusinessChallengeForm> list = BusinessChallengeForm.GetByBusinessChallengeID(bcID);
		if (list != null && list.Count > 0) {
			RepeaterBc.DataSource = list;
			RepeaterBc.DataBind();
		} else {
			RepeaterBc.DataSource = null;
			RepeaterBc.DataBind();
			infoMessage.Text = "There are no forms for this business challenge.";
			infoMessage.Type = InfoType.Error;
		}
	}

	protected string GetLink(object obj) {
		string link = "BusinessChallengeFormView.aspx?fid=" + obj.ToString() + "&id=" + eventID.ToString();
		return link;
	}
}