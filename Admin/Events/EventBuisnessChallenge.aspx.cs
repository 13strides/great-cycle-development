﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;
using GS.Generic.Web;

public partial class Admin_Events_EventBuisnessChallenge : System.Web.UI.Page {
	int eventID = 0;

	protected void Page_Load(object sender, EventArgs e) {
		infoMessage.Text = string.Empty;
		int.TryParse(Request.QueryString["id"], out eventID);
		if (eventID <= 0) {
			Response.Redirect("Default.aspx");
		} else {
			AddNewLink.Text = "<img src='../Images/icons/plus.png' /><a href='EditBusinessChallenge.aspx?id="
				+ eventID.ToString() + "'>ADD NEW</a>";
			AddNewTermLink.Text = "<img src='../Images/icons/plus.png' /><a href='BusinessChallengeTerms.aspx?id="
			 + eventID.ToString() + "'>ADD NEW TERMS AND CONDITIONS</a>";
		}
	}

	protected void Page_PreRender(object sender, EventArgs e) {
		List<BusinessChallenge> list = BusinessChallenge.GetByEventID(eventID);
		if (list != null && list.Count > 0) {
			RepeaterBc.DataSource = list;
			RepeaterBc.DataBind();
		} else {
			RepeaterBc.DataSource = null;
			RepeaterBc.DataBind();
			infoMessage.Text = "There was no business challenge for this event.";
			infoMessage.Type = InfoType.Error;
		}
	}

	protected void ItemCommandHandler(Object sender, RepeaterCommandEventArgs e) {
		int eventID = 0;
		switch (e.CommandName) {
			case "delete":
				RemoveBusinessChallenge(Convert.ToInt32(e.CommandArgument));
				break;

			default:
				break;
		}
	}

	protected void AddDeleteConfirmation(Object sender, RepeaterItemEventArgs e) {
		if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
			// adds a confirmation javascript to ensure that deleting the image is really what the user wants.
			((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this business challenge?')";
		}
	}

	private void RemoveBusinessChallenge(int id) {
		if (BusinessChallenge.Delete(id)) {
			infoMessage.Text = "Deleted successfully";
			infoMessage.Type = InfoType.Success;
		} else {
			infoMessage.Text = "Unable to deleted";
			infoMessage.Type = InfoType.Error;
		}
	}

	protected string GetCount(object bcID) {
		string count = "0";
		int id = 0;
		int.TryParse(bcID.ToString(), out id);
		if (id > 0) {
			List<BusinessChallengeForm> list = BusinessChallengeForm.GetByBusinessChallengeID(id);
			if (list != null)
				count = list.Count.ToString();
		}
		return count;
	}
}