﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;
using GS.Generic.Web;

public partial class Admin_Events_EditBusinessChallenge : System.Web.UI.Page {
	int eventID = 0;
	int bID = 0;

	protected void Page_Load(object sender, EventArgs e) {
		infoMessage.Text = string.Empty;
		int.TryParse(Request.QueryString["id"], out eventID);
		int.TryParse(Request.QueryString["bid"], out bID);
	}

	protected void BtnSave_Click(Object sender, EventArgs e) {
		BusinessChallenge bc = new BusinessChallenge();
		if (IsValid()) {
			if (bID > 0) {
				//save edit
				bc = BusinessChallenge.GetByID(bID);
				if (bc != null && bc.ID > 0) {
					bc.Name = nameTextBox.Text;
					bc.Description = descriptionTextBox.Text;
					bc.FormDescription = formDescriptionTextBox.Text;
					bc.StreamID = streamIDTextBox.Text;
					bc.EventYear = int.Parse(eventYearTextBox.Text);
					bc.LinkActive = bool.Parse(isActiveDropDown.SelectedValue);
					bc.IsOpen = bool.Parse(isOpenDropDown.SelectedValue);
					bc.TermsID = int.Parse(termsDropDown.SelectedValue);
					bc.DisplayMode = (BcDisplayMode)Enum.Parse(typeof(BcDisplayMode), displayModeDropDown.SelectedValue, true);
				}
			} else if (eventID > 0) {
				//save new
				bc.EventID = eventID;
				bc.Name = nameTextBox.Text;
				bc.Description = descriptionTextBox.Text;
				bc.FormDescription = formDescriptionTextBox.Text;
				bc.StreamID = streamIDTextBox.Text;
				bc.EventYear = int.Parse(eventYearTextBox.Text);
				bc.LinkActive = bool.Parse(isActiveDropDown.SelectedValue);
				bc.IsOpen = bool.Parse(isOpenDropDown.SelectedValue);
				bc.TermsID = int.Parse(termsDropDown.SelectedValue);
				bc.DisplayMode = (BcDisplayMode)Enum.Parse(typeof(BcDisplayMode), displayModeDropDown.SelectedValue, true);
			}
			if (bc != null && bc.EventID > 0) {
				BusinessChallenge resp = BusinessChallenge.Save(bc);
				if (resp != null && resp.ID > 0) {
					infoMessage.Text = "Successfully Saved.";
					infoMessage.Type = InfoType.Success;
				} else {
					infoMessage.Text = "There was an error whilst saving Business Challenge.";
					infoMessage.Type = InfoType.Error;
				}
			}
		}
	}

	protected void BtnCancel_Click(Object sender, EventArgs e) {
		if (eventID > 0)
			Response.Redirect("EventBuisnessChallenge.aspx?id=" + eventID.ToString());
		else
			Response.Redirect("Default.aspx");
	}

	private bool IsValid() {
		bool valid = false;
		int year = 0;
		if (!string.IsNullOrEmpty(nameTextBox.Text) &&
			!string.IsNullOrEmpty(descriptionTextBox.Text)
			&& !string.IsNullOrEmpty(eventYearTextBox.Text)) {
			int.TryParse(eventYearTextBox.Text, out year);
			if (year > 1900 && year < 9999) {
				valid = true;
			} else {
				infoMessage.Text = "Event Year must have 'yyyy' fromat.";
				infoMessage.Type = InfoType.Error;
			}
		} else {
			infoMessage.Text = "Name, Description and Event Year are required.";
			infoMessage.Type = InfoType.Error;
		}
		return valid;
	}

	protected void Page_PreRender(object sender, EventArgs e) {
		if (!Page.IsPostBack) {
			if (bID > 0) {
				BindEdit();
			} else if (eventID > 0) {
				BindNew();
			}
		}
	}

	private void BindEdit() {
		BusinessChallenge bc = BusinessChallenge.GetByID(bID);
		if (bc != null && bc.ID > 0) {
			EventIDLabel.Text = bc.EventID.ToString() + " (" + GetEventName(bc.EventID) + ")";
			nameTextBox.Text = bc.Name;
			descriptionTextBox.Text = Server.HtmlDecode(bc.Description);
			formDescriptionTextBox.Text = Server.HtmlDecode(bc.FormDescription);
			streamIDTextBox.Text = bc.StreamID;
			eventYearTextBox.Text = bc.EventYear.ToString();
			isActiveDropDown.SelectedValue = bc.LinkActive.ToString();
			isOpenDropDown.SelectedValue = bc.IsOpen.ToString();
			BindTerms(bc.TermsID);
			BindDisplayMode(bc.DisplayMode);
		}
	}

	private void BindNew() {
		EventIDLabel.Text = eventID.ToString() + " (" + GetEventName(eventID) + ")";
		BindTerms(0);
		BindDisplayMode(BcDisplayMode.Default);
	}

	private void BindTerms(int selectedTerm) {
		List<BusinessChallengeTerm> terms = BusinessChallengeTerm.GetAll();
		if (termsDropDown.Items != null && termsDropDown.Items.Count > 0)
			termsDropDown.Items.Clear();
		ListItem li;
		li = new ListItem("-- select term --", "0");
		termsDropDown.Items.Add(li);
		if (terms != null && terms.Count > 0) {
			foreach (BusinessChallengeTerm bct in terms) {
				li = new ListItem(bct.Name, bct.ID.ToString());
				if (selectedTerm == bct.ID)
					li.Selected = true;
				termsDropDown.Items.Add(li);
			}
		}
	}

	private void BindDisplayMode(BcDisplayMode selectedMode) {
		List<BcDisplayMode> modes = Gr.Utils.CustomConverter.EnumToList<BcDisplayMode>();
		if (displayModeDropDown.Items != null && displayModeDropDown.Items.Count > 0)
			displayModeDropDown.Items.Clear();
		ListItem li;
		if (modes != null && modes.Count > 0) {
			foreach (BcDisplayMode mode in modes) {
				li = new ListItem(mode.ToString(), mode.ToString());
				if (selectedMode == mode) {
					li.Selected = true;
				}
				displayModeDropDown.Items.Add(li);
			}
		}
	}

	private string GetEventName(int evtID) {
		string name = "Unknown Event";
		GS.Generic.Events.Event evt = new GS.Generic.Events.Event(evtID);
		if (evt != null && evt.Id > 0)
			name = evt.Name;
		return name;
	}
}