﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="AddStreamInfo.aspx.cs" Inherits="Admin_Events_information_new" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
   <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
   
    <div class="content-box">
        <div class="content-box-header"><h3>New stream page - 
			<asp:Label ID="lblEventName" runat="server" Text="Label"></asp:Label></h3></div>
        <div class="content-box-content">
            <table id="editForm" class="">
                <thead>
                    <tr>
                        <th width="15%">Setting</th>
                        <th width="70%">Value</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tr>
                    <td>Stream ID:</td>
                    <td><asp:Textbox ID="Textbox1" runat="server" CssClass="" />
</td>
                </tr>
                <tr>
                    <td>Start Date:</td>
                    <td><gs:DatePicker ID="publicationDate" runat="server" /></td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td>
						<asp:DropDownList ID="ddlStatus" runat="server">
						<asp:ListItem Value="1">Open</asp:ListItem>
						<asp:ListItem Value="2">Closed</asp:ListItem>
						<asp:ListItem Value="3">OpenToCharities</asp:ListItem>
						<asp:ListItem Value="4">ReminderService</asp:ListItem>
						<asp:ListItem Value="5">PriorityEntry</asp:ListItem>
						<asp:ListItem Value="6">Ballot</asp:ListItem>
						
						</asp:DropDownList>
</td>
                </tr>
                           
                
                
            </table>
			<asp:Button ID="Button1" runat="server" Text="Button" onclick="Button1_Click" />
        
								
        </div>
    </div>

</asp:Content>

