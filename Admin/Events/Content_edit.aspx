﻿<%@ Page Title="" Language="C#" ValidateRequest="false" MasterPageFile="~/Admin/Admin.master"
	AutoEventWireup="true" CodeFile="Content_edit.aspx.cs" Inherits="Admin_Events_Content_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<ni:NiSwimSubNav runat="server" ID="swimSubNav" />
	<div style="position: fixed; background-color: #89a7c1; padding: 10px 0; bottom: 0px;
		left: 0; width: 100%; text-align: right;">
		<input type="button" id="Button2" value="Cancel" onclick="javascript: history.go(-1)"
			style="margin-right: 20px; margin-top: 0;" /><asp:Button ID="Button1" runat="server"
				Text="Save" OnClick="btnSave_Click" Style="margin-right: 20px; margin-top: 0;" />
	</div>
	<div class="content-box column-left main">
		<div class="content-box-header">
			<h3>
				Create page</h3>
		</div>
		<div class="content-box-content">
			<gr:TimedRedirector CssClass="redirection" runat="server" ID="redirector" RedirectUrl="~/admin/events/EventInformation.aspx?id=1"
				Text="The content has been saved successfully." Visible="false" />
			<gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
			<asp:Panel runat="server" ID="editPanel">
				<table width="600px" id="editTable">
					<tr>
						<th style="width: 15%">
							Page Name:
						</th>
						<td>
							<asp:TextBox runat="server" ID="titleTextBox" CssClass="text" />
						</td>
					</tr>
					<tr>
						<th>
						</th>
						<td>
						</td>
					</tr>
					<tr>
						<th>
							Content:
						</th>
						<td>
							<asp:TextBox runat="server" ID="contentTextBox" CssClass="full wysiwyg" TextMode="MultiLine"
								Rows="30" />
						</td>
					</tr>
				</table>
			</asp:Panel>
		</div>
	</div>
	<div class="content-box column-right sidebar ">
		<div class="content-box-header">
			<h3>
				Advanced:</h3>
		</div>
		<div class="content-box-content">
			<table>
				<tr>
					<td>
						Start Date:
					</td>
					<td>
						<gs:DatePicker ID="publicationDate" runat="server" />
					</td>
				</tr>
				<tr>
					<td>
						End Date:
					</td>
					<td>
						<gs:DatePicker ID="expirationDate" runat="server" />
					</td>
				</tr>
				<tr>
					<td>
						Status:
					</td>
					<td>
						<asp:DropDownList ID="ddlStatus" runat="server">
							<asp:ListItem Value="1">Live</asp:ListItem>
							<asp:ListItem Value="2">Draft</asp:ListItem>
							<asp:ListItem Value="3">New</asp:ListItem>
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td>
						Friendly Url
					</td>
					<td>
						<asp:TextBox ID="friendlyTextBox" CssClass="full" runat="server"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Summary:
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:TextBox runat="server" ID="summaryTextBox" CssClass="full wysiwyg" TextMode="MultiLine"
							Rows="8" />
					</td>
				</tr>
			</table>
			<asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
		</div>
	</div>
	<div class="clear">
	</div>
	<!--<script type="text/javascript">
		new TINY.editor.edit('editor', {
			id: 'ctl00_MainContent_contentTextBox',
			width: 500,
			height: 600,
			cssclass: 'te',
			controlclass: 'tecontrol',
			rowclass: 'teheader',
			dividerclass: 'tedivider',
			controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
			          'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
			          'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
			          '|', 'image', 'hr', 'link', 'unlink', '|', 'cut', 'copy', 'paste'],
			footer: true,
			fonts: ['Verdana', 'Arial', 'Georgia', 'Trebuchet MS'],
			xhtml: true,
			cssfile: 'style.css',
			bodyid: 'editor',
			footerclass: 'tefooter',
			toggle: { text: 'source', activetext: 'wysiwyg', cssclass: 'toggle' },
			resize: { cssclass: 'resize' }
		});
	</script>-->
</asp:Content>