﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventDetails;
using ThirteenStrides.Database;
using Ni.Generic.Measurement;
using GS.Generic.Events;
using Ni.Events.EventContent;
using Ni.Events.EventContentList;
using Ni.Events.EventPage;
using Ni.Events.StreamManagement;
using System.Data;
using System.Configuration;

public partial class Admin_Events_EventInformation : System.Web.UI.Page
{
    EventInfo _activeInfo;
    Event _currentEvent;
	EventPageList _pageList;

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Request.QueryString["id"] != null) && (Request.QueryString["id"] != string.Empty))
        {
            try
            {
                _currentEvent = new Event(Convert.ToInt32(Request.QueryString["id"]));

                //addLink.NavigateUrl="~/Admin/Events/information_new.aspx?evnt=" + _currentEvent.Id.ToString();
				hypNewPageLink.NavigateUrl = "~/Admin/Events/page_new.aspx?id=" + _currentEvent.Id.ToString();
				_pageList = new EventPageList(_currentEvent.Id);
                _activeInfo = EventInfo.GetActive(_currentEvent);
				
            }
            catch(Exception ex2)
            {

            }
        }
        else
        {
            Response.Redirect("Default.aspx");
        }
        if (!IsPostBack)
        {
            BindEventDetails();
            BindEventSubStreams();
            BindMobileContent(Convert.ToInt32(Request.QueryString["id"]), 0);
            addMobileLink.NavigateUrl += "?id=" + Request.QueryString["id"].ToString();
        }
    }

    protected void BindEventSubStreams()
    {
        EventContainerEventList eventlist = new EventContainerEventList(_currentEvent.Id);
        
        foreach (int eventId in eventlist.subevents)
        {
			EventStreamContainer thisSubEvent = new EventStreamContainer(eventId);

			Literal thisLit = new Literal();
			thisLit.Text ="<tr><td><a href=\"ViewStreamInfo.aspx?id="+eventId.ToString()+"\">"+thisSubEvent._event_name+"</a></td>";
			if (thisSubEvent.currentStream != null) {
				thisLit.Text += "<td><span class=\"statusList " 
                    + (SwimEventStatus)thisSubEvent.currentStream.Status + "\">" + (SwimEventStatus)thisSubEvent.currentStream.Status + "</span></td>"
                    + "<td>" + thisSubEvent.currentStream.StreamID.ToString() + "</td></tr>";
			} else {
                thisLit.Text += "<td colspan=\"2\">No Stream Information</td></tr>";
                //thisLit.Text += " No Stream Information"; 
			}
			//thisLit.Text += "<br />";
			//thisSubEvent._event_name;


			/*  Admin_Admin_controls_EventStreamList newList = new Admin_Admin_controls_EventStreamList();
				 newList.SubEventID = eventId;*/
			pnlEventStreamLists.Controls.Add(thisLit);
			
        }
    }
	protected string FormatCurrentPage(object id) {
		int pageID = int.Parse(id.ToString());

		try {
			EventPage pageContent = new EventPage(_currentEvent.Id, pageID, true);

			if (pageContent._contentid != null && pageContent._contentid > 0) {
				return "<a href=\"Content_edit.aspx?cid=" + pageContent._contentid.ToString() + "\">" + pageContent._pageName + "</a>";
			} else {

				return pageContent._pageName;
			}
		} catch {
			return "Issue";
		}

	}

	protected string FormatContentList(object id) {
		int pageID = int.Parse(id.ToString());
		EventContentList pageContentList = new EventContentList(pageID);
		string returner = "";
		foreach (EventContent ec in pageContentList.pageVersions)
		{
			//returner += "<tr style=\"display:table-row;\"><td></td><td></td><td><a href=\"Content_edit.aspx?cid=" + ec.ContentID.ToString() + "\">";
			
			// kev edited to try to shorten the page length
			
			if (ec.PubDate >= DateTime.Now) {
				returner += "<a href=\"Content_edit.aspx?cid=" + ec.ContentID.ToString() + "\" class=\"pageVersion future\">";
				returner += "<span class=\"month\">" + ec.PubDate.Date.ToString("MMM") + "</span><span class=\"day\">" 
					+ ec.PubDate.Date.Day.ToString() + "</span><span class=\"year\">" 
					+ ec.PubDate.Date.Year.ToString() + "</span>";
				//returner += ec.PubDate.Date.ToString();
				returner += "</a>";
			} else {
				returner += "<a href=\"Content_edit.aspx?cid=" + ec.ContentID.ToString() + "\" class=\"pageVersion past\">";
				returner += "<span class=\"month\">" + ec.PubDate.Date.ToString("MMM") + "</span><span class=\"day\">"
					+ ec.PubDate.Date.Day.ToString() + "</span><span class=\"year\">"
					+ ec.PubDate.Date.Year.ToString() + "</span>";
				//returner += ec.PubDate.Date.ToString();
				returner += "</a>";
			}
			
			
			//returner += "</a></td></tr>";
		}
		return returner;
	}



    
    /// this function displays the event information but the fields are disabled
    /// 


    private void BindEventDetails()
    {
        try
        {
			pageList.DataSource = _pageList.Pages;
			pageList.DataBind();
			/*
            nameTextbox.Text = _activeInfo.EventName;
            dateTextbox.Text = _activeInfo.EventDate.Date.ToString();
            statusTextbox.Text = _activeInfo.Status.ToString();
            distanceTextbox.Text = _activeInfo.RaceLength.Metres.ToString();
            distanceDesTextbox.Text = _activeInfo.DistanceTitle;
            locationTextbox.Text = _activeInfo.RaceLocation;
            locationRefTextbox.Text = _activeInfo.LocationRef;
            stdAdultPriceTextbox.Text = _activeInfo.StandardPrice;
            startTimeTextbox.Text = _activeInfo.StartTime;
            ap16StreamIdTextbox.Text = _activeInfo.Ap16Id.ToString();
            entryStreamTextbox.Text = _activeInfo.StreamId.ToString();
            mpuTextbox.Text = _activeInfo.MpuAd;
            leaderboardTextbox.Text = _activeInfo.Leaderboard;
		*/
	
        }
        catch(Exception ex)
        {

        }

    }

    /// <summary>
    /// This is the mobile content stuff
    /// </summary>
    /// <param name="evt"></param>
    /// <param name="archived"></param>

    private void BindMobileContent(int evt, int archived)
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = {new SqlParam("@id", SqlDbType.Int, evt) ,
                               new SqlParam("@archived", SqlDbType.Int, archived) };

        DataSetResponse res = connection.RunCommand("m_select_events_content", paras, "m_eventContent");

        if (res.ReturnCode == 0)
        {
            mobileContentRepeater.DataSource = res.Result.Tables["m_eventContent"].DefaultView;
            mobileContentRepeater.DataBind();
        }
        else
        {
            errorMessage.Text += "There was an error looking up the mobile content in the database.";
            //infoMessage.Type = InfoType.Error;
        }
    }

    protected void MobileItemCommandHandler(Object sender, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "edit":
                Response.Redirect("EditMobileContent.aspx?mid=" + e.CommandArgument);
                break;
            case "delete":
                DeleteMobileContent(Convert.ToInt32(e.CommandArgument));
                break;
        }
    }

    protected void MobileAddDeleteConfirmation(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {
            // adds a confirmation javascript to ensure that deleting the template is really what the user wants.
            ((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this Content?')";
        }
    }

    private void DeleteMobileContent(int id)
    {
        /*EventContentBase _content = new EventContentBase(id);
        if (_content.Delete())
        {
            infoMessage.Text = "The event content was successfully deleted.";
            infoMessage.Type = InfoType.Success;
            if (archiveDisplayCheckbox.Checked)
            {
                BindData(Convert.ToInt32(Request.QueryString["evtId"]), Request.QueryString["language"], 1);

            }
            else
            {
                BindData(Convert.ToInt32(Request.QueryString["evtId"]), Request.QueryString["language"], 0);

            }
        }
        else
        {
            infoMessage.Text = "The event content couldn't be deleted.";
            infoMessage.Type = InfoType.Error;
        }*/
    }
}
