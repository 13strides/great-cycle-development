﻿<%@ Page Title="" Language="C#" ValidateRequest="false" MasterPageFile="~/Admin/Admin.master"
	AutoEventWireup="true" CodeFile="BusinessChallengeTerms.aspx.cs" Inherits="Admin_Events_BusinessChallengeTerms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		<!--
		th{text-align:left !important;}
		-->
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				Edit Business Challenge Term</h3>
		</div>
		<div class="content-box-content">
			<table>
				<tr>
					<td>
						<asp:Literal ID="AddNewLink" runat="server" />
					</td>
				</tr>
			</table>
			<gs:Info runat="server" ID="infoMessage" CssClass="info" />
			<table>
				<tr>
					<th>
						Name:
					</th>
					<td>
						<asp:TextBox ID="nameTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Title:
					</th>
					<td>
						<asp:TextBox ID="titleTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Body:
					</th>
					<td>
						<asp:TextBox ID="bodyTextBox" runat="server" TextMode="MultiLine" Columns="50" Rows="6"
							CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Footer:
					</th>
					<td>
						<asp:TextBox ID="footerTextBox" runat="server" TextMode="MultiLine" Columns="50"
							Rows="4" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<asp:Button ID="BtnSave" runat="server" OnClick="BtnSave_Click" Text="Save" CssClass="button" />
						<asp:Button ID="BtnCancel" runat="server" OnClick="BtnCancel_Click" Text="Cancel"
							CssClass="button" />
					</td>
				</tr>
			</table>
		</div>
	</div>
</asp:Content>