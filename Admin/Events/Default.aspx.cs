﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using System.Configuration;

public partial class Admin_Events_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }


    }

    protected void BindData()
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = { };

        DataSetResponse res = connection.RunCommand("gs_select_event_list", paras, "events");

        if (res.ReturnCode == 0)
        {
            eventslist.DataSource = res.Result.Tables["events"].DefaultView;
            eventslist.DataBind();
        }
        else
        {
            messageLiteral.Text = "<div class=\"notification error\">Failed to load events: " + res.ErrorMessage + "</div>";
        }
    }
}