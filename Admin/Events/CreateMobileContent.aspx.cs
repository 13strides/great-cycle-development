﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using System.Data;
using System.Configuration;

public partial class Admin_Events_CreateMobileContent : System.Web.UI.Page
{
    int _mid, _eid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            expiresDate.SelectedDate = DateTime.Today.AddYears(1);
            BindMobileContent();
        }
    }

    private void BindMobileContent()
    {
        ownerIdHidden.Value = GS.Generic.GreatSwim.CurrentUser.Details.Id;
        evtHidden.Value = Request.QueryString["id"].ToString();
    }


    protected void SaveButtonClk(Object sender, EventArgs e)
    {
        if ((titleTextBox.Text != String.Empty) && (friendlyTextBox.Text != String.Empty) && (orderTextbox.Text != String.Empty))
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
            SqlParam[] paras = { new SqlParam("@evtId", SqlDbType.Int, Convert.ToInt32(evtHidden.Value)),
                                   new SqlParam("@id", SqlDbType.Int, -1),
                                   new SqlParam("@title", SqlDbType.VarChar,256, titleTextBox.Text),
                                   new SqlParam("@friendlyUrl", SqlDbType.VarChar,128, friendlyTextBox.Text),
                                   new SqlParam("@order", SqlDbType.Int, Convert.ToInt32(orderTextbox.Text)),
                                   new SqlParam("@pubDate", SqlDbType.DateTime, publishDate.SelectedDate),
                                   new SqlParam("@expDate", SqlDbType.DateTime, expiresDate.SelectedDate),
                                   new SqlParam("@article", SqlDbType.Text, articleTextBox.Text),
                                   new SqlParam("@lastEdit", SqlDbType.VarChar, 128, GS.Generic.GreatSwim.CurrentUser.Details.Username + " @ " + DateTime.Today.ToString("dd/MM/yyyy")),
                                   new SqlParam("@userId", SqlDbType.VarChar, 36, ownerIdHidden.Value)
                               };

            Int32Response result = connection.RunInt32Command("m_save_event_content", paras);

            if (result.ReturnCode >= 0)
            {
                Response.Redirect("EventInformation.aspx?id=" + evtHidden.Value + "");

            }
            else
            {
#if DEBUG
                throw new Exception(result.ErrorMessage);
#endif

            }
        }
        else
        {

        }
    }
}