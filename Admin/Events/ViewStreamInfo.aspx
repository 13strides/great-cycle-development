﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="ViewStreamInfo.aspx.cs" Inherits="Admin_Events_information_new" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
   <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
   
    <div class="content-box">
        <div class="content-box-header"><h3>Stream page - 
			<asp:Label ID="lblEventName" runat="server" Text="Label"></asp:Label></h3></div>
        <div class="content-box-content">
            
			<ni:EventStreamList runat="server" ID="streamList" />
								
        </div>
    </div>
    
     <div class="content-box">
        <div class="content-box-header"><h3>Advanced Controls</h3></div>
        <div class="content-box-content">
              <table id="editForm" class="">
                <thead>
                    <tr>
                        <th width="15%">Setting</th>
                        <th width="70%">Value</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                       <tr>
                    <td>Event Name:</td><td><asp:Textbox ID="txtEventName" runat="server" CssClass="" />
</td>
                </tr>
                <tr>
                    <td>Event Date:</td><td><gs:DatePicker ID="eventDate" runat="server" />
</td>
                </tr>
                 <tr>
                    <td>Event Distance:</td><td><asp:Textbox ID="txtEventDistance" runat="server" CssClass="" />
</td>
                </tr>
                <tr>
                    <td>Event Location:</td><td><asp:Textbox ID="txtEventLocation" runat="server" CssClass="" />
</td>
                </tr>
			   <tr>
                    <td>Event Start Times:</td><td><asp:Textbox ID="txtEventStatTime" runat="server" CssClass="" />
</td>
                </tr>
				 <tr>
                    <td>Event Temperature:</td><td><asp:Textbox ID="txtEventTemp" runat="server" CssClass="" />
</td>
                </tr>	
               <tr>
                    <td> AP16 ID:</td><td><asp:Textbox ID="txtAP16ID" runat="server" CssClass="" />
</td>
                </tr>
                <tr>
                    <td>Banner Id</td><td><asp:Textbox ID="txtBannerId" runat="server" CssClass="" />
</td>
                </tr>
                <tr>
                    <td>Sky Scrapper Id:</td><td><asp:Textbox ID="txtSkyScraperID" runat="server" CssClass="" />
</td>
                </tr>
                <tr>
                    <td>Standard Price:</td><td><asp:Textbox ID="txtEvemtPrice" runat="server" CssClass="" />
</td>
                </tr>
                <tr>
                <td>
                
					<asp:Button ID="Button1" runat="server" Text="Save" onclick="Button1_Click1" /></td>
                </tr>
                </table>
        </div>
        
    </div>

</asp:Content>

