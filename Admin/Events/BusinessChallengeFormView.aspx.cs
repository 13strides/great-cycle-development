﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;
using GS.Generic.Web;

public partial class Admin_Events_BusinessChallengeFormView : System.Web.UI.Page {
	int eventID = 0;
	int fID = 0;

	protected void Page_Load(object sender, EventArgs e) {
		infoMessage.Text = string.Empty;
		int.TryParse(Request.QueryString["id"], out eventID);
		int.TryParse(Request.QueryString["fid"], out fID);
	}

	protected void BtnCancel_Click(Object sender, EventArgs e) {
		if (eventID > 0)
			Response.Redirect("EventBuisnessChallenge.aspx?id=" + eventID.ToString());
		else
			Response.Redirect("Default.aspx");
	}

	protected void Page_PreRender(object sender, EventArgs e) {
		if (!Page.IsPostBack) {
			if (fID > 0) {
				BindEdit();
			} else if (eventID > 0) {
				Response.Redirect("EventBuisnessChallenge.aspx?id=" + eventID.ToString());
			}
		}
	}

	private void BindEdit() {
		BusinessChallengeForm bcf = BusinessChallengeForm.GetByID(fID);
		if (bcf != null && bcf.ID > 0) {
			businessChallengeTextBox.Text = BusinessChallenge.GetByID(bcf.BusinessChallengeID).Name;
			businessNameTextBox.Text = bcf.BusinessName;
			businessAddress1TextBox.Text = bcf.BusinessAddress1;
			businessAddress2TextBox.Text = bcf.BusinessAddress2;
			businessTelephoneTextBox.Text = bcf.BusinessTelephone;
			businessCityTextBox.Text = bcf.BusinessCity;
			businessCountryTextBox.Text = Country.GetByID(bcf.BusinessCountryID).Name;
			businessPostcodeTextBox.Text = bcf.BusinessPostcode;
            businessDistanceTextBox.Text = bcf.BusinessChallengeDistance.ToString();
			entrantsTextBox.Text = bcf.NumberOfEntrants.ToString();
			coordinatorEmailTextBox.Text = bcf.CoordinatorEmail;
			coordinatorNameTextBox.Text = bcf.CoordinatorName;
			coordinatorTelephoneTextBox.Text = bcf.CoordinatorTelephone;
			isEmailedTextBox.Text = bcf.IsEmailed.ToString();
		} else {
			infoMessage.Text = "Business Challenge Form was not found.";
			infoMessage.Type = InfoType.Error;
		}
	}
}