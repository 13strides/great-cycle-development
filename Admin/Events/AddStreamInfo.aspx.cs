﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ni.Events.EventDetails;
using ThirteenStrides.Database;
using Ni.Generic.Measurement;
using GS.Generic.Events;
using Ni.Events.EventContent;
using Ni.Events.EventContentList;
using Ni.Events.EventPage;
using Ni.Events.StreamManagement;


public partial class Admin_Events_information_new : System.Web.UI.Page
{

	EventInfo _activeInfo;
	Event _currentEvent;
	EventPageList _pageList;
	 EventStream _currentStream;
	EventStreamContainer _currentStreamContainer;
    protected void Page_Load(object sender, EventArgs e)
    {
		if (  (Request.QueryString["id"] != null) && (Request.QueryString["id"] != string.Empty)) {
			try {
				//_currentEvent = new Event(Convert.ToInt32(Request.QueryString["id"]));
				_currentStream = new EventStream(-1,Convert.ToInt32(Request.QueryString["id"]));
				_currentStreamContainer = new EventStreamContainer(Convert.ToInt32(Request.QueryString["id"]));
				//addLink.NavigateUrl = "~/Admin/Events/information_new.aspx?evnt=" + _currentEvent.Id.ToString();
				//_pageList = new EventPageList(_currentEvent.Id);
				//_activeInfo = EventInfo.GetActive(_currentEvent);

			} catch (Exception ex2) {

			}
		} else {
			Response.Redirect("Default.aspx");
		}
		if (!IsPostBack) {
			lblEventName.Text = _currentStream._event_description;
		}
    }
	protected void Button1_Click(object sender, EventArgs e) {
		_currentStream._event_info_stream_id = int.Parse(Textbox1.Text);
		_currentStream._event_info_publication_date = publicationDate.SelectedDate;
		_currentStream._event_info_status = int.Parse(ddlStatus.SelectedValue);

		//EventPage _newPage = new EventPage(_currentEvent.Id);
		//_newPage._pageName = Textbox1.Text;
		//_newPage._sortorder = int.Parse(Textbox2.Text);
		if (_currentStream.Save()) {
			Response.Redirect("ViewStreamInfo.aspx?id=" + _currentStreamContainer._eventID.ToString());
		}
	}
}
