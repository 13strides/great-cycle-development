﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="information_new.aspx.cs" Inherits="Admin_Events_information_new" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    
    <div class="content-box">
        <div class="content-box-header"><h3>New event set</h3></div>
        <div class="content-box-content">
            <table id="editForm" class="pagination">
                <thead>
                    <tr>
                        <th width="15%">Setting</th>
                        <th width="70%">Value</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tr>
                    <td>Event Name:</td>
                </tr>
                <tr>
                    <td>Event Date:</td>
                </tr>
                <tr>
                    <td>Status:</td>
                </tr>
                <tr>
                    <td>Event Distance:</td>
                </tr>
                <tr>
                    <td>Distance Desc.</td>
                </tr>
                <tr>
                    <td>Location:</td>
                </tr>
                <tr>
                    <td>Location ref:</td>
                </tr>
                <tr>
                    <td>Adult Price:</td>
                </tr>
                <tr>
                    <td>Start Time:</td>
                </tr>
                <tr>
                    <td>AP16 stream id:</td>
                </tr>
                <tr>
                    <td>Entry Stream:</td>
                </tr>
                <tr>
                    <td>MPU AD:</td>
                </tr>
                <tr>
                    <td>Leaderboard ad:</td>
                </tr>
                
                
                
            </table>
        
            <fieldset>
								<p>
									<label>Event Name:</label>
									<asp:Textbox ID="nameTextbox" runat="server" CssClass="" disabled="disabled" />
<span class="notification"></span>
									<label>Event Date:</label>
								  <asp:Textbox ID="dateTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
									<span class="notification"></span>
								
									<label>Status:</label>
									<asp:Textbox ID="statusTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
									<span class="notification"></span>
									<label>Event Distance:</label>
									<asp:Textbox ID="distanceTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
								
									<label>Distance Title:</label>
									<asp:Textbox ID="distanceDesTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
								<span class="notification"></span>
									<label>Location:</label>
									<asp:Textbox ID="locationTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
								<span class="notification"></span>
									<label>Location Ref:</label>
									<asp:Textbox ID="locationRefTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
								<span class="notification"></span>
									<label>Adult Price:</label>
									<asp:Textbox ID="stdAdultPriceTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>Start Time:</label>
									<asp:Textbox ID="startTimeTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>AP16 stream id:</label>
									<asp:Textbox ID="ap16StreamIdTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>Entry Stream:</label>
									<asp:Textbox ID="entryStreamTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>MPU Ad:</label>
									<asp:Textbox ID="mpuTextbox" runat="server" CssClass="datepicker" disabled="disabled" />
<span class="notification"></span>
									<label>Leaderboard Ad:</label>
									<asp:Textbox ID="leaderboardTextbox" runat="server" CssClass="datepicker" disabled="disabled" /><span class="notification"></span>
								</p>
                            </fieldset>
        </div>
    </div>

</asp:Content>

