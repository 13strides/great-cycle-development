﻿<%@ Page Title="" Language="C#" ValidateRequest="false" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Content_new.aspx.cs" Inherits="Admin_Events_Content_new" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    
    <div class="content-box column-left main">
        <div class="content-box-header">
             <h3>Create page</h3>
        </div>    
        <div class="content-box-content">
            <gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
        <table width="600px" id="editTable">
            <tr>
                <th style="width:15%">Page Name: </th><td><asp:TextBox runat="server" ID="titleTextBox" CssClass="text" /></td>
            </tr>

            <tr>
                <th></th><td></td>
            </tr>
            <tr>
                <th>Content:</th><td><asp:TextBox runat="server" ID="contentTextBox" CssClass="full wysiwyg" TextMode="MultiLine" Rows="30" /></td> 
            </tr>
        </table>
        			<asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />

        </div>
    </div>
    <div class="content-box column-right sidebar ">
        <div class="content-box-header">
            <h3>Advanced:</h3>
        </div>
        <div class="content-box-content">
            <table>
                <tr>
                    <td>Start Date:</td>
                    <td><gs:DatePicker ID="publicationDate" runat="server" /></td>
                </tr>
                <tr>
                    <td>End Date:</td>
                    <td><gs:DatePicker ID="expirationDate" runat="server" /></td>
                </tr>
            
                <tr>
                <td>Status:</td>
                <td>
					<asp:DropDownList ID="ddlStatus" runat="server">
					<asp:ListItem Value="1">Live</asp:ListItem>
					<asp:ListItem Value="2">Draft</asp:ListItem>
					<asp:ListItem Value="3">New</asp:ListItem>
					</asp:DropDownList>
				</td>
                </tr>
                  <tr>
                    <td>Friendly Url</td>
                    <td><asp:TextBox ID="friendlyTextBox"  CssClass="full" runat="server"></asp:TextBox> </td>
                </tr>
                  <tr>
					<td colspan="2">Summary:</td>
                </tr>
                <tr>
                <td colspan="2"><asp:TextBox runat="server" ID="summaryTextBox" CssClass="full wysiwyg" TextMode="MultiLine" Rows="8" /></td></tr>

            </table>
        </div>
    </div>
    <div class="clear"></div>
</asp:Content>

