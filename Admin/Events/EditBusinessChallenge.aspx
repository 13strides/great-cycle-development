﻿<%@ Page Title="" Language="C#" ValidateRequest="false" MasterPageFile="~/Admin/Admin.master"
	AutoEventWireup="true" CodeFile="EditBusinessChallenge.aspx.cs" Inherits="Admin_Events_EditBusinessChallenge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		<!--
		th{text-align:left !important;}
		-->
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				Edit Business Challenges</h3>
		</div>
		<div class="content-box-content">
			<table>
				<tr>
					<td>
						<asp:Literal ID="AddNewLink" runat="server" />
					</td>
				</tr>
			</table>
			<gs:Info runat="server" ID="infoMessage" CssClass="info" />
			<table>
				<tr>
					<th>
						Event ID:
					</th>
					<td>
						<asp:Label ID="EventIDLabel" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<th>
						Name:
					</th>
					<td>
						<asp:TextBox ID="nameTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Description:
					</th>
					<td>
						<asp:TextBox ID="descriptionTextBox" runat="server" TextMode="MultiLine" Columns="50"
							Rows="6" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Form Description:
					</th>
					<td>
						<asp:TextBox ID="formDescriptionTextBox" runat="server" TextMode="MultiLine" Columns="50"
							Rows="4" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Steam ID:
					</th>
					<td>
						<asp:TextBox ID="streamIDTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Event Year:
					</th>
					<td>
						<asp:TextBox ID="eventYearTextBox" runat="server" Columns="50" CssClass="text"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<th>
						Is Active
					</th>
					<td>
						<asp:DropDownList ID="isActiveDropDown" runat="server">
							<asp:ListItem Text="No" Value="False" />
							<asp:ListItem Text="Yes" Value="True" />
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<th>
						Is Open:
					</th>
					<td>
						<asp:DropDownList ID="isOpenDropDown" runat="server">
							<asp:ListItem Text="No" Value="False" />
							<asp:ListItem Text="Yes" Value="True" />
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<th>
						Display Mode:
					</th>
					<td>
						<asp:DropDownList ID="displayModeDropDown" runat="server" />
					</td>
				</tr>
				<tr>
					<th>
						Terms:
					</th>
					<td>
						<asp:DropDownList ID="termsDropDown" runat="server" />
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<asp:Button ID="BtnSave" runat="server" OnClick="BtnSave_Click" Text="Save" CssClass="button" />
						<asp:Button ID="BtnCancel" runat="server" OnClick="BtnCancel_Click" Text="Cancel"
							CssClass="button" />
					</td>
				</tr>
			</table>
		</div>
	</div>
</asp:Content>