﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="BusinessChallengeForms.aspx.cs" Inherits="Admin_Events_BusinessChallengeForms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				Business Challenge Forms</h3>
		</div>
		<div class="content-box-content">
			<gs:Info runat="server" ID="infoMessage" CssClass="info" />
			<asp:Repeater runat="server" ID="RepeaterBc">
				<HeaderTemplate>
					<table cellpadding="0" cellspacing="0" class="pagination" rel="400">
						<thead>
							<tr>
								<th style="width: 3%;">
									ID
								</th>
								<th>
									Business Name
								</th>
								<th style="width: 7%">
									View
								</th>
							</tr>
						</thead>
				</HeaderTemplate>
				<ItemTemplate>
					<tr class="std-row">
						<td>
							<%# Eval("ID") %>
						</td>
						<td>
							<%# Eval("BusinessName") %>
						</td>
						<td>
							<asp:HyperLink runat="server" ID="editButton" NavigateUrl='<%# GetLink(Eval("ID")) %>'
								Text="view" />
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					</table></FooterTemplate>
			</asp:Repeater>
		</div>
	</div>
</asp:Content>