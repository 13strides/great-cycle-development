﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using System.Data;
using System.Configuration;

public partial class Admin_Events_EditMobileContent : System.Web.UI.Page
{
    int _mid, _eid;
    string _uid;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["mid"] != null)
        {
            try
            {
                _mid = Convert.ToInt32(Request.QueryString["mid"]);
                if (!IsPostBack)
                {
                    BindMobileContent();
                }
            }
            catch
            {

            }
        }
    }
    private void BindMobileContent()
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, _mid),
                               new SqlParam("@useDate", SqlDbType.Int, 1)};

        SqlDataReaderResponse reader = connection.RunCommand("m_select_event_content", paras);

        if (reader.ReturnCode == 0)
        {
            if (reader.Result.Read())
            {
                _eid = Convert.ToInt32(reader.Result["m_content_event_id"]);
                titleTextBox.Text = reader.Result["m_content_title"].ToString();
                friendlyTextBox.Text = reader.Result["m_content_friendly"].ToString();
                articleTextBox.Text = reader.Result["m_content_article"].ToString();
                publishDate.SelectedDate = Convert.ToDateTime(reader.Result["m_content_pub"]);
                expiresDate.SelectedDate = Convert.ToDateTime(reader.Result["m_content_exp"]);
                orderTextbox.Text = reader.Result["m_content_order"].ToString();
                ownerIdHidden.Value = reader.Result["m_content_user"].ToString();
                evtHidden.Value = reader.Result["m_content_event_id"].ToString();
            }
            else
            {
                throw new Exception("The content wasn't found in the database.");
            }
            reader.Result.Close();
        }
        else
        {
            throw new Exception(reader.ErrorMessage);
        }
    }

    protected void SaveButtonClk(Object sender, EventArgs e)
    {
        if ((titleTextBox.Text != String.Empty) && (friendlyTextBox.Text != String.Empty) && (orderTextbox.Text != String.Empty))
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
            SqlParam[] paras = { new SqlParam("@evtId", SqlDbType.Int, Convert.ToInt32(evtHidden.Value)),
                                    new SqlParam("@id", SqlDbType.Int, _mid),
                                   new SqlParam("@title", SqlDbType.VarChar,256, titleTextBox.Text),
                                   new SqlParam("@friendlyUrl", SqlDbType.VarChar,128, friendlyTextBox.Text),
                                   new SqlParam("@order", SqlDbType.Int, Convert.ToInt32(orderTextbox.Text)),
                                   new SqlParam("@pubDate", SqlDbType.DateTime, publishDate.SelectedDate),
                                   new SqlParam("@expDate", SqlDbType.DateTime, expiresDate.SelectedDate),
                                   new SqlParam("@article", SqlDbType.Text, articleTextBox.Text),
                                   new SqlParam("@lastEdit", SqlDbType.VarChar, 128, GS.Generic.GreatSwim.CurrentUser.Details.Username + " @ " + DateTime.Today.ToString("dd/MM/yyyy")),
                                   new SqlParam("@userId", SqlDbType.VarChar, 36, ownerIdHidden.Value)
                               };

            Int32Response result = connection.RunInt32Command("m_save_event_content", paras);

            if (result.ReturnCode >= 0)
            {
                Response.Redirect("EventInformation.aspx?Id=" + evtHidden.Value);

            }
            else
            {
#if DEBUG
                throw new Exception(result.ErrorMessage);
#endif

            }
        }
    }
}
