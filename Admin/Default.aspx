﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="Default.aspx.cs" Inherits="Admin_Default" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		<!--
			.content-box-content table tr td h1{
				font-size:12px;
				margin:0;padding:0;line-height:100%
			}
			.content-box-content table tr td p{
				padding:0; margin:0;}
			.content-box-content table tr td{padding-bottom:10px !important;}
		-->
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box column-left main">
		<div class="content-box-header">
			<h3>
				Homepage Carousel</h3>
		</div>
		<div class="content-box-content">
			<a href="/admin/heroslider/create_slide.aspx">Create slide</a>
			<table class="pagination" rel="5">
				<thead>
					<tr>
						<th width="5%">
							id
						</th>
						<th width="45%">
							Image
						</th>
						<th width="45%">
							Message
						</th>
						<th width="5%">
							Status
						</th>
						<th width="5%">
							Actions
						</th>
					</tr>
				</thead>
				<asp:Repeater runat="server" ID="heroSlides" OnItemCommand="ItemCommandHandler" OnItemDataBound="AddDeleteConfirmation">
					<ItemTemplate>
						<tr>
							<td>
								<%# Eval("id") %>
							</td>
							<td>
								<%# GetImagePath(Eval("image")) %>
							</td>
							<td>
								<%# Eval("message") %>
							</td>
							<td>
								<%# FormatStatus(Eval("banner_status")) %>
							</td>
							<td>
								<asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# "#" + Eval("id") %>'
									CommandName="edit" Text="edit" />
								/
								<asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("id") %>'
									CommandName="delete" Text="delete" />
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</table>
		</div>
	</div>
</asp:Content>