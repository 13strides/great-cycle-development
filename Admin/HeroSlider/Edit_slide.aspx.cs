﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;
using GS.Generic.Web;

public partial class Admin_HeroSlider_Edit_slider : System.Web.UI.Page {
	int heroID = 0;
	HeroSlider hs = null;

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			BindStatusList();
			int.TryParse(Request.QueryString["sid"], out heroID);

			hs = HeroSlider.GetByID(heroID, 0);

			if (hs != null && hs.ID > 0) {
				BindHeroSlide();
			} else {
				//no news found
			}
			GetFileList();
		}
	}

	private void BindHeroSlide() {
		message.Text = hs.Message;
		linkTextTextbox.Text = hs.Link_text;
		linkUrlTextbox.Text = hs.Link_url;
		dateTextbox.Text = hs.Pub_date.ToString("yyyy-MM-dd");
		priorityTextBox.Text = hs.Priority.ToString();
		//newsStatusDropDown.SelectedValue = na.NewsStatus.ToString();
		statusDDL.SelectedValue = hs.Banner_status.ToString();
		newsImage.ImageUrl = "/App_Files/ImageLibrary/NewsMedium/" + hs.Image;
	}

	protected void GetFileList() {
		fileListRepeater.DataSource = (new DirectoryInfo(Server.MapPath("~/App_Files/ImageLibrary/NewsThumb/"))).GetFiles();
		fileListRepeater.DataBind();
	}

	private void BindStatusList() {
		statusDDL.DataSource = Enum.GetNames(typeof(GS.Generic.Web.NewsStatus));
		statusDDL.DataBind();
	}

	protected string GetFriendlyName(string fullFileName) {
		//Response.Write(fullFileName);
		if (fullFileName.Length > 11) {
			string strToShorten = fullFileName.Substring(11, fullFileName.Length - 11);
			return strToShorten;
		}
		return "";
	}

	protected void BackStepButtonClk(Object sender, EventArgs e) {
		Response.Redirect("~/Admin/Default.aspx");
	}

	protected void SaveButtonClk(Object sender, EventArgs e) {
		if ((!string.IsNullOrEmpty(message.Text)) && (!string.IsNullOrEmpty(priorityTextBox.Text))
			&& (!string.IsNullOrEmpty(dateTextbox.Text))) {
			int.TryParse(Request.QueryString["sid"], out heroID);
			hs = HeroSlider.GetByID(heroID, 0);
			hs.Message = message.Text;
			hs.Link_text = linkTextTextbox.Text;
			hs.Link_url = linkUrlTextbox.Text;
			hs.Priority = Convert.ToInt32(priorityTextBox.Text);
			if (dateTextbox.Text != String.Empty) {
				hs.Pub_date = Convert.ToDateTime(dateTextbox.Text);
			}
			hs.Banner_status = (GS.Generic.Web.NewsStatus)Enum.Parse(typeof(GS.Generic.Web.NewsStatus), statusDDL.SelectedValue);

			if (imageFileNameHidden.Value != String.Empty) {
				hs.Image = imageFileNameHidden.Value;
			}

			HeroSlider resp = HeroSlider.Save(hs);
			if (resp != null && resp.ID > 0) {
				//infoMessage.Text = "Successfully Saved.";
				infoMessage.Type = InfoType.Success;
				redirector.Visible = true;
				editPanel.Visible = false;
			} else {
				infoMessage.Text = "There was an error whilst saving the slide.";
				infoMessage.Type = InfoType.Error;
			}
		}
	}
}