﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="Create_slide.aspx.cs" Inherits="Admin_HeroSlider_Create_slide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box column-left sidebar">
		<div class="content-box-header">
			<h3>
				Choose your image</h3>
		</div>
		<div class="content-box-content">
		</div>
	</div>
	<div class="content-box column-right main">
		<div class="content-box-header">
			<h3>
				Messaging</h3>
		</div>
		<div class="content-box-content">
			<div class="sep">
				<label>
					Message:</label>
				<asp:TextBox runat="server" ID="message" Columns="44" Rows="5" TextMode="MultiLine" />
			</div>
			<div class="sep">
				<label>
					Link text:</label>
				<asp:TextBox runat="server" ID="linkTextTextbox"></asp:TextBox>
			</div>
			<div class="sep">
				<label>
					Link url:</label>
				<asp:TextBox runat="server" ID="linkUrlTextbox"></asp:TextBox>
			</div>
			<div class="sep">
				<label>
					Priority:</label>
				<asp:TextBox runat="server" ID="priorityTextBox"></asp:TextBox>
			</div>
			<div class="sep">
				<label>
					Status:</label>
				<asp:DropDownList runat="server" ID="statusDDL">
				</asp:DropDownList>
			</div>
			<div class="sep">
				<label>
					Pub Date:</label>
				<asp:TextBox runat="server" ID="dateTextbox"></asp:TextBox>
			</div>
		</div>
	</div>
</asp:Content>