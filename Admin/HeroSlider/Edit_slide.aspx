﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="Edit_slide.aspx.cs" Inherits="Admin_HeroSlider_Edit_slider" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		.imageHolder
		{
			height: 102px;
			margin: 12px 6px 0 6px;
			border: 1px solid #cdcdcd;
			position: relative;
			float: left;
		}

		.actions a
		{
			color: #ffffff;
			font-size: 11px;
			display: block;
			padding: 30px;
			float: left;
		}
		.actions a img
		{
			margin: 5px;
		}

		.actions
		{
			background: black;
			opacity: 0.2;
			position: absolute;
			top: -1px;
			right: -1px;
			display: none;
			padding: 1px 2px;
			text-align: center;
			width: 100%;
			height: 100%;
		}
		.imageHolder:hover .actions
		{
			display: block;
		}

		#popUpFiles
		{
			display: none;
			position: absolute;
			top: 50px;
			left: 10%;
			width: 70%;
			padding: 10px;
			height: 500px;
			border: 2px solid #cccccc;
			border-radius: 10px;
			background-color: #ffffff;
		}
		#popUpFiles .zone
		{
			height: 450px;
			overflow: auto;
		}
		#names li
		{
			list-style: none;
			margin: 0;
			padding: 0;
			float: left;
			width: 100px;
		}
		#names li p
		{
			text-align: center;
			clear: both;
			min-height: 40px;
		}
	</style>
	<script type="text/javascript">

		function setUpImageSelector() {
			$("#chooseImage").click(function () {
				$("#popUpFiles").show();
				$("img.lazy").lazyload();
			});

			$("a.select-image").click(function () {
				/*alert(
				$(this).attr("rel")
				);*/
				$(".headlineHolder img").attr("src", "/App_Files/ImageLibrary/NewsFull/" + $(this).attr("rel"));
				$(".headlineHolder input[type='hidden']").val($(this).attr("rel"))
				$("#popUpFiles").hide();
			});
			$("#close-popupfiles").click(function () {
				$("#popUpFiles").hide();
			});
		}

		$(function () {

			//$(".js-datepicker").datepicker();
			//$(".js-datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
			setUpImageSelector();

		});

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box column-left sidebar">
		<div class="content-box-header">
			<h3>
				Choose your image</h3>
		</div>
		<div class="content-box-content">
			<div class="headlineHolder">
				<asp:Image runat="server" ID="newsImage" ImageUrl="http://placehold.it/200" />
				<asp:HiddenField runat="server" ID="imageFileNameHidden" />
				<br />
				<a href="#" id="chooseImage">Choose image</a>
			</div>
		</div>
	</div>
	<div class="content-box column-right main">
		<div class="content-box-header">
			<h3>
				Messaging</h3>
		</div>
		<div class="content-box-content">
			<gs:Info runat="server" ID="infoMessage" CssClass="info">
			</gs:Info>
			<gr:TimedRedirector CssClass="redirection" runat="server" ID="redirector" RedirectUrl="/Admin/Default.aspx"
				Text="The news has been saved successfully." Visible="false" />
			<asp:Panel runat="server" ID="editPanel">
				<div class="sep">
					<label>
						Message:</label>
					<asp:TextBox runat="server" ID="message" Columns="44" Rows="5" TextMode="MultiLine" />
				</div>
				<div class="sep">
					<label>
						Link text:</label>
					<asp:TextBox runat="server" ID="linkTextTextbox"></asp:TextBox>
				</div>
				<div class="sep">
					<label>
						Link url:</label>
					<asp:TextBox runat="server" ID="linkUrlTextbox"></asp:TextBox>
				</div>
				<div class="sep">
					<label>
						Priority:</label>
					<asp:TextBox runat="server" ID="priorityTextBox"></asp:TextBox>
				</div>
				<div class="sep">
					<label>
						Status:</label>
					<asp:DropDownList runat="server" ID="statusDDL">
					</asp:DropDownList>
				</div>
				<div class="sep">
					<label>
						Pub Date:</label>
					<asp:TextBox runat="server" ID="dateTextbox"></asp:TextBox>
				</div>
			</asp:Panel>
		</div>
	</div>
	<div class="clear">
	</div>
	<div class="mainActionButtons">
		<asp:Button runat="server" ID="previousStepButton" OnClick="BackStepButtonClk" Text="Back"
			CssClass="" />
		<asp:Button runat="server" ID="saveButton" OnClientClick="MoveContent()" OnClick="SaveButtonClk"
			Text="Save" CssClass="button" />
	</div>
	<asp:HiddenField runat="server" ID="articleId" />
	<div id="popUpFiles">
		<a href="#" id="close-popupfiles">
			<img src="/Admin/Images/icons/cross.png" /></a>
		<p>
			Search:
			<input type="text" name="q" value="">
			<span id="count"></span>
		</p>
		<p id="none" style="display: none">
			There were no names to match your search!</p>
		<div class="zone">
			<asp:Repeater runat="server" ID="fileListRepeater">
				<HeaderTemplate>
					<ul id="names">
				</HeaderTemplate>
				<ItemTemplate>
					<li>
						<div class="imageHolder" style="width: 84px; height: 72px; background-color: #ddd">
							<img src="/App_Files/ImageLibrary/NewsThumb/<%# System.IO.Path.GetFileName(Eval("Name").ToString())  %>">
							<div class="actions">
								<a href="#" class="select-image" rel="<%# System.IO.Path.GetFileName(Eval("Name").ToString())  %>">
									&nbsp;</a>
							</div>
						</div>
						<p>
							<%# GetFriendlyName(System.IO.Path.GetFileName(Eval("Name").ToString()))%></p>
					</li>
				</ItemTemplate>
				<FooterTemplate>
					</ul></FooterTemplate>
			</asp:Repeater>
		</div>
		<div class="clear">
		</div>
	</div>
</asp:Content>