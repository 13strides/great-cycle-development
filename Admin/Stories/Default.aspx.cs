﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GS.Generic.Membership;
using ThirteenStrides.Database;
using ThirteenStrides.Web;
using GS.Generic.Web;

public partial class Admin_Stories_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindData();
    }
    protected void AddDeleteConfirmation(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {
            // adds a confirmation javascript to ensure that deleting the template is really what the user wants.
            ((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this story?')";
        }
    }

    private void BindData()
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = { };

        DataSetResponse res = connection.RunCommand("gs_select_insp_stories", paras, "stories");

        if (res.ReturnCode == 0)
        {
            pager.ControlToPage = newsListRepeater;
            pager.DataSource = res.Result.Tables["stories"].DefaultView;
            pager.DataBind();
        }
        else
        {
            infoMessage.Text = "There was an error looking for inspiring stories in the database.";
            infoMessage.Type = InfoType.Error;
        }
    }
    protected void ItemCommandHandler(Object sender, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "edit":
                Response.Redirect("EditStory.aspx?sid=" + e.CommandArgument.ToString());
                break;
            case "delete":
                DeleteNews(Convert.ToInt32(e.CommandArgument));
                break;
        }
    }
    private void DeleteNews(int id)
    {
        if (id != -1)
        {
            try
            {
                MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);
                SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, id) };
                Int32Response result = connection.RunInt32Command("gs_delete_insp_story", paras);

                if (result.ReturnCode != 0)
                {
                    throw new Exception(result.ErrorMessage);
                }
            }
            catch
            {
                infoMessage.Text = "There was an error deleting the news";
                infoMessage.Type = InfoType.Error;
                BindData();
            }
        }
        else
        {
#if DEBUG
				throw new Exception("The news was never saved into the database.");
#endif
            infoMessage.Text = "there was an error and the story hasn't been deleted.";
            infoMessage.Type = InfoType.Error;

        }
        BindData();
    }
}
