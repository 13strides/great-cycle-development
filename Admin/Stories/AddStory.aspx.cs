﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ThirteenStrides.Database;
using GS.Generic.Web;
using GS.Generic.Events;
using GS.Generic.Membership;

public partial class Admin_Stories_AddStory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            eventListDropDown.DataSource = new EventList();
            eventListDropDown.DataTextField = "Name";
            eventListDropDown.DataValueField = "Id";
            eventListDropDown.DataBind();
        }
    }

    protected void SaveButtonClk(Object sender, EventArgs e)
    {
        if (SaveStory())
        {
            infoMessage.Text = "Story Added <a href=\"default.aspx\">Click here to return to main section</a>";
            infoMessage.Type = InfoType.Success;
        }
        else
        {
            infoMessage.Text = "Story Didn't get added <a href=\"default.aspx\">Click here to return to main section</a>";
            infoMessage.Type = InfoType.Error;
        }




    }

    private bool SaveStory()
    {
        //need a few checks in here for min value
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = {new SqlParam("@id", SqlDbType.Int, -1),
                               new SqlParam("@publicationDate", SqlDbType.DateTime, publicationDate.SelectedDate),
                               new SqlParam("@expirationDate", SqlDbType.DateTime, expirationDate.SelectedDate),
                               new SqlParam("@eventDate", SqlDbType.DateTime, eventDate.SelectedDate),
                               new SqlParam("@dateModified", SqlDbType.VarChar, 32, GS.Generic.GreatSwim.CurrentUser.Details.Username + "@: " + DateTime.Now),
                               new SqlParam("@name", SqlDbType.VarChar, 256, titleTextBox.Text),
                               new SqlParam("@excerpt", SqlDbType.Text, summaryTextBox.Text),
                               new SqlParam("@source", SqlDbType.Text, contentTextBox.Text),
                               new SqlParam("@html", SqlDbType.Text, contentTextBox.Text),
                               new SqlParam("@status", SqlDbType.SmallInt, NewsStatus.New),
                               new SqlParam("@event", SqlDbType.Int, eventListDropDown.SelectedValue)};

        Int32Response result = connection.RunInt32Command("gs_save_insp_story", paras);

        if (result.ReturnCode >= 0)
        {
            //want to redirect here
            infoMessage.Text = "Story Added <a href=\"default.aspx\">Click here to return to main section</a>";
            infoMessage.Type = InfoType.Success;
            return true;
        }
        else
        {
            throw new Exception(result.ErrorMessage);
            return false;
        }
    }
}
