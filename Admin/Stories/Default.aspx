﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Admin_Stories_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="content-box">
        <div class="content-box-header">
            <h3>Inspiring Stories</h3>
        </div>
        <div class="content-box-content">
            <asp:HyperLink runat="server" ID="AddNewsLink" NavigateUrl="~/admin/Stories/AddStory.aspx">Add a Story</asp:HyperLink>
            <gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
            <asp:Repeater runat="server" ID="newsListRepeater" OnItemCommand="ItemCommandHandler" OnItemDataBound="AddDeleteConfirmation">
                <HeaderTemplate>
                    <table cellpadding="0" class="pagination" rel="50">
                        <thead>
                        <tr>
                            <th>#id</th>
                            <th>Name</th>
                            <th>Event</th>
                            <th>Publication Date</th>
                            <th>Expiration Date</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="std-row">
                        <th><%# Eval("story_id") %></th>
                        <td><%# Eval("story_name") %></td>
                        <td><%# Eval("event_name") %></td>
                        <td><%# Eval("story_pub_date", "{0:dd/MM/yyyy}") %></td>
                        <td><%# Eval("story_end_date", "{0:dd/MM/yyyy}") %></td>
                        <td><asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# Eval("story_id") %>' CommandName="edit" Text="edit" /></td>
				        <td><asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("story_id") %>' CommandName="delete" Text="delete" /></td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="alt-row">
                        <th><%# Eval("story_id") %></th>
                        <td><%# Eval("story_name") %></td>
                        <td><%# Eval("event_name") %></td>
                        <td><%# Eval("story_pub_date", "{0:dd/MM/yyyy}") %></td>
                        <td><%# Eval("story_end_date", "{0:dd/MM/yyyy}") %></td>
                        <td><asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# Eval("story_id") %>' CommandName="edit" Text="edit" /></td>
				        <td><asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("story_id") %>' CommandName="delete" Text="delete" /></td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <gr:RepeaterPager runat="server" ID="pager" PageSize="50" CssClass="pager" CurrentPageLinkCssClass="current-page-link" LinksCssClass="page-link" NextPreviousCssClass="next-prev-link" NextPreviousDisabledCssClass="next-prev-disabled"></gr:RepeaterPager>

        </div>
    </div>
    
    
</asp:Content>

