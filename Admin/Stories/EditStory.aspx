﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
 ValidateRequest="false"
 CodeFile="EditStory.aspx.cs" Inherits="Admin_Stories_EditStory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="content-box">
        <div class="content-box-header">
            <h3>Edit Story</h3>
        </div>
        <div class="content-box-content">
            <gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
            <table width="600px">
                <tr>
                    <th style="width:15%">Name: </th><td><asp:TextBox runat="server" ID="titleTextBox" CssClass="text" /></td>
                </tr>
                <tr>
                    <th>Publication Date:</th><td><gs:DatePicker ID="publicationDate" runat="server" /></td>
                </tr>
                <tr>
                    <th>Expiration Date:</th><td><gs:DatePicker ID="expirationDate" runat="server" /></td>
                </tr>
                <tr>
                    <th>Summary:</th><td><asp:TextBox runat="server" ID="summaryTextBox" CssClass="text" TextMode="MultiLine" Rows="8" /></td>
                </tr>
                <tr>
                    <th>Content:</th><td><asp:TextBox runat="server" ID="contentTextBox" CssClass="wysiwyg" TextMode="MultiLine" Rows="15" /></td>
                </tr>
                
                <tr>
                    <td>Event:</td><td><asp:DropDownList runat="server" ID="eventListDropDown" /></td>
                </tr>
                <tr>
                    <td>Event Date:</td><td><gs:DatePicker ID="eventDate" runat="server" /></td>
                </tr>
                <tr>
                    <td></td><td><asp:Button runat="server" ID="saveButton" OnClick="SaveButtonClk" Text="save" CssClass="button" /></td>
                </tr>
                <tr>
                    <td colspan="2"><asp:HiddenField runat="server" ID="storyIdHidden" /></td>
                </tr>
            </table>
        </div>
    </div>
    
    
</asp:Content>

