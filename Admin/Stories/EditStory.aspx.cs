﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ThirteenStrides.Database;
using GS.Generic.Web;
using GS.Generic.Membership;
using GS.Generic.Events;

public partial class Admin_Stories_EditStory : System.Web.UI.Page
{
    int _story_id;
    protected void Page_Load(object sender, EventArgs e)
    {


        if (Request.QueryString["sid"] != null)
        {
            try
            {
                _story_id = Convert.ToInt32(Request.QueryString["sid"]);
                if (!IsPostBack)
                {
                    eventListDropDown.DataSource = new EventList();
                    eventListDropDown.DataTextField = "Name";
                    eventListDropDown.DataValueField = "Id";
                    eventListDropDown.DataBind();
                    BindData(Convert.ToInt32(Request.QueryString["sid"]));

                }
            }
            catch
            {
                Response.Redirect("Default.aspx");

            }
        }
        else
        {
            Response.Redirect("Default.aspx");
        }


    }

    protected void BindData(int newsId)
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, newsId) };

        SqlDataReaderResponse reader = connection.RunCommand("gs_select_insp_story_by_id", paras);

        if (reader.ReturnCode == 0)
        {
            if (reader.Result.Read())
            {
                _story_id = Convert.ToInt32(reader.Result["story_id"]);
                titleTextBox.Text = reader.Result["story_name"].ToString();
                publicationDate.SelectedDate = Convert.ToDateTime(reader.Result["story_pub_date"]);
                expirationDate.SelectedDate = Convert.ToDateTime(reader.Result["story_end_date"]);
                summaryTextBox.Text = reader.Result["story_excerpt"].ToString();
                contentTextBox.Text = reader.Result["story_source"].ToString();
                eventListDropDown.SelectedValue = reader.Result["story_event"].ToString();
                eventDate.SelectedDate = Convert.ToDateTime(reader.Result["story_event_date"]);
                storyIdHidden.Value = reader.Result["story_id"].ToString();
            }
            else
            {
                reader.Result.Close();
                throw new Exception("The story wasn't found in the database.");
            }
            reader.Result.Close();
        }
        else
        {
            throw new Exception(reader.ErrorMessage);
        }
    }
    protected void SaveButtonClk(Object sender, EventArgs e)
    {
        if (SaveNews())
        {
            infoMessage.Text = "Story Updated <a href=\"default.aspx\">Click here to return to main section</a>";
            infoMessage.Type = InfoType.Success;
        }
        else
        {
            infoMessage.Text = "Story Didn't get updated <a href=\"default.aspx\">Click here to return to main section</a>";
            infoMessage.Type = InfoType.Error;
        }

    }
    private bool SaveNews()
    {
        //need a few checks in here for min value
        if ((summaryTextBox.Text != String.Empty) || (titleTextBox.Text != String.Empty) || (contentTextBox.Text != String.Empty) || (storyIdHidden.Value != String.Empty))
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

            SqlParam[] paras = {new SqlParam("@id", SqlDbType.Int, Convert.ToInt32(storyIdHidden.Value)),
                               new SqlParam("@publicationDate", SqlDbType.DateTime, publicationDate.SelectedDate),
                               new SqlParam("@expirationDate", SqlDbType.DateTime, expirationDate.SelectedDate),
                               new SqlParam("@eventDate", SqlDbType.DateTime, eventDate.SelectedDate),
                               new SqlParam("@dateModified", SqlDbType.VarChar, 32, GS.Generic.GreatSwim.CurrentUser.Details.Username + "@: " + DateTime.Now),
                               new SqlParam("@name", SqlDbType.VarChar, 256, titleTextBox.Text),
                               new SqlParam("@excerpt", SqlDbType.Text, summaryTextBox.Text),
                               new SqlParam("@source", SqlDbType.Text, contentTextBox.Text),
                               new SqlParam("@html", SqlDbType.Text, contentTextBox.Text),
                               new SqlParam("@status", SqlDbType.SmallInt, NewsStatus.New),
                               new SqlParam("@event", SqlDbType.Int, eventListDropDown.SelectedValue)};

            Int32Response result = connection.RunInt32Command("gs_save_insp_story", paras);

            if (result.ReturnCode >= 0)
            {
                //want to redirect here
                return true;
            }
            else
            {
                throw new Exception(result.ErrorMessage);
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
