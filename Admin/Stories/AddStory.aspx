﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="AddStory.aspx.cs" Inherits="Admin_Stories_AddStory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="content-box">
        <div class="content-box-header">
            <h3>Create an Inspiring Story</h3>
        </div>
        <div class="content-box-content">
            <gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
            <table width="600px">
                <tr>
                    <th style="width:15%">Title: </th><td><asp:TextBox runat="server" ID="titleTextBox" CssClass="text" /></td>
                </tr>
                <tr>
                    <th>Publication Date:</th><td><gs:DatePicker ID="publicationDate" runat="server" /></td>
                </tr>
                <tr>
                    <th>Expiration Date:</th><td><gs:DatePicker ID="expirationDate" runat="server" /></td>
                </tr>
                <tr>
                    <th>Summary:</th><td><asp:TextBox runat="server" ID="summaryTextBox" CssClass="text" TextMode="MultiLine" Rows="8" /></td>
                </tr>
                <tr>
                    <th>Content:</th><td><asp:TextBox runat="server" ID="contentTextBox" CssClass="text" TextMode="MultiLine" Rows="15" /></td>
                </tr>
                <tr>
                    <td>Event:</td><td><asp:DropDownList runat="server" ID="eventListDropDown" /></td>
                </tr>
                <tr>
                    <td>Event Date:</td><td><gs:DatePicker ID="eventDate" runat="server" /></td>
                </tr>
                <tr>
                    <td></td><td><asp:Button runat="server" ID="saveButton" OnClick="SaveButtonClk" Text="save" CssClass="button" /></td>
                </tr>
                
                <tr>
                    <td colspan="2">Image tag:<br />
                                    <code>&lt;p&gt;&lt;img src="../App_Files/Gr_Files/xxxFILENAMExxx" alt="xxxDESCRIPTIONxxx" /&gt;&lt;/p&gt;</code>
                                    <br />Link tag:<br />
                                    <code>&lt;a href="xxxhttp://www.address.comxxx" target="_blank"&gt;xxxCLICK HERE textxxx&lt;/a&gt;</code>
        </td>
                </tr>
            </table>
        </div>
    </div>
    
    
</asp:Content>

