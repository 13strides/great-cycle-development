﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using GS.Generic.Membership;

public partial class Admin_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void OnLoginClk(Object sender, EventArgs e)
    {
        try
        {
            if (FormsAuthentication.Authenticate(login.UserName, login.Password))
            {
                //assign to 13strides
                //GSUser user = GSUser.AdminLogin(login.UserName, login.Password);
                GSUser user = GSUser.AdminLogin(ConfigurationManager.AppSettings["appUsername"], ConfigurationManager.AppSettings["appPassword"]);
                if (user != null)
                {
                    FormsAuthentication.RedirectFromLoginPage(login.UserName, false);
                }
            }
        }
        catch
        {
            Response.Write("Error logging you in.");
        }
    }

    protected void LoginClk(Object sender, EventArgs e)
    {
        try
        {
            GSUser user = GSUser.AdminLogin(ConfigurationManager.AppSettings["appUsername"], ConfigurationManager.AppSettings["appPassword"]);
            if (user != null)
            {
                FormsAuthentication.RedirectFromLoginPage("13Strides", false);
            }
        }
        catch
        {
            Response.Write("Error logging you in.");
        }
    }
}
