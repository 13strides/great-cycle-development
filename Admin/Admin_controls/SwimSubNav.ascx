﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SwimSubNav.ascx.cs" Inherits="Admin_Admin_controls_SwimSubNav" %>
<ul id="subnav">
	<li>
		<asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl="~/admin/Default.aspx">HOMEPAGE</asp:HyperLink></li>
	<li>
		<asp:HyperLink runat="server" ID="newsLink" NavigateUrl="~/admin/newsarticles/Default.aspx">NEWS</asp:HyperLink></li>
	<li>
		<asp:HyperLink runat="server" ID="blogLink" NavigateUrl="~/admin/BlogArticles/default.aspx">BLOG</asp:HyperLink></li>
	<li>
		<asp:HyperLink runat="server" ID="eventsLink" NavigateUrl="~/admin/events/EventInformation.aspx?id=1">EVENTS</asp:HyperLink></li>
	<li>
		<asp:HyperLink runat="server" ID="HyperLink3" NavigateUrl="~/admin/newsarticles/images/Default.aspx">IMAGES</asp:HyperLink></li>
	<li>
		<asp:HyperLink runat="server" ID="HyperLink2" NavigateUrl="~/admin/files/default.aspx">FILES</asp:HyperLink></li>
</ul>