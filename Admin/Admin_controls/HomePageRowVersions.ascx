﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomePageRowVersions.ascx.cs" Inherits="Admin_Admin_controls_HomePageRowVersions" %>
<asp:Repeater ID="rowVersionRepeater" runat="server">
<HeaderTemplate>
<table>
<tr>
<th>Date</th>
<th>Description</th>
<th>Status</th>
</tr>
</HeaderTemplate>

<ItemTemplate>
<tr>
<td><asp:HyperLink runat="server" ID="editLink" NavigateUrl='<%# "~/admin/Homepage/ViewRowSet.aspx?id=" + DataBinder.Eval(Container.DataItem,"Version") %>'><%#  DataBinder.Eval(Container.DataItem,"PublicationDate") %></asp:HyperLink></td>
<td><%#  DataBinder.Eval(Container.DataItem,"Description") %></td>
<td><%#  DataBinder.Eval(Container.DataItem,"Status") %></td>

</tr>
</ItemTemplate>

<FooterTemplate>
</table>
</FooterTemplate>
	
</asp:Repeater>
<asp:Button ID="Button1" runat="server" Text="Add New Row Set" 
	 />