﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gs.Generic.HomePage;
public partial class Admin_Admin_controls_HomePageRowVersions : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
		AllHomePageVersions currentVersions = new AllHomePageVersions();
		if (currentVersions.hpVersions != null) {
			rowVersionRepeater.DataSource = currentVersions.hpVersions;
			rowVersionRepeater.DataBind();
		}
    }
}
