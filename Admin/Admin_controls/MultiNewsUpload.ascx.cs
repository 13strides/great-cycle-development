﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GS.Generic.Web;
using ThirteenStrides.Database;

public partial class Admin_Admin_controls_MultiNewsUpload : System.Web.UI.UserControl {
	const string ORIGINAL_IMAGE = "~/App_Files/ImageLibrary/RAW/";
	const string NEWS_THUMB = "~/App_Files/ImageLibrary/NewsThumb/";
	const string NEWS_MEDIUM = "~/App_Files/ImageLibrary/NewsMedium/";
	const string NEWS_FULL = "~/App_Files/ImageLibrary/NewsFull/";
	const string HOMEPAGE = "~/App_Files/ImageLibrary/Homepage/";

	const string MIN_WIDTH = "721";
	const string MIN_HEIGHT = "417";

	string localFileName = "";

	protected void Page_Load(object sender, EventArgs e) {
		if (!Page.IsPostBack) {
		}
	}

	protected void Upload_Click(object sender, EventArgs e) {
		if (UploadTheOriginalImage()) {
			UploadWizardPanel.Visible = false;
			CropWizardPanel.Visible = true;
			image_target.ImageUrl = ORIGINAL_IMAGE + "/" + DateTime.Now.ToString("yyyy-MM-dd") + "-" + fileNameTextbox.Text + ".jpg";
			GC.Collect();
		} else {
			infoMessage.Type = InfoType.Error;
			infoMessage.Text += "something went wrong";
			infoMessage.Visible = true;
		}
	}

	protected void ButtonCrop_Click(object sender, EventArgs e) {
		//process cropping
		int leftX = Convert.ToInt32(LeftX.Value.ToString());
		int rightX = Convert.ToInt32(RightX.Value.ToString());
		int topY = Convert.ToInt32(TopY.Value.ToString());
		int bottomY = Convert.ToInt32(BottomY.Value.ToString());
		int selectedWidth = Convert.ToInt32(SelectedWidth.Value.ToString());
		int selectedHeight = Convert.ToInt32(SelectedHeight.Value.ToString());

		if (CropAndGenerateImages(leftX, topY, selectedWidth, selectedHeight)) {
			CropWizardPanel.Visible = false;
			string fileName = image_target.ImageUrl.Substring(image_target.ImageUrl.LastIndexOf("/") + 1).ToLower();
			newsArticleImage.ImageUrl = NEWS_FULL + fileName;
			newsMediumThumbImage.ImageUrl = NEWS_MEDIUM + fileName;
			newsSmallThumbImage.ImageUrl = NEWS_THUMB + fileName;
			homepageHeroImage.ImageUrl = HOMEPAGE + fileName;
			FinishWizardPanel.Visible = true;
			GC.Collect();
		}

		//PreviewCroppedImage(CROPPED_AND_PREVIEWED, );
		/*PreviewCroppedImage(NEWS_FULL, leftX, topY, selectedWidth, selectedHeight);

		//Hide all panels but Step3
		Step1.Visible = false;
		Step2.Visible = false;
		Step3.Visible = true;
		Step4.Visible = false;*/
	}

	/// <summary>
	/// captures the selected image and passes to the Create Crop Image
	/// </summary>
	/// <returns></returns>
	private bool UploadTheOriginalImage() {
		if (!string.IsNullOrEmpty(fieldFileUpload.FileName) && !string.IsNullOrEmpty(fileNameTextbox.Text)) {
			if ((System.IO.Path.GetExtension(fieldFileUpload.FileName).ToLower() == ".jpg")
				|| (System.IO.Path.GetExtension(fieldFileUpload.FileName).ToLower() == ".gif")
				|| (System.IO.Path.GetExtension(fieldFileUpload.FileName).ToLower() == ".png")
				) {
				;
				using (Bitmap uploadFileImage = new Bitmap(fieldFileUpload.FileContent)) {
					if (uploadFileImage.Width >= Convert.ToInt32(MIN_WIDTH) && uploadFileImage.Height >= Convert.ToInt32(MIN_HEIGHT)) {
						if (CreateCropImage(uploadFileImage)) {
							uploadFileImage.Dispose();
							return true;
						} else {
							infoMessage.Type = InfoType.Error;
							infoMessage.Text += "CreateCropImage Failed";
							infoMessage.Visible = true;
							uploadFileImage.Dispose();
							return false;
						}
					} else {
						infoMessage.Type = InfoType.Warning;
						infoMessage.Text = "We need a bigger image to maintain good quality";
						infoMessage.Visible = true;
						uploadFileImage.Dispose();
						return false;
					}
				}
			} else {
				infoMessage.Type = InfoType.Error;
				infoMessage.Text = "File type must be an image";
				infoMessage.Visible = true;
				return false;
			}
		} else {
			infoMessage.Type = InfoType.Warning;
			infoMessage.Text = "Name and image must be provided";
			infoMessage.Visible = true;
			return false;
		}
	}

	/// <summary>
	/// resizes the original image into the raw folder
	/// </summary>
	/// <param name="uploadFileImage"></param>
	/// <returns></returns>
	private bool CreateCropImage(Bitmap uploadFileImage) {
		//try {
		//need to preceed this with working area dimensions
		int width, height;
		float aspectRatio = (float)uploadFileImage.Width / (float)uploadFileImage.Height;
		float finalAspectRatio = (float)(Convert.ToInt32(MIN_WIDTH)) / (float)(Convert.ToInt32(MIN_HEIGHT));
		if (aspectRatio > 1) {
			if (aspectRatio == finalAspectRatio) {
				height = Convert.ToInt32(MIN_HEIGHT);
				width = Convert.ToInt32(MIN_WIDTH);
			} else if (aspectRatio < finalAspectRatio) {
				//special case
				//image is likely to squarer

				width = Convert.ToInt32(MIN_WIDTH);
				height = (int)(Convert.ToInt32(MIN_WIDTH) / aspectRatio);

				//height = Convert.ToInt32(MIN_HEIGHT) + 100;
				//width = (int)(Convert.ToInt32(height) * aspectRatio);
			} else {
				//this case is if the aspect ratio is > than our final
				// i.e. more widescreen
				height = Convert.ToInt32(MIN_HEIGHT);
				width = (int)(Convert.ToInt32(MIN_HEIGHT) * aspectRatio);
			}
		} else {
			width = Convert.ToInt32(MIN_WIDTH);
			height = (int)(Convert.ToInt32(MIN_WIDTH) / aspectRatio);
		}
		using (System.Drawing.Image thumbnail = new Bitmap(width, height)) {
			Graphics graphics = Graphics.FromImage(thumbnail);

			graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
			graphics.CompositingQuality = CompositingQuality.HighQuality;

			graphics.DrawImage(uploadFileImage, 0, 0, width, height);

			ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
			EncoderParameters encoderParameters;
			encoderParameters = new EncoderParameters(1);
			encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 99L);

			localFileName = DateTime.Now.ToString("yyyy-MM-dd") + "-" + fileNameTextbox.Text + ".jpg";

			thumbnail.Save(Server.MapPath(ORIGINAL_IMAGE + "/" + localFileName), info[1], encoderParameters);
		}

		uploadFileImage.Dispose();
		//graphics.Dispose();
		//thumbnail.Dispose();
		return true;
		//} catch (Exception ex) {
		/*infoMessage.Type = InfoType.Error;
		infoMessage.Text += " Error: " + ex.Message + " ";
		infoMessage.Visible = true;
		return false;*/
		//}
	}

	private bool CropAndGenerateImages(int leftX, int topY, int selectedWidth, int selectedHeight) {
		string fileName = image_target.ImageUrl.Substring(image_target.ImageUrl.LastIndexOf("/") + 1).ToLower();

		if (fileName.IndexOf('.') == -1)
			return false;

		//Response.Write(Server.MapPath(ORIGINAL_IMAGE + fileName));
		Bitmap ImageToCrop = new Bitmap(Server.MapPath(ORIGINAL_IMAGE + fileName));

		//System.Drawing.Image thumbnail = new Bitmap(721, 417);
		//create the image
		//Graphics graphics = Graphics.FromImage(thumbnail);

		/**/

		ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
		EncoderParameters encoderParameters;
		encoderParameters = new EncoderParameters(1);
		encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 99L);

		//dimensions of biggest final image
		Bitmap CroppedImage = GetCroppedImage(ImageToCrop, leftX, topY, 721, 417);

		localFileName = DateTime.Now.ToString("yyyy-MM-dd") + "-" + fileNameTextbox.Text + ".jpg";

		CroppedImage.Save(Server.MapPath(NEWS_FULL + "/" + localFileName), info[1], encoderParameters);

		//dimensions of thumb image

		System.Drawing.Image thumbnail = new Bitmap(300, 142);
		Graphics graphics = Graphics.FromImage(thumbnail);
		graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
		graphics.SmoothingMode = SmoothingMode.HighQuality;
		graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
		graphics.CompositingQuality = CompositingQuality.HighQuality;

		graphics.DrawImage(CroppedImage, 0, 0, 300, 174);

		thumbnail.Save(Server.MapPath(NEWS_MEDIUM + "/" + localFileName), info[1], encoderParameters);

		//dimensions of small thumb

		System.Drawing.Image smThumbnail = new Bitmap(84, 72);
		graphics = Graphics.FromImage(smThumbnail);
		graphics.DrawImage(CroppedImage, -20, 0, 124, 72);
		smThumbnail.Save(Server.MapPath(NEWS_THUMB + "/" + localFileName), info[1], encoderParameters);

		//dimensions of Homepage

		System.Drawing.Image NewsFull = new Bitmap(651, 370);
		graphics = Graphics.FromImage(NewsFull);
		graphics.DrawImage(CroppedImage, 0, -5, 651, 378);
		NewsFull.Save(Server.MapPath(NEWS_FULL + "/" + localFileName), info[1], encoderParameters);

		//dimensions of new full

		System.Drawing.Image homeImage = new Bitmap(620, 326);
		graphics = Graphics.FromImage(homeImage);
		graphics.DrawImage(CroppedImage, 0, 0, 620, 360);
		homeImage.Save(Server.MapPath(HOMEPAGE + "/" + localFileName), info[1], encoderParameters);

		graphics.Dispose();
		thumbnail.Dispose();
		CroppedImage.Dispose();

		//thumbnail.Dispose();

		return true;
	}

	public Bitmap GetCroppedImage(Bitmap source, int x1, int y1, int cropWidth, int cropHeight) {
		Rectangle cropRect = new Rectangle(0, 0, cropWidth, cropHeight);
		Bitmap crop = new Bitmap(cropRect.Width, cropRect.Height);

		using (Graphics g = Graphics.FromImage(crop)) {
			//g.DrawImage(source, cropRect, new Rectangle(((source.Width / 2) - (cropWidth / 2)), ((source.Height / 2) - (cropHeight / 2)), crop.Width, crop.Height), GraphicsUnit.Pixel);
			g.DrawImage(source, cropRect, new Rectangle(x1, y1, crop.Width, crop.Height), GraphicsUnit.Pixel);
			g.InterpolationMode = InterpolationMode.HighQualityBicubic;
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.PixelOffsetMode = PixelOffsetMode.HighQuality;
			g.CompositingQuality = CompositingQuality.HighQuality;
		}
		return crop;
	}
}