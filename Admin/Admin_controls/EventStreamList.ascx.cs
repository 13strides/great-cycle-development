﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ni.Events.StreamManagement;


public partial class NIEventStreamList : System.Web.UI.UserControl
{

	int _subEventID;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

	public void BindStreamTable(int eventID) {
		EventStreamContainer thisList = new EventStreamContainer(eventID);
	//	lblEventTitle.Text = thisList._event_name;


		eventStreamRepeater.DataSource = thisList.EventStreams;
			eventStreamRepeater.DataBind();
	}

	/// <summary>
	/// gets or sets the page to load
	/// </summary>
	public int SubEventID {
		get { return _subEventID; }
		set {
			_subEventID = value;
			BindStreamTable(_subEventID);
		}
	}
	protected void Button1_Click(object sender, EventArgs e) {
		Response.Redirect("AddStreamInfo.aspx?id=" + _subEventID.ToString());
	}

	public string StatusBind(object id) {
		return ((SwimEventStatus)int.Parse(id.ToString())).ToString();
	}
}
