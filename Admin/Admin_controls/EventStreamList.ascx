﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventStreamList.ascx.cs" Inherits="NIEventStreamList" %>



<asp:Label ID="lblEventTitle" runat="server" Text=""></asp:Label>
<asp:Repeater ID="eventStreamRepeater" runat="server">
<HeaderTemplate>
<table>
<tr>
<th>Date</th>
<th>StreamID</th>
<th>Status</th>
</tr>
</HeaderTemplate>

<ItemTemplate>
<tr>
<td><a href="/Admin/Events/EditStream.aspx?id=<%#  DataBinder.Eval(Container.DataItem,"eventInfoID") %>"><%#  DataBinder.Eval(Container.DataItem,"PublicationDate") %></a></td>
<td><%#  DataBinder.Eval(Container.DataItem,"StreamId") %></td>
<td><%#  StatusBind(DataBinder.Eval(Container.DataItem,"Status")) %></td>

</tr>
</ItemTemplate>

<FooterTemplate>
</table>
</FooterTemplate>
	
</asp:Repeater>
<asp:Button ID="Button1" runat="server" Text="Add New Status" 
	onclick="Button1_Click" />