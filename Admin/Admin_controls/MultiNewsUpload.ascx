﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiNewsUpload.ascx.cs"
	Inherits="Admin_Admin_controls_MultiNewsUpload" %>
<div class="content-box">
	<div class="content-box-header">
		<h3>
			News Image Upload</h3>
	</div>
	<div class="content-box-content">
		<gs:info runat="server" id="infoMessage" cssclass="info">
		</gs:info>
		<asp:Panel runat="server" ID="UploadWizardPanel">
			<table>
				<tr>
					<td>
						Short Name:
					</td>
					<td>
						<asp:TextBox runat="server" ID="fileNameTextbox" />
					</td>
				</tr>
				<tr>
					<td>
						File:
					</td>
					<td>
						<asp:FileUpload ID="fieldFileUpload" runat="server" />
					</td>
				</tr>
				<%--<tr>
                <td colspan="2">

                    <asp:RadioButtonList ID="RblOrientation" runat="server">
                        <asp:ListItem Selected="True" Text="Portrait" Value="1" />
                        <asp:ListItem Text="Landscape (not applicable)" Value="2" />
                    </asp:RadioButtonList>
                </td>
            </tr>--%>
				<tr>
					<td colspan="2">
						<asp:Button ID="FinishStep1" runat="server" Text="Upload" OnClick="Upload_Click" />
					</td>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel runat="server" ID="CropWizardPanel" Visible="false">
			<div style="padding: 10px;">
				<asp:Button ID="ButtonCrop" runat="server" OnClientClick="return getCoordinates();"
					Text="Crop" OnClick="ButtonCrop_Click" />
			</div>
			<table>
				<tr>
					<td>
						<asp:HiddenField ID="FileNameOnPreview" Value="" runat="server" />
					</td>
				</tr>
				<tr>
					<td>
						<asp:Image ID="image_target" CssClass="target-image" runat="server" />
					</td>
				</tr>
				<tr>
					<td>
						<div>
							X1
							<input name="x" id="x" type="text" size="4" readonly="readonly" />
							<asp:HiddenField ID="LeftX" runat="server" />
							Y1
							<input name="y" id="y" type="text" size="4" readonly="readonly" />
							<asp:HiddenField ID="TopY" runat="server" />
							X2
							<input name="x2" id="x2" type="text" size="4" readonly="readonly" />
							<asp:HiddenField ID="RightX" runat="server" />
							Y2
							<input name="y2" id="y2" type="text" size="4" readonly="readonly" />
							<asp:HiddenField ID="BottomY" runat="server" />
							W
							<input name="w" id="w" type="text" size="4" readonly="readonly" />
							<asp:HiddenField ID="SelectedWidth" runat="server" />
							H
							<input name="h" id="h" type="text" size="4" readonly="readonly" /></div>
						<asp:HiddenField ID="SelectedHeight" runat="server" />
					</td>
				</tr>
			</table>
			<p>
				<asp:HyperLink ID="HLinkBackToList1" runat="server" NavigateUrl="~/Admin/NewsArticles/Images/Default.aspx">Back to Image List</asp:HyperLink></p>
		</asp:Panel>
		<asp:Panel runat="server" ID="FinishWizardPanel" Visible="false">
			<h3>
				News Article Image</h3>
			<asp:Image runat="server" ID="newsArticleImage" />
			<h3>
				News Medium Thumbnail</h3>
			<asp:Image runat="server" ID="newsMediumThumbImage" />
			<h3>
				News Small Thumbnail</h3>
			<asp:Image runat="server" ID="newsSmallThumbImage" />
			<h3>
				Website Homepage hero</h3>
			<asp:Image runat="server" ID="homepageHeroImage" />
			<p>
				<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Admin/NewsArticles/Images/Default.aspx">Back to Image List</asp:HyperLink></p>
		</asp:Panel>
	</div>
</div>