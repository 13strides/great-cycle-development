﻿<%@ Page Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="EditNews.aspx.cs" Inherits="admin_News_EditNews" Title="Edit News" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<ni:NiSwimSubNav runat="server" ID="swimSubNav" />

    <div class="content-box">
        <div class="content-box-header">
             <h3>Edit News</h3>
        </div>    
        <div class="content-box-content">
            <gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
        <table width="600px" id="editTable">
            <tr>
                <th style="width:15%">Title: </th><td><asp:TextBox runat="server" ID="titleTextBox" CssClass="text" /></td>
            </tr>
                       <tr>
            <th style="width:15%">HTML Title: </th><td><asp:TextBox runat="server" ID="htmlTitleTextBox" CssClass="text" /></td>
        </tr>
            <tr>
                <th>Publication Date:</th><td><gs:DatePicker ID="publicationDate" runat="server" /></td>
            </tr>
            <tr>
                <th>Expiration Date:</th><td><gs:DatePicker ID="expirationDate" runat="server" /></td>
            </tr>
            <tr>
                <th>Summary:</th><td><asp:TextBox runat="server" ID="summaryTextBox" CssClass="full" TextMode="MultiLine" Rows="8" /></td>
            </tr>
            <tr>
                <th>Content:</th><td><asp:TextBox runat="server" ID="contentTextBox" CssClass="full" TextMode="MultiLine" Rows="15" /></td>
            </tr>
            <tr>
                <th>Event:</th><td><asp:DropDownList runat="server" ID="eventListDropDown" /></td>
            </tr>
            <tfoot>
            <tr>
                <td>&nbsp;</td><td><asp:Button runat="server" ID="saveButton" OnClick="SaveButtonClk" Text="save" CssClass="button" /></td>
            </tr></tfoot>
            <!--<tr>
                <td colspan="2">Image tag:<br />
                                <code>&lt;p&gt;&lt;img src="../App_Files/Gr_Files/xxxFILENAMExxx" alt="xxxDESCRIPTIONxxx" /&gt;&lt;/p&gt;</code>
                                <br />Link tag:<br />
                                <code>&lt;a href="xxxhttp://www.address.comxxx" target="_blank"&gt;xxxCLICK HERE textxxx&lt;/a&gt;</code></td>
            </tr>-->
        </table>
        </div>
    </div>
    
   <script type="text/javascript">
       new TINY.editor.edit('editor', {
       id: 'ctl00_MainContent_summaryTextBox',
	        width:800,
	        height:175,
	        cssclass:'te',
	        controlclass:'tecontrol',
	        rowclass:'teheader',
	        dividerclass:'tedivider',
	        controls:['bold','italic','underline','strikethrough','|','subscript','superscript','|',
			          'orderedlist','unorderedlist','|','outdent','indent','|','leftalign',
			          'centeralign','rightalign','blockjustify','|','unformat','|','undo','redo','n',
			          '|','image','hr','link','unlink','|','cut','copy','paste'],
	        footer:true,
	        fonts:['Verdana','Arial','Georgia','Trebuchet MS'],
	        xhtml:true,
	        cssfile:'style.css',
	        bodyid:'editor',
	        footerclass:'tefooter',
	        toggle:{text:'source',activetext:'wysiwyg',cssclass:'toggle'},
	        resize:{cssclass:'resize'}
	    });
	    new TINY.editor.edit('editor2', {
	    id: 'ctl00_MainContent_contentTextBox',
	        width: 800,
	        height: 175,
	        cssclass: 'te',
	        controlclass: 'tecontrol',
	        rowclass: 'teheader',
	        dividerclass: 'tedivider',
	        controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
			          'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
			          'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
			          '|', 'image', 'hr', 'link', 'unlink', '|', 'cut', 'copy', 'paste'],
	        footer: true,
	        fonts: ['Verdana', 'Arial', 'Georgia', 'Trebuchet MS'],
	        xhtml: true,
	        cssfile: 'style.css',
	        bodyid: 'editor',
	        footerclass: 'tefooter',
	        toggle: { text: 'source', activetext: 'wysiwyg', cssclass: 'toggle' },
	        resize: { cssclass: 'resize' }
	    });
</script>
    
</asp:Content>

