﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ThirteenStrides.Database;
using GS.Generic.Web;
using GS.Generic.Membership;
using GS.Generic.Events;

public partial class admin_News_EditNews : System.Web.UI.Page
{
    int _news_id;
    protected void Page_Load(object sender, EventArgs e)
    {

        
            if (Request.QueryString["nid"] != null)
            {
                try
                {
                    _news_id = Convert.ToInt32(Request.QueryString["nid"]);
                    if (!IsPostBack)
                    {
                        eventListDropDown.DataSource = new EventList();
                        eventListDropDown.DataTextField = "Name";
                        eventListDropDown.DataValueField = "Id";
                        eventListDropDown.DataBind();
                        BindData(Convert.ToInt32(Request.QueryString["nid"]));
                        
                    }
                }
                catch
                {
                    Response.Redirect("Default.aspx");

                }
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        

    }

    protected void BindData(int newsId)
    {
        MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

        SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, _news_id) };

        SqlDataReaderResponse reader = connection.RunCommand("gs_select_news_by_id", paras);

        if (reader.ReturnCode == 0)
        {
            if (reader.Result.Read())
            {
                _news_id = Convert.ToInt32(reader.Result["news_id"]);
                titleTextBox.Text = reader.Result["news_title"].ToString();
				htmlTitleTextBox.Text = reader.Result["news_html_title"].ToString();
                publicationDate.SelectedDate = Convert.ToDateTime(reader.Result["news_publication_date"]);
                expirationDate.SelectedDate = Convert.ToDateTime(reader.Result["news_expiration_date"]);
                summaryTextBox.Text = reader.Result["news_summary"].ToString();
                contentTextBox.Text = reader.Result["news_content"].ToString();
                eventListDropDown.SelectedValue = reader.Result["news_event"].ToString();
            }
            else
            {
                reader.Result.Close();
                throw new Exception("The news wasn't found in the database.");
            }
            reader.Result.Close();
        }
        else
        {
            throw new Exception(reader.ErrorMessage);
        }
    }
    protected void SaveButtonClk(Object sender, EventArgs e)
    {
        if (SaveNews())
        {
            infoMessage.Text = "News Updated <a href=\"default.aspx\">Click here to return to main news</a>";
            infoMessage.Type = InfoType.Success;
        }
        else
        {
            infoMessage.Text = "News Didn't get updated <a href=\"default.aspx\">Click here to return to main news</a>";
            infoMessage.Type = InfoType.Error;
        }
        
    }
    private bool SaveNews()
    {
        //need a few checks in here for min value
        if ((summaryTextBox.Text != String.Empty) || (titleTextBox.Text != String.Empty) || (contentTextBox.Text != String.Empty))
        {
            MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

            SqlParam[] paras = {new SqlParam("@id", SqlDbType.Int, _news_id),
                               new SqlParam("@publicationDate", SqlDbType.DateTime, publicationDate.SelectedDate),
                               new SqlParam("@expirationDate", SqlDbType.DateTime, expirationDate.SelectedDate),
                               new SqlParam("@title", SqlDbType.VarChar, 256, titleTextBox.Text),
							   new SqlParam("@htmltitle", SqlDbType.VarChar, 256, htmlTitleTextBox.Text),
                               new SqlParam("@summary", SqlDbType.Text, summaryTextBox.Text),
                               new SqlParam("@content", SqlDbType.Text, contentTextBox.Text),
                               new SqlParam("@status", SqlDbType.SmallInt, NewsStatus.New),
                               new SqlParam("@userId", SqlDbType.VarChar, 36, GS.Generic.GreatSwim.CurrentUser.Details.Id),
                               new SqlParam("@event", SqlDbType.Int, eventListDropDown.SelectedValue)};

            Int32Response result = connection.RunInt32Command("gs_save_news", paras);

            if (result.ReturnCode >= 0)
            {
                //want to redirect here
                return true;
            }
            else
            {
                throw new Exception(result.ErrorMessage);
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
