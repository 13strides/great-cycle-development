﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Gr.Model;
using Gr.Utils;
using GS.Generic.Events;
using GS.Generic.Web;
using ThirteenStrides.Web;

public partial class Admin_BlogArticles_EditNormal : System.Web.UI.Page {
	int articleID = 0;
	BlogArticle ba = null;

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			BindStatusList();
			int.TryParse(Request.QueryString["nid"], out articleID);

			ba = BlogArticle.GetByID(articleID, true, 0);
			//Response.Write(na.ID.ToString());
			if (ba != null && ba.ID > 0) {
				BindBlogArticle();
			} else {
				//no news found
			}
			GetFileList();
		}
	}

	protected void GetFileList() {
		fileListRepeater.DataSource = (new DirectoryInfo(Server.MapPath("~/App_Files/ImageLibrary/NewsThumb/"))).GetFiles();
		fileListRepeater.DataBind();
	}

	protected void BackStepButtonClk(Object sender, EventArgs e) {
		Response.Redirect("~/Admin/BlogArticles/Default.aspx");
	}

	private void BindBlogArticle() {
		//Response.Write(na.ID.ToString());
		blogTitleTextbox.Text = ba.Title;
		blogDateTextbox.Text = ba.PublishDate.ToString("yyyy-MM-dd");
		shortDescriptionTextbox.Text = ba.ShortDescription;
		articleTextBox.Text = ba.ArticleContent;
		TbMetaTitle.Text = ba.MetaTitle;
		TbMetaKey.Text = ba.MetaKey;
		TbMetaDescription.Text = ba.MetaDescription;
		TbFriendlyUrl.Text = ba.FriendlyUrl;
		BindCategoryList();

		blogStatusDropDown.SelectedValue = ba.BlogStatus.ToString();
		blogImage.ImageUrl = "/App_Files/ImageLibrary/NewsMedium/" + ba.ImageFileName;

		//bind active tags
		bool isFirst = true;
		string tagsString = "";
		foreach (BlogArticleTag tag in ba.Tags) {
			if (isFirst) {
				tagsString = tag.Name;
				isFirst = false;
			} else {
				tagsString += "," + tag.Name;
			}
		}

		//find the html control and assign the string
		//HtmlInputHidden hiddenTags = (HtmlInputHidden)this.Master.FindControl("singleTagsField");
		singleTagsField.Value = tagsString;
	}

	private void BindCategoryList() {
		List<BlogArticleTag> list = BlogArticleTag.GetBlogTags();

		/*foreach (BlogArticleTag tag in list) {
			if (tag.IsVisible) {
				tagsCBL.Items.Add(new ListItem(tag.Name, tag.ID.ToString()));
			}
		}

		foreach (ListItem item in tagsCBL.Items) {
			if (ba.Tags.Contains(Convert.ToInt32(item.Value))) {
				item.Selected = true;
			}
		}*/

		bool counter = true;
		//LiteralControl lit = new LiteralControl();
		StringBuilder script = new StringBuilder("");
		script.Append("<script type=\"text/javascript\">");
		script.Append("$(function () {");
		script.Append("var sampleTags = [");
		//var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];

		tagsLiteral.Text = script.ToString();

		foreach (BlogArticleTag tag in list) {
			StringBuilder sb = new StringBuilder("");
			if (counter) {
				sb.Append("'" + tag.Name + "'");
				counter = false;
			} else {
				sb.Append(",'" + tag.Name + "'");
			}
			tagsLiteral.Text += sb.ToString();
		}

		StringBuilder endScript = new StringBuilder("");
		endScript.Append("];");
		endScript.Append("$('#singleFieldTags').tagit({");
		endScript.Append("		availableTags: sampleTags,");
		//endScript.Append("		// This will make Tag-it submit a single form value, as a comma-delimited field.");
		endScript.Append("		singleField: true,");
		endScript.Append("		singleFieldNode: $('#ctl00_MainContent_singleTagsField')");
		endScript.Append("	});");
		endScript.Append("});");

		endScript.Append("</script>");
		tagsLiteral.Text += endScript.ToString();
	}

	private void BindStatusList() {
		blogStatusDropDown.DataSource = Enum.GetNames(typeof(GS.Generic.Web.NewsStatus));
		blogStatusDropDown.DataBind();
	}

	protected void SaveButtonClk(Object sender, EventArgs e) {
		if (!string.IsNullOrEmpty(blogTitleTextbox.Text)) {
			int.TryParse(Request.QueryString["nid"], out articleID);
			ba = BlogArticle.GetByID(articleID, false, 0);
			ba.Title = blogTitleTextbox.Text;
			ba.ArticleContent = articleTextBox.Text;
			ba.ShortDescription = shortDescriptionTextbox.Text;
			ba.MetaTitle = TbMetaTitle.Text;
			ba.MetaDescription = TbMetaDescription.Text;
			ba.MetaKey = TbMetaKey.Text;
			if (!string.IsNullOrEmpty(TbFriendlyUrl.Text)) {
				ba.FriendlyUrl = StringManuplation.CleanStringForURL(TbFriendlyUrl.Text);
			} else {
				ba.FriendlyUrl = StringManuplation.CleanStringForURL(blogTitleTextbox.Text);
			}
			if (blogDateTextbox.Text != String.Empty) {
				ba.PublishDate = Convert.ToDateTime(blogDateTextbox.Text);
				ba.ExpiryDate = Convert.ToDateTime(blogDateTextbox.Text).AddYears(1);
			}
			//get the filename from the image picker
			if (imageFileNameHidden.Value != String.Empty) {
				ba.ImageFileName = imageFileNameHidden.Value;
			}

			//ba.LastEdited = GS.Generic.GreatSwim.CurrentUser.Details.Username + " @:" + DateTime.Now.ToShortDateString();
			ba.LastEdited = "Admin @:" + DateTime.Now.ToShortDateString();
			ba.BlogStatus = (NewsStatus)Enum.Parse(typeof(NewsStatus), blogStatusDropDown.SelectedValue);

			//Extract the tags
			ba.Tags.Clear();
			BlogArticleTag bat = new BlogArticleTag();
			ba.Tags.ArticleId = ba.ID;
			if (!String.IsNullOrEmpty(singleTagsField.Value)) {
				string[] tagsList = singleTagsField.Value.Split(',');
				foreach (string tag in tagsList) {
					try {
						bat = new BlogArticleTag();
						Response.Write("stringtag = " + tag.ToLower() + ",<br />");
						bat.Name = tag.ToLower();
						ba.Tags.Add(bat);
					} catch (Exception ex) {
						infoMessage.Text = "A tag wasn't assigned. (Error Message: " + ex.Message + ")";
						infoMessage.Type = InfoType.Error;
					}
				}
			}
			Response.Write(ba.Tags.Count);

			foreach (BlogArticleTag tag in ba.Tags) {
				Response.Write(tag.Name);
			}

			//Extract the checkbox list
			/*BlogArticleTag nac = new BlogArticleTag();
			ba.Tags.ArticleId = ba.ID;
			foreach (ListItem item in tagsCBL.Items) {
				if (item.Selected == true) {
					//add category map
					try {
						nac = new BlogArticleTag();
						nac.ID = Convert.ToInt32(item.Value);
						ba.Tags.Add(nac);
					} catch (Exception ex) {
						infoMessage.Text = "Some tags weren't assigned. (Error Message: " + ex.Message + ")";
						infoMessage.Type = InfoType.Error;
					}
				}
			}*/
			//Response.Write(na.Categories.Count);

			//if (na != null && na.ID > 0) {
			BlogArticle resp = BlogArticle.Save(ba);
			if (resp != null && resp.ID > 0) {
				infoMessage.Text = "Successfully Saved.";
				infoMessage.Type = InfoType.Success;
				redirector.Visible = true;
				editPanel.Visible = false;
			} else {
				infoMessage.Text = "There was an error whilst saving the blog article.";
				infoMessage.Type = InfoType.Error;
			}/**/
			//}
		}
	}

	protected string GetFriendlyName(string fullFileName) {
		//Response.Write(fullFileName);
		if (fullFileName.Length > 11) {
			string strToShorten = fullFileName.Substring(11, fullFileName.Length - 11);
			return strToShorten;
		}
		return "";
	}
}