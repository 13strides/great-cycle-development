﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Gr.Model;
using Gr.Utils;
using GS.Generic.Web;
using ThirteenStrides.Web;

public partial class Admin_BlogArticles_CreateNormal : System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			GetFileList();
			BindCategoryList();
			//expirationDate.SelectedDate = (DateTime)DateTime.Today.AddYears(1);
		}
	}

	protected void GetFileList() {
		fileListRepeater.DataSource = (new DirectoryInfo(Server.MapPath("~/App_Files/ImageLibrary/NewsThumb/"))).GetFiles();
		fileListRepeater.DataBind();
	}

	protected void BackStepButtonClk(Object sender, EventArgs e) {
		Response.Redirect("~/Admin/BlogArticles/Default.aspx");
	}

	protected void BindCategoryList() {
		List<BlogArticleTag> list = BlogArticleTag.GetBlogTags();

		/*foreach (BlogArticleTag tag in list) {
			if (tag.IsVisible) {
				tagsCBL.Items.Add(new ListItem(tag.Name, tag.ID.ToString()));
			}
		}

		foreach (ListItem item in tagsCBL.Items) {
			if (ba.Tags.Contains(Convert.ToInt32(item.Value))) {
				item.Selected = true;
			}
		}*/

		bool counter = true;
		//LiteralControl lit = new LiteralControl();
		StringBuilder script = new StringBuilder("");
		script.Append("<script type=\"text/javascript\">");
		script.Append("$(function () {");
		script.Append("var sampleTags = [");
		//var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];

		tagsLiteral.Text = script.ToString();

		foreach (BlogArticleTag tag in list) {
			StringBuilder sb = new StringBuilder("");
			if (counter) {
				sb.Append("'" + tag.Name + "'");
				counter = false;
			} else {
				sb.Append(",'" + tag.Name + "'");
			}
			tagsLiteral.Text += sb.ToString();
		}

		StringBuilder endScript = new StringBuilder("");
		endScript.Append("];");
		endScript.Append("$('#singleFieldTags').tagit({");
		endScript.Append("		availableTags: sampleTags,");
		//endScript.Append("		// This will make Tag-it submit a single form value, as a comma-delimited field.");
		endScript.Append("		singleField: true,");
		endScript.Append("		singleFieldNode: $('#ctl00_MainContent_singleTagsField')");
		endScript.Append("	});");
		endScript.Append("});");

		endScript.Append("</script>");
		tagsLiteral.Text += endScript.ToString();
	}

	protected void SaveButtonClk(Object sender, EventArgs e) {
		if (!string.IsNullOrEmpty(blogTitleTextbox.Text)) {
			BlogArticle ba = new BlogArticle();
			ba.ID = -1;
			ba.Title = blogTitleTextbox.Text;
			ba.ArticleContent = articleTextBox.Text; //articleContentHidden.Value;
			ba.ShortDescription = shortDescriptionTextbox.Text;
			ba.PublishDate = Convert.ToDateTime(blogDateTextbox.Text);
			ba.ExpiryDate = Convert.ToDateTime(blogDateTextbox.Text).AddYears(1);
			ba.MetaTitle = TbMetaTitle.Text;
			ba.MetaDescription = TbMetaDescription.Text;
			ba.MetaKey = TbMetaKey.Text;
			if (!string.IsNullOrEmpty(TbFriendlyUrl.Text)) {
				ba.FriendlyUrl = StringManuplation.CleanStringForURL(TbFriendlyUrl.Text);
			} else {
				ba.FriendlyUrl = StringManuplation.CleanStringForURL(blogTitleTextbox.Text);
			}
			//get the filename from the image picker
			ba.ImageFileName = imageFileNameHidden.Value;
			//ba.Author =GS.Generic.GreatSwim.CurrentUser.Id;
			//ba.LastEdited = GS.Generic.GreatSwim.CurrentUser.Details.Username + " @:" + DateTime.Now.ToShortDateString();
			ba.Author = ConfigurationManager.AppSettings["ApplicationGUID"].ToString();
			ba.LastEdited = "Admin @:" + DateTime.Now.ToShortDateString();

			ba.IsJunior = ba.IsOnRunning = ba.AllowComments = false;

			ba.BlogStatus = NewsStatus.Live;
			//new tag implementation

			//Extract the tags
			ba.Tags.Clear();
			BlogArticleTag bat = new BlogArticleTag();
			ba.Tags.ArticleId = ba.ID;
			if (!String.IsNullOrEmpty(singleTagsField.Value)) {
				string[] tagsList = singleTagsField.Value.Split(',');
				foreach (string tag in tagsList) {
					try {
						bat = new BlogArticleTag();
						Response.Write("stringtag = " + tag.ToLower() + ",<br />");
						bat.Name = tag.ToLower();
						ba.Tags.Add(bat);
					} catch (Exception ex) {
						infoMessage.Text = "A tag wasn't assigned. (Error Message: " + ex.Message + ")";
						infoMessage.Type = InfoType.Error;
					}
				}
			}
			/*Response.Write(ba.Tags.Count);

			foreach (BlogArticleTag tag in ba.Tags) {
				Response.Write(tag.Name);
			}*/

			//Extract the checkbox list
			/*BlogArticleTag bat = new BlogArticleTag();
			foreach (ListItem item in tagsCBL.Items) {
				if (item.Selected == true) {
					//add category map
					try {
						bat.ID = Convert.ToInt32(item.Value);
						ba.Tags.Add(bat);
					} catch (Exception ex) {
						infoMessage.Text = "Some tag weren't assigned. (Error Message: " + ex.Message + ")";
						infoMessage.Type = InfoType.Error;
					}
				}
			}/**/

			//if (na != null && na.ID > 0) {
			BlogArticle resp = BlogArticle.Save(ba);
			if (resp != null && resp.ID > 0) {
				//infoMessage.Text = "Successfully Saved.";
				infoMessage.Type = InfoType.Success;
				createPanel.Visible = false;
				redirector.Visible = true;
			} else {
				infoMessage.Text = "There was an error whilst saving the blog article. " + resp.ErrorMessage;
				infoMessage.Type = InfoType.Error;
			}
			//}
		}
	}

	protected string GetFriendlyName(string fullFileName) {
		//Response.Write(fullFileName);
		if (fullFileName.Length > 11) {
			string strToShorten = fullFileName.Substring(11, fullFileName.Length - 11);
			return strToShorten;
		}
		return "";
	}
}