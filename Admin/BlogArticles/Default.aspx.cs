﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Gr.Model;
using GS.Generic.Events;
using GS.Generic.Membership;
using GS.Generic.Web;
using ThirteenStrides.Database;


public partial class Admin_BlogArticles_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
        else
        {
            BindBlogList();
        }
    }

    protected string HightlightNew(Object count)
    {
        if (count != DBNull.Value)
        {
            int c = (int)count;
            if (c > 0)
            {
                return "<span style=\"color: red; font-weight: bold\">" + c.ToString() + "</span>";
            }
            else
            {
                return c.ToString();
            }
        }
        else
        {
            return "0";
        }
    }

    protected void AddDeleteConfirmation(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {
            // adds a confirmation javascript to ensure that deleting the template is really what the user wants.
            ((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this blog?')";
        }
    }

    protected void BindData()
    {
        BindBlogList();

        BindTopLevelDDL();
    }

    private void BindBlogList()
    {
        List<BlogArticle> blogs = BlogArticle.GetBlogs(0, false, false);
        if (blogs != null && blogs.Count > 0)
        {
            pager.ControlToPage = blogListRepeater;
            pager.DataSource = blogs;
            pager.DataBind();
        }
      
        
    }

    private void BindTopLevelDDL()
    {
        List<BlogArticleTag> list = BlogArticleTag.GetBlogTags();

        foreach (BlogArticleTag tag in list)
        {
            if (tag.IsTopLevel)
            {
                topLevelCatsDDL.Items.Add(new ListItem(tag.Name, tag.ID.ToString()));
            }
        }
    }

    protected void OnFilterClk(Object sender, EventArgs e)
    {
        BindBlogList();
    }

    protected string FormatStatus(Object status)
    {
        NewsStatus s = (NewsStatus)Convert.ToInt32(status);
        switch (s)
        {
            case NewsStatus.Draft:
                return "<span class=\"status-draft\">" + s.ToString() + "</span>";
            case NewsStatus.Live:
                return "<span class=\"status-live\">" + s.ToString() + "</span>";
            default:
            case NewsStatus.New:
                return "<span class=\"status-new\">" + s.ToString() + "</span>";
        }
    }

    protected void ItemCommandHandler(Object sender, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "edit":
                RedirectForEdition(e.CommandArgument.ToString());
                break;
            case "delete":
                DeleteBlog(Convert.ToInt32(e.CommandArgument));
                break;
        }
    }

    private void RedirectForEdition(string evt)
    {
        string[] info = evt.Split('#');
        if (info.Length == 2)
        {
            Response.Redirect("EditNormal.aspx?nid=" + info[1]);
        }
        else
        {
            infoMessage.Text = "There was an error locating the blog.";
            infoMessage.Type = InfoType.Error;
        }
    }

    private void DeleteBlog(int id)
    {
        Response.Write("<!-- BlogDelete: " + id.ToString() + " -->");
        if (BlogArticle.Delete(id))
        {
            infoMessage.Text = "The blog was successfully deleted.";
            infoMessage.Type = InfoType.Success;
            BindBlogList();
        }
        else
        {
            infoMessage.Text = "The blog couldn't be deleted.";
            infoMessage.Type = InfoType.Error;
        }
    }

    protected string FormatCommentViewUrl(object id)
    {
        return "javascript:Popup('Comments.aspx?id=" + id.ToString() + "','Comments','scrollbars=yes,resizable=yes,width=500,height=500')";
    }

    protected void NewActiveTabClk(Object sender, EventArgs e)
    {
        int tab = 0;
        int.TryParse(topLevelCatsDDL.SelectedValue, out tab);
        if (tab > 0)
        {
            bool trySave = BlogArticleTag.SetActiveTab(tab);
            //SetActiveTab
            if (trySave)
            {
                //infoMessage.Text = "Successfully Saved.";
                infoMessage.Type = InfoType.Success;
                redirector.Visible = true;
            }
            else
            {
                infoMessage.Text = "There was an error whilst saving the Active tab.";
                infoMessage.Type = InfoType.Error;
            }
        }
        else
        {
            infoMessage.Text = "Active tab selection was not made or incorrectly formatted.";
            infoMessage.Type = InfoType.Error;
        }
    }
}