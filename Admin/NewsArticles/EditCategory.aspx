﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="EditCategory.aspx.cs" Inherits="Admin_NewsArticles_EditCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		<!--
			#content table{width:auto;}
		-->
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				Edit News Category</h3>
		</div>
		<div class="content-box-content">
			<gs:Info runat="server" ID="infoMessage" CssClass="info">
			</gs:Info>
			<gr:TimedRedirector CssClass="redirection" runat="server" ID="redirector" RedirectUrl="ManageCategories.aspx"
				Text="The news has been saved successfully." Visible="false" />
			<asp:Panel runat="server" ID="editPanel">
				<div class="newsArticle infull edit">
					<dl>
						<dt>
							<label>
								Name</label></dt>
						<dd>
							<asp:TextBox runat="server" ID="newsTitleTextbox" CssClass="text" /></dd>
						<dt>
							<label>
								Parent</label></dt>
						<dd>
							<asp:DropDownList runat="server" ID="parentDDL">
							</asp:DropDownList>
						</dd>
						<dt>
							<label>
								Top Level</label></dt>
						<dd>
							<asp:RadioButtonList runat="server" ID="IsTopLevelRBL">
								<asp:ListItem Value="0">False</asp:ListItem>
								<asp:ListItem Value="1">True</asp:ListItem>
							</asp:RadioButtonList>
						</dd>
						<dt>
							<label>
								Visible</label></dt>
						<dd>
							<asp:RadioButtonList runat="server" ID="VisibleRBL">
								<asp:ListItem Value="0">False</asp:ListItem>
								<asp:ListItem Value="1">True</asp:ListItem>
							</asp:RadioButtonList>
						</dd>
						<dt>
							<label>
								Order</label></dt>
						<dd>
							<asp:TextBox runat="server" ID="orderTextBox" CssClass="text" /></dd>
					</dl>
				</div>
			</asp:Panel>
			<div class="clear">
			</div>
			<div class="mainActionButtons">
				<asp:Button runat="server" ID="previousStepButton" OnClick="BackStepButtonClk" Text="Back"
					CssClass="" />
				<asp:Button runat="server" ID="saveButton" OnClientClick="MoveContent()" OnClick="SaveButtonClk"
					Text="Save" CssClass="button" />
			</div>
		</div>
	</div>
</asp:Content>