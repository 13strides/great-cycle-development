﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;
using GS.Generic.Web;

public partial class Admin_NewsArticles_ManageCategories : System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		BindTopLevelDDL();
	}

	private void BindTopLevelDDL() {
		List<NewsArticleCategory> EventNews = NewsArticleCategory.GetNewsCategories();
		if (EventNews != null && EventNews.Count > 0) {
			pager.ControlToPage = newsListRepeater;
			pager.DataSource = EventNews;
			pager.DataBind();
		} else {
			pager.DataSource = null;
			pager.DataBind();
			//infoMessage.Text = "There are no forms for this business challenge.";
			//infoMessage.Type = InfoType.Error;
		}
	}

	protected void AddDeleteConfirmation(Object sender, RepeaterItemEventArgs e) {
		if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
			// adds a confirmation javascript to ensure that deleting the template is really what the user wants.
			((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this news?')";
		}
	}

	protected void ItemCommandHandler(Object sender, RepeaterCommandEventArgs e) {
		switch (e.CommandName) {
			case "edit":
				RedirectForEdition(e.CommandArgument.ToString());
				break;
			case "delete":
				//DeleteNews(Convert.ToInt32(e.CommandArgument));
				break;
		}
	}

	private void RedirectForEdition(string evt) {
		string[] info = evt.Split('#');
		if (info.Length == 2) {
			Response.Redirect("EditCategory.aspx?cid=" + info[1]);
		} else {
			infoMessage.Text = "There was an error locating the news.";
			infoMessage.Type = InfoType.Error;
		}
	}
}