﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Gr.Model;
using GS.Generic.Events;
using GS.Generic.Web;
using ThirteenStrides.Web;
using Gr.Utils;
public partial class Admin_NewsArticles_CreateNormal : System.Web.UI.Page {
	int articleID = 0;
	NewsArticle na = null;

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			BindStatusList();
			int.TryParse(Request.QueryString["nid"], out articleID);

			na = NewsArticle.GetByID(articleID, true, 0);
            Response.Write("<!--" + na.FriendlyUrl + "-->");

			if (na != null && na.ID > 0) {
				BindNewsArticle();
			} else {
				//no news found
			}
			GetFileList();
		}
	}

	protected void GetFileList() {
		fileListRepeater.DataSource = (new DirectoryInfo(Server.MapPath("~/App_Files/ImageLibrary/NewsThumb/"))).GetFiles();
		fileListRepeater.DataBind();
	}

	protected void BackStepButtonClk(Object sender, EventArgs e) {
		Response.Redirect("~/Admin/NewsArticles/Default.aspx");
	}

	private void BindNewsArticle() {
		//Response.Write(na.ID.ToString());
		newsTitleTextbox.Text = na.Title;
		newsDateTextbox.Text = na.PublishDate.ToString("yyyy-MM-dd");
		shortDescriptionTextbox.Text = na.ShortDescription;
		articleTextBox.Text = na.ArticleContent;
        TbMetaTitle.Text = na.MetaTitle;
        TbMetaKey.Text = na.MetaKey;
        TbMetaDescription.Text = na.MetaDescription;
        TbFriendlyUrl.Text = na.FriendlyUrl;
		BindCategoryList();
		//newsStatusDropDown.SelectedValue = na.NewsStatus.ToString();
		newsStatusDropDown.SelectedValue = na.NewsStatus.ToString();
		newsImage.ImageUrl = "/App_Files/ImageLibrary/NewsFull/" + na.ImageFileName;
	}

	private void BindCategoryList() {
		List<NewsArticleCategory> list = NewsArticleCategory.GetNewsCategories();

		foreach (NewsArticleCategory cat in list) {
			if (cat.ID > 1 && cat.IsVisible) {
				if (cat.IsTopLevel) {
					categoriesCBL.Items.Add(new ListItem(cat.Name, cat.ID.ToString()));
				} else {
					categoriesCBL.Items.Add(new ListItem("- " + cat.Name, cat.ID.ToString()));
				}
			}
		}
		/*foreach (NewsArticleCategory cat in na.Categories) {
			Response.Write("cat id " + cat.ID);
		}*/
		foreach (ListItem item in categoriesCBL.Items) {
			if (na.Categories.Contains(Convert.ToInt32(item.Value))) {
				item.Selected = true;
			}
		}
	}

	private void BindStatusList() {
		newsStatusDropDown.DataSource = Enum.GetNames(typeof(GS.Generic.Web.NewsStatus));
		newsStatusDropDown.DataBind();
	}

	protected void SaveButtonClk(Object sender, EventArgs e) {
		if (!string.IsNullOrEmpty(newsTitleTextbox.Text) )
        {
			int.TryParse(Request.QueryString["nid"], out articleID);
			na = NewsArticle.GetByID(articleID, false, 0);
			na.Title = newsTitleTextbox.Text;
			na.ArticleContent = articleTextBox.Text;
			na.ShortDescription = shortDescriptionTextbox.Text;
            na.MetaTitle = TbMetaTitle.Text;
            na.MetaDescription = TbMetaDescription.Text;
            na.MetaKey = TbMetaKey.Text;
            if (!string.IsNullOrEmpty(TbFriendlyUrl.Text))
            {
                na.FriendlyUrl = StringManuplation.CleanStringForURL(TbFriendlyUrl.Text);
            }
            else
            {
                na.FriendlyUrl = StringManuplation.CleanStringForURL(newsTitleTextbox.Text);    
            }
           
			//na.NewsStatus = newsStatusDropDown.SelectedValue;
			if (newsDateTextbox.Text != String.Empty) {
				na.PublishDate = Convert.ToDateTime(newsDateTextbox.Text);
				na.ExpiryDate = Convert.ToDateTime(newsDateTextbox.Text).AddYears(1);
			}
			//get the filename from the image picker
			if (imageFileNameHidden.Value != String.Empty) {
				na.ImageFileName = imageFileNameHidden.Value;
			}

			na.LastEdited = GS.Generic.GreatSwim.CurrentUser.Details.Username + " @:" + DateTime.Now.ToShortDateString();
			na.NewsStatus = (GS.Generic.Web.NewsStatus)Enum.Parse(typeof(GS.Generic.Web.NewsStatus), newsStatusDropDown.SelectedValue);

			//Extract the checkbox list
			NewsArticleCategory nac = new NewsArticleCategory();
			na.Categories.ArticleId = na.ID;
			foreach (ListItem item in categoriesCBL.Items) {
				if (item.Selected == true) {
					//add category map
					try {
						nac = new NewsArticleCategory();
						nac.ID = Convert.ToInt32(item.Value);
						na.Categories.Add(nac);
					} catch (Exception ex) {
						infoMessage.Text = "Some categories weren't assigned. (Error Message: " + ex.Message + ")";
						infoMessage.Type = InfoType.Error;
					}
				}
			}
			//Response.Write(na.Categories.Count);

			//if (na != null && na.ID > 0) {
			NewsArticle resp = NewsArticle.Save(na);
			if (resp != null && resp.ID > 0) {
				//infoMessage.Text = "Successfully Saved.";
				infoMessage.Type = InfoType.Success;
				redirector.Visible = true;
				editPanel.Visible = false;
			} else {
				infoMessage.Text = "There was an error whilst saving the news article.";
				infoMessage.Type = InfoType.Error;
			}
			//}
		}
	}

	protected string GetFriendlyName(string fullFileName) {
		//Response.Write(fullFileName);
		if (fullFileName.Length > 11) {
			string strToShorten = fullFileName.Substring(11, fullFileName.Length - 11);
			return strToShorten;
		}
		return "";
	}
}