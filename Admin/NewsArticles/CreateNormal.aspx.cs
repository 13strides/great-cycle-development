﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Gr.Model;
using GS.Generic.Events;
using GS.Generic.Web;
using ThirteenStrides.Web;
using Gr.Utils;
public partial class Admin_NewsArticles_CreateNormal : System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			GetFileList();
			BindCategoryList();
			//expirationDate.SelectedDate = (DateTime)DateTime.Today.AddYears(1);
		}
	}

	protected void GetFileList() {
		fileListRepeater.DataSource = (new DirectoryInfo(Server.MapPath("~/App_Files/ImageLibrary/NewsThumb/"))).GetFiles();
		fileListRepeater.DataBind();
	}

	protected void BackStepButtonClk(Object sender, EventArgs e) {
		Response.Redirect("~/Admin/NewsArticles/Default.aspx");
	}

	protected void BindCategoryList() {
		List<NewsArticleCategory> list = NewsArticleCategory.GetNewsCategories();

		foreach (NewsArticleCategory cat in list) {
			if (cat.ID > 1 && cat.IsVisible) {
				if (cat.IsTopLevel) {
					categoriesCBL.Items.Add(new ListItem(cat.Name, cat.ID.ToString()));
				} else {
					categoriesCBL.Items.Add(new ListItem("- " + cat.Name, cat.ID.ToString()));
				}
			}
		}
	}

	protected void SaveButtonClk(Object sender, EventArgs e) {
		if (!string.IsNullOrEmpty(newsTitleTextbox.Text)) {
			NewsArticle na = new NewsArticle();
			na.ID = -1;
			na.Title = newsTitleTextbox.Text;
			na.ArticleContent = articleTextBox.Text; //articleContentHidden.Value;
			na.ShortDescription = shortDescriptionTextbox.Text;
			na.PublishDate = Convert.ToDateTime(newsDateTextbox.Text);
			na.ExpiryDate = Convert.ToDateTime(newsDateTextbox.Text).AddYears(1);
            na.MetaTitle = TbMetaTitle.Text;
            na.MetaDescription = TbMetaDescription.Text;
            na.MetaKey = TbMetaKey.Text;
            if (!string.IsNullOrEmpty(TbFriendlyUrl.Text))
            {
                na.FriendlyUrl = StringManuplation.CleanStringForURL(TbFriendlyUrl.Text);
            }
            else
            {
                na.FriendlyUrl = StringManuplation.CleanStringForURL(newsTitleTextbox.Text);
            }
			//get the filename from the image picker
			na.ImageFileName = imageFileNameHidden.Value;
			na.Author = Gr.Generic.GreatRun.CurrentUser.Id;
			na.LastEdited = Gr.Generic.GreatRun.CurrentUser.BasicUser.Username + " @:" + DateTime.Now.ToShortDateString();

			na.IsJunior = na.IsOnRunning = na.AllowComments = false;

			na.NewsStatus = GS.Generic.Web.NewsStatus.Draft;
			//Extract the checkbox list
			NewsArticleCategory nac = new NewsArticleCategory();
			foreach (ListItem item in categoriesCBL.Items) {
				if (item.Selected == true) {
					//add category map
					try {
						nac.ID = Convert.ToInt32(item.Value);
						na.Categories.Add(nac);
					} catch (Exception ex) {
						infoMessage.Text = "Some categories weren't assigned. (Error Message: " + ex.Message + ")";
						infoMessage.Type = InfoType.Error;
					}
				}
			}/**/

			//if (na != null && na.ID > 0) {
			NewsArticle resp = NewsArticle.Save(na);
			if (resp != null && resp.ID > 0) {
				//infoMessage.Text = "Successfully Saved.";
				infoMessage.Type = InfoType.Success;
				createPanel.Visible = false;
				redirector.Visible = true;
			} else {
				infoMessage.Text = "There was an error whilst saving the news article.";
				infoMessage.Type = InfoType.Error;
			}
			//}
		}
	}

	protected string GetFriendlyName(string fullFileName) {
		//Response.Write(fullFileName);
		if (fullFileName.Length > 11) {
			string strToShorten = fullFileName.Substring(11, fullFileName.Length - 11);
			return strToShorten;
		}
		return "";
	}
}