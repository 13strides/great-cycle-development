﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Gr.Model;
using GS.Generic.Events;
using GS.Generic.Membership;
using GS.Generic.Web;
using ThirteenStrides.Database;

public partial class Admin_NewsArticles_Default : System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			BindData();
		} else {
			BindNewsList();
		}
	}

	protected string HightlightNew(Object count) {
		if (count != DBNull.Value) {
			int c = (int)count;
			if (c > 0) {
				return "<span style=\"color: red; font-weight: bold\">" + c.ToString() + "</span>";
			} else {
				return c.ToString();
			}
		} else {
			return "0";
		}
	}

	protected void AddDeleteConfirmation(Object sender, RepeaterItemEventArgs e) {
		if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
			// adds a confirmation javascript to ensure that deleting the template is really what the user wants.
			((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this news?')";
		}
	}

	protected void BindData() {
		BindNewsList();

		BindTopLevelDDL();
	}

	private void BindNewsList() {
		List<NewsArticle> EventNews = NewsArticle.GetNews(0, false, false);
		if (EventNews != null && EventNews.Count > 0) {
			pager.ControlToPage = newsListRepeater;
			pager.DataSource = EventNews;
			pager.DataBind();
		} else {
			pager.DataSource = null;
			pager.DataBind();
			//infoMessage.Text = "There are no forms for this business challenge.";
			//infoMessage.Type = InfoType.Error;
		}

		/*MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatRun"].ConnectionString);

		int eventId = Convert.ToInt32(eventList.SelectedValue);
		SqlParam[] paras = { new SqlParam("@eventId", SqlDbType.Int, eventId == -1 ? (object)DBNull.Value : eventId),
			new SqlParam("@notice", SqlDbType.Bit, noticeCheckBox.Checked),
			new SqlParam("@blog", SqlDbType.Bit, blogCheckBox.Checked),
			new SqlParam("@featured", SqlDbType.Bit, featuredCheckBox.Checked),
			new SqlParam("@normal", SqlDbType.Bit, normalCheckBox.Checked),
			new SqlParam("@archive", SqlDbType.Bit, archiveCheckBox.Checked),
			new SqlParam("@language", SqlDbType.VarChar, 10, languageList.SelectedValue == "ALL" ? (Object)DBNull.Value : languageList.SelectedValue) };

		DataSetResponse res = connection.RunCommand("gr_select_news_filtered", paras, "news");

		if (res.ReturnCode == 0) {
			pager.ControlToPage = newsListRepeater;
			pager.DataSource = res.Result.Tables["news"].DefaultView;
			pager.DataBind();
		} else {
			infoMessage.Text = "There was an error looking up for news in the database.";
			infoMessage.Type = InfoType.Error;
		}*/
	}

	private void BindTopLevelDDL() {
		List<NewsArticleCategory> list = NewsArticleCategory.GetNewsCategories();

		foreach (NewsArticleCategory cat in list) {
			if (cat.IsTopLevel) {
				topLevelCatsDDL.Items.Add(new ListItem(cat.Name, cat.ID.ToString()));
			}
		}
	}

	protected void OnFilterClk(Object sender, EventArgs e) {
		BindNewsList();
	}

	protected string FormatStatus(Object status) {
		NewsStatus s = (NewsStatus)Convert.ToInt32(status);
		switch (s) {
			case NewsStatus.Draft:
				return "<span class=\"status-draft\">" + s.ToString() + "</span>";
				break;
			case NewsStatus.Live:
				return "<span class=\"status-live\">" + s.ToString() + "</span>";
				break;
			default:
			case NewsStatus.New:
				return "<span class=\"status-new\">" + s.ToString() + "</span>";
				break;
		}
	}

	protected void ItemCommandHandler(Object sender, RepeaterCommandEventArgs e) {
		switch (e.CommandName) {
			case "edit":
				RedirectForEdition(e.CommandArgument.ToString());
				break;
			case "delete":
				DeleteNews(Convert.ToInt32(e.CommandArgument));
				break;
		}
	}

	private void RedirectForEdition(string evt) {
		string[] info = evt.Split('#');
		if (info.Length == 2) {
			Response.Redirect("EditNormal.aspx?nid=" + info[1]);
		} else {
			infoMessage.Text = "There was an error locating the news.";
			infoMessage.Type = InfoType.Error;
		}
	}

	private void DeleteNews(int id) {
		//NewsArticle news = NewsArticle.GetByID(id, false, 0);
		if (NewsArticle.Delete(id)) {
			infoMessage.Text = "The news was successfully deleted.";
			infoMessage.Type = InfoType.Success;
			BindNewsList();
		} else {
			infoMessage.Text = "The news couldn't be deleted.";
			infoMessage.Type = InfoType.Error;
		}
	}

	protected string FormatCommentViewUrl(object id) {
		return "javascript:Popup('Comments.aspx?id=" + id.ToString() + "','Comments','scrollbars=yes,resizable=yes,width=500,height=500')";
	}

	protected void NewActiveTabClk(Object sender, EventArgs e) {
		bool trySave = NewsArticleCategory.SetActiveTab(Convert.ToInt32(topLevelCatsDDL.SelectedValue));
		//SetActiveTab
		if (trySave) {
			//infoMessage.Text = "Successfully Saved.";
			infoMessage.Type = InfoType.Success;
			redirector.Visible = true;
		} else {
			infoMessage.Text = "There was an error whilst saving the Active tab.";
			infoMessage.Type = InfoType.Error;
		}
	}
}