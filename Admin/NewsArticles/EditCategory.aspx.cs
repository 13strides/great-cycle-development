﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gr.Model;

public partial class Admin_NewsArticles_EditCategory : System.Web.UI.Page {
	int categoryID = 0;
	NewsArticleCategory nac = null;

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			int.TryParse(Request.QueryString["cid"], out categoryID);
			nac = NewsArticleCategory.GetByID(categoryID);
			if (nac != null && nac.ID > 0) {
				BindNewsCategory();
			} else {
				//no news found
			}
		}
	}

	private void BindNewsCategory() {
		//Response.Write(na.ID.ToString());
		newsTitleTextbox.Text = nac.Name;
		if (nac.IsTopLevel) {
			IsTopLevelRBL.SelectedValue = "1";
		} else {
			IsTopLevelRBL.SelectedValue = "0";
		}
		if (nac.IsVisible) {
			VisibleRBL.SelectedValue = "1";
		} else {
			VisibleRBL.SelectedValue = "0";
		}
		orderTextBox.Text = nac.Order.ToString();
		BindData();
		//newsStatusDropDown.SelectedValue = na.NewsStatus.ToString();
	}

	protected void BindData() {
		List<NewsArticleCategory> list = NewsArticleCategory.GetNewsCategories();

		foreach (NewsArticleCategory cat in list) {
			if (cat.IsVisible) {
				if (cat.IsTopLevel) {
					parentDDL.Items.Add(new ListItem(cat.Name, cat.ID.ToString()));
				} else {
					parentDDL.Items.Add(new ListItem("- " + cat.Name, cat.ID.ToString()));
				}
			}
		}
		/*foreach (NewsArticleCategory cat in na.Categories) {
			Response.Write("cat id " + cat.ID);
		}*/
		foreach (ListItem item in parentDDL.Items) {
			if (nac.Parent == Convert.ToInt32(item.Value)) {
				item.Selected = true;
			}
		}
	}

	protected void SaveButtonClk(Object sender, EventArgs e) {
		if (!string.IsNullOrEmpty(newsTitleTextbox.Text)) {
			int.TryParse(Request.QueryString["cid"], out categoryID);
			nac = NewsArticleCategory.GetByID(categoryID);
			nac.Name = newsTitleTextbox.Text;

			if (VisibleRBL.SelectedValue == "1") {
				nac.IsVisible = true;
			} else {
				nac.IsVisible = false;
			}

			if (IsTopLevelRBL.SelectedValue == "1") {
				nac.IsTopLevel = true;
			} else {
				nac.IsTopLevel = false;
			}
			nac.Parent = Convert.ToInt32(parentDDL.SelectedValue);
			nac.Order = Convert.ToInt32(orderTextBox.Text);

			//if (na != null && na.ID > 0) {
			NewsArticleCategory resp = NewsArticleCategory.Save(nac);
			if (resp != null && resp.ID > 0) {
				//infoMessage.Text = "Successfully Saved.";
				infoMessage.Type = GS.Generic.Web.InfoType.Success;
				redirector.Visible = true;
				editPanel.Visible = false;
			} else {
				infoMessage.Text = "There was an error whilst saving the news category.";
				infoMessage.Type = GS.Generic.Web.InfoType.Error;
			}
			//}
		}
	}

	protected void BackStepButtonClk(Object sender, EventArgs e) {
		Response.Redirect("~/Admin/NewsArticles/ManageCategories.aspx");
	}
}