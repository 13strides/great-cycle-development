﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="uploadImage.aspx.cs" Inherits="Admin_NewsArticles_Images_uploadImage" %>

<%@ Register TagPrefix="grt" TagName="Uploader" Src="~/Admin/Admin_controls/MultiNewsUpload.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<link href="/Admin/Admin_Css/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
	<script src="/Admin/Admin_Scripts/NIjquery.Jcrop.js" type="text/javascript"></script>
	<script type="text/javascript" language="Javascript">
		$(function () {
			$('#ctl00_MainContent_Uploader1_image_target').Jcrop({
				onChange: showCoords,
				onSelect: showCoords
			});
		});
		function showCoords(c) {
			$('#x').val(c.x);
			$('#y').val(c.y);
			$('#x2').val(c.x2);
			$('#y2').val(c.y2);
			$('#w').val(c.w);
			$('#h').val(c.h);
		};
		function getCoordinates() {
			var x1 = document.getElementById("x").value;
			var x2 = document.getElementById("x2").value;
			var y1 = document.getElementById("y").value;
			var y2 = document.getElementById("y2").value;
			var w = document.getElementById("w").value;
			var h = document.getElementById("h").value;
			if (x1.length == 0 || x2.length == 0 || y1.length == 0 || y2.length == 0 || w.length == 0 || h.length == 0) {
				return false;
			}
			else {
				document.getElementById("ctl00_MainContent_Uploader1_LeftX").value = x1;
				document.getElementById("ctl00_MainContent_Uploader1_RightX").value = x2;
				document.getElementById("ctl00_MainContent_Uploader1_TopY").value = y1;
				document.getElementById("ctl00_MainContent_Uploader1_BottomY").value = y2;
				document.getElementById("ctl00_MainContent_Uploader1_SelectedWidth").value = w;
				document.getElementById("ctl00_MainContent_Uploader1_SelectedHeight").value = h;
				return true;
			}
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<%-- <div class="content-box">
		<div class="content-box-header">
		</div>
		<div class="content-box-content">
			<gr:Info runat="server" ID="infoMessage" CssClass="info"></gr:Info>
			<table>
				<tr>
					<td>
						Short Name
					</td>
					<td>
						<asp:TextBox runat="server" ID="fileNameTextbox" CssClass="text" />
					</td>
				</tr>
				<tr>
					<td>
						File:
					</td>
					<td>
						<asp:FileUpload runat="server" ID="fileUpload" CssClass="text" />
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td style="text-align: right">
						<asp:Button runat="server" ID="uploadButton" Text="Upload" CssClass="button" OnClick="UploadBtnClk" />
					</td>
				</tr>
			</table>
		</div>
	</div>--%>
	<grt:uploader id="Uploader1" runat="server" />
</asp:Content>