﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="Default.aspx.cs" Inherits="Admin_NewsArticles_Images_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style>
		<!--
			.imageHolder{height:102px; margin:12px 6px 0 6px; border:1px solid #cdcdcd; position:relative;/*float:left;*/}
			#names li{list-style:none; margin:0; padding:0; float:left; width:100px;}
			#names li p{text-align:center;min-height:40px;font-size:0.8em;}
			.actions a{color:#ffffff;font-size:11px; display:block; padding:4px 5px 3px 0; float:left}
			.actions a img{margin:5px;}
			.actions a:first-child{padding-right:0;}
			.actions{background:black; opacity:0.8; position:absolute; top:-1px; right:-1px; display:none; padding:1px 2px;}
			.imageHolder:hover .actions{display:block}
		-->
	</style>
	<script type="text/javascript" src="/Admin/Admin_Scripts/livesearch.js"></script>
	<script type="text/javascript" charset="utf-8">
		(function ($) {
			$(document).ready(function () {
				$('input[name="q"]').search('#names li', function (on) {
					on.all(function (results) {
						var size = results ? results.size() : 0
						$('#count').text(size + ' results');
					});

					on.reset(function () {
						$('#none').hide();
						$('#names li').show();
					});

					on.empty(function () {
						$('#none').show();
						$('#names li').hide();
					});

					on.results(function (results) {
						$('#none').hide();
						$('#names li').hide();
						results.show();
					});
				});
			});
		})(jQuery);
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				Images</h3>
		</div>
		<div class="content-box-content">
			<p>
				<a href="uploadImage.aspx" style="display: block;">
					<img src="/admin/Images/icons/plus.png">
					Add</a></p>
			<p>
				Search:
				<input type="text" name="q" value="">
				<span id="count"></span>
			</p>
			<p id="none" style="display: none">
				There were no names to match your search!</p>
			<gs:Info runat="server" ID="infoMessage" CssClass="info">
			</gs:Info>
			<asp:Repeater runat="server" ID="fileListRepeater" OnItemDataBound="AddDeleteConfirmation"
				OnItemCommand="ItemCommandHandler">
				<HeaderTemplate>
					<ul id="names">
				</HeaderTemplate>
				<ItemTemplate>
					<li>
						<div class="imageHolder" style="width: 84px; height: 72px; background-color: #ddd">
							<a href="/App_Files/ImageLibrary/Homepage/<%# System.IO.Path.GetFileName(Eval("Name").ToString())  %>"
								rel="prettyPhoto">
								<img src="/App_Files/ImageLibrary/NewsThumb/<%# System.IO.Path.GetFileName(Eval("Name").ToString())  %>"
									alt="" class="lazy" /></a>
							<!--<div class="actions">
							<a href="#" rel="prettyPhoto">
								<img src="/Admin/Images/icons/update.png" /></a>
							<asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("Name") %>'
								CommandName="delete" Text="delete">
									<img src="/Admin/Images/icons/delete.png" /></asp:LinkButton>
						</div>-->
						</div>
						<p>
							<%# GetFriendlyName(System.IO.Path.GetFileName(Eval("Name").ToString()))%></p>
					</li>
					<%-- <tr class="std-row">
						<td>
							<%# System.IO.Path.GetFileNameWithoutExtension(Eval("Name").ToString()) %>
						</td>
						<td>
							<%# Eval("CreationTime", "{0:dd.MM.yyyy}")%>
						</td>
						<td>
							<%# System.IO.Path.GetExtension(Eval("Name").ToString()) %>
						</td>
						<td>
							<%# Math.Round(Convert.ToDouble(Eval("Length")) / (double)1024, 2).ToString() %>
							KB
						</td>
						<td>
							<asp:HyperLink runat="server" ID="viewLink" NavigateUrl='<%# "~/App_Files/Gr_Files/" + Eval("Name") %>'>view</asp:HyperLink>
						</td>
						<td>
						</td>
					</tr>--%>
				</ItemTemplate>
				<FooterTemplate>
					</ul>
				</FooterTemplate>
			</asp:Repeater>
			<div class="clear">
			</div>
			<!--<div class="imageHolder" style="width: 89px; height: 72px; background-color: #ddd">
				<img src="../" alt="" />
				<div class="actions">
					<a href="#">
						<img src="/Admin/Images/icons/update.png" /></a> <a href="#">
							<img src="/Admin/Images/icons/delete.png" /></a>
				</div>
			</div>-->
		</div>
	</div>
</asp:Content>