﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ThirteenStrides.Web;

public partial class Admin_NewsArticles_Images_Default : System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			BindFileList();
		}
	}

	protected void BindFileList() {
		fileListRepeater.DataSource = (new DirectoryInfo(Server.MapPath("~/App_Files/ImageLibrary/NewsThumb/"))).GetFiles();
		fileListRepeater.DataBind();
	}

	protected void AddDeleteConfirmation(Object sender, RepeaterItemEventArgs e) {
		if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
			// adds a confirmation javascript to ensure that deleting the template is really what the user wants.
			((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this file?')";
		}
	}

	protected void ItemCommandHandler(Object sender, RepeaterCommandEventArgs e) {
		switch (e.CommandName) {
			case "delete":
				DeleteFile(e.CommandArgument.ToString());
				break;
		}
	}

	private void DeleteFile(string file) {
		try {
			File.Delete(Server.MapPath("~/App_Files/ImageLibrary/" + file));
			infoMessage.Text = "File successfully deleted.";
			infoMessage.Type = GS.Generic.Web.InfoType.Success;
			BindFileList();
		} catch {
			infoMessage.Text = "There was an error deleting the file.";
			infoMessage.Type = GS.Generic.Web.InfoType.Error;
		}
	}

	protected string GetFriendlyName(string fullFileName) {
		//Response.Write(fullFileName);
		if (fullFileName.Length > 11) {
			string strToShorten = fullFileName.Substring(11, fullFileName.Length - 11);
			return strToShorten;
		}
		return "";
	}
}