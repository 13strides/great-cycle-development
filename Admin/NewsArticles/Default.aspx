﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="Default.aspx.cs" Inherits="Admin_NewsArticles_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript">
		/* <![CDATA[ */

		function Popup(theURL,winName,features) { //v2.0
          popup = window.open(theURL,winName,features);
          popup.focus();
        }

		/* ]]> */
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				News</h3>
		</div>
		<div class="content-box-content">
			<gs:Info runat="server" ID="infoMessage" CssClass="info">
			</gs:Info>
			<gr:TimedRedirector CssClass="redirection" runat="server" ID="redirector" RedirectUrl="Default.aspx"
				Text="The Tab has been saved successfully." Visible="false" />
			<p>
				<img src="../Images/icons/plus.png" /><asp:HyperLink ID="AddContentLink" NavigateUrl="~/Admin/NewsArticles/CreateNormal.aspx"
					runat="server"> Add a content page</asp:HyperLink><br />
				<a href="ManageCategories.aspx">Manage Categories</a></p>
			<asp:Repeater runat="server" ID="newsListRepeater" OnItemCommand="ItemCommandHandler"
				OnItemDataBound="AddDeleteConfirmation">
				<HeaderTemplate>
					<table cellspacing="0" class="pagination" rel="50">
						<thead>
							<tr>
								<!--<th style="width: 3%;">#id</th>-->
								<th>
									Title
								</th>
								<th style="width: 5%;">
									Status
								</th>
								<th style="width: 10%;">
									Pub. Date
								</th>
								<th style="width: 10%;">
									Exp. Date
								</th>
								<th style="width: 5%;">
									Edit
								</th>
								<th style="width: 5%;">
									Delete
								</th>
							</tr>
						</thead>
				</HeaderTemplate>
				<ItemTemplate>
					<tr class="std-row">
						<!--<th><%# Eval("id") %></th>-->
						<td>
							<%# Eval("Title") %>
						</td>
						<td>
							<%# FormatStatus(Eval("newsstatus")) %>
						</td>
						<td>
							<%# Eval("publishDate", "{0:dd/MM/yyyy}") %>
						</td>
						<td>
							<%# Eval("expiryDate", "{0:dd/MM/yyyy}") %>
						</td>
						<td>
							<asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# "#" + Eval("id") %>'
								CommandName="edit" Text="edit" />
						</td>
						<td>
							<asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("id") %>'
								CommandName="delete" Text="delete" />
						</td>
					</tr>
				</ItemTemplate>
				<AlternatingItemTemplate>
					<tr class="alt-row">
						<td>
							<%# Eval("Title") %>
						</td>
						<td>
							<%# FormatStatus(Eval("newsstatus")) %>
						</td>
						<td>
							<%# Eval("publishDate", "{0:dd/MM/yyyy}") %>
						</td>
						<td>
							<%# Eval("expiryDate", "{0:dd/MM/yyyy}") %>
						</td>
						<td>
							<asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# "#" + Eval("id") %>'
								CommandName="edit" Text="edit" />
						</td>
						<td>
							<asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("id") %>'
								CommandName="delete" Text="delete" />
						</td>
					</tr>
				</AlternatingItemTemplate>
				<FooterTemplate>
					</table></FooterTemplate>
			</asp:Repeater>
			<gr:RepeaterPager runat="server" ID="pager" PageSize="50" CssClass="pager" CurrentPageLinkCssClass="current-page-link"
				LinksCssClass="page-link" NextPreviousCssClass="next-prev-link" NextPreviousDisabledCssClass="next-prev-disabled">
			</gr:RepeaterPager>
		</div>
	</div>
	<div class="content-box closed">
		<div class="content-box-header">
			<h3>
				Active Tab</h3>
		</div>
		<div class="content-box-content">
			<table>
				<tr>
					<td width="25%">
						Select the active tab:
					</td>
					<td>
						<asp:DropDownList runat="server" ID="topLevelCatsDDL">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
					<td>
						<asp:Button runat="server" ID="saveButton" OnClick="NewActiveTabClk" Text="Save"
							CssClass="button" />
					</td>
				</tr>
			</table>
		</div>
	</div>
</asp:Content>