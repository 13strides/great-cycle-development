﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
	CodeFile="ManageCategories.aspx.cs" Inherits="Admin_NewsArticles_ManageCategories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				News Categories</h3>
		</div>
		<div class="content-box-content">
			<gs:Info runat="server" ID="infoMessage" CssClass="info">
			</gs:Info>
			<asp:Repeater runat="server" ID="newsListRepeater" OnItemCommand="ItemCommandHandler"
				OnItemDataBound="AddDeleteConfirmation">
				<HeaderTemplate>
					<table cellspacing="0" class="pagination" rel="50">
						<thead>
							<tr>
								<th style="width: 3%;">
									#id
								</th>
								<th>
									Name
								</th>
								<th style="width: 5%;">
									Parent
								</th>
								<th style="width: 5%;">
									Order
								</th>
								<th style="width: 10%;">
									Active
								</th>
								<th style="width: 5%;">
									Edit
								</th>
								<th style="width: 5%;">
									Delete
								</th>
							</tr>
						</thead>
				</HeaderTemplate>
				<ItemTemplate>
					<tr class="std-row">
						<th>
							<%# Eval("id") %>
						</th>
						<td>
							<%# Eval("Name") %>
						</td>
						<td>
							<%# Eval("Parent") %>
						</td>
						<td>
							<%# Eval("Order") %>
						</td>
						<td>
							<%# Eval("IsVisible")%>
						</td>
						<td>
							<asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# "#" + Eval("id") %>'
								CommandName="edit" Text="edit" />
						</td>
						<td>
							<asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("id") %>'
								CommandName="delete" Text="delete" />
						</td>
					</tr>
				</ItemTemplate>
				<AlternatingItemTemplate>
					<tr class="alt-row">
						<th>
							<%# Eval("id") %>
						</th>
						<td>
							<%# Eval("Name") %>
						</td>
						<td>
							<%# Eval("Parent") %>
						</td>
						<td>
							<%# Eval("Order") %>
						</td>
						<td>
							<%# Eval("IsVisible")%>
						</td>
						<td>
							<asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# "#" + Eval("id") %>'
								CommandName="edit" Text="edit" />
						</td>
						<td>
							<asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("id") %>'
								CommandName="delete" Text="delete" />
						</td>
					</tr>
				</AlternatingItemTemplate>
				<FooterTemplate>
					</table></FooterTemplate>
			</asp:Repeater>
			<gr:RepeaterPager runat="server" ID="pager" PageSize="50" CssClass="pager" CurrentPageLinkCssClass="current-page-link"
				LinksCssClass="page-link" NextPreviousCssClass="next-prev-link" NextPreviousDisabledCssClass="next-prev-disabled">
			</gr:RepeaterPager>
		</div>
	</div>
</asp:Content>