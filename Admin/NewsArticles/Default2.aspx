﻿<%@ Page Title="" ValidateRequest="false" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master"
	AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Admin_NewsArticles_Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		<!--
			.edit{width:721px; margin:0 auto;}
			.edit h2, .edit p{border:2px dashed #efefef;padding:5px;}
			/*.pullleft{float:left;width:260px; height:260px; margin-right:30px;margin-left:-30px;padding:10px;border:2px dashed #FCEFA1;}
			.pullright{float:right;width:260px; height:260px;margin-left:30px; margin-right:-30px;padding:10px;border:2px dashed #FCEFA1;}*/
			.videoInStory{}
			.tableInStory{}
			.tableInStory table{background:url(/App_Images/css/Backgrounds/news_tableInStory.gif) repeat-x 0 42px}
			.tableInStory table tr{border:1px solid #abddff}
			.tableInStory table tr th{background-color:#d5eafb;padding:12px 8px}
			.tableInStory table tr td{padding:12px 8px; border-bottom: 1px solid #dddddd}
			.quoteInStory{font-style:italic;width:400px;
              height:auto;
              background:#f0f0f0 url(/App_Images/css/Backgrounds/news_quoteInStory.gif)
              no-repeat bottom left;
              }
			.quoteInStory p{font-size:1.5em;margin:15px 10px; padding:0 20px;
                background: url(/App_Images/css/Backgrounds/news_quoteInStory_before.gif)
              no-repeat top left;}
			.quoteInStory p em{font-style:normal; font-size:0.9em;display:inline;}

			.shareInstory{margin:30px;}

			.imageHolder{height:102px; margin:12px 6px 0 6px; border:1px solid #cdcdcd; position:relative;float:left;}
			.actions a{color:#ffffff;font-size:11px; display:block; padding:4px 5px 3px 0; float:left}
			.actions a img{margin:5px;}
			.actions a:first-child{padding-right:0;}
			.actions{background:black; opacity:0.8; position:absolute; top:-1px; right:-1px; display:none; padding:1px 2px;}
			.imageHolder:hover .actions{display:block}

			#popUpFiles{display:none;position:absolute; top:50px; left:20%; width:50%;padding:50px; height:50%;
				border:2px solid #cccccc; border-radius:10px;background-color:#ffffff;
			}
			.headlineHolder{background-color:#cdcdcd; margin:12px 6px 0 6px; border:1px solid #cdcdcd;min-height:200px; }
			#redactorHeading{width:100%;}
			#date{border:none;}
		-->
	</style>
	<link rel="stylesheet" href="js/redactor/redactor.css" />
	<script src="js/redactor/redactor.js"></script>
	<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	<%-- <script type="text/javascript">
	    $(document).ready(
		function () {
		    $('#redactor').redactor({air:true});
		}
	);
	</script>	--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				Edit Article</h3>
		</div>
		<div class="content-box-content">
			<script type="text/javascript">
				function exampleClickToEdit() {
					$("#redactor_content").sortable("disable");
					$('#redactor_content').redactor({ focus: true, fixed: true });

				}
				function exampleClickToSave() {
					$("#redactor_content").sortable("enable");
					// save content if you need
					var html = $('#redactor_content').getCode();

					// destroy editor
					$('#redactor_content').destroyEditor();
				}
				function setUpImageSelector() {
					$("#chooseImage").click(function () {
						$("#popUpFiles").show();
					});

					$("a.select-image").click(function () {
						/*alert(
						$(this).attr("rel")
						);*/
						$("#headlineImage").attr("src", "/App_Files/ImageLibrary/NewsFull/" + $(this).attr("rel"));
						$("#popUpFiles").hide();
					});
					$("#close-popupfiles").click(function () {
						$("#popUpFiles").hide();
					});
				}

				$(function () {
					$("#redactor_content").sortable({
						items: "h2:not(.ui-state-not-sortable),p:not(.ui-state-not-sortable),div:not(.ui-state-not-sortable)"
					});
					$("#datePick").datepicker();
					$("#redactorHeading").redactor({ air: true });
					setUpImageSelector();
				});
			</script>
			<div class="newsArticle infull edit ">
				<input type="text" id="redactorHeading" value="Mokoka and Pavey score contrasting victories in Bupa Great South Run" />
				<!--<h2 class="ui-state-not-sortable">
					Mokoka and Pavey score contrasting victories in Bupa Great South Run</h2>-->
				<p id="date" class="ui-state-not-sortable">
					<input type="datepicker" id="datePick" />
				</p>
				<div class="headlineHolder">
					<img src="#" class="headlineImage" id="headlineImage">
					<p>
						<a href="#" id="chooseImage">Choose image</a></p>
				</div>
				.....
				<p>
					<a class="btn" onclick="exampleClickToEdit();">Edit</a> <a class="btn" onclick="exampleClickToSave();">
						Save</a>
				</p>
				.....
				<div id="redactor_content">
					<p>
						<strong>On another day of African domination, Wilson Kipsang and Tirunesh Dibaba scored
							thrilling half marathon victories in wet conditions at the Bupa Great North Run
							on Sunday.</strong>
					</p>
					<p>
						Kipsang the World's second fastest ever marathon runner stepped down to half the
						distance and when all seemed lost, rallied in the last 20 metres to defeat fellow
						Kenyan Micah Kogo by a second in 59 minutes 06 seconds with Imane Merga of Ethiopia
						third in 59:56.
					</p>
					<div class="pullleft videoInStory">
						<iframe width="260" height="250" src="http://www.youtube.com/embed/C1mdvASMgQY" frameborder="0"
							allowfullscreen></iframe>
					</div>
					<p>
						en I saw the clock showing 59:57. But this time it wasn't to be and I want to come
						back and do it next year."
					</p>
					<h3>
						Elite wheelchair event</h3>
					<p>
						Canada's Josh Cassidy who scored his first GNR victory in 2008 and again last year,
						made it a hatrick when easily winning the Wheelchair race by a huge margin in 43:18.
					</p>
					<p>
						The 27-year-old leading from start to finish made up for the disappointment of failing
						to get a medal at the Paralympic Games with a thoroughbread performance ahead of
						Simon Lawson and Phil Hogg who clocked 47:23 and 49:52.
					</p>
					<p>
						Cassidy despite the ease of his victory, said: "It was quite tough out there and
						quite windy. At the halfway point I knew I had won the race but really appreciated
						the great atmosphere displayed by the crowd."
					</p>
					<div class="pullright tableInStory">
						<table cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<th>
									Pos
								</th>
								<th>
									Name
								</th>
								<th>
									Finish Time
								</th>
							</tr>
							<tr>
								<td>
									1
								</td>
								<td>
									Tirunesh Dibaba
								</td>
								<td>
									01:07:35
								</td>
							</tr>
							<tr>
								<td>
									2
								</td>
								<td>
									Edna Kiplagat
								</td>
								<td>
									01:07:41
								</td>
							</tr>
							<tr>
								<td>
									3
								</td>
								<td>
									Tiki Gelana
								</td>
								<td>
									01:07:48
								</td>
							</tr>
							<tr>
								<td>
									4
								</td>
								<td>
									Jelena Prokopcuka
								</td>
								<td>
									01:08:09
								</td>
							</tr>
							<tr>
								<td>
									5
								</td>
								<td>
									Jo Pavey
								</td>
								<td>
									01:09:20
								</td>
							</tr>
						</table>
					</div>
					<p>
						Mickey Bushell the Paralympic T53 gold medallist competing well over distance and
						sixth in 54:34 enjoyed the experience. "I've had a great season. It was very windy
						and wet out there today. Now I'm going to rest for two weeks then begin winter training."
					</p>
					<p>
						Jane Egan the World and European Paratriathlon title holder took the women's race
						in 1:15:00 from Liz McTerran and Kirsty Grange who recorded 1:28:21 and 1:32:56.</p>
					<p>
						Dibaba follows in the footsteps of other great Ethiopian superstars, including her
						cousin Derartu Tulu, Berhane Adere and Gete Wami. Arguably the world's greatest
						ever track distance runner, with three Olympic gold medals under her belt, made
						a superfast start to what promises to be a great road running career. She won in
						a very quick 1:07:35. The 26-year-old was given a close fight by Edna Kiplagat and
						Olympic Marathon champion Tiki Gelana.
					</p>
					<p>
						Kenya's Kiplagat, last year's World Marathon gold medallist, challenged her to the
						bitter end before surrendering to her rival's superior sprinting skills and losing
						out on a win by six seconds. The winner's teammate Gelana who found the pace to
						brisk in the last mile finished in 1:07:48.
					</p>
					<p>
						"It's a new experience for me, but I think I know how to run on the road as
					well as on the track. Yes, I was expecting to win," added Dibaba who showed no signs
					of a post Olympic Games hamstring injury which saw her having urgent pre-race specialist
					treatment.
					<p>
						said Dibaba aiming to move up to the marathon in the very foreseeable future. "I'm
						testing the water and gradually stepping up in distance as I go.
					</p>
					<p>
						Dibaba revealed: "I was surprised the race was so fast early on. I had to take my
						time to make my move. This is my first race over this distance and I wasn't going
						to take any chances.
					</p>
					<div class="pullright quoteInStory">
						<p>
							I believe this experience will help me in the future, <em>Tirunesh Dibaba</em></p>
					</div>
					<p>
						"It has been a great season for me. I won a gold medal over 10,000m at the Olympics
						and also won a bronze in the 5,000m. Now, I have won over 21k and that is also very
						special.
					</p>
					<p>
						"I intend to run my first full marathon in 2013, although I haven't decided which
						marathon yet. It could be in the spring or the autumn, but I will also continue
						on the track and intend to compete in the 10,000m at the worlds.
					</p>
					<p>
						"If the course is good, then in my next half-marathon I intend to go for the world
						record."
					</p>
				</div>
				<div id="popUpFiles">
					<gr:info runat="server" id="infoMessage" cssclass="info"></gr:info>
					<a href="#" id="close-popupfiles">Cancel</a>
					<div class="zone">
						<asp:Repeater runat="server" ID="fileListRepeater" OnItemCommand="ItemCommandHandler">
							<ItemTemplate>
								<div class="imageHolder" style="width: 84px; height: 72px; background-color: #ddd">
									<img src="blank.gif" data-src="/App_Files/ImageLibrary/NewsThumb/<%# System.IO.Path.GetFileName(Eval("Name").ToString())  %>"
										class="lazy">
									<div class="actions">
										<a href="#" class="select-image" rel="<%# System.IO.Path.GetFileName(Eval("Name").ToString())  %>">
											<img src="/Admin/Images/icons/update.png" /></a>
									</div>
								</div>
							</ItemTemplate>
						</asp:Repeater>
					</div>
					<div class="clear">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="../Admin_Scripts/lazyload.js"></script>
</asp:Content>