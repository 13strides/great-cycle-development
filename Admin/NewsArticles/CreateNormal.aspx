﻿<%@ Page Title="" ValidateRequest="false" Language="C#" MasterPageFile="~/Admin/Admin.master"
	AutoEventWireup="true" CodeFile="CreateNormal.aspx.cs" Inherits="Admin_NewsArticles_CreateNormal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	<style type="text/css">
		<!--
			.edit{width:721px;float:left; }
			/*.edit h2, .edit p{border:2px dashed #ff0000;padding:5px;}*/

			.imageHolder{height:102px; margin:12px 6px 0 6px; border:1px solid #cdcdcd; position:relative;float:left;}

			.actions a{color:#ffffff;font-size:11px; display:block; padding:30px; float:left}
			.actions a img{margin:5px;}

			.actions{background:black; opacity:0.2; position:absolute; top:-1px; right:-1px; display:none; padding:1px 2px; text-align:center; width:100%;height:100%}
			.imageHolder:hover .actions{display:block}

			#popUpFiles{display:none;position:absolute; top:50px; left:10%; width:70%;padding:10px; height:500px;
				border:2px solid #cccccc; border-radius:10px;background-color:#ffffff;
			}
			#popUpFiles .zone{height:450px; overflow:auto;}
			#names li{list-style:none; margin:0; padding:0; float:left; width:100px;}
			#names li p{text-align:center;clear:both; min-height:40px;}
			/*.headlineHolder{background-color:#cdcdcd; margin:12px 6px 0 6px; border:1px solid #cdcdcd;min-height:200px; }*/
			#redactorHeading{width:100%;}
			#date{border:none;}
			a.btn{
			position: relative;cursor: pointer;outline: 0;display: inline-block;text-align: center;text-decoration: none;
			font-family: Arial,Helvetica,sans-serif;line-height: 1;font-size: 13px;font-weight: normal;padding: 6px 16px;
			border-radius: 4px;background-color: #F3F3F3;background-image: -moz-linear-gradient(top,white,#E1E1E1);background-image: -ms-linear-gradient(top,white,#E1E1E1);
			background-image: -webkit-gradient(linear,0 0,0 100%,from(white),to(#E1E1E1));
			background-image: -webkit-linear-gradient(top,white,#E1E1E1);
			background-image: -o-linear-gradient(top,white,#E1E1E1);
			background-image: linear-gradient(top,white,#E1E1E1);
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff',endColorstr='#e1e1e1',GradientType=0);
			border: 1px solid #DADADA;
			border-left: 1px solid #D2D2D2;
			border-right: 1px solid #D2D2D2;
			border-bottom-color: darkGray;
			box-shadow: 0 1px 0 rgba(0, 0, 0, 0.15),inset 0 1px 1px 0 rgba(255, 255, 255, 0.6);
			text-shadow: 0 1px 0 white;
			}
			form label{float: none;width: auto;display: block;padding: 4px 0;clear: left;height: auto;}

			.NewsCategories{float:right; width:180px; font-size:0.9em;}
		-->
		</style>
	<script type="text/javascript" src="/Admin/Admin_Scripts/livesearch.js"></script>
	<script type="text/javascript" charset="utf-8">
		(function ($) {
			$(document).ready(function () {
				$('input[name="q"]').search('#names li', function (on) {
					on.all(function (results) {
						var size = results ? results.size() : 0
						$('#count').text(size + ' results');
					});

					on.reset(function () {
						$('#none').hide();
						$('#names li').show();
					});

					on.empty(function () {
						$('#none').show();
						$('#names li').hide();
					});

					on.results(function (results) {
						$('#none').hide();
						$('#names li').hide();
						results.show();
					});
				});
			});
		})(jQuery);
	</script>
	<script type="text/javascript">
		/*function exampleClickToEdit() {
		$("#ctl00_MainContent_redactor_content").sortable("disable");
		$('#ctl00_MainContent_redactor_content').redactor({ focus: true, fixed: true });

		}

		function exampleClickToSave() {
		$("#ctl00_MainContent_redactor_content").sortable("enable");
		// save content if you need
		var html = $('#ctl00_MainContent_redactor_content').getCode();

		// destroy editor
		$('#ctl00_MainContent_redactor_content').destroyEditor();
		}*/
		/*function MoveContent() {
		//articleContentTransfer
		//articleContentHidden

		$("#ctl00_MainContent_articleContentHidden").val(
		$("#ctl00_MainContent_redactor_content").html()
		);
		}*/
		function setUpImageSelector() {
			$("#chooseImage").click(function () {
				$("#popUpFiles").show();
				$("img.lazy").lazyload();
			});

			$("a.select-image").click(function () {
				/*alert(
				$(this).attr("rel")
				);*/
				$("#headlineImage").attr("src", "/App_Files/ImageLibrary/NewsFull/" + $(this).attr("rel"));
				$(".headlineHolder input[type='hidden']").val($(this).attr("rel"))
				$("#popUpFiles").hide();
			});
			$("#close-popupfiles").click(function () {
				$("#popUpFiles").hide();
			});
		}

		$(function () {

			$(".js-datepicker").datepicker();
			$(".js-datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
			setUpImageSelector();

		});

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="content-box">
		<div class="content-box-header">
			<h3>
				Create Article</h3>
		</div>
		<div class="content-box-content">
			<gs:Info runat="server" ID="infoMessage" CssClass="info">
			</gs:Info>
			<gr:TimedRedirector CssClass="redirection" runat="server" ID="redirector" RedirectUrl="Default.aspx"
				Text="The news has been saved successfully." Visible="false" />
			<asp:Panel runat="server" ID="createPanel">
				<div class="newsArticle infull edit ">
					<table>
						<tr>
							<td width="10%">
								Title
							</td>
							<td>
								<asp:TextBox runat="server" ID="newsTitleTextbox" CssClass="text" />
							</td>
						</tr>
						<tr>
							<td style="vertical-align: top;">
								Short Description:
							</td>
							<td>
								<asp:TextBox runat="server" ID="shortDescriptionTextbox" CssClass="text" />
							</td>
						</tr>
						<tr>
							<td>
								Date:
							</td>
							<td>
								<asp:TextBox runat="server" ID="newsDateTextbox" CssClass="js-datepicker" />
							</td>
						</tr>
                        <tr>
							<td width="10%">
								Meta Title
							</td>
							<td>
								<asp:TextBox runat="server" ID="TbMetaTitle" CssClass="text" Columns="70" />
							</td>
						</tr>
						<tr>
							<td>
								Meta Key:
							</td>
							<td> 
								<asp:TextBox runat="server" ID="TbMetaKey" CssClass="text" Columns="70" />
							</td>
						</tr>
                        <tr>
							<td width="10%">
								Meta Description
							</td>
							<td>
								<asp:TextBox runat="server" ID="TbMetaDescription" CssClass="text" Columns="70" />
							</td>
						</tr>
						<tr>
							<td>
								Friendly Url:
							</td>
							<td> 
								<asp:TextBox runat="server" ID="TbFriendlyUrl" CssClass="text" Columns="70" />
							</td>
						</tr>
					</table>
					<div class="headlineHolder">
						<img src="http://placehold.it/721x395" class="headlineImage" id="headlineImage">
						<asp:HiddenField runat="server" ID="imageFileNameHidden" />
						<a href="#" id="chooseImage">Choose image</a>
					</div>
					<br />
					<!--<a class="btn" onclick="exampleClickToEdit();">Edit</a> <a class="btn" onclick="exampleClickToSave();">
						Save</a>-->
					<br />
					<br />
					<!--<div id="redactor_content" runat="server">
						<p>
							&nbsp;</p>
					</div>
					-->
					<asp:TextBox ID="articleTextBox" CssClass="full wysiwyg" runat="server" TextMode="MultiLine"
						Rows="15"></asp:TextBox>
					<div id="articleContentTransfer">
						<asp:HiddenField runat="server" ID="articleContentHidden" />
					</div>
					<div id="popUpFiles">
						<a href="#" id="close-popupfiles">
							<img src="/Admin/Images/icons/cross.png" /></a>
						<p>
							Search:
							<input type="text" name="q" value="">
							<span id="count"></span>
						</p>
						<p id="none" style="display: none">
							There were no names to match your search!</p>
						<div class="zone">
							<asp:Repeater runat="server" ID="fileListRepeater">
								<HeaderTemplate>
									<ul id="names">
								</HeaderTemplate>
								<ItemTemplate>
									<li>
										<div class="imageHolder" style="width: 84px; height: 72px; background-color: #ddd">
											<img src="/App_Files/ImageLibrary/NewsThumb/<%# System.IO.Path.GetFileName(Eval("Name").ToString())  %>">
											<div class="actions">
												<a href="#" class="select-image" rel="<%# System.IO.Path.GetFileName(Eval("Name").ToString())  %>">
													&nbsp;</a>
											</div>
										</div>
										<p>
											<%# GetFriendlyName(System.IO.Path.GetFileName(Eval("Name").ToString()))%></p>
									</li>
								</ItemTemplate>
								<FooterTemplate>
									</ul></FooterTemplate>
							</asp:Repeater>
						</div>
						<div class="clear">
						</div>
					</div>
				</div>
				<div class="NewsCategories">
					<h4>
						Choose Categories</h4>
					<asp:CheckBoxList runat="server" ID="categoriesCBL">
					</asp:CheckBoxList>
				</div>
				<div class="clear">
				</div>
				<div class="mainActionButtons">
					<asp:Button runat="server" ID="previousStepButton" OnClick="BackStepButtonClk" Text="Back"
						CssClass="" />
					<asp:Button runat="server" ID="saveButton" OnClick="SaveButtonClk" Text="Save" CssClass="button" />
					<!-- OnClientClick="MoveContent()" -->
				</div>
			</asp:Panel>
		</div>
	</div>
</asp:Content>