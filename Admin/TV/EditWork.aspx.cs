﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ThirteenStrides.Database;
using GS.Generic.Web;
using GS.Generic.Membership;
using GS.Generic.Events;

public partial class admin_News_EditNews : System.Web.UI.Page
{
	int _service_id;
    protected void Page_Load(object sender, EventArgs e)
    {

        
            if (Request.QueryString["nid"] != null)
            {
                try
                {
                    _service_id = Convert.ToInt32(Request.QueryString["nid"]);
                    if (!IsPostBack)
                    {
                        /*eventListDropDown.DataSource = new EventList();
                        eventListDropDown.DataTextField = "Name";
                        eventListDropDown.DataValueField = "Id";
                        eventListDropDown.DataBind();*/
                        BindData(Convert.ToInt32(Request.QueryString["nid"]));
                        
                    }
                }
                catch
                {
                    Response.Redirect("Default.aspx");

                }
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        

    }

    protected void BindData(int newsId)
    {
		MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

		SqlParam[] paras = { new SqlParam("@id", SqlDbType.Int, _service_id),
						   new SqlParam("@status", SqlDbType.Int, -1)};

		SqlDataReaderResponse reader = connection.RunCommand("gs_select_work_by_id", paras);

        if (reader.ReturnCode == 0)
        {
            if (reader.Result.Read())
            {
                //_news_id = Convert.ToInt32(reader.Result["news_id"]);
     
              //  contentTextBox.Text = reader.Result["news_content"].ToString();
               // eventListDropDown.SelectedValue = reader.Result["news_event"].ToString();
				 publicationDate.SelectedDate= Convert.ToDateTime(reader.Result["work_published"]);
				 //titleTextBox.Text = reader.Result["work_title"].ToString();
				nameTextBox.Text= reader.Result["work_name"].ToString();
				 summaryTextBox.Text = reader.Result["work_description"].ToString();
				 DropDownList1.SelectedValue = reader.Result["work_status"].ToString();
				 DropDownList2.SelectedValue = reader.Result["work_section"].ToString();
				 TextBox1.Text = reader.Result["work_video_path"].ToString();
				 lblImage.Text = reader.Result["work_image_path"].ToString();
				  //FileUpload1.FileName = reader.Result["work_published"].ToString();


            }
            else
            {
                reader.Result.Close();
                throw new Exception("The TV Item wasn't found in the database.");
            }
            reader.Result.Close();
        }
        else
        {
            throw new Exception(reader.ErrorMessage);
        }
    }
    protected void SaveButtonClk(Object sender, EventArgs e)
    {
        if (SaveNews())
        {
			infoMessage.Text = " TV Item Updated <a href=\"default.aspx\">Click here to return to main  TV Item list</a>";
            infoMessage.Type = InfoType.Success;
        }
        else
        {
			infoMessage.Text = " TV Item Didn't get updated <a href=\"default.aspx\">Click here to return to main  TV Item list</a>";
            infoMessage.Type = InfoType.Error;
        }
        
    }
    private bool SaveNews()
    {
        //need a few checks in here for min value
        if ((summaryTextBox.Text != String.Empty) || (titleTextBox.Text != String.Empty) /*|| (contentTextBox.Text != String.Empty)*/)
        {
			MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);



			string filename;
			if (FileUpload1.FileName.Length == 0)
				filename = lblImage.Text;
			else {

				string path = Server.MapPath("~\\App_Files\\Gr_files");
				filename = "work_" + FileUpload1.FileName;
				FileUpload1.SaveAs(path + "\\" + filename);


			}


			SqlParam[] paras = {new SqlParam("@id", SqlDbType.Int,_service_id),
                               new SqlParam("@publicationDate", SqlDbType.DateTime, publicationDate.SelectedDate),
                               new SqlParam("@title", SqlDbType.VarChar, 256, ""),
							    new SqlParam("@name", SqlDbType.VarChar, 256, nameTextBox.Text),
                               new SqlParam("@description", SqlDbType.Text, summaryTextBox.Text),
							   new SqlParam("@status", SqlDbType.Int, int.Parse(DropDownList1.SelectedValue)),
							   new SqlParam("@section", SqlDbType.Int, int.Parse(DropDownList2.SelectedValue)),
							   new SqlParam("@VideoPath", SqlDbType.Text,  TextBox1.Text),
							    new SqlParam("@imageName", SqlDbType.Text, filename),

                              // new SqlParam("@content", SqlDbType.Text, contentTextBox.Text),
                             //  new SqlParam("@status", SqlDbType.SmallInt, NewsStatus.New),
                               //new SqlParam("@userId", SqlDbType.VarChar, 36, GS.Generic.GreatSwim.CurrentUser.Details.Id)
							  // ,                               new SqlParam("@event", SqlDbType.Int, eventListDropDown.SelectedValue)
						   };

			Int32Response result = connection.RunInt32Command("gs_save_work", paras);

            if (result.ReturnCode >= 0)
            {
                //want to redirect here
				BindData(_service_id);
                return true;
            }
            else
            {
                throw new Exception(result.ErrorMessage);
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
