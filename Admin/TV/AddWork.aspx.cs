﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ThirteenStrides.Database;
using GS.Generic.Web;
using GS.Generic.Events;
using GS.Generic.Membership;

public partial class admin_News_AddNews : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            /*eventListDropDown.DataSource = new EventList();
            eventListDropDown.DataTextField = "Name";
            eventListDropDown.DataValueField = "Id";
            eventListDropDown.DataBind();*/
        }
    }

    protected void SaveButtonClk(Object sender, EventArgs e) {
        if (SaveNews())
        {
			infoMessage.Text = "TV Item Added <a href=\"default.aspx\">Click here to return to main TV Item list</a>";
            infoMessage.Type = InfoType.Success;
        }
        else
        {
			infoMessage.Text = "TV Item Didn't get added <a href=\"default.aspx\">Click here to return to main TV Item list</a>";
            infoMessage.Type = InfoType.Error;
        }
        



    }

    private bool SaveNews()
    {
        //need a few checks in here for min value
		MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatSwim"].ConnectionString);

		/*
@id					INT,
@title				VARCHAR(256),
@description text,
@status int,
@publicationDate	DATETIME,
	
@VideoPath			varchar(300),
@imageName varchar(300),
@section				int
*/
		string filename;
		if (FileUpload1.FileName.Length == 0)
			filename = "";
		else {

			string path = Server.MapPath("~\\App_Files\\Gr_files");
			filename = "work_" + FileUpload1.FileName;
			FileUpload1.SaveAs(path + "\\" + filename);


		}
        SqlParam[] paras = {new SqlParam("@id", SqlDbType.Int, -1),
                               new SqlParam("@publicationDate", SqlDbType.DateTime, publicationDate.SelectedDate),
                               new SqlParam("@title", SqlDbType.VarChar, 256,""),
							                                  new SqlParam("@name", SqlDbType.VarChar, 256, nameTextBox.Text),

                               new SqlParam("@description", SqlDbType.Text, summaryTextBox.Text),
							   new SqlParam("@status", SqlDbType.Int, int.Parse(DropDownList1.SelectedValue)),
							   new SqlParam("@section", SqlDbType.Int, int.Parse(DropDownList2.SelectedValue)),
							   new SqlParam("@VideoPath", SqlDbType.Text,  TextBox1.Text),
							    new SqlParam("@imageName", SqlDbType.Text,  filename),

                              // new SqlParam("@content", SqlDbType.Text, contentTextBox.Text),
                             //  new SqlParam("@status", SqlDbType.SmallInt, NewsStatus.New),
                               //new SqlParam("@userId", SqlDbType.VarChar, 36, GS.Generic.GreatSwim.CurrentUser.Details.Id)
							  // ,                               new SqlParam("@event", SqlDbType.Int, eventListDropDown.SelectedValue)
						   };

		Int32Response result = connection.RunInt32Command("gs_save_work", paras);

        if (result.ReturnCode >= 0)
        {
            //want to redirect here
			infoMessage.Text = "TV Item  Added <a href=\"default.aspx\">Click here to return to main TV Item list</a>";
            infoMessage.Type = InfoType.Success;
            return true;
        }
        else
        {
            throw new Exception(result.ErrorMessage);
            return false;
        }
    }
}
