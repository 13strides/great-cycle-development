﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_News_Default" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    <div class="content-box  main">
    <h2>Work</h2>
    <asp:HyperLink runat="server" ID="AddNewsLink" NavigateUrl="addwork.aspx">Add Work</asp:HyperLink>
    <gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
    <asp:Repeater runat="server" ID="newsListRepeater" OnItemCommand="ItemCommandHandler" OnItemDataBound="AddDeleteConfirmation">
   <HeaderTemplate>
            <table cellpadding="0" class="item-list">
                <tr>
                    <th>#id</th>
                    <th>Title</th>
                    <th>Publication Date</th>
                    <th>Section</th>
                    <th>Status</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="std-row">
                <th><%# Eval("work_id") %></th>
                <td><%# Eval("work_name")%></td>

                <%-- <td><%# FormatStatus(Eval("news_Status")) %></td>--%>
                <td><%# Eval("work_published", "{0:dd/MM/yyyy}")%></td>
                <td><%# Eval("work_section") %></td>
                <td><%# FormatStatus(Eval("work_status"))%>
                
                </td>
                <td><asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# Eval("work_id") %>' CommandName="edit" Text="edit" /></td>
				<td><asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("work_id") %>' CommandName="delete" Text="delete" /></td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="alt-row">
                <th><%# Eval("work_id")%></th>
                <td><%# Eval("work_name")%></td>

                <%-- <td><%# FormatStatus(Eval("news_Status")) %></td>--%>
                <td><%# Eval("work_published", "{0:dd/MM/yyyy}")%></td>
                 <td><%# Eval("work_section") %></td>
                  <td><%# FormatStatus(Eval("work_status"))%></td>
                <td><asp:LinkButton runat="server" ID="editButton" CommandArgument='<%# Eval("work_id") %>' CommandName="edit" Text="edit" /></td>
				<td><asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("work_id") %>' CommandName="delete" Text="delete" /></td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
                    <table cellpadding="0" class="item-list">

        </FooterTemplate>
    </asp:Repeater>
    <gr:RepeaterPager runat="server" ID="pager" PageSize="50" CssClass="pager" CurrentPageLinkCssClass="current-page-link" LinksCssClass="page-link" NextPreviousCssClass="next-prev-link" NextPreviousDisabledCssClass="next-prev-disabled"></gr:RepeaterPager>
    </div>
</asp:Content>

