﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="AddWork.aspx.cs" Inherits="admin_News_AddNews" Title="Add News" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <ni:NiSwimSubNav runat="server" ID="swimSubNav" />
    <div class="content-box  main">
    <h2>Create work
    </h2>
    <gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
    <table width="600px">
                   <tr>
            <th style="width:15%">Name: </th><td><asp:TextBox runat="server" ID="nameTextBox" CssClass="text" /></td>
        </tr>
        <!--<tr>
            <th style="width:15%">Title: </th><td><asp:TextBox runat="server" ID="titleTextBox" CssClass="text" /></td>
        </tr>-->
        <tr>
            <th>Publication Date:</th><td><gs:DatePicker ID="publicationDate" runat="server" /></td>
        </tr>
        <tr>
            <th>Into:</th><td><asp:TextBox runat="server" ID="summaryTextBox" CssClass="text" TextMode="MultiLine" Rows="8" /></td>
        </tr>
                        <tr>
            <th>Section:</th>
            <td>
            	<asp:DropDownList ID="DropDownList2" runat="server">
				<asp:ListItem Text="Work" Value="1"></asp:ListItem>
				</asp:DropDownList>
            </td>
        </tr>
                <tr>
            <th>Status:</th>
            <td>
				<asp:DropDownList ID="DropDownList1" runat="server">
				<asp:ListItem Text="Live" Value="1"></asp:ListItem>
				<asp:ListItem Text="Draft" Value="2"></asp:ListItem>
				</asp:DropDownList>
			</td>
        </tr>
                <tr>
            <th style="width:15%">Video Path: </th><td><asp:TextBox runat="server" ID="TextBox1" CssClass="text" /></td>
        </tr>
        
                <tr>
            <th style="width:15%">Image (MUST be 16:9 Ratio): </th><td>
				<asp:FileUpload ID="FileUpload1" runat="server" CssClass="text" />
            
            </td>
        </tr>
        
        
        <tr>
            <td></td><td><asp:Button runat="server" ID="saveButton" OnClick="SaveButtonClk" Text="save" CssClass="button" /></td>
        </tr>
        
        <tr>
            <td colspan="2">Image tag:<br />
                            <code>&lt;p&gt;&lt;img src="../App_Files/Gr_Files/xxxFILENAMExxx" alt="xxxDESCRIPTIONxxx" /&gt;&lt;/p&gt;</code>
                            <br />Link tag:<br />
                            <code>&lt;a href="xxxhttp://www.address.comxxx" target="_blank"&gt;xxxCLICK HERE textxxx&lt;/a&gt;</code>
</td>
        </tr>
    </table>
    </div>
</asp:Content>

