﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Admin_Admin : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.IsAuthenticated && GS.Generic.GreatSwim.CurrentUser == null)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }

        string dir = Path.GetDirectoryName(Request.Url.AbsolutePath);
        string[] folders = dir.Split('\\');

        if (folders.Length > 0)
        {
            switch (folders[folders.Length - 1].ToLower())
            {
                case "news":
                    newsLink.CssClass = "active";
                    break;
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
