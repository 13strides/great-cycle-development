﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ThirteenStrides.Web;
using System.IO;

public partial class admin_Files_Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindFileList();
        }
    }
    protected void BindFileList()
    {
        fileListRepeater.DataSource = (new DirectoryInfo(Server.MapPath("~/App_Files/Gr_Files"))).GetFiles();
        fileListRepeater.DataBind();
    }

    protected void ItemCommandHandler(Object sender, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "delete":
                DeleteFile(e.CommandArgument.ToString());
                break;
        }
    }
    protected void AddDeleteConfirmation(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {
            // adds a confirmation javascript to ensure that deleting the template is really what the user wants.
            ((LinkButton)e.Item.FindControl("deleteButton")).Attributes["onclick"] = "javascript: return confirm('Are you sure you want to delete this file?')";
        }
    }
    private void DeleteFile(string file)
    {
        try
        {
            File.Delete(Server.MapPath("~/App_Files/Gr_Files/" + file));
            infoMessage.Text = "File successfully deleted.";
            infoMessage.Type = GS.Generic.Web.InfoType.Success;
            BindFileList();
        }
        catch
        {
            infoMessage.Text = "There was an error deleting the file.";
            infoMessage.Type = GS.Generic.Web.InfoType.Error;
        }
    }
    protected void UploadBtnClk(Object sender, EventArgs e)
    {
        string fileName = "";

        if (fileUpload.HasFile)
        {
            try
            {
                fileName = ThirteenStrides.Web.Upload.UploadFile(Server.MapPath("~/App_Files/Gr_Files"), fileUpload, new string[] { "png", "jpg", "jpeg", "gif", "pdf", "xls", "doc", "xlsx", "docx", "swf" }, new string[] { }, 8192000);
            }
            catch (Exception ex)
            {
                infoMessage.Text = "An error occured when uploading the file: " + ex.Message;
                infoMessage.Type = GS.Generic.Web.InfoType.Error;
                return;
            }

            infoMessage.Text = "File successfully saved.";
            infoMessage.Type = GS.Generic.Web.InfoType.Success;
            BindFileList();
        }
        else
        {
            infoMessage.Text = "Please pick a file before uploading.";
            infoMessage.Type = GS.Generic.Web.InfoType.Warning;
            return;
        }
    }
}
