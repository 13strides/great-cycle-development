﻿<%@ Page Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_Files_Default2" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Files</h2>
	<gs:Info runat="server" ID="infoMessage" CssClass="info"></gs:Info>
	<h3>Add File</h3>
	
	<table>
		<tr>
			<th>File:</th>
			<td><asp:FileUpload runat="server" ID="fileUpload" CssClass="text" /></td>
		</tr>
		<tr>
			<th></th>
			<td style="text-align: right"><asp:Button runat="server" ID="uploadButton" Text="Upload" CssClass="button" OnClick="UploadBtnClk" /></td>
		</tr>
	</table>
	
	<h3>File List</h3>
	<asp:Repeater runat="server" ID="fileListRepeater" OnItemDataBound="AddDeleteConfirmation" 
	OnItemCommand="ItemCommandHandler">
		<HeaderTemplate>
			<table cellspacing="0" class="item-list">
			<tr><th>Name</th>
				<th style="width: 5%">Creation Date</th>
				<th style="width: 5%">Extension</th>
				<th style="width: 5%">Size (KB)</th>
				<th style="width: 3%;">View</th>
				<th style="width: 5%;">Delete</th>
			</tr>
		</HeaderTemplate>
		<ItemTemplate>
			<tr class="std-row">
				<td><%# System.IO.Path.GetFileNameWithoutExtension(Eval("Name").ToString()) %></td>
				<td><%# Eval("CreationTime", "{0:dd.MM.yyyy}")%></td>
				<td><%# System.IO.Path.GetExtension(Eval("Name").ToString()) %></td>
				<td><%# Math.Round(Convert.ToDouble(Eval("Length")) / (double)1024, 2).ToString() %> KB</td>
				<td><asp:HyperLink runat="server" ID="viewLink" Target="_blank" NavigateUrl='<%# "~/App_Files/Gr_Files/" + Eval("Name") %>'>view</asp:HyperLink></td>
				<td><asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("Name") %>' CommandName="delete" Text="delete" /></td>
			</tr>
		</ItemTemplate>
		<AlternatingItemTemplate>
			<tr class="alt-row">
				<td><%# System.IO.Path.GetFileNameWithoutExtension(Eval("Name").ToString()) %></td>
				<td><%# Eval("CreationTime", "{0:dd.MM.yyyy}")%></td>
				<td><%# System.IO.Path.GetExtension(Eval("Name").ToString()) %></td>
				<td><%# Math.Round(Convert.ToDouble(Eval("Length")) / (double)1024, 2).ToString() %> KB</td>
				<td><asp:HyperLink runat="server" ID="viewLink" Target="_blank" NavigateUrl='<%# "~/App_Files/Gr_Files/" + Eval("Name") %>'>view</asp:HyperLink></td>
				<td><asp:LinkButton runat="server" ID="deleteButton" CommandArgument='<%# Eval("Name") %>' CommandName="delete" Text="delete" /></td>
			</tr>
		</AlternatingItemTemplate>
		<FooterTemplate></table></FooterTemplate>
	</asp:Repeater>
</asp:Content>

