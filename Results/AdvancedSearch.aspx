﻿<%@ Page Title="Great Cycle Series Results" Language="C#" ValidateRequest="false" MasterPageFile="~/Site.master" AutoEventWireup="true" 
CodeFile="AdvancedSearch.aspx.cs" Inherits="Results_AdvancedSearch" %>
<%@ Register TagPrefix="ddlb" Assembly="OptionDropDownList" Namespace="OptionDropDownList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/Results/ExtendedCss/results.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript">
        function addToCompare(name, bib, race) {
            var value = $('#ctl00_MainContent_CompareValue');
            var personName = $('#ctl00_MainContent_CompareName');
            var text = $('#ctl00_MainContent_DivCompare');
            var raceID = $('#ctl00_MainContent_HiddenRaceId');
            var maxComparison = 20;
            name = jQuery.trim(name); //make sure string is trimed
            if (value) {
                //avoid duplicated and excessive entry
                if (value.val().indexOf(bib) >= 0 || value.val().split(",").length > maxComparison) return; 
                if (name.length === 0) name = "Athlete Name";
                value.val(value.val() + bib + "r" + raceID.val() + ",");
            }
            if (text) {
                var raceNameObj = $("#ctl00_MainContent_RaceFullName");
                if (raceNameObj) raceName = raceNameObj.val();
                text.append("<p id='name_" + bib + "' name='" + raceID.val() + "' onclick='removeCompare(this)'><em>" + raceName + "</em><br /><span>" + name + "</span></p>");
                if (personName) {
                    personName.val(personName.val() + name + ",");
                }
                $.ajax({
                    url: 'Ajax/Retain.aspx?name=' + name + '&bib=' + bib + '&rid=' + raceID.val(),
                    error: function() {
                        //$('#message').html("Racers in your compare list may not be retained.");                       
                    }
                });
            }            

        }
        function resetCompare() {
            var value = $('#ctl00_MainContent_CompareValue');
            var text = $('#ctl00_MainContent_DivCompare');
            var personName = $('#ctl00_MainContent_CompareName');
            if (text)
                text.html("");
            if (value)
                value.val("");
            if (personName)
                personName.val("");
            if (value.val().length === 0 && personName.val().length === 0) {
                return true;
            }
            else {
                return false;
            }
        }
        function checkCompare() {
            var value = $('#ctl00_MainContent_CompareValue');
            if (value.val().length <= 0) return false;
        }
        function removeCompare(obj) {
            var element = $("#" + obj.id);
            if (element) {
                var name = jQuery.trim($("#" + obj.id + " span").text());
                var value = $('#ctl00_MainContent_CompareValue');
                var personName = $('#ctl00_MainContent_CompareName');
                var bib = obj.id.split("_")[1];
                if (value && value.val().length > 0) {
                    value.val(value.val().replace(bib + "r" + element.attr("name") + ",", ""));                    
                    if (personName && personName.val().length > 0) {                        
                        personName.val(personName.val().replace(name + ",", ""));
                    }
                    element.remove();

                }
                
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="row">
        <div class="span11" style="padding: 0;
margin: 0;">

            <div id="r-search">
                <div id="advanced-search">
                    <h1>Advanced Search</h1>
                    <span class="event-select">
                        <label>Select Race: </label>
                        <ddlb:OptionGroupSelect ID="RaceList" runat="server" AutoPostBack="True" />
                        <br />
                        <br />
                        <asp:HyperLink ID="HyperLink2" NavigateUrl="~/Results/FullSearch.aspx" runat="server">Full Result List</asp:HyperLink>
                        <asp:HyperLink ID="HyperLinkBack" NavigateUrl="~/Results/Default.aspx" runat="server">Back to Basic Search</asp:HyperLink>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="help.aspx" Target="_blank">Help</asp:HyperLink></span>
                    <div class="clearer"></div>
                </div>


                <fieldset>

                    <label>
                        <asp:Label ID="FirstNameLabel" runat="server" Text="First Name:"></asp:Label></label>
                    <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
                    <input runat="server" id="FirstNameStored" type="hidden" />
                    <label>
                        <asp:Label ID="SurnameLabel" runat="server" Text="Surname:"></asp:Label></label>
                    <asp:TextBox ID="Surname" runat="server"></asp:TextBox>
                    <input runat="server" id="SurnameStored" type="hidden" />



                    <label>
                        <asp:Label ID="AgeGroupLabel" runat="server" Text="Age Group:"></asp:Label>
                        <asp:Label ID="JuniorRaceLabel" runat="server" Text="Junior Race:"></asp:Label>
                        <!-- alternate label -->
                    </label>
                    <asp:DropDownList ID="AgeGroup" runat="server">
                        <asp:ListItem Value="0">-- Age Group Filter --</asp:ListItem>
                        <asp:ListItem>34 and under</asp:ListItem>
                        <asp:ListItem>35-39</asp:ListItem>
                        <asp:ListItem>40-44</asp:ListItem>
                        <asp:ListItem>45-49</asp:ListItem>
                        <asp:ListItem>50-54</asp:ListItem>
                        <asp:ListItem>55-59</asp:ListItem>
                        <asp:ListItem>60-64</asp:ListItem>
                        <asp:ListItem>65-69</asp:ListItem>
                        <asp:ListItem>70-74</asp:ListItem>
                        <asp:ListItem>75-79</asp:ListItem>
                        <asp:ListItem>80+</asp:ListItem>
                    </asp:DropDownList>
                    <input runat="server" id="AgeGroupStored" type="hidden" />
                    <asp:DropDownList ID="JuniorRace" runat="server">
                    </asp:DropDownList>
                    <input runat="server" id="JuniorRaceStored" type="hidden" />
                    <br />
                    <br />




                    <label>
                        <asp:Label ID="CountryLabel" runat="server" Text="Country:"></asp:Label></label>
                    <asp:DropDownList ID="Country" runat="server">
                        <asp:ListItem Value="0">-- Country Filter --</asp:ListItem>
                        <asp:ListItem Value="GB">United Kingdom</asp:ListItem>
                        <asp:ListItem Value="AF">Afghanistan</asp:ListItem>
                        <asp:ListItem Value="AL">Albania</asp:ListItem>
                        <asp:ListItem Value="DZ">Algeria</asp:ListItem>
                        <asp:ListItem Value="AS">American Samoa</asp:ListItem>
                        <asp:ListItem Value="AD">Andorra</asp:ListItem>
                        <asp:ListItem Value="AO">Angola</asp:ListItem>
                        <asp:ListItem Value="AI">Anguilla</asp:ListItem>
                        <asp:ListItem Value="AQ">Antarctica</asp:ListItem>
                        <asp:ListItem Value="AG">Antigua And Barbuda</asp:ListItem>
                        <asp:ListItem Value="AR">Argentina</asp:ListItem>
                        <asp:ListItem Value="AM">Armenia</asp:ListItem>
                        <asp:ListItem Value="AW">Aruba</asp:ListItem>
                        <asp:ListItem Value="AU">Australia</asp:ListItem>
                        <asp:ListItem Value="AT">Austria</asp:ListItem>
                        <asp:ListItem Value="AZ">Azerbaijan</asp:ListItem>
                        <asp:ListItem Value="BS">Bahamas</asp:ListItem>
                        <asp:ListItem Value="BH">Bahrain</asp:ListItem>
                        <asp:ListItem Value="BD">Bangladesh</asp:ListItem>
                        <asp:ListItem Value="BB">Barbados</asp:ListItem>
                        <asp:ListItem Value="BY">Belarus</asp:ListItem>
                        <asp:ListItem Value="BE">Belgium</asp:ListItem>
                        <asp:ListItem Value="BZ">Belize</asp:ListItem>
                        <asp:ListItem Value="BJ">Benin</asp:ListItem>
                        <asp:ListItem Value="BM">Bermuda</asp:ListItem>
                        <asp:ListItem Value="BT">Bhutan</asp:ListItem>
                        <asp:ListItem Value="BO">Bolivia</asp:ListItem>
                        <asp:ListItem Value="BA">Bosnia And Herzegowina</asp:ListItem>
                        <asp:ListItem Value="BW">Botswana</asp:ListItem>
                        <asp:ListItem Value="BV">Bouvet Island</asp:ListItem>
                        <asp:ListItem Value="BR">Brazil</asp:ListItem>
                        <asp:ListItem Value="IO">British Indian Ocean Territory</asp:ListItem>
                        <asp:ListItem Value="BN">Brunei Darussalam</asp:ListItem>
                        <asp:ListItem Value="BG">Bulgaria</asp:ListItem>
                        <asp:ListItem Value="BF">Burkina Faso</asp:ListItem>
                        <asp:ListItem Value="BI">Burundi</asp:ListItem>
                        <asp:ListItem Value="KH">Cambodia</asp:ListItem>
                        <asp:ListItem Value="CM">Cameroon</asp:ListItem>
                        <asp:ListItem Value="CA">Canada</asp:ListItem>
                        <asp:ListItem Value="CV">Cape Verde</asp:ListItem>
                        <asp:ListItem Value="KY">Cayman Islands</asp:ListItem>
                        <asp:ListItem Value="CF">Central African Republic</asp:ListItem>
                        <asp:ListItem Value="TD">Chad</asp:ListItem>
                        <asp:ListItem Value="CL">Chile</asp:ListItem>
                        <asp:ListItem Value="CN">China</asp:ListItem>
                        <asp:ListItem Value="CX">Christmas Island</asp:ListItem>
                        <asp:ListItem Value="CC">Cocos (Keeling) Islands</asp:ListItem>
                        <asp:ListItem Value="CO">Colombia</asp:ListItem>
                        <asp:ListItem Value="KM">Comoros</asp:ListItem>
                        <asp:ListItem Value="CG">Congo</asp:ListItem>
                        <asp:ListItem Value="CK">Cook Islands</asp:ListItem>
                        <asp:ListItem Value="CR">Costa Rica</asp:ListItem>
                        <asp:ListItem Value="CI">Cote D'Ivoire</asp:ListItem>
                        <asp:ListItem Value="HR">Croatia (Local Name: Hrvatska)</asp:ListItem>
                        <asp:ListItem Value="CU">Cuba</asp:ListItem>
                        <asp:ListItem Value="CY">Cyprus</asp:ListItem>
                        <asp:ListItem Value="CZ">Czech Republic</asp:ListItem>
                        <asp:ListItem Value="DK">Denmark</asp:ListItem>
                        <asp:ListItem Value="DJ">Djibouti</asp:ListItem>
                        <asp:ListItem Value="DM">Dominica</asp:ListItem>
                        <asp:ListItem Value="DO">Dominican Republic</asp:ListItem>
                        <asp:ListItem Value="TP">East Timor</asp:ListItem>
                        <asp:ListItem Value="EC">Ecuador</asp:ListItem>
                        <asp:ListItem Value="EG">Egypt</asp:ListItem>
                        <asp:ListItem Value="SV">El Salvador</asp:ListItem>
                        <asp:ListItem Value="GQ">Equatorial Guinea</asp:ListItem>
                        <asp:ListItem Value="ER">Eritrea</asp:ListItem>
                        <asp:ListItem Value="EE">Estonia</asp:ListItem>
                        <asp:ListItem Value="ET">Ethiopia</asp:ListItem>
                        <asp:ListItem Value="FK">Falkland Islands (Malvinas)</asp:ListItem>
                        <asp:ListItem Value="FO">Faroe Islands</asp:ListItem>
                        <asp:ListItem Value="FJ">Fiji</asp:ListItem>
                        <asp:ListItem Value="FI">Finland</asp:ListItem>
                        <asp:ListItem Value="FR">France</asp:ListItem>
                        <asp:ListItem Value="GF">French Guiana</asp:ListItem>
                        <asp:ListItem Value="PF">French Polynesia</asp:ListItem>
                        <asp:ListItem Value="TF">French Southern Territories</asp:ListItem>
                        <asp:ListItem Value="GA">Gabon</asp:ListItem>
                        <asp:ListItem Value="GM">Gambia</asp:ListItem>
                        <asp:ListItem Value="GE">Georgia</asp:ListItem>
                        <asp:ListItem Value="DE">Germany</asp:ListItem>
                        <asp:ListItem Value="GH">Ghana</asp:ListItem>
                        <asp:ListItem Value="GI">Gibraltar</asp:ListItem>
                        <asp:ListItem Value="GR">Greece</asp:ListItem>
                        <asp:ListItem Value="GL">Greenland</asp:ListItem>
                        <asp:ListItem Value="GD">Grenada</asp:ListItem>
                        <asp:ListItem Value="GP">Guadeloupe</asp:ListItem>
                        <asp:ListItem Value="GU">Guam</asp:ListItem>
                        <asp:ListItem Value="GT">Guatemala</asp:ListItem>
                        <asp:ListItem Value="GN">Guinea</asp:ListItem>
                        <asp:ListItem Value="GW">Guinea-Bissau</asp:ListItem>
                        <asp:ListItem Value="GY">Guyana</asp:ListItem>
                        <asp:ListItem Value="HT">Haiti</asp:ListItem>
                        <asp:ListItem Value="HM">Heard And McDonald Islands</asp:ListItem>
                        <asp:ListItem Value="VA">Holy See (Vatican City State)</asp:ListItem>
                        <asp:ListItem Value="HN">Honduras</asp:ListItem>
                        <asp:ListItem Value="HK">Hong Kong</asp:ListItem>
                        <asp:ListItem Value="HU">Hungary</asp:ListItem>
                        <asp:ListItem Value="IS">Iceland</asp:ListItem>
                        <asp:ListItem Value="IN">India</asp:ListItem>
                        <asp:ListItem Value="ID">Indonesia</asp:ListItem>
                        <asp:ListItem Value="IR">Iran (Islamic Republic Of)</asp:ListItem>
                        <asp:ListItem Value="IQ">Iraq</asp:ListItem>
                        <asp:ListItem Value="IE">Ireland</asp:ListItem>
                        <asp:ListItem Value="IL">Israel</asp:ListItem>
                        <asp:ListItem Value="IT">Italy</asp:ListItem>
                        <asp:ListItem Value="JM">Jamaica</asp:ListItem>
                        <asp:ListItem Value="JP">Japan</asp:ListItem>
                        <asp:ListItem Value="JO">Jordan</asp:ListItem>
                        <asp:ListItem Value="KZ">Kazakhstan</asp:ListItem>
                        <asp:ListItem Value="KE">Kenya</asp:ListItem>
                        <asp:ListItem Value="KI">Kiribati</asp:ListItem>
                        <asp:ListItem Value="KP">Korea, Dem People's Republic</asp:ListItem>
                        <asp:ListItem Value="KR">Korea, Republic Of</asp:ListItem>
                        <asp:ListItem Value="KW">Kuwait</asp:ListItem>
                        <asp:ListItem Value="KG">Kyrgyzstan</asp:ListItem>
                        <asp:ListItem Value="LA">Lao People's Dem Republic</asp:ListItem>
                        <asp:ListItem Value="LV">Latvia</asp:ListItem>
                        <asp:ListItem Value="LB">Lebanon</asp:ListItem>
                        <asp:ListItem Value="LS">Lesotho</asp:ListItem>
                        <asp:ListItem Value="LR">Liberia</asp:ListItem>
                        <asp:ListItem Value="LY">Libyan Arab Jamahiriya</asp:ListItem>
                        <asp:ListItem Value="LI">Liechtenstein</asp:ListItem>
                        <asp:ListItem Value="LT">Lithuania</asp:ListItem>
                        <asp:ListItem Value="LU">Luxembourg</asp:ListItem>
                        <asp:ListItem Value="MO">Macau</asp:ListItem>
                        <asp:ListItem Value="MK">Macedonia</asp:ListItem>
                        <asp:ListItem Value="MG">Madagascar</asp:ListItem>
                        <asp:ListItem Value="MW">Malawi</asp:ListItem>
                        <asp:ListItem Value="MY">Malaysia</asp:ListItem>
                        <asp:ListItem Value="MV">Maldives</asp:ListItem>
                        <asp:ListItem Value="ML">Mali</asp:ListItem>
                        <asp:ListItem Value="MT">Malta</asp:ListItem>
                        <asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
                        <asp:ListItem Value="MQ">Martinique</asp:ListItem>
                        <asp:ListItem Value="MR">Mauritania</asp:ListItem>
                        <asp:ListItem Value="MU">Mauritius</asp:ListItem>
                        <asp:ListItem Value="YT">Mayotte</asp:ListItem>
                        <asp:ListItem Value="MX">Mexico</asp:ListItem>
                        <asp:ListItem Value="FM">Micronesia, Federated States</asp:ListItem>
                        <asp:ListItem Value="MD">Moldova, Republic Of</asp:ListItem>
                        <asp:ListItem Value="MC">Monaco</asp:ListItem>
                        <asp:ListItem Value="MN">Mongolia</asp:ListItem>
                        <asp:ListItem Value="MS">Montserrat</asp:ListItem>
                        <asp:ListItem Value="MA">Morocco</asp:ListItem>
                        <asp:ListItem Value="MZ">Mozambique</asp:ListItem>
                        <asp:ListItem Value="MM">Myanmar</asp:ListItem>
                        <asp:ListItem Value="NA">Namibia</asp:ListItem>
                        <asp:ListItem Value="NR">Nauru</asp:ListItem>
                        <asp:ListItem Value="NP">Nepal</asp:ListItem>
                        <asp:ListItem Value="NL">Netherlands</asp:ListItem>
                        <asp:ListItem Value="AN">Netherlands Ant Illes</asp:ListItem>
                        <asp:ListItem Value="NC">New Caledonia</asp:ListItem>
                        <asp:ListItem Value="NZ">New Zealand</asp:ListItem>
                        <asp:ListItem Value="NI">Nicaragua</asp:ListItem>
                        <asp:ListItem Value="NE">Niger</asp:ListItem>
                        <asp:ListItem Value="NG">Nigeria</asp:ListItem>
                        <asp:ListItem Value="NU">Niue</asp:ListItem>
                        <asp:ListItem Value="NF">Norfolk Island</asp:ListItem>
                        <asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
                        <asp:ListItem Value="NO">Norway</asp:ListItem>
                        <asp:ListItem Value="OM">Oman</asp:ListItem>
                        <asp:ListItem Value="PK">Pakistan</asp:ListItem>
                        <asp:ListItem Value="PW">Palau</asp:ListItem>
                        <asp:ListItem Value="PA">Panama</asp:ListItem>
                        <asp:ListItem Value="PG">Papua New Guinea</asp:ListItem>
                        <asp:ListItem Value="PY">Paraguay</asp:ListItem>
                        <asp:ListItem Value="PE">Peru</asp:ListItem>
                        <asp:ListItem Value="PH">Philippines</asp:ListItem>
                        <asp:ListItem Value="PN">Pitcairn</asp:ListItem>
                        <asp:ListItem Value="PL">Poland</asp:ListItem>
                        <asp:ListItem Value="PT">Portugal</asp:ListItem>
                        <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
                        <asp:ListItem Value="QA">Qatar</asp:ListItem>
                        <asp:ListItem Value="RE">Reunion</asp:ListItem>
                        <asp:ListItem Value="RO">Romania</asp:ListItem>
                        <asp:ListItem Value="RU">Russian Federation</asp:ListItem>
                        <asp:ListItem Value="RW">Rwanda</asp:ListItem>
                        <asp:ListItem Value="KN">Saint Kitts And Nevis</asp:ListItem>
                        <asp:ListItem Value="LC">Saint Lucia</asp:ListItem>
                        <asp:ListItem Value="VC">Saint Vincent, The Grenadines</asp:ListItem>
                        <asp:ListItem Value="WS">Samoa</asp:ListItem>
                        <asp:ListItem Value="SM">San Marino</asp:ListItem>
                        <asp:ListItem Value="ST">Sao Tome And Principe</asp:ListItem>
                        <asp:ListItem Value="SA">Saudi Arabia</asp:ListItem>
                        <asp:ListItem Value="SN">Senegal</asp:ListItem>
                        <asp:ListItem Value="SS">Serbia &amp; Montenegro</asp:ListItem>
                        <asp:ListItem Value="SC">Seychelles</asp:ListItem>
                        <asp:ListItem Value="SL">Sierra Leone</asp:ListItem>
                        <asp:ListItem Value="SG">Singapore</asp:ListItem>
                        <asp:ListItem Value="SK">Slovakia (Slovak Republic)</asp:ListItem>
                        <asp:ListItem Value="SI">Slovenia</asp:ListItem>
                        <asp:ListItem Value="SB">Solomon Islands</asp:ListItem>
                        <asp:ListItem Value="SO">Somalia</asp:ListItem>
                        <asp:ListItem Value="ZA">South Africa</asp:ListItem>
                        <asp:ListItem Value="GS">South Georgia, S Sandwich Is.</asp:ListItem>
                        <asp:ListItem Value="ES">Spain</asp:ListItem>
                        <asp:ListItem Value="LK">Sri Lanka</asp:ListItem>
                        <asp:ListItem Value="SH">St. Helena</asp:ListItem>
                        <asp:ListItem Value="PM">St. Pierre And Miquelon</asp:ListItem>
                        <asp:ListItem Value="SD">Sudan</asp:ListItem>
                        <asp:ListItem Value="SR">Suriname</asp:ListItem>
                        <asp:ListItem Value="SJ">Svalbard, Jan Mayen Islands</asp:ListItem>
                        <asp:ListItem Value="SZ">Swaziland</asp:ListItem>
                        <asp:ListItem Value="SE">Sweden</asp:ListItem>
                        <asp:ListItem Value="CH">Switzerland</asp:ListItem>
                        <asp:ListItem Value="SY">Syrian Arab Republic</asp:ListItem>
                        <asp:ListItem Value="TW">Taiwan</asp:ListItem>
                        <asp:ListItem Value="TJ">Tajikistan</asp:ListItem>
                        <asp:ListItem Value="TZ">Tanzania, United Republic Of</asp:ListItem>
                        <asp:ListItem Value="TH">Thailand</asp:ListItem>
                        <asp:ListItem Value="TG">Togo</asp:ListItem>
                        <asp:ListItem Value="TK">Tokelau</asp:ListItem>
                        <asp:ListItem Value="TO">Tonga</asp:ListItem>
                        <asp:ListItem Value="TT">Trinidad And Tobago</asp:ListItem>
                        <asp:ListItem Value="TN">Tunisia</asp:ListItem>
                        <asp:ListItem Value="TR">Turkey</asp:ListItem>
                        <asp:ListItem Value="TM">Turkmenistan</asp:ListItem>
                        <asp:ListItem Value="TC">Turks And Caicos Islands</asp:ListItem>
                        <asp:ListItem Value="TV">Tuvalu</asp:ListItem>
                        <asp:ListItem Value="UG">Uganda</asp:ListItem>
                        <asp:ListItem Value="UA">Ukraine</asp:ListItem>
                        <asp:ListItem Value="AE">United Arab Emirates</asp:ListItem>
                        <asp:ListItem Value="US">United States</asp:ListItem>
                        <asp:ListItem Value="UM">United States Minor Is.</asp:ListItem>
                        <asp:ListItem Value="UY">Uruguay</asp:ListItem>
                        <asp:ListItem Value="UZ">Uzbekistan</asp:ListItem>
                        <asp:ListItem Value="VU">Vanuatu</asp:ListItem>
                        <asp:ListItem Value="VE">Venezuela</asp:ListItem>
                        <asp:ListItem Value="VN">Vietnam</asp:ListItem>
                        <asp:ListItem Value="VG">Virgin Islands (British)</asp:ListItem>
                        <asp:ListItem Value="VI">Virgin Islands (U.S.)</asp:ListItem>
                        <asp:ListItem Value="WF">Wallis And Futuna Islands</asp:ListItem>
                        <asp:ListItem Value="EH">Western Sahara</asp:ListItem>
                        <asp:ListItem Value="YE">Yemen</asp:ListItem>
                        <asp:ListItem Value="YU">Yugoslavia</asp:ListItem>
                        <asp:ListItem Value="ZR">Zaire</asp:ListItem>
                        <asp:ListItem Value="ZM">Zambia</asp:ListItem>
                        <asp:ListItem Value="ZW">Zimbabwe</asp:ListItem>
                    </asp:DropDownList>
                    <input runat="server" id="CountryStored" type="hidden" />
                    <label>
                        <asp:Label ID="PostcodeLabel" runat="server" Text="Postcode:"></asp:Label>
                    </label>
                    <asp:TextBox ID="Postcode" runat="server" MaxLength="8"></asp:TextBox>
                    <input runat="server" id="PostcodeStored" type="hidden" />


                    <label>
                        <asp:Label ID="RaceCategoryLabel" runat="server" Text="Category:"></asp:Label></label>
                    <asp:DropDownList ID="RaceCategory" runat="server">
                    </asp:DropDownList>
                    <input runat="server" id="RaceCategoryStored" type="hidden" />

                    <br />
                    <br />

                    <%-- <label>
                <asp:Label ID="AthleticsClubLabel" runat="server" Text="Club:"></asp:Label></label>
            <asp:TextBox ID="AthleticsClub" runat="server"></asp:TextBox>
            <input runat="server" id="AthleticsClubStored" type="hidden" />--%>



                    <label>
                        <asp:Label ID="GenderLabel" runat="server" Text="Gender:"></asp:Label></label>
                    <asp:RadioButtonList ID="Gender" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Selected="True" Value="0">Either</asp:ListItem>
                        <asp:ListItem Value="1">Male</asp:ListItem>
                        <asp:ListItem Value="2">Female</asp:ListItem>
                    </asp:RadioButtonList>
                    <input runat="server" id="GenderStored" type="hidden" />
                    <br />
                    <br />

                    <span class="advanced-submit">
                        <asp:Button ID="DoSearch" runat="server" Text="Search" OnClick="DoSearch_Click" />
                        <asp:Button ID="ClearSearch" runat="server" Text="Reset" OnClick="ClearSearch_Click" />
                    </span>

                    <div class="clearer"></div>


                </fieldset>


            </div>

            <div class="span6">
                <div id="primary">
                    <input runat="server" id="HiddenRaceId" type="hidden" />
                    <input runat="server" id="NikePlusUrl" type="hidden" />
                    <input runat="server" id="EventType" type="hidden" />
                    <input runat="server" id="Active" type="hidden" />
                    <input runat="server" id="PhotosRaceId" type="hidden" />
                    <input runat="server" id="PhotosRaceName" type="hidden" value="0" />
                    <input runat="server" id="RaceYear" type="hidden" />
                    <input runat="server" id="RaceFullName" type="hidden" />
                    <div class="com75">
                        <div class="header">
                            <h1 class="box-title">
                                <asp:Literal ID="RaceName" runat="server" />
                                <asp:Literal ID="NikePlusGraphic" runat="server"></asp:Literal></h1>
                        </div>
                        <div class="content" style="min-height: 400px;">
                            <input runat="server" id="ResultCount" type="hidden" />
                            <input runat="server" id="NumberOfPages" type="hidden" />
                            <input runat="server" id="CurrentSortColumn" type="hidden" value="Finish Time" />
                            <input runat="server" id="CurrentSortOrder" type="hidden" value="Ascending" />
                            <asp:GridView ID="ResultsGrid" runat="server" CssClass="results-table" PageSize="20"
                                AllowSorting="True" OnSorting="SortResults" EnableViewState="False" OnRowDataBound="ResultsGrid_RowDataBound"
                                EmptyDataText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We could not find a result for your search.  Please try again or use the <a href='./advancedsearch.aspx'>advanced search</a>."
                                EmptyDataRowStyle-CssClass="error">
                                <PagerSettings Mode="NextPreviousFirstLast" />
                                <PagerStyle HorizontalAlign="Center" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>
                            <br />
                            <!--<asp:Label ID="PageDetails" runat="server" Text=""></asp:Label>-->
                            <asp:Panel ID="PanelPageNav" runat="server" Visible="false" CssClass="pagination">

                                <asp:Button ID="PreviousPage" CommandName="Previous" runat="server" OnCommand="ChangePage"
                                    Text="Previous Page" Enabled="False" CausesValidation="False" EnableViewState="False"
                                    UseSubmitBehavior="False" />
                                <asp:Button ID="NextPage" runat="server" CommandName="Next" OnCommand="ChangePage"
                                    Text="Next Page" CausesValidation="False" EnableViewState="False" UseSubmitBehavior="False" />
                                <asp:Label ID="JumpPageLabel" runat="server" Text="Jump to Page:"></asp:Label>&nbsp;
                <asp:TextBox ID="JumpPage" runat="server" EnableViewState="False" MaxLength="5"></asp:TextBox>&nbsp;<asp:Button
                    ID="JumpPageButton" runat="server" Text="Go" OnClick="JumpPageButton_Click" />
                            </asp:Panel>
                            <asp:Panel ID="PanelDefaultView" runat="server" CssClass="notification information" Visible="false">
                                <p>Your search results will display here.</p>
                                <p>Enter the names of the cyclists you wish to compare your performances with.</p>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="box oneThirds">
                    <h2 class="box-title">Quick Search</h2>
                    <div class="box-content quick-search">
                        <div id="SearchFields">

                            <asp:Label ID="RaceNumberLabel" runat="server" Text="Bib:"></asp:Label>
                            <asp:TextBox ID="RaceNumber" runat="server" MaxLength="6"></asp:TextBox>
                            <input runat="server" id="RaceNumberStored" type="hidden" />
                            <asp:CompareValidator ID="RaceNumberValidator" runat="server" ErrorMessage="Please enter a numeric Bib value "
                                ControlToValidate="RaceNumber" Operator="DataTypeCheck" Type="Integer" SetFocusOnError="True"
                                CssClass="information" ForeColor="#1967ab"></asp:CompareValidator>
                            <asp:Button ID="ButtonQuickSearch" runat="server" Text="Quick Search"
                                OnClick="ButtonQuickSearch_Click" />

                        </div>
                    </div>

                </div>

                <div>
                    <h2 class="box-title">Cyclist Comparison</h2>
                    <div class="compare-bg" />
                    <div class="box-content compare-partners">
                        <div id="DivCompare" class="compare-box" runat="server">
                        </div>
                        <asp:HiddenField ID="CompareValue" runat="server" />
                        <asp:HiddenField ID="CompareName" runat="server" />
                        <br />
                        <asp:Button ID="ButtonCompare" OnClientClick="return checkCompare();" runat="server"
                            Text="Compare" OnClick="ButtonCompare_Click" />
                        <asp:Button ID="ButtonResetCompare" Text="Reset" OnClientClick="return resetCompare();"
                            runat="server" OnClick="ButtonResetCompare_Click" />
                    </div>

                </div>
            </div>
        </div>
        </div>
    
  
</asp:Content>
<asp:Content  ID="Content3" runat="server" ContentPlaceHolderID="AdditionalFooter"></asp:Content>
