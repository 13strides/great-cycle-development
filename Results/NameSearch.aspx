﻿<%@ Page Title="Daily Mirror Great Cycle Results" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="NameSearch.aspx.cs" Inherits="Results_NameSearch" %>
<%@ Register TagPrefix="ddlb" Assembly="OptionDropDownList" Namespace="OptionDropDownList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="ExtendedCss/results.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../App_js/cufon-yui.js"></script>
    <script type="text/javascript">
        function addToCompare(name, bib, race) {
            var value = $('#ctl00_MainContent_CompareValue');
            var personName = $('#ctl00_MainContent_CompareName');
            var text = $('#ctl00_MainContent_DivCompare');
            var raceID = $('#ctl00_MainContent_HiddenRaceId');
            var maxComparison = 20;
            name = jQuery.trim(name); //make sure string is trimed
            if (value) {
                //avoid duplicated and excessive entry
                if (value.val().indexOf(bib) >= 0 || value.val().split(",").length > maxComparison) return; 
                if (name.length === 0) name = "Athlete Name";
                value.val(value.val() + bib + "r" + raceID.val() + ",");
            }
            if (text) {
                var raceNameObj = $("#ctl00_MainContent_RaceFullName");
                if (raceNameObj) raceName = raceNameObj.val();
                text.append("<p id='name_" + bib + "' name='" + raceID.val() + "' onclick='removeCompare(this)'><em>" + raceName + "</em><br /><span>" + name + "</span></p>");
                if (personName) {
                    personName.val(personName.val() + name + ",");
                }
                $.ajax({
                    url: '/Results/Ajax/Retain.aspx?name='+ name + '&bib=' + bib + '&rid=' + raceID.val(),
                    error: function() {
                        //$('#message').html("Racers in your compare list may not be retained.");                       
                    }
                });
            }
            
        }
        
        function resetCompare() {
            var value = $('#ctl00_MainContent_CompareValue');
            var text = $('#ctl00_MainContent_DivCompare');
            var personName = $('#ctl00_MainContent_CompareName');
            if (text)
                text.html("");
            if (value)
                value.val("");
            if (personName)
                personName.val("");
            if (value.val().length === 0 && personName.val().length === 0) {
                return true;
            }
            else {
                return false;
            }
        }
        function checkCompare() {
            var value = $('#ctl00_MainContent_CompareValue');
            if (value.val().length <= 0) return false;
        }
        function removeCompare(obj) {
            var element = $("#" + obj.id);
            if (element) {
                var name = jQuery.trim($("#" + obj.id + " span").text());
                var value = $('#ctl00_MainContent_CompareValue');
                var personName = $('#ctl00_MainContent_CompareName');
                var bib = obj.id.split("_")[1];
                if (value && value.val().length > 0) {
                    value.val(value.val().replace(bib + "r" + element.attr("name") + ",", ""));                    
                    if (personName && personName.val().length > 0) {                        
                        personName.val(personName.val().replace(name + ",", ""));
                    }
                    element.remove();

                }
                
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row pageheading">
        <div class="span6">
            <h1>Results</h1>
            <ul class="breadcrumb">
                <li><a href="/">Home</a> <span class="divider">/</span></li>
                <li class="active">Blog</li>
            </ul>
        </div>
      </div>
        
         <div class="row results-search">
                   

                    <asp:TextBox ID="TextBoxSearch" runat="server" Text="Surname"></asp:TextBox>

                    <ddlb:OptionGroupSelect ID="RaceList" runat="server" AutoPostBack="True"
                        OnValueChanged="RaceList_ValueChanged" />
                    <asp:Button ID="DoSearch" runat="server" Text="Search" OnClick="DoSearch_Click" />
                    <asp:HyperLink ID="HyperLink2" NavigateUrl="~/Results/FullSearch.aspx" runat="server">Full Result List</asp:HyperLink>
                    <asp:HyperLink ID="HyperLinkBack" NavigateUrl="~/Results/AdvancedSearch.aspx" runat="server">Advanced Search</asp:HyperLink>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="help.aspx" Target="_blank">Help</asp:HyperLink>

                    <div class="clearer"></div>
              </div>

           


<div class="row">
<div class="span6">
                    <div id="primary">
                        <input runat="server" id="HiddenRaceId" type="hidden" />
                        <input runat="server" id="NikePlusUrl" type="hidden" />
                        <input runat="server" id="EventType" type="hidden" />
                        <input runat="server" id="Active" type="hidden" />
                        <input runat="server" id="PhotosRaceId" type="hidden" />
                        <input runat="server" id="PhotosRaceName" type="hidden" value="0" />
                        <input runat="server" id="RaceYear" type="hidden" />
                        <input runat="server" id="RaceFullName" type="hidden" />
                        <div class="com75">
                            <div class="header">
                                <h2 class="box-title">
                                    <asp:Literal ID="RaceName" runat="server" /></h2>
                            </div>
                            <div class="content" style="min-height: 400px;">
                                <input runat="server" id="ResultCount" type="hidden" />
                                <input runat="server" id="NumberOfPages" type="hidden" />
                                <input runat="server" id="CurrentSortColumn" type="hidden" value="Finish Time" />
                                <input runat="server" id="CurrentSortOrder" type="hidden" value="Ascending" />
                                <asp:GridView ID="ResultsGrid" runat="server" CssClass="results-table" PageSize="20"
                                    AllowSorting="True" OnSorting="SortResults" EnableViewState="False" OnRowDataBound="ResultsGrid_RowDataBound"
                                    EmptyDataText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We could not find a result for your search.  Please try again or use the <a href='./advancedsearch.aspx'>advanced search</a>."
                                    EmptyDataRowStyle-CssClass="error">
                                    <PagerSettings Mode="NextPreviousFirstLast" />
                                    <PagerStyle HorizontalAlign="Center" />
                                    <AlternatingRowStyle CssClass="altrow" />
                                </asp:GridView>
                                <br />
                                <!--<asp:Label ID="PageDetails" runat="server" Text=""></asp:Label>-->
                                <asp:Panel ID="PanelPageNav" runat="server" Visible="false">

                                    <asp:Button ID="PreviousPage" CommandName="Previous" runat="server" OnCommand="ChangePage"
                                        Text="Previous Page" Enabled="False" CausesValidation="False" EnableViewState="False"
                                        UseSubmitBehavior="False" />
                                    <asp:Button ID="NextPage" runat="server" CommandName="Next" OnCommand="ChangePage"
                                        Text="Next Page" CausesValidation="False" EnableViewState="False" UseSubmitBehavior="False" />
                                    <asp:Label ID="JumpPageLabel" runat="server" Text="Jump to Page:"></asp:Label>&nbsp;
                <asp:TextBox ID="JumpPage" runat="server" EnableViewState="False" MaxLength="5"></asp:TextBox>&nbsp;<asp:Button
                    ID="JumpPageButton" runat="server" Text="Go" OnClick="JumpPageButton_Click" />
                                </asp:Panel>
                                <asp:Panel ID="PanelDefaultView" runat="server" CssClass="notification information" Visible="false">
                                    <p>Your search results will display here.</p>
                                    <p>Enter the names of the cyclists you wish to compare your performances with.</p>
                                </asp:Panel>
                            </div>
                        </div>
                  
                </div>
                </div>
                
                <div class="span3">
                    <div class="box oneThirds">
                        <h2 class="box-title">Quick Search</h2>
                        <div class="box-content quick-search">
                            <div id="SearchFields">

                                <asp:Label ID="RaceNumberLabel" runat="server" Text="Bib Number:"></asp:Label>
                                <asp:TextBox ID="RaceNumber" runat="server" MaxLength="6"></asp:TextBox>
                                <input runat="server" id="RaceNumberStored" type="hidden" />
                                <asp:CompareValidator ID="RaceNumberValidator" runat="server" ErrorMessage="Please enter a numeric value for Race Number "
                                    ControlToValidate="RaceNumber" Operator="DataTypeCheck" Type="Integer" SetFocusOnError="True"
                                    CssClass="information" ForeColor="#1967ab"></asp:CompareValidator>
                                <asp:Button ID="ButtonQuickSearch" runat="server" Text="Quick Search" cssClass="btn-primary"
                                    OnClick="ButtonQuickSearch_Click" />

                            </div>
                        </div>

                   
                    

                    <div class="box oneThirds" >
                        <h2 class="box-title">Cyclist Comparison</h2>
                        <div class="compare-bg">
                            <div class="box-content compare-partners">

                                <div id="DivCompare" class="compare-box" runat="server">
                                </div>
                                <asp:HiddenField ID="CompareValue" runat="server" />
                                <asp:HiddenField ID="CompareName" runat="server" />
                                <br />
                                <asp:Button ID="ButtonCompare" OnClientClick="return checkCompare();" runat="server"
                                    Text="Compare" cssClass="btn-primary" OnClick="ButtonCompare_Click" />
                                <asp:Button ID="ButtonResetCompare" Text="Reset" OnClientClick="return resetCompare();"
                                    runat="server" cssClass="btn-primary" OnClick="ButtonResetCompare_Click" />
                            </div>

                        </div>
                    </div>
              
                


            
               
                
             
                <div class="clearer"></div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content  ID="Content3" runat="server" ContentPlaceHolderID="AdditionalFooter"></asp:Content>