﻿function showHideSubMenu(id) {
    $('.displaybox').each(function () {
    if ($(this).attr('id') != id && $(this).css('display') == 'block') { $(this).animate({ "height": "toggle", "opacity": "toggle" }, "600"); } 
    //$.plot($("#myGraph-dispbox"), fh_data, options);
    });
    $("#" + id).animate({ "height": "toggle", "opacity": "toggle" }, "600");
    return false;
}
function SetOnClickMenuItem() {
    $('.hasGraph').click(function (event) {
        showHideSubMenu($(this).attr("id") + '-dispbox');
        return false;
    });
}
$(document).ready(function(){
    $('body').removeClass('nojQuery');
	SetOnClickMenuItem();	
});