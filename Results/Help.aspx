﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Help.aspx.cs" Inherits="Results_Help" Title="Race Results Help" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="row">
        <div class="span7 abouttheswim">
            <div class="right-col">
                <div class="primary">
                    <div class="header">
                        <h1 class="box-title"><span id="questionsTitleLabel">Event Results Help</span></h1>
                    </div>
                    <div class="content">
                        <p>
                            <strong>How do I find my result?</strong>
                        </p>
                        <p>
                            The easiest way to find your result is by <strong>bib number</strong>. On the results <a href="/Results">landing page</a>,
                enter your bib number into the search box, choose your event and click search.
                        </p>
                        <p>
                            You can also search by the <strong>cyclist's name</strong> and you will get a list of all the people
                matching that name. Finally, you can do a more complex search using the <a href="/Results/Advanced-Search">advanced
                search page</a> where there are lots of options to refine the criteria of the person
                you are searching for.
                        </p>
                        <br />
                        <p>
                            <strong>How do I compare my time with my friends?</strong>
                        </p>
                        <ul>
                            <li>Using the <a href="/Results/Advanced-Search">Advanced Search page</a>, search for a cyclist.</li>
                            <li>When you find the cyclist you are searching for, to add them to the cyclist comparison
                    list, click the '+' icon corresponding to the cyclist's name in the search results.</li>
                            <li>Repeat these steps in the <a href="/Results/Advanced-Search">Advanced Search page</a> for each cyclist in your comparison,
                    up to a maximum of 10 cyclist comparisons.</li>
                            <li>When you have finished your list click the 'compare' button and you will be taken
                    to a page comparing the cyclist's results.</li>
                            <li>You can share a link to the compared results by clicking on the Facebook or Twitter
                    icons.</li>
                        </ul>
                        <br />
                        <p>
                            <strong>Can I find my results from older events?</strong>
                        </p>
                        <ul>
                            <li>Each event listed in the drop down box has the results for that event.</li>
                        </ul>
                        <br />
                        <!--<p>
                <strong>What is the difference between Gun and Chip Times? </strong>
            </p>
            <p>
                Gun Times are displayed for a set number of athletes who are first to cross the
                line. After this point, chip times are displayed for the rest of the field.</p>
                <br />-->
            <p> <strong>Can I share my results?</strong></p>
            <p>
                Using the social media icons, you can share your current page link with your friends
                in Facebook & Twitter. This will open a window to the website with the sharing link
                already embedded. You will need to have an account with the social networking site
                before you can post a link.
            </p>
                        <br />

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content  ID="Content2" runat="server" ContentPlaceHolderID="AdditionalFooter"></asp:Content>