﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using ThirteenStrides.Database;
using GCResults;

public partial class Results_FullSearch : System.Web.UI.Page
{
    int selectedRaceID = 0;
    string searchSurname = string.Empty;
    string searchFirstName = string.Empty;
    string searchRaceNumber = string.Empty;
    string selectedGender = "0";
    string selectedCountry = "0";
    string selectedAgeCategory = "0";
    string selectedJuniorRace = "";
    string selectedRaceCategory = "0";
    string searchPostCode = string.Empty;
    string searchClub = string.Empty;
    string selectedGCFilter = "";
    string theRaceDistance = string.Empty;
    bool isPageChanging = false;
    bool isPageJumping = false;
    bool isSorting = false;
    bool isQueryValid = false;
    bool isException = false;
    bool isRaceListChanged = false;
   
    private int PAGE_SIZE = 10;
    private string compareList;
    DataOperations _dataOperations = new DataOperations();

    protected void Page_Load(object sender, EventArgs e)
    {
        Form.Action = Request.RawUrl;
        RaceName.Text = "<h2 class='box-title'>Search Result</h2>";
        if (Request.QueryString["r"] != null && Request.QueryString["surname"] != null )
        {
            isQueryValid = int.TryParse(Request.QueryString["r"].ToString(), out selectedRaceID);             
            
        }
        if (!Page.IsPostBack)
        {
            //Populate Race List
            DataSet raceData = _dataOperations.GetRaces();
            if (raceData != null && raceData.Tables.Count > 0 && raceData.Tables[0].Rows.Count > 0)
            {
                raceData.Tables[0].Rows[0].Delete(); //hide select events default selection
                RaceList.DataTextField = "description"; //Text
                RaceList.DataValueField = "idrace"; //Value
               
                if (isQueryValid)
                {
                    RaceList.SelectedValue = selectedRaceID.ToString();
                    //searchSurname = Request.QueryString["surname"].ToString();
                   // TextBoxSearch.Text = searchSurname;
                }
                else if (Request.QueryString["r"] == null)
                {                   
                    RaceList.SelectedValue = raceData.Tables[0].Rows[1]["idrace"].ToString();
                }
                RaceList.OptionGroupField = "raceyear"; //Grouping in Drop Down List

                RaceList.DataSource = raceData;
                RaceList.DataBind();

            }

        }
       
        try
        {
            if (RaceList != null && !string.IsNullOrEmpty(RaceList.SelectedValue))
                selectedRaceID = Convert.ToInt32(RaceList.SelectedValue);
             RaceName.Text = "<h2 class='box-title'>" + RaceList.SelectedItem.Text + "</h2>";

             RaceName.Text = RaceList.SelectedItem.Text;
             RaceFullName.Value = RaceList.SelectedItem.Text;//requrid by the client side script
             HiddenRaceId.Value = selectedRaceID.ToString();
             GetRaceInfo();
             PopulateRaceCategoryList();
            //PanelDefaultView.Visible;
        }
        catch (Exception ex)
        {
            isException = true;
        }

    }
    private void PopulateRaceCategoryList()
    {
        //Populate Race Category List
        string selectedValue = "0";
        if (Page.IsPostBack && isRaceListChanged == false)
        {
            selectedValue = RaceCategory.SelectedValue;
        }
        DataSet categoryData = _dataOperations.GetRaceCategories(HiddenRaceId.Value);
        RaceCategory.DataSource = categoryData;
        RaceCategory.DataValueField = categoryData.Tables[0].Columns[0].ToString();
        RaceCategory.DataTextField = categoryData.Tables[0].Columns[1].ToString();
        RaceCategory.DataBind();

        //ListItem CategoryFirstItem = new ListItem("-- Race Category Filter --", "0");
        //RaceCategory.Items.Insert(0, CategoryFirstItem);

       /* if (EventType.Value != "4" && EventType.Value != "5")
        {
            ListItem CategorySecondItem = new ListItem("Elite Men and Masses", "1");
            RaceCategory.Items.Insert(1, CategorySecondItem);
        }*/
        try
        {
            RaceCategory.SelectedValue = selectedValue;
        }
        catch
        {
            RaceCategory.SelectedValue = RaceCategory.Items[0].Value;
           
        }
        selectedRaceCategory = RaceCategory.SelectedValue;
    }
    private void GetSearchResults()
    {
        if (Helper.IsSearchContainsBadPhrases(searchSurname) || Helper.IsSearchContainsBadPhrases(searchFirstName)
            || Helper.IsSearchContainsBadPhrases(searchClub))
        {
            ResultCount.Value = "0";
        }
        else
        {
            //Do Search
            
            ResultCount.Value = _dataOperations.GetResultsCount(selectedRaceID.ToString(), searchRaceNumber, searchSurname, searchFirstName,
                selectedGender, searchPostCode, selectedCountry, selectedRaceCategory, selectedAgeCategory, searchClub, selectedJuniorRace, selectedGCFilter).ToString();
            NumberOfPages.Value = Math.Ceiling(Convert.ToDecimal(ResultCount.Value) / PAGE_SIZE).ToString();

        }

        if (ResultsGrid.PageIndex == null || ResultsGrid.PageIndex < 0) ResultsGrid.PageIndex = 0;
        if (Convert.ToInt32(ResultCount.Value) > 0)
        {
            //We have found results, populate the grid
            DataSet resultsData = _dataOperations.GetFullResults(selectedRaceID.ToString(),"","", EventType.Value, ResultsGrid.PageIndex, selectedRaceCategory);

            ResultsGrid.DataSource = resultsData;
            ResultsGrid.DataBind();

            //UpdateSplitNames(selectedRaceID.ToString());
            PanelPageNav.Visible = true;
        }
        else
        {
            //We have no results, bind to an empty dataset to clear grid
            DataTable EmptyDataTable = new DataTable();
            ResultsGrid.DataSource = EmptyDataTable;
            PanelPageNav.Visible = false;
            ResultsGrid.DataBind();

        }


        if (Convert.ToInt32(ResultCount.Value) > 0)
        {
            NextPage.Visible = true;
            PreviousPage.Visible = true;
            JumpPage.Visible = true;
            JumpPageLabel.Visible = true;
            JumpPageButton.Visible = true;

            if (Convert.ToInt32(NumberOfPages.Value) <= ResultsGrid.PageIndex + 1)
            {
                NextPage.Enabled = false;
            }
            else
            {
                NextPage.Enabled = true;
            }

            if (ResultsGrid.PageIndex == 0)
            {
                PreviousPage.Enabled = false;
            }
            else
            {
                PreviousPage.Enabled = true;
            }

        }
        else
        {
            NextPage.Visible = false;
            PreviousPage.Visible = false;
            JumpPage.Visible = false;
            JumpPageLabel.Visible = false;
            JumpPageButton.Visible = false;

        }

        //UpdatePageDetailsLabel(ResultsGrid.PageIndex + 1, Convert.ToInt32(NumberOfPages.Value), Convert.ToInt32(ResultCount.Value));
    }

    /// <summary>
    /// Fired when a column header is clicked on the results
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SortResults(object sender, GridViewSortEventArgs e)
    {
        isSorting = true;
        string SortOrder = "";
        string SortColumn = "";

        switch (e.SortExpression.ToString())
        {
            case "Name":
                SortColumn = "r.surname, r.forenames";
                break;

            case "BIB":
                SortColumn = "r.racenumber";
                break;

            case "Club":
                SortColumn = "p.AthleticsClubName";
                break;

            case "Pos":
                SortColumn = "r.CategoryPosition";
                break;

            case "Age Group Position":
                SortColumn = "r.AgeCatPos";
                break;

            case "Gender Position":
                SortColumn = "r.GenderPos";
                break;

            case "Age Group and Gender Position":
                SortColumn = "r.AgeGenderPos";
                break;

            case "Finish Time":
                SortColumn = "r.TimeFinish";
                break;     

            case "Gender":
                SortColumn = "r.Gender";
                break;
            case "Wetsuit":
                SortColumn = "r.wetsuit";
                break;

            default:
                if (searchFirstName != "" || searchSurname != "")
                {
                    SortColumn = "r.surname, r.forenames";
                }
                else
                {
                    SortColumn = "r.TimeFinish";
                }
                break;
        }

        if (e.SortExpression.ToString() == CurrentSortColumn.Value)
        {
            if (CurrentSortOrder.Value == "Ascending")
            {
                SortOrder = "DESC";
                CurrentSortOrder.Value = "Descending";
            }
            else
            {
                SortOrder = "ASC";
                CurrentSortOrder.Value = "Ascending";
            }
        }
        else
        {
            SortOrder = "ASC";
            CurrentSortOrder.Value = "Ascending";
            CurrentSortColumn.Value = e.SortExpression.ToString();
        }

        ResultsGrid.PageIndex = 0;
        DataSet resultsData = _dataOperations.GetFullResults(selectedRaceID.ToString(), SortColumn, SortOrder, EventType.Value, ResultsGrid.PageIndex, selectedRaceCategory);
        if (resultsData != null && resultsData.Tables.Count > 0 && resultsData.Tables[0].Rows.Count > 0)
            PanelPageNav.Visible = true;
        ResultsGrid.DataSource = resultsData;
        ResultsGrid.DataBind();
        //UpdateSplitNames(selectedRaceID.ToString());

        if (Convert.ToInt32(NumberOfPages.Value) <= ResultsGrid.PageIndex + 1)
        {
            NextPage.Enabled = false;
        }
        else
        {
            NextPage.Enabled = true;
        }

        if (ResultsGrid.PageIndex == 0)
        {
            PreviousPage.Enabled = false;
        }
        else
        {
            PreviousPage.Enabled = true;
        }

        //UpdatePageDetailsLabel(ResultsGrid.PageIndex + 1, Convert.ToInt32(NumberOfPages.Value), Convert.ToInt32(ResultCount.Value));

    }

    /// <summary>
    /// This method will handle the navigation / paging index
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChangePage(object sender, CommandEventArgs e)
    {
        isPageChanging = true;
        switch (e.CommandName)
        {
            case "Previous":
                ResultsGrid.PageIndex = ResultsGrid.PageIndex - 1;
                break;

            case "Next":
                ResultsGrid.PageIndex = ResultsGrid.PageIndex + 1;
                break;
        }

        string SortOrder = "";

        if (CurrentSortOrder.Value == "Ascending")
        {
            SortOrder = "ASC";
        }
        else
        {
            SortOrder = "DESC";
        }

        string SortColumn = "";

        switch (CurrentSortColumn.Value)
        {
            case "Name":
                SortColumn = "r.surname, r.forenames";
                break;

            case "BIB":
                SortColumn = "r.racenumber";
                break;

            case "Club":
                SortColumn = "p.AthleticsClubName";
                break;

            case "Pos":
                SortColumn = "r.CategoryPosition";
                break;

            case "Age Group Position":
                SortColumn = "r.AgeCatPos";
                break;

            case "Gender Position":
                SortColumn = "r.GenderPos";
                break;

            case "Age Group and Gender Position":
                SortColumn = "r.AgeGenderPos";
                break;

            case "Finish Time":
                SortColumn = "r.TimeFinish";
                break;
            case "Gender":
                SortColumn = "r.Gender";
                break;
            case "Wetsuit":
                SortColumn = "r.wetsuit";
                break;

            default:
                if (searchFirstName != "" || searchSurname != "")
                {
                    SortColumn = "r.surname, r.forenames";
                }
                else
                {
                    SortColumn = "r.TimeFinish";
                }
                break;
        }

        DataSet resultsData = _dataOperations.GetFullResults(selectedRaceID.ToString(), SortColumn, SortOrder, EventType.Value, ResultsGrid.PageIndex, selectedRaceCategory);
        if (resultsData != null && resultsData.Tables.Count > 0 && resultsData.Tables[0].Rows.Count > 0)
            PanelPageNav.Visible = true;
        ResultsGrid.DataSource = resultsData;
        ResultsGrid.DataBind();
        //UpdateSplitNames(selectedRaceID.ToString());

        if (Convert.ToInt32(NumberOfPages.Value) <= ResultsGrid.PageIndex + 1)
        {
            NextPage.Enabled = false;
        }
        else
        {
            NextPage.Enabled = true;
        }

        if (ResultsGrid.PageIndex == 0)
        {
            PreviousPage.Enabled = false;
        }
        else
        {
            PreviousPage.Enabled = true;
        }

        //UpdatePageDetailsLabel(ResultsGrid.PageIndex + 1, Convert.ToInt32(NumberOfPages.Value), Convert.ToInt32(ResultCount.Value));

        //Keep focus at same part of page (on a non disabled command)
        if (e.CommandName == "Previous")
        {
            NextPage.Focus();
        }
        else
        {
            PreviousPage.Focus();
        }
    }

    

    /// <summary>
    /// This function is fired when data is bound to the results grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ResultsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.CssClass = "head";
            e.Row.Cells[3].Text = "Finish Time";
            e.Row.Cells[4].Text = "Speed mph";
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            TableCellCollection cells = e.Row.Cells;
            if (!string.IsNullOrEmpty(cells[2].Text) && !string.IsNullOrEmpty(cells[1].Text))
            {
                cells[2].Attributes.Add("class", "athlete-name");
                cells[2].Text = "<a href='/Results/" + selectedRaceID.ToString() + "/" + cells[1].Text + "'>" + cells[2].Text + "</a>";
            }
            if (cells[0].Text == "1")
            {
                cells[0].Text = "M";
            }
            else
            {
                cells[0].Text = "F";
            }
            if (!string.IsNullOrEmpty(cells[4].Text) && cells[4].Text.ToLower().Contains("average"))
            {
                cells[4].Text = Helper.ComputeAverageSpeed(cells[3].Text, theRaceDistance);
            }
            //if (!cells[6].Text.ToLower().Contains("compare") && !string.IsNullOrEmpty(RaceName.Text) && !string.IsNullOrEmpty(RaceYear.Value) && !string.IsNullOrEmpty(cells[0].Text))
            //    cells[6].Text = "<a href=\"" + ConfigurationManager.AppSettings["MarathonPhotosUrl"].ToString() + RaceYear.Value + "/" + Server.UrlEncode(RaceName.Text) + "&bib=" + cells[0].Text + "\"  target=\"_blank\" title=\"See your photos\"><img src=\"images\\buttonSeeYourPhotos.gif\" width=\"30\" border=\"0\" alt=\"See your photos\"></a>";
            for (int c = 0; c < cells.Count; c++)
            {
                cells[c].Text = Server.HtmlDecode(cells[c].Text);
                switch (c)
                {
                    case 0:
                        cells[c].Width = 30;
                        break;
                    case 1:
                        cells[c].Width = 30;
                        break;
                    case 2:
                        cells[c].Width = 300;
                        break;
                    case 3:
                        cells[c].Width = 80;
                        break;
                    case 4:
                        cells[c].Width = 80;
                        break;
                    case 5:
                        cells[c].Width = 40;
                        break;
                    case 6:
                        cells[c].Width = 30;
                        break;
                    case 7:
                        cells[c].Width = 30;
                        break;
                    default:
                        break;
                }

            }

        }

    }



    protected void JumpPageButton_Click(object sender, EventArgs e)
    {
        isPageJumping = true;
        int NewPageIndex;

        if (!isNumeric(JumpPage.Text, System.Globalization.NumberStyles.Integer))
        {
            NewPageIndex = 0;
        }
        else
        {
            NewPageIndex = Convert.ToInt32(JumpPage.Text) - 1;
        }

        //If Jump to Page Index is less than 1, then set to first page
        if (NewPageIndex < 0)
        {
            NewPageIndex = 0;
        }

        //If Jump to Page Index is greater than new page index, set to last page
        if (Convert.ToInt32(NumberOfPages.Value) < NewPageIndex + 1)
        {
            NewPageIndex = Convert.ToInt32(NumberOfPages.Value) - 1;
        }

        ResultsGrid.PageIndex = NewPageIndex;

        string SortOrder = "";

        if (CurrentSortOrder.Value == "Ascending")
        {
            SortOrder = "ASC";
        }
        else
        {
            SortOrder = "DESC";
        }

        string SortColumn = "";

        switch (CurrentSortColumn.Value)
        {
            case "Name":
                SortColumn = "r.surname, r.forenames";
                break;

            case "BIB":
                SortColumn = "r.racenumber";
                break;

            case "Club":
                SortColumn = "p.AthleticsClubName";
                break;

            case "Pos":
                SortColumn = "r.CategoryPosition";
                break;

            case "Age Group Position":
                SortColumn = "r.AgeCatPos";
                break;

            case "Gender Position":
                SortColumn = "r.GenderPos";
                break;

            case "Age Group and Gender Position":
                SortColumn = "r.AgeGenderPos";
                break;

            case "Finish Time":
                SortColumn = "r.TimeFinish";
                break;

            case "Gender":
                SortColumn ="r.Gender";
                break;

            case "Wetsuit":
                SortColumn = "r.wetsuit";
                break;

            default:
                if (searchFirstName != "" || searchSurname != "")
                {
                    SortColumn = "r.surname, r.forenames";
                }
                else
                {
                    SortColumn = "r.TimeFinish";
                }
                break;
        }

        DataSet resultsData = _dataOperations.GetFullResults(selectedRaceID.ToString(), SortColumn, SortOrder, EventType.Value, ResultsGrid.PageIndex, selectedRaceCategory);
        ResultsGrid.DataSource = resultsData;
        ResultsGrid.DataBind();
        //UpdateSplitNames(selectedRaceID.ToString());

        if (Convert.ToInt32(NumberOfPages.Value) <= NewPageIndex + 1)
        {
            NextPage.Enabled = false;
        }
        else
        {
            NextPage.Enabled = true;
        }

        if (ResultsGrid.PageIndex == 0)
        {
            PreviousPage.Enabled = false;
        }
        else
        {
            PreviousPage.Enabled = true;
        }

        //UpdatePageDetailsLabel(NewPageIndex + 1, Convert.ToInt32(NumberOfPages.Value), Convert.ToInt32(ResultCount.Value));

        //Keep focus at same part of page (on a non disabled command)
        JumpPage.Text = "";
        JumpPage.Focus();
    }

    public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
    {
        Double result;
        return Double.TryParse(val, NumberStyle, System.Globalization.CultureInfo.CurrentCulture, out result);
    }
 
    /// <summary>
    /// Fired when the user clicks the Clear Search button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
  

    protected void Page_PreRender(object sender, EventArgs e)
    {

        //on page direct call (if page loads with out query strings)
        if (isPageJumping == false && isPageChanging == false && isSorting == false)
        {
            //Default search column
            CurrentSortColumn.Value = "Pos";
            //bind Gridview with a generic of the latest event
            if (selectedRaceID > 0)
            {
                GetSearchResults();
            }
            
        }    
        
                

    }

    private void GetRaceInfo()
    {
        if (HiddenRaceId.Value == "0")
        {
            NikePlusUrl.Value = "";
            Active.Value = "0";
            EventType.Value = "0";
           
            PhotosRaceId.Value = "";
            PhotosRaceName.Value = "";
            RaceYear.Value = "";
        }
        else
        {
            DataSet raceData = _dataOperations.GetRace(HiddenRaceId.Value);
            if (raceData.Tables[0].Rows.Count > 0)
            {
                //Then we have a race with results available
                theRaceDistance = raceData.Tables[0].Rows[0]["Distance"].ToString();
                NikePlusUrl.Value = raceData.Tables[0].Rows[0]["NikePlusUrl"].ToString();
                Active.Value = raceData.Tables[0].Rows[0]["Active"].ToString();
                EventType.Value = raceData.Tables[0].Rows[0]["idEventType"].ToString();
                PhotosRaceId.Value = raceData.Tables[0].Rows[0]["PhotosRaceId"].ToString();
                //PhotosRaceName.Value = raceData.Tables[0].Rows[0]["PhotosRaceName"].ToString();
                PhotosRaceName.Value = "";
                RaceYear.Value = raceData.Tables[0].Rows[0]["RaceYear"].ToString();
                
            }
        }
    }
  
 
    
    protected void ButtonQuickSearch_Click(object sender, EventArgs e)
    {
        GetSearchResults();
    }
  
    
    protected void RaceList_ValueChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(RaceList.SelectedValue))
        {
            //Response.Redirect("FullSearch.aspx?r=" + RaceList.SelectedValue, true);
            selectedRaceID = Convert.ToInt32(RaceList.SelectedValue);
        }
        isRaceListChanged = true;
        if (RaceCategory.Items != null && RaceCategory.Items.Count > 0)
        {
            RaceCategory.Items.Clear();
            PopulateRaceCategoryList();
        }
        //
        ResultsGrid.PageIndex = 0;

    }
    protected void RaceCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        ResultsGrid.PageIndex = 0;
    }
}
