﻿<%@ Page Title="Daily Mirror Great Cycle Results" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="FullSearch.aspx.cs" Inherits="Results_FullSearch" %>
<%@ Register TagPrefix="ddlb" Assembly="OptionDropDownList" Namespace="OptionDropDownList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/Results/ExtendedCss/results.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/App_js/cufon-yui.js"></script>
     <script type="text/javascript">

         $(document).ready(function() {
             //var leftBox = $("#left-content").height();
             //var rightBox = $("#right-content").height();
             //$("#right-content").height(leftBox - rightBox)
             if (!($.browser.msie && $.browser.version == 7.0)) {

                 $('#left-content, #right-content').equalHeightColumns();
             }         });
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
   
        
            <div id="r-search">


                <div id="main-search">
                    <div id="full-search">
                        

                        <asp:Label ID="Label1" runat="server" Text="Race:"></asp:Label>
                        <ddlb:OptionGroupSelect ID="RaceList" runat="server" AutoPostBack="True"
                            OnValueChanged="RaceList_ValueChanged" />
                        <asp:Label ID="RaceCategoryLabel" runat="server" Text="Category:"></asp:Label>
                        <asp:DropDownList ID="RaceCategory" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="RaceCategory_SelectedIndexChanged">
                        </asp:DropDownList>

                        <asp:HyperLink ID="HyperLinkBack" NavigateUrl="~/Results/Advanced-Search" runat="server">Advanced Search</asp:HyperLink>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Results/Help" Target="_blank">Help</asp:HyperLink>

                        <div class="clearer"></div>
                    </div>
                </div>
            </div>
<div class="row">
			<div class="span11">
            <div id="content">
                <div class="right-col">
                    <div id="primary">
                        <input runat="server" id="HiddenRaceId" type="hidden" />
                        <input runat="server" id="NikePlusUrl" type="hidden" />
                        <input runat="server" id="EventType" type="hidden" />
                        <input runat="server" id="Active" type="hidden" />
                        <input runat="server" id="PhotosRaceId" type="hidden" />
                        <input runat="server" id="PhotosRaceName" type="hidden" value="0" />
                        <input runat="server" id="RaceYear" type="hidden" />
                        <input runat="server" id="RaceFullName" type="hidden" />
                        <div class="com75">
                            <div class="header">
                                <h1 class="box-title">
                                    <asp:Literal ID="RaceName" runat="server" /></h1>
                            </div>
                            <div class="content" style="min-height: 400px;">
                                <input runat="server" id="ResultCount" type="hidden" />
                                <input runat="server" id="NumberOfPages" type="hidden" />
                                <input runat="server" id="CurrentSortColumn" type="hidden" value="Finish Time" />
                                <input runat="server" id="CurrentSortOrder" type="hidden" value="Ascending" />
                                <asp:GridView ID="ResultsGrid" runat="server" CssClass="results-table" PageSize="20"
                                    AllowSorting="True" OnSorting="SortResults" EnableViewState="False" OnRowDataBound="ResultsGrid_RowDataBound"
                                    EmptyDataText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We could not find a result for your search.  Please try again or use the <a href='/Results/Advanced_Search'>advanced search</a>."
                                    EmptyDataRowStyle-CssClass="error">
                                    <PagerSettings Mode="NextPreviousFirstLast" />
                                    <PagerStyle HorizontalAlign="Center" />
                                    <AlternatingRowStyle CssClass="altrow" />
                                </asp:GridView>
                                <br />
                                <!--<asp:Label ID="PageDetails" runat="server" Text=""></asp:Label>-->
                                <asp:Panel ID="PanelPageNav" runat="server" Visible="false" CssClass="pagination">

                                    <asp:Button ID="PreviousPage" CommandName="Previous" runat="server" OnCommand="ChangePage"
                                        Text="Previous Page" Enabled="False" CausesValidation="False" EnableViewState="False"
                                        UseSubmitBehavior="False" />
                                    <asp:Button ID="NextPage" runat="server" CommandName="Next" OnCommand="ChangePage"
                                        Text="Next Page" CausesValidation="False" EnableViewState="False" UseSubmitBehavior="False" />
                                    <asp:Label ID="JumpPageLabel" runat="server" Text="Jump to Page:"></asp:Label>&nbsp;
                <asp:TextBox ID="JumpPage" runat="server" EnableViewState="False" MaxLength="5"></asp:TextBox>&nbsp;<asp:Button
                    ID="JumpPageButton" runat="server" Text="Go" OnClick="JumpPageButton_Click" />
                                </asp:Panel>
                                <%-- <asp:Panel ID="PanelDefaultView" runat="server"  CssClass="notification information" Visible="false">
                Search Results 
            </asp:Panel>--%>
                            </div>
                        </div>
                    </div>
                
                <div class="left-col">
                    <div class="box oneThirds">
                        <h2 class="box-title">Other Search Options</h2>
                        <div id="right-content" class="box-content">
                            <ul>
                                <li><a href="/Results/Name-Search">Name Search</a></li>
                                <li><a href="/Results/Advanced-Search">Advanced Search</a></li>
                                <li><a href="/Results/Full-Search">Full Results List</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="clearer"></div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content  ID="Content3" runat="server" ContentPlaceHolderID="AdditionalFooter"></asp:Content>