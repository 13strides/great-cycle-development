﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using ThirteenStrides.Database;
using GCResults;

public partial class Results_AdvancedSearch : System.Web.UI.Page
{
    int selectedRaceID = 0;
    string searchSurname = string.Empty;
    string searchFirstName = string.Empty;
    string searchRaceNumber = string.Empty;
    string selectedGender = "0";
    string selectedCountry = "0";
    string selectedAgeCategory = "0";
    string selectedJuniorRace = "";
    string selectedRaceCategory = "0";
    string searchPostCode = string.Empty;
    string searchClub = string.Empty;
    string selectedGCFilter = "";
    string theRaceDistance = string.Empty;
    bool isPageChanging = false;
    bool isPageJumping = false;
    bool isSorting = false;
    private const int PAGE_SIZE = 10;
    private string compareList;
    DataOperations _dataOperations = new DataOperations();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Form.Action = Request.RawUrl;
        //populate (bind) controls
        if (!Page.IsPostBack)
        {
            //Populate Race List
            DataSet raceData = _dataOperations.GetRaces();
            if (raceData != null && raceData.Tables.Count > 0 && raceData.Tables[0].Rows.Count > 0)
            {
                raceData.Tables[0].Rows[0].Delete(); //hide select events default selection
                RaceList.DataTextField = "description"; //Text
                RaceList.DataValueField = "idrace"; //Value
                if(Session["CompareAll"] != null)
                {
                     List<ComparePerson> cList = (List<ComparePerson>)Session["CompareAll"];
                     if (cList != null && cList.Count > 0)
                     {
                         RaceList.SelectedValue  = ((ComparePerson)cList.ToArray()[cList.Count - 1]).RaceID.ToString();
                     }
                }
                else
                {
                    RaceList.SelectedValue = raceData.Tables[0].Rows[1]["idrace"].ToString();
                }
                RaceList.OptionGroupField = "raceyear"; //Grouping in Drop Down List

                RaceList.DataSource = raceData;
                RaceList.DataBind();

            }

        }
        else
        {
           
            selectedRaceID = Convert.ToInt32(RaceList.SelectedValue);
        }
        //get selected race
        if (RaceList != null && RaceList.Items.Count > 0 && selectedRaceID == 0)
                selectedRaceID = Convert.ToInt32(RaceList.SelectedValue);   
 
        
        //maintaining comparison list
        MaintainComparisonList();          
        
        RaceName.Text = RaceList.SelectedItem.Text;
        RaceFullName.Value = RaceList.SelectedItem.Text;//requrid by the client side script
        HiddenRaceId.Value = selectedRaceID.ToString();
        GetRaceInfo();
        PopulateRaceCategoryList();
        PopulateAgeCategoryList();
        if(selectedRaceID.ToString().Equals("222"))
        PhotosRaceId.Value = "9999"; //any number

    }
    private void GetSearchResults()
    {
   
        if (Helper.IsSearchContainsBadPhrases(searchSurname) || Helper.IsSearchContainsBadPhrases(searchFirstName)
            || Helper.IsSearchContainsBadPhrases(searchClub))
        { 
            ResultCount.Value = "0";
        }
        else
        {
            //Do Search
            ResultCount.Value = _dataOperations.GetResultsCount(selectedRaceID.ToString(), searchRaceNumber, searchSurname, searchFirstName,
                selectedGender, searchPostCode, selectedCountry, selectedRaceCategory, selectedAgeCategory, searchClub, selectedJuniorRace, selectedGCFilter).ToString();
            NumberOfPages.Value = Math.Ceiling(Convert.ToDecimal(ResultCount.Value) / PAGE_SIZE).ToString();
 
        }
         
        if (ResultsGrid.PageIndex == null || ResultsGrid.PageIndex < 0) ResultsGrid.PageIndex = 0;
        if (Convert.ToInt32(ResultCount.Value) > 0)
        {
            //We have found results, populate the grid
            DataSet resultsData = _dataOperations.GetResults2(selectedRaceID.ToString(), searchRaceNumber, searchSurname, searchFirstName, selectedGender,
                searchPostCode, selectedCountry, selectedRaceCategory, selectedAgeCategory, searchClub, ResultsGrid.PageIndex, "r.surname, r.forenames ", "ASC",
                NikePlusUrl.Value, Active.Value, selectedJuniorRace, PhotosRaceId.Value, "","", selectedGCFilter, EventType.Value);

            ResultsGrid.DataSource = resultsData;
            ResultsGrid.DataBind();
            
            UpdateSplitNames(selectedRaceID.ToString());
            UpdateNikePlusGraphic();
            PanelPageNav.Visible = true;
        }
        else
        {
            //We have no results, bind to an empty dataset to clear grid
            DataTable EmptyDataTable = new DataTable();
            ResultsGrid.DataSource = EmptyDataTable;
            PanelPageNav.Visible = false;
            ResultsGrid.DataBind();

        }


        if (Convert.ToInt32(ResultCount.Value) > 0)
        {
            NextPage.Visible = true;
            PreviousPage.Visible = true;
            JumpPage.Visible = true;
            JumpPageLabel.Visible = true;
            JumpPageButton.Visible = true;

            if (Convert.ToInt32(NumberOfPages.Value) <= ResultsGrid.PageIndex + 1)
            {
                NextPage.Enabled = false;
            }
            else
            {
                NextPage.Enabled = true;
            }

            if (ResultsGrid.PageIndex == 0)
            {
                PreviousPage.Enabled = false;
            }
            else
            {
                PreviousPage.Enabled = true;
            }

        }
        else
        {
            NextPage.Visible = false;
            PreviousPage.Visible = false;
            JumpPage.Visible = false;
            JumpPageLabel.Visible = false;
            JumpPageButton.Visible = false;

        }

        UpdatePageDetailsLabel(ResultsGrid.PageIndex + 1, Convert.ToInt32(NumberOfPages.Value), Convert.ToInt32(ResultCount.Value));
    }
    
    /// <summary>
    /// Fired when a column header is clicked on the results
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SortResults(object sender, GridViewSortEventArgs e)
    {
        isSorting = true;
        GetSearchParameters();
        string SortOrder = "";
        string SortColumn = "";

        switch (e.SortExpression.ToString())
        {
            case "Name":
                SortColumn = "r.surname, r.forenames";
                break;

            case "BIB":
                SortColumn = "r.racenumber";
                break;

            case "Club":
                SortColumn = "p.AthleticsClubName";
                break;

            case "Pos":
                SortColumn = "r.CategoryPosition";
                break;

            case "Age Group Position":
                SortColumn = "r.AgeCatPos";
                break;

            case "Gender Position":
                SortColumn = "r.GenderPos";
                break;

            case "Age Group and Gender Position":
                SortColumn = "r.AgeGenderPos";
                break;

            case "Finish Time":
                SortColumn = "r.TimeFinish";
                break;

            case "Split 1":
                SortColumn = "r.TimeInt1";
                break;

            case "Split 2":
                SortColumn = "r.TimeInt2";
                break;

            case "Split 3":
                SortColumn = "r.TimeInt3";
                break;

            case "Split 4":
                SortColumn = "r.TimeInt4";
                break;

            case "Split 5":
                SortColumn = "r.TimeInt5";
                break;

            case "Split 6":
                SortColumn = "r.TimeInt6";
                break;

            case "Split 7":
                SortColumn = "r.TimeInt7";
                break;

            case "Split 8":
                SortColumn = "r.TimeInt8";
                break;

            case "Split 9":
                SortColumn = "r.TimeInt9";
                break;

            case "Split 10":
                SortColumn = "r.TimeInt10";
                break;

            case "Wetsuit":
                SortColumn = "r.wetsuit";
                break;

            default:
                if (searchFirstName != "" || searchSurname != "")
                {
                    SortColumn = "r.surname, r.forenames";
                }
                else
                {
                    SortColumn = "r.TimeFinish";
                }
                break;
        }

        if (e.SortExpression.ToString() == CurrentSortColumn.Value)
        {
            if (CurrentSortOrder.Value == "Ascending")
            {
                SortOrder = "DESC";
                CurrentSortOrder.Value = "Descending";
            }
            else
            {
                SortOrder = "ASC";
                CurrentSortOrder.Value = "Ascending";
            }
        }
        else
        {
            SortOrder = "ASC";
            CurrentSortOrder.Value = "Ascending";
            CurrentSortColumn.Value = e.SortExpression.ToString();
        }

        ResultsGrid.PageIndex = 0;
        DataSet resultsData = _dataOperations.GetResults2(selectedRaceID.ToString(), searchRaceNumber, searchSurname, searchFirstName, selectedGender,
                searchPostCode, selectedCountry, selectedRaceCategory, selectedAgeCategory, searchClub, ResultsGrid.PageIndex, SortColumn, SortOrder,
                NikePlusUrl.Value, Active.Value, selectedJuniorRace, PhotosRaceId.Value, "", "", selectedGCFilter, EventType.Value);
        if (resultsData != null && resultsData.Tables.Count > 0 && resultsData.Tables[0].Rows.Count > 0)
            PanelPageNav.Visible = true;
        ResultsGrid.DataSource = resultsData;
        ResultsGrid.DataBind();
        UpdateSplitNames(selectedRaceID.ToString());

        if (Convert.ToInt32(NumberOfPages.Value) <= ResultsGrid.PageIndex + 1)
        {
            NextPage.Enabled = false;
        }
        else
        {
            NextPage.Enabled = true;
        }

        if (ResultsGrid.PageIndex == 0)
        {
            PreviousPage.Enabled = false;
        }
        else
        {
            PreviousPage.Enabled = true;
        }

        UpdatePageDetailsLabel(ResultsGrid.PageIndex + 1, Convert.ToInt32(NumberOfPages.Value), Convert.ToInt32(ResultCount.Value));

    }

    /// <summary>
    /// This method will handle the navigation / paging index
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChangePage(object sender, CommandEventArgs e)
    {
        isPageChanging = true;
        GetSearchParameters();
        switch (e.CommandName)
        {
            case "Previous":
                ResultsGrid.PageIndex = ResultsGrid.PageIndex - 1;
                break;

            case "Next":
                ResultsGrid.PageIndex = ResultsGrid.PageIndex + 1;
                break;
        }

        string SortOrder = "";

        if (CurrentSortOrder.Value == "Ascending")
        {
            SortOrder = "ASC";
        }
        else
        {
            SortOrder = "DESC";
        }

        string SortColumn = "";

        switch (CurrentSortColumn.Value)
        {
            case "Name":
                SortColumn = "r.surname, r.forenames";
                break;

            case "BIB":
                SortColumn = "r.racenumber";
                break;

            case "Club":
                SortColumn = "p.AthleticsClubName";
                break;

            case "Pos":
                SortColumn = "r.CategoryPosition";
                break;

            case "Age Group Position":
                SortColumn = "r.AgeCatPos";
                break;

            case "Gender Position":
                SortColumn = "r.GenderPos";
                break;

            case "Age Group and Gender Position":
                SortColumn = "r.AgeGenderPos";
                break;

            case "Finish Time":
                SortColumn = "r.TimeFinish";
                break;

            case "Split 1":
                SortColumn = "r.TimeInt1";
                break;

            case "Split 2":
                SortColumn = "r.TimeInt2";
                break;

            case "Split 3":
                SortColumn = "r.TimeInt3";
                break;

            case "Split 4":
                SortColumn = "r.TimeInt4";
                break;

            case "Split 5":
                SortColumn = "r.TimeInt5";
                break;

            case "Split 6":
                SortColumn = "r.TimeInt6";
                break;

            case "Split 7":
                SortColumn = "r.TimeInt7";
                break;

            case "Split 8":
                SortColumn = "r.TimeInt8";
                break;

            case "Split 9":
                SortColumn = "r.TimeInt9";
                break;

            case "Split 10":
                SortColumn = "r.TimeInt10";
                break;

            case "Wetsuit":
                SortColumn = "r.wetsuit";
                break;

            default:
                if (searchFirstName != "" || searchSurname != "")
                {
                    SortColumn = "r.surname, r.forenames";
                }
                else
                {
                    SortColumn = "r.TimeFinish";
                }
                break;
        }

        DataSet resultsData = _dataOperations.GetResults2(selectedRaceID.ToString(), searchRaceNumber, searchSurname, searchFirstName, selectedGender,
                searchPostCode, selectedCountry, selectedRaceCategory, selectedAgeCategory, searchClub, ResultsGrid.PageIndex, SortColumn, SortOrder,
                NikePlusUrl.Value, Active.Value, selectedJuniorRace, PhotosRaceId.Value, "", "", selectedGCFilter, EventType.Value);
        if (resultsData != null && resultsData.Tables.Count > 0 && resultsData.Tables[0].Rows.Count > 0)
            PanelPageNav.Visible = true;
        ResultsGrid.DataSource = resultsData;
        ResultsGrid.DataBind();
        UpdateSplitNames(selectedRaceID.ToString());

        if (Convert.ToInt32(NumberOfPages.Value) <= ResultsGrid.PageIndex + 1)
        {
            NextPage.Enabled = false;
        }
        else
        {
            NextPage.Enabled = true;
        }

        if (ResultsGrid.PageIndex == 0)
        {
            PreviousPage.Enabled = false;
        }
        else
        {
            PreviousPage.Enabled = true;
        }

        //UpdatePageDetailsLabel(ResultsGrid.PageIndex + 1, Convert.ToInt32(NumberOfPages.Value), Convert.ToInt32(ResultCount.Value));

        //Keep focus at same part of page (on a non disabled command)
        if (e.CommandName == "Previous")
        {
            NextPage.Focus();
        }
        else
        {
            PreviousPage.Focus();
        }
    }

    /// <summary>
    /// This function updates the page details label
    /// </summary>
    /// <param name="pageNumber"></param>
    /// <param name="numberOfPages"></param>
    /// <param name="numberOfResults"></param>
    protected void UpdatePageDetailsLabel(int pageNumber, int numberOfPages, int numberOfResults)
    {
        //***PH 2010-09-29 Code commented out to prevent this being displayed in source code of page***
        //if (numberOfResults > 0)
        //{
        //    //PageDetails.Text = numberOfResults.ToString() + " Results Found : ";
        //    //PageDetails.Text += "Displaying Page " + pageNumber.ToString() + " of " + numberOfPages.ToString() + "<br /><br />";
        //    PageDetails.Text = "Displaying Page " + pageNumber.ToString() + " of " + numberOfPages.ToString() + "<br /><br />";
        //}
        //else
        //{
        //    PageDetails.Text = "Sorry, no results have been found";
        //}
    }

    /// <summary>
    /// This function is fired when data is bound to the results grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ResultsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.CssClass = "head";
            e.Row.Cells[2].Text = "Finish Time";
            e.Row.Cells[3].Text = "Speed mph";
            if (e.Row.Cells[4].Text.ToLower().Contains("photos"))
                e.Row.Cells[4].Text = "Photos";
            else
                e.Row.Cells[4].Text = "Compare";
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            TableCellCollection cells = e.Row.Cells;
            if (!string.IsNullOrEmpty(cells[0].Text) && !string.IsNullOrEmpty(cells[1].Text))
            {
                cells[0].Attributes.Add("class", "athlete-name");
                cells[0].Text = "<a href='/Results/" + selectedRaceID.ToString() + "/" + cells[1].Text + "'>" + cells[0].Text + "</a>";
            }
            if (cells[3].Text.ToLower().Contains("average"))
            {
                cells[3].Text = Helper.ComputeAverageSpeed(cells[2].Text, theRaceDistance);
            }
            if (cells[4].Text.ToLower().Contains("photos") && !string.IsNullOrEmpty(RaceName.Text)
                && !string.IsNullOrEmpty(RaceYear.Value) /*&& string.IsNullOrEmpty(cells[2].Text)*/ && !string.IsNullOrEmpty(cells[1].Text))
            {
                string raceNameForUrl = RaceName.Text;
                cells[4].Text = "<a href=\"" + ConfigurationManager.AppSettings["MarathonPhotosUrl"].ToString() + RaceYear.Value + "/" + Server.UrlEncode(raceNameForUrl) + "&bib=" + cells[1].Text + "\"  target=\"_blank\" title=\"See your photos\"><img src=\"images\\buttonSeeYourPhotos.png\" width=\"22\" border=\"0\" alt=\"See your photos\"></a>";
            }
            for (int c = 0; c < cells.Count; c++ )
            {
                cells[c].Text = Server.HtmlDecode(cells[c].Text);
                switch (c)
                {
                    case 0:
                        cells[c].Width = 300;
                        break;
                    case 1:
                        cells[c].Width = 30;
                        break;
                    case 2:
                        cells[c].Width = 80;
                        break;
                    case 3:
                        cells[c].Width = 30;
                        break;
                    case 4:
                        cells[c].Width = 80;
                        break;
                    case 5:
                        cells[c].Width = 40;
                        break;
                    case 6:
                        cells[c].Width = 30;
                        break;
                    case 7:
                        cells[c].Width = 30;
                        break;
                    default:
                        break;
                }

            }

        }
       
    }

    /// <summary>
    /// This function is called to update the nike+ graphic
    /// </summary>
    protected void UpdateNikePlusGraphic()
    {
        //if (Active.Value != "2")
        //{
        //    if (Active.Value == "1")
        //    {
        //        //Show Active Nike+ Graphic
        //        NikePlusGraphic.Text = "<img src=\"images\\GIR_buttonResults.gif\" border=\"0\" alt=\"See your run powered by Nike+\">";

        //    }
        //    else
        //    {
        //        //Show Coming Soon Nike+ Graphic
        //        NikePlusGraphic.Text = "<img src=\"images\\GIR_buttonSoon.gif\" border=\"0\"  alt=\"Come back soon to see your run powered by Nike+\">";
        //    }
        //}
        //else
        //{
        //    NikePlusGraphic.Text = "";
        //}

    }


    protected void UpdateSplitNames(string raceId)
    {
        DataSet SplitNames = _dataOperations.GetSplitNames(raceId);

        //int ColumnNumber = 5;

        //foreach (DataRow row in SplitNames.Tables[0].Rows)
        //{
        //    ResultsGrid.HeaderRow.Cells[ColumnNumber].Text = row["TimeName"].ToString();
        //    ColumnNumber++;
        //}
    }

    protected void JumpPageButton_Click(object sender, EventArgs e)
    {
        isPageJumping = true;
        GetSearchParameters();
        int NewPageIndex;

        if (!isNumeric(JumpPage.Text, System.Globalization.NumberStyles.Integer))
        {
            NewPageIndex = 0;
        }
        else
        {
            NewPageIndex = Convert.ToInt32(JumpPage.Text) - 1;
        }

        //If Jump to Page Index is less than 1, then set to first page
        if (NewPageIndex < 0)
        {
            NewPageIndex = 0;
        }

        //If Jump to Page Index is greater than new page index, set to last page
        if (Convert.ToInt32(NumberOfPages.Value) < NewPageIndex + 1)
        {
            NewPageIndex = Convert.ToInt32(NumberOfPages.Value) - 1;
        }

        ResultsGrid.PageIndex = NewPageIndex;

        string SortOrder = "";

        if (CurrentSortOrder.Value == "Ascending")
        {
            SortOrder = "ASC";
        }
        else
        {
            SortOrder = "DESC";
        }

        string SortColumn = "";

        switch (CurrentSortColumn.Value)
        {
            case "Name":
                SortColumn = "r.surname, r.forenames";
                break;

            case "BIB":
                SortColumn = "r.racenumber";
                break;

            case "Club":
                SortColumn = "p.AthleticsClubName";
                break;

            case "Pos":
                SortColumn = "r.CategoryPosition";
                break;

            case "Age Group Position":
                SortColumn = "r.AgeCatPos";
                break;

            case "Gender Position":
                SortColumn = "r.GenderPos";
                break;

            case "Age Group and Gender Position":
                SortColumn = "r.AgeGenderPos";
                break;

            case "Finish Time":
                SortColumn = "r.TimeFinish";
                break;

            case "Split 1":
                SortColumn = "r.TimeInt1";
                break;

            case "Split 2":
                SortColumn = "r.TimeInt2";
                break;

            case "Split 3":
                SortColumn = "r.TimeInt3";
                break;

            case "Split 4":
                SortColumn = "r.TimeInt4";
                break;

            case "Split 5":
                SortColumn = "r.TimeInt5";
                break;

            case "Split 6":
                SortColumn = "r.TimeInt6";
                break;

            case "Split 7":
                SortColumn = "r.TimeInt7";
                break;

            case "Split 8":
                SortColumn = "r.TimeInt8";
                break;

            case "Split 9":
                SortColumn = "r.TimeInt9";
                break;

            case "Split 10":
                SortColumn = "r.TimeInt10";
                break;

            case "Wetsuit":
                SortColumn = "r.wetsuit";
                break;

            default:
                if (searchFirstName != "" || searchSurname != "")
                {
                    SortColumn = "r.surname, r.forenames";
                }
                else
                {
                    SortColumn = "r.TimeFinish";
                }
                break;
        }

        DataSet resultsData = _dataOperations.GetResults2(selectedRaceID.ToString(), searchRaceNumber, searchSurname, searchFirstName, selectedGender,
                searchPostCode, selectedCountry, selectedRaceCategory, selectedAgeCategory, searchClub, ResultsGrid.PageIndex, SortColumn, SortOrder,
                NikePlusUrl.Value, Active.Value, selectedJuniorRace, PhotosRaceId.Value, "", "", selectedGCFilter, EventType.Value);
        ResultsGrid.DataSource = resultsData;
        ResultsGrid.DataBind();
        UpdateSplitNames(selectedRaceID.ToString());

        if (Convert.ToInt32(NumberOfPages.Value) <= NewPageIndex + 1)
        {
            NextPage.Enabled = false;
        }
        else
        {
            NextPage.Enabled = true;
        }

        if (ResultsGrid.PageIndex == 0)
        {
            PreviousPage.Enabled = false;
        }
        else
        {
            PreviousPage.Enabled = true;
        }

        UpdatePageDetailsLabel(NewPageIndex + 1, Convert.ToInt32(NumberOfPages.Value), Convert.ToInt32(ResultCount.Value));

        //Keep focus at same part of page (on a non disabled command)
        JumpPage.Text = "";
        JumpPage.Focus();
    }

    public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
    {
        Double result;
        return Double.TryParse(val, NumberStyle, System.Globalization.CultureInfo.CurrentCulture, out result);
    }
    protected void DoSearch_Click(object sender, EventArgs e)
    {
        ResultsGrid.PageIndex = 0;
        //ensure the RaceNumber field and query parameter are all empty
        searchRaceNumber = string.Empty;
        RaceNumber.Text = searchRaceNumber;

        //GetSearchParameters();        
        //GetSearchResults();

    }
    /// <summary>
    /// Fired when the user clicks the Clear Search button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ClearSearch_Click(object sender, EventArgs e)
    {
        RaceNumber.Text = "";

        if (EventType.Value == "4" || EventType.Value == "5")
        {
           // AgeGroup.SelectedIndex = 0;

            if (EventType.Value == "4")
            {
                JuniorRace.SelectedIndex = 0;
            }
        }

        FirstName.Text = "";
        Surname.Text = "";
        Gender.SelectedIndex = 0;
        Country.SelectedIndex = 0;
        AgeGroup.SelectedIndex = 0;
        RaceCategory.SelectedIndex = 0;
        //AthleticsClub.SelectedIndex = 0;
        //AthleticsClub.Text = "";
        //GunChip.SelectedIndex = 0;
        //NonRaceNumberSearch.Attributes.Add("style", "visibility:visible");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //clear the comparor list
        ClearComparorVariables();
        //repopulate the comparor list from session
        if (Session["CompareAll"] != null)
        {
            List<ComparePerson> list = (List<ComparePerson>)Session["CompareAll"];
            if (list != null && list.Count > 0)
            {
                string raceName = string.Empty;
                foreach (ComparePerson p in list)
                {
                    raceName = _dataOperations.GetRaceName(p.RaceID);
                    DivCompare.InnerHtml += "<p id='name_" + p.Bib.ToString() + "' name='" + p.RaceID.ToString()+ "' onclick='removeCompare(this)'><em>" + raceName + "</em><br /><span>" + p.FullName + "</span></p>";
                    CompareName.Value += p.FullName + ",";
                    CompareValue.Value += p.Bib.ToString() + "r" + p.RaceID.ToString() + ",";
                }
            }
        }
        //on page direct call (if page loads with out query strings)
        if (isPageJumping == false && isPageChanging==false && isSorting == false)
        {
            //Default search column
            CurrentSortColumn.Value = "Name";
            //bind Gridview with a generic of the latest event
            if (!Page.IsPostBack)
                PanelDefaultView.Visible = true;
            else
            {
                GetSearchParameters();
                GetSearchResults();
                PanelDefaultView.Visible = false;
            }
        }
        
    }

    private void GetRaceInfo()
    {
        if (HiddenRaceId.Value == "0")
        {
            NikePlusUrl.Value = "";
            Active.Value = "0";
            EventType.Value = "0";
            AgeGroupStored.Value = "0";
            AgeGroup.SelectedIndex = 0;
            PhotosRaceId.Value = "";
            PhotosRaceName.Value = "";
            RaceYear.Value = "";
        }
        else
        {
            DataSet raceData = _dataOperations.GetRace(HiddenRaceId.Value);
            if (raceData.Tables[0].Rows.Count > 0)
            {
                //Then we have a race with results available
                theRaceDistance = raceData.Tables[0].Rows[0]["Distance"].ToString();
                NikePlusUrl.Value = raceData.Tables[0].Rows[0]["NikePlusUrl"].ToString();
                Active.Value = raceData.Tables[0].Rows[0]["Active"].ToString();
                EventType.Value = raceData.Tables[0].Rows[0]["idEventType"].ToString();
                if (!HiddenRaceId.Value.Equals("222"))
                {
                    PhotosRaceId.Value = raceData.Tables[0].Rows[0]["PhotosRaceId"].ToString();
                }
                //PhotosRaceName.Value = raceData.Tables[0].Rows[0]["PhotosRaceName"].ToString();
                PhotosRaceName.Value = "";
                RaceYear.Value = raceData.Tables[0].Rows[0]["RaceYear"].ToString();
            }
        }
    }
    private void PopulateRaceCategoryList()
    {
        //Populate Race Category List
        string selectedValue = "0";
        if (Page.IsPostBack)
        {
            selectedValue = RaceCategory.SelectedValue;
        }
        DataSet categoryData = _dataOperations.GetRaceCategories(HiddenRaceId.Value);
        RaceCategory.DataSource = categoryData;
        RaceCategory.DataValueField = categoryData.Tables[0].Columns[0].ToString();
        RaceCategory.DataTextField = categoryData.Tables[0].Columns[1].ToString();
        RaceCategory.DataBind();

        ListItem CategoryFirstItem = new ListItem("-- Category Filter --", "0");
        RaceCategory.Items.Insert(0, CategoryFirstItem);

        if (EventType.Value != "4" && EventType.Value != "5")
        {
            //ListItem CategorySecondItem = new ListItem("Elite Men and Masses", "1");
            //RaceCategory.Items.Insert(1, CategorySecondItem);
        }
        try
        {
            RaceCategory.SelectedValue = selectedValue;
        }
        catch
        {
            RaceCategory.SelectedValue = "0";
        }
    }
    private void PopulateAgeCategoryList()
    {
        //Determine whether to display age range filter
        //If its a junior or mini run, then dont display age range drop down
        //and if its a junior run show junior race drop down
        if (EventType.Value == "4" || EventType.Value == "5")
        {
            AgeGroupLabel.Visible = false;
            AgeGroup.Visible = false;
            AgeGroupStored.Value = "0";

            if (EventType.Value == "4")
            {
                selectedJuniorRace = JuniorRace.SelectedValue;
                JuniorRaceLabel.Visible = true;
                JuniorRace.Visible = true;
                
                //Populate Junior Race Dropdown
                DataSet waveData = _dataOperations.GetRaceWaves(HiddenRaceId.Value);
                JuniorRace.DataSource = waveData;
                JuniorRace.DataValueField = waveData.Tables[0].Columns[1].ToString();
                JuniorRace.DataTextField = waveData.Tables[0].Columns[0].ToString();
                JuniorRace.DataBind();

                ListItem JuniorRaceFirstItem = new ListItem("-- Junior Race Filter --", "");
                JuniorRace.Items.Insert(0, JuniorRaceFirstItem);
                try { JuniorRace.SelectedValue = selectedJuniorRace; }
                catch { JuniorRace.SelectedValue = ""; }
            }
            else
            {
                JuniorRaceLabel.Visible = false;
                JuniorRace.Visible = false;
                //JuniorRace.SelectedIndex = 0;
                JuniorRaceStored.Value = "";
            }
        }
        else
        {
            AgeGroupLabel.Visible = true;
            AgeGroup.Visible = true;
            JuniorRaceLabel.Visible = false;
            JuniorRace.Visible = false;
            //JuniorRace.SelectedIndex = 0;
            JuniorRaceStored.Value = "";
        }
    }
    private void GetSearchParameters()
    {
        int raceNumber = 0;
        if (!string.IsNullOrEmpty(RaceNumber.Text))
        {
            bool isNumber = int.TryParse(RaceNumber.Text, out raceNumber);
            if (isNumber)
            { searchRaceNumber = RaceNumber.Text; }
            else { searchRaceNumber = string.Empty; }
        }

        searchFirstName = FirstName.Text;
        searchSurname = Surname.Text;
        searchPostCode = Postcode.Text;
        //searchClub = AthleticsClub.Text;
        selectedGender = Gender.SelectedValue;
        selectedCountry = Country.SelectedValue;
        if(AgeGroup.Visible)
        selectedAgeCategory = AgeGroup.SelectedValue;
        if(JuniorRace.Visible)
        selectedJuniorRace = JuniorRace.SelectedValue;
        selectedRaceCategory = RaceCategory.SelectedValue;
        selectedGCFilter = "";
    }
    protected void ButtonCompare_Click(object sender, EventArgs e)
    {
        string query = string.Empty;
        if (CompareValue.Value.Length > 0 && CompareName.Value.Length > 0)
        {
            ManageComparorList(ref query);
            if (query.EndsWith("c"))
            {
                query = query.Remove(query.LastIndexOf("c"), 1);               
               
            }
            Response.Redirect("/Results/Compare/" + selectedRaceID.ToString() + "/" + query);

        }
    }
    private void MaintainComparisonList()
    {

            if (Session["CompareAll"] != null)
            {
                List<ComparePerson> cList = (List<ComparePerson>)Session["CompareAll"];
                if (cList != null && cList.Count > 0)
                {
                        string query = string.Empty;
                        ManageComparorList(ref query);           
                    
                }
            }
            else  //if post back and null
            {
                if (Session["CompareAll"] == null)
                {
                    string query = string.Empty;
                    ManageComparorList(ref query);
                }
            }

        
        
    }

    protected void ButtonQuickSearch_Click(object sender, EventArgs e)
    {
        GetSearchParameters();
        GetSearchResults();
    }
    private void ManageComparorList(ref string query)
    {
        List<ComparePerson> list = new List<ComparePerson>();
        int maxComparison = 10;
        if (CompareValue.Value.Length > 0 && CompareName.Value.Length > 0)
        {
            try
            {
                if (CompareValue.Value.EndsWith(","))
                    CompareValue.Value = CompareValue.Value.Remove(CompareValue.Value.LastIndexOf(","), 1);
                if (CompareName.Value.EndsWith(","))
                    CompareName.Value = CompareName.Value.Remove(CompareName.Value.LastIndexOf(","), 1);
                string[] bibs = CompareValue.Value.Split(",".ToCharArray());
                string[] persons = CompareName.Value.Split(",".ToCharArray());
                if (bibs.Length > 0 && persons.Length > 0)
                {
                    int num = 0;
                    bool refined = false;
                    ComparePerson cp;
                    for (int i = 0; i < bibs.Length; i++)
                    {
                        if (i < maxComparison)
                        {
                            refined = int.TryParse(bibs[i].Split("r".ToCharArray())[0], out num);
                            if (refined)
                            {
                                cp = new ComparePerson(num, persons[i].ToString(), Convert.ToInt32(bibs[i].Split("r".ToCharArray())[1]));
                                list.Add(cp);
                                query += bibs[i] + "c";
                            }
                        }

                    }
                }
            }
            catch
            {
                query = "0";
            }
            if (list != null && list.Count > 0)
            {
                if (Session["CompareAll"] != null)
                {
                    Session["CompareAll"] = list;
                }
                else
                {
                    Session.Add("CompareAll", list);
                }
            }           
        }
    }
    private void ClearComparorVariables()
    {
        DivCompare.InnerHtml = "";
        CompareName.Value = "";
        CompareValue.Value = "";
    }

    protected void ButtonResetCompare_Click(object sender, EventArgs e)
    {
        if (Session["CompareAll"] != null)
        {
            Session.Remove("CompareAll");
        }
    }
}
