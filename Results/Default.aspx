<%@ Page Title="Great Cycle Results" Language="C#" MasterPageFile="~/Site.master"
	AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Results_Default" %>

<%@ Register TagPrefix="ddlb" Assembly="OptionDropDownList" Namespace="OptionDropDownList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<meta property="og:image" content="http://www.greatcycle.org/results/images/default_fb_share_thumb.png" />
	<meta property="og:title" content="Daily Mirror Great Cycle Results" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://www.greatcycle.org/results/" />
	<link href="/Results/ExtendedCss/results.css" type="text/css" rel="stylesheet" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="text/javascript">

		$(document).ready(function () {
			var boxHeight = ($(".right-column").height() - $("#mainResult").height()) + $("#mainResult").height();
		});

	</script>
	<style type="text/css">
		.left-column
		{
			float: left;
			width: 640px;
		}

		.right-column
		{
			float: right;
			width: 316px;
			margin-right: 8px;
		}

		.contentContainer
		{
			padding: 8px 0 0px 8px;
			clear: both;
		}

		#main-search
		{
			padding: 0px 0 0px 0;
		}

		.comp-img
		{
		}
	</style>
	<script type="text/javascript">
		function fbs_click(obj) {
			var raceName = '<%=RaceList.SelectedItem.Text %>';
			if (raceName) {
				//pageTracker._trackPageview('/EXIT/RESULTS/DEFAULT_PAGE/' + raceName + '/FACEBOOK.html');
			}
			u = obj.href; t = document.title;
			window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
			return false;

		}
		function twt_click(obj) {
			var raceName = '<%=RaceList.SelectedItem.Text %>';
			var txt = '#greatcycle';
			if (raceName) {
				//pageTracker._trackPageview('/EXIT/RESULTS/DEFAULT_PAGE/' + raceName + '/TWITTER.html');
			}
			u = obj.href;
			window.open('http://twitter.com/share?original_referer=' + encodeURIComponent(u) + '&text=' + encodeURIComponent(txt), 'sharer', 'toolbar=0,status=0,width=626,height=436');
			return false;
		}</script>
	<link href="/Results/ExtendedCss/graphics.css" rel="stylesheet" type="text/css" />
	<script src="/Results/Scripts/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="/Results/Scripts/flot/jquery.flot.selection.js" type="text/javascript"></script>
	<script src="/Results/Scripts/pagehelpers.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<asp:Panel ID="PanelDefaultView" runat="server" Visible="true">
		<div class="row pageheading">
			<div class="span6">
				<h1>
					Results</h1>
				<ul class="breadcrumb">
                 <strong>You are here:</strong>
					<li><a href="/">Home</a> <span class="divider">/</span></li>
					<li class="active">Results</li>
				</ul>
			</div>
			<a href="/Event/" class="btn btn-primary btn-large">Enter Now &raquo;</a>
		</div>
    <img src="/img/css/resultsbanner.jpg" />
	</asp:Panel>
	
	<div class="row results-search">
		<asp:TextBox ID="TextBoxSearch" runat="server" Text="Race Number or Surname"></asp:TextBox>
		<!--<span>For The</span>-->
		<ddlb:OptionGroupSelect ID="RaceList" runat="server" AutoPostBack="False" />
		<asp:Button ID="ButtonGo" CssClass="btn-primary" runat="server" Text="Search" OnClick="ButtonGo_Click" />
		<asp:HyperLink ID="HyperLink2" NavigateUrl="~/Results/Full-Search" runat="server">Full Result List</asp:HyperLink>
		<asp:HyperLink ID="HyperLinkBack" NavigateUrl="~/Results/Advanced-Search" runat="server">Advanced Search</asp:HyperLink>
		<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Results/Help" Target="_blank">Help</asp:HyperLink>
		<div class="clearer">
		</div>
	</div>
	<asp:Panel ID="PanelBigResultShow" runat="server" Visible="false">
	</asp:Panel>
	<%-- <div id="myGraph-dispbox" class="displaybox">

   </div>--%>
	<div class="row">
		<div id="speed-dispbox" class="displaybox">
			<div id="myGraph-dispbox" style="width: 880px; height: 250px; clear: both;">
			</div>
		</div>
	</div>
	<asp:Literal ID="Literal1" runat="server"></asp:Literal>
	<div class="row">
		<div id="content">
			<div class="span6">
				<asp:Panel ID="PanelResults" runat="server">
					<div id="primary">
						<div class="com75">
							<div class="header">
								<h1 class='box-title'>
									<asp:Literal ID="LiteralRaceTitle" runat="server"></asp:Literal>
									<asp:LinkButton ID="BtnAddToCompare" runat="server" Text="Compare" CssClass="compare-btn"
										OnClick="BtnAddToCompare_Click">Compare With Others</asp:LinkButton>
									<input type="hidden" runat="server" id="CompareInfo" value="" />
								</h1>
							</div>
							<div id="mainResult" class="content">
								<asp:GridView ID="ResultsGrid" CssClass="results-table" runat="server" EnableViewState="False"
									OnRowDataBound="ResultsGrid_RowDataBound" Visible="true" EmptyDataRowStyle-CssClass="error"
									EmptyDataText="There were no records found for your search criteria. Please use our <a href='/Results/Advanced-Search'>advanced search</a>.">
									<AlternatingRowStyle CssClass="altrow" />
								</asp:GridView>
							</div>
						</div>
					</div>
				</asp:Panel>
			</div>
			<div class="span3">
				<asp:Panel ID="PanelAgeGroupResults" runat="server">
					<div class="box oneThirds">
						<h2 class="box-title">
							Age Group Results
							<%=ageGroupTitle %></h2>
						<div class="box-content" style="min-height: 130px">
							<asp:GridView ID="AgeGroupGrid" EnableViewState="False" runat="server" CssClass="results-table side-tables"
								OnRowDataBound="AgeGroupGrid_RowDataBound" EmptyDataText="There were no records found for your search criteria. Please use our <a href='/Results/Advanced-Search'>advanced search</a>."
								EmptyDataRowStyle-CssClass="notification information">
							</asp:GridView>
						</div>
					</div>
					<div class="box oneThirds">
						<h2 class="box-title">
							Gender Group Results
						</h2>
						<div class="box-content" style="min-height: 130px">
							<asp:GridView ID="GenderGroupGrid" EnableViewState="False" runat="server" CssClass="results-table side-tables"
								OnRowDataBound="GenderGroupGrid_RowDataBound" EmptyDataText="There were no records found for your search criteria. Please use our <a href='/Results/Advanced-Search'>advanced search</a>."
								EmptyDataRowStyle-CssClass="notification information">
							</asp:GridView>
						</div>
					</div>
				</asp:Panel>
			</div>
			<div style="clear: both">
			</div>
		</div>
	</div>
	<script type="text/javascript">

		if ($.browser.msie) {
			//alert($.browser.version);
			if ($.browser.version < 9) {
				//alert("true;");
				$("#speed").hide();
			}
		}

	</script>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="AdditionalFooter">
</asp:Content>