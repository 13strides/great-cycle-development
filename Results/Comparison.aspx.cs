﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using ThirteenStrides.Database;
using System.Text;
using GCResults;

public partial class Results_Comparison : System.Web.UI.Page
{
    
    DataOperations _dataOperations = new DataOperations();
    int selectedRaceID;
    int selectedBibID;
    string searchText = string.Empty;
    //bool isCompare = false;
    bool isQueryValid = false;
    string graphData = string.Empty;
    List<RaceBib> rbList = null;
    double raceDistance = 0;
    DataTable splitNames = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        Form.Action = Request.RawUrl;
        string bibs = string.Empty;
        
        LiteralRaceTitle.Text = "<h2 class='box-title'>Race Result Comparison</h2>";
        if (Request.QueryString["r"] != null && (Request.QueryString["bib"] != null || Request.QueryString["bibs"] != null))
        {           
            isQueryValid = int.TryParse(Request.QueryString["r"].ToString(), out selectedRaceID);
            if (Request.QueryString["bib"] != null)
            isQueryValid = int.TryParse(Request.QueryString["bib"].ToString(), out selectedBibID);
            if (Request.QueryString["bibs"] != null)
            {
                bibs = Request.QueryString["bibs"].ToString();
                if (bibs.IndexOf("c") > -1)
                    bibs = bibs.Replace("c", ",");
                searchText = bibs;
                isQueryValid = true;
            }
        }
        
        //raceDistance = Helper.GetRaceDistance("km", selectedRaceID);
        if (isQueryValid)
           {
               DetermineSearch();
               DoSearch();
               DoAgeGroupSearch();
           }

       ResultsGrid.Visible = true;
    }

   
    protected void Page_PreRender(object sender, EventArgs e)
    {
       //Page.Header.Controls.Add(new LiteralControl(Helper.GenerateGraphScript(graphData)));
        if (Request.UrlReferrer != null)
        {
            string referrer = Request.UrlReferrer.PathAndQuery;
            if (referrer.ToLower().Contains("namesearch"))
            {
                HyperLinkBack.NavigateUrl = "~/Results/Name-Search";
                HyperLinkBack.Text = "Back to Name Search";
            }
            else
            {
                HyperLinkBack.NavigateUrl = "~/Results/Advanced-Search";
                HyperLinkBack.Text = "Back to Advanced Search";
            }
        }
    }
   

    private void DoSearch()
    {
        string selectedRaceIDs = string.Empty;
        
        if (ResultsGrid != null)
        {
            DataTable dtAll = new DataTable();
           
            DataSet resultsData = null;
            if (rbList != null && rbList.Count > 0)
            {
                rbList.Sort(RaceBib.RaceIDComparison);
                int previousRaceID = rbList.ToArray()[0].RaceID;
                raceDistance = rbList.ToArray()[0].Distance;
                searchText = "";
                               
                foreach (RaceBib r in rbList)
                {
                    if (previousRaceID != r.RaceID)
                    {                                            
                        AddToDtAllRace(ref resultsData, ref dtAll, searchText, previousRaceID);                        
                        searchText = "";
                    }
                    searchText += r.Bib.ToString() + ",";
                    previousRaceID = r.RaceID;
                }
                AddToDtAllRace(ref resultsData, ref dtAll, searchText, previousRaceID);
            }
            
             
             //We have found results, populate the grid
            if (dtAll != null && dtAll.Rows.Count > 0)
            {
                dtAll.DefaultView.Sort = "Pos ASC";	
                ResultsGrid.DataSource = dtAll;
                ResultsGrid.DataBind();
                
            }
            else
            {
                //We have no results, bind to an empty dataset to clear grid
                DataTable EmptyDataTable = new DataTable();
                ResultsGrid.DataSource = EmptyDataTable;
                ResultsGrid.DataBind();
            }
        }
    }
    private void DoAgeGroupSearch()
    {
        if (AgeGroupGrid != null)
        {
           
            DataTable dtAll = new DataTable();
            DataSet resultsData = null;
            if (rbList != null && rbList.Count > 0)
            {
                rbList.Sort(RaceBib.RaceIDComparison);
                int previousRaceID = rbList.ToArray()[0].RaceID;
                raceDistance = rbList.ToArray()[0].Distance;
                searchText = "";

                foreach (RaceBib r in rbList)
                {
                    if (previousRaceID != r.RaceID)
                    {                       
                        AddToDtAllAge(ref resultsData, ref dtAll, searchText, previousRaceID);
                        searchText = "";
                    }
                    searchText += r.Bib.ToString() + ",";
                    previousRaceID = r.RaceID;
                }
                
                AddToDtAllAge(ref resultsData, ref dtAll, searchText, previousRaceID);
            }
                        
            //We have found results, populate the grid
            if (dtAll != null && dtAll.Rows.Count > 0)
            {
               
                AgeGroupGrid.DataSource = dtAll;
                AgeGroupGrid.DataBind();
               
            }
            else
            {
                //We have no results, bind to an empty dataset to clear grid
                DataTable EmptyDataTable = new DataTable();

                AgeGroupGrid.DataSource = EmptyDataTable;
                AgeGroupGrid.DataBind();
            }
        }
    }
    private void AddToDtAllRace(ref DataSet rData, ref DataTable dt, string search, int pRaceID)
    {
        if (search.EndsWith(",")) search = search.Remove(searchText.Length - 1);
        rData = _dataOperations.CompareByRaceNumber(pRaceID.ToString(), search);
        if (rData != null && rData.Tables.Count > 0)
        {
            rData.Tables[0].Columns.Add("Event");
            rData.Tables[0].Columns["Event"].DataType = typeof(string);
            //rData.Tables[0].Columns.Add("RaceID");
            //rData.Tables[0].Columns["RaceID"].DataType = typeof(string);
            string raceName = string.Empty;
            Helper.RaceInfo info = new Helper.RaceInfo();
            foreach (DataRow dr in rData.Tables[0].Rows)
            {
                raceName = _dataOperations.GetRaceName(pRaceID);
                //dr.SetField("Event", raceName);
                //dr.SetField("RaceID", pRaceID);
                dr["Event"] = raceName;
                //dr["RaceID"] = pRaceID;
            }
            dt.Merge(rData.Tables[0]);
        }
    }
    private void AddToDtAllAge(ref DataSet rData, ref DataTable dt, string search, int pRaceID)
    {
        if (search.EndsWith(",")) search = searchText.Remove(search.Length - 1);
        rData = _dataOperations.CompareByAgeGroup(pRaceID.ToString(), search, true);
        if (rData != null && rData.Tables.Count > 0)
        {
            rData.Tables[0].Columns.Add("Event");
            rData.Tables[0].Columns["Event"].DataType = typeof(string);
            string raceName = string.Empty;
            Helper.RaceInfo info = new Helper.RaceInfo();
            foreach (DataRow dr in rData.Tables[0].Rows)
            {
                raceName = _dataOperations.GetRaceName(pRaceID);
                dr["Event"] = raceName;
            }
            dt.Merge(rData.Tables[0]);
        }
    }
    private void DetermineSearch()
    {
        int bib;
        int race;
        bool isNum = true;
        if (!string.IsNullOrEmpty(searchText) && selectedRaceID > 0)
        {
            if (searchText.StartsWith(","))searchText = searchText.Remove(0, 1);
            if (searchText.EndsWith(",")) searchText = searchText.Remove(searchText.Length - 1, 1);
            
            string[] raceBibs = searchText.Split(",".ToCharArray());
            if (raceBibs.Length > 0)
            {
                rbList = new List<RaceBib>();
                RaceBib rbObj;
                double distance = 0;
                int loopIndex = 0;
                int maxComparison = 10;
                foreach (string rbib in raceBibs)
                {
                    if (loopIndex < maxComparison)
                    {
                        isNum = int.TryParse(rbib.Split("r".ToCharArray())[0], out bib);
                        isNum = int.TryParse(rbib.Split("r".ToCharArray())[1], out race);
                        if (isNum == false) break;
                        //Helper.RaceInfo info = Helper.GetRaceInfo("km", race);
                        //if (info != null && info.Distance > 0) distance = info.Distance;
                        rbObj = new RaceBib(race, distance, bib);
                        rbList.Add(rbObj);
                    }
                    loopIndex += 1;
                }
            }
            if (!isNum)
            {
                searchText= string.Empty;
            }
            

        }
        
        
    }

    
    /// <summary>
    /// This function is fired when data is bound to the results grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ResultsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        TableCellCollection cells = e.Row.Cells;
       
        int race = 0;
        int bib = 0; 
        bool isValidRaceID = false;
        bool isValidBib = false;
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Attributes.Add("class", "head");
        }
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (cells.Count > 6)
            {
                isValidRaceID = int.TryParse(cells[6].Text, out race);
               
            }
            isValidBib = int.TryParse(cells[1].Text, out bib);
            cells[1].Attributes.Add("class", "athlete-name");
            
            foreach (TableCell cell in cells)
            {
                cell.Text = Server.HtmlDecode(cell.Text);
            }
            SetTableStructure(ref cells);

            //if this is the first data row
            if (e.Row.RowIndex == 0)
            {
                CreateBigResultDisplay(cells);
            }
        }

        //Finally, hide race id column as values are only being used at the server side
        if (cells.Count > 6)
        {
            cells[6].Visible = false;
        }
     
    }
   
    protected void AgeGroupGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
            e.Row.Attributes.Add("class", "head");
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cells = e.Row.Cells;
            cells[2].Attributes.Add("class", "athlete-name");          
            foreach (TableCell cell in cells)
            {
                cell.Text = Server.HtmlDecode(cell.Text);
            }
            SetTableStructure(ref cells);
        }
       
    }

    private void SetTableStructure(ref TableCellCollection cells)
    {
        if (cells != null && cells.Count > 0)
        {
            for (int c = 0; c < cells.Count; c++)
            {
                cells[c].Text = Server.HtmlDecode(cells[c].Text);
                switch (c)
                {
                    case 0:
                        cells[c].Width = 30;
                        break;
                    case 1:
                        cells[c].Width = 30;
                        break;
                    case 2:
                        cells[c].Width = 250;
                        break;
                    case 3:
                        cells[c].Width = 100;
                        break;
                    case 4:                      
                            cells[c].Width = 70;                     
                        break;
                    case 5:
                        cells[c].Width = 250;
                        break;
                    default:
                        break;
                }

            }
        }
    }

    private void CreateBigResultDisplay(TableCellCollection cells)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendLine(@"<div id=""stats"">");
        sb.AppendLine(@"<div class=""stat-third name"">");
        sb.AppendLine(@"<span>Best Performance</span>");
        sb.AppendLine(@"<h1>" + cells[2].Text + "</h1>");
        sb.AppendLine(@"</div>");
        sb.AppendLine(@"<div class=""stat-third position"">");
        sb.AppendLine(@"<span>Position</span>");
        sb.AppendLine(@"<h2>" + cells[0].Text + "<sup>");
        
	if (cells[0].Text.EndsWith("11") || cells[0].Text.EndsWith("12")
            || cells[0].Text.EndsWith("13"))
        {
            sb.Append("th");
        }
	else if (cells[0].Text.EndsWith("1"))
        {
            sb.Append("st");
        }
        else if (cells[0].Text.EndsWith("2"))
        {

            sb.Append("nd");
        }
        else if (cells[0].Text.EndsWith("3"))
        {
            sb.Append("rd");
        }
        else
        {
            sb.Append("th");
        }                
        sb.Append("</sup></h2>");
        sb.AppendLine(@"</div>");
        sb.AppendLine(@"<div class=""stat-third time"">");
        sb.AppendLine(@"<span>Finish Time</span>");
        sb.AppendLine(@"<h2>" + cells[3].Text + "</h2>");
        sb.AppendLine(@"</div>");
        sb.AppendLine(@"<div class=""clearer""></div>");
        sb.AppendLine(@"</div>");
        PanelBigResultShow.Controls.Add(new LiteralControl(sb.ToString()));
       
    }
}
