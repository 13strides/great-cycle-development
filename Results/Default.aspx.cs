﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using ThirteenStrides.Database;
using GCResults;
public partial class Results_Default : System.Web.UI.Page
{
    private const int PAGE_SIZE = 10;
    DataOperations _dataOperations = new DataOperations();
    int selectedRaceID;
    int selectedBibID;
    string searchText = string.Empty;
    bool isCompare = false;
    bool isQueryValid = false;
    bool isSingleRaceIDSearch = false;
    double raceDistance = 0;
    public string ageGroupTitle;
    bool isException = false;
    string linkCookie = String.Empty;
    string facebook = string.Empty;
    string fbDescriptionName = string.Empty;
    string fbDescriptionTime = string.Empty;
    string fbDescriptionEventName = string.Empty;
    string twitter = string.Empty;
    string photoLink = string.Empty;
    string graphData = string.Empty;
    string theRaceDistance = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        Form.Action = Request.RawUrl;
        ageGroupTitle = string.Empty;
        LiteralRaceTitle.Text = "Search Result";
        if (Request.QueryString["r"] != null && (Request.QueryString["bib"] != null || Request.QueryString["bibs"] != null))
        {           
            isQueryValid = int.TryParse(Request.QueryString["r"].ToString(), out selectedRaceID);
            if (Request.QueryString["bib"] != null)
            isQueryValid = int.TryParse(Request.QueryString["bib"].ToString(), out selectedBibID);
            if (Request.QueryString["bibs"] != null)
            {
                string bibs = Request.QueryString["bibs"].ToString();
                if (bibs.IndexOf("c") > -1)
                    bibs = bibs.Replace("c", ",");
                TextBoxSearch.Text = bibs;
                isQueryValid = true;
            }
        }
        if (!Page.IsPostBack)
        {
            //Populate Race List
            DataSet raceData = _dataOperations.GetRaces();
            if (raceData != null && raceData.Tables.Count > 0 && raceData.Tables[0].Rows.Count > 0)
            {
                raceData.Tables[0].Rows[0].Delete(); //hide select events default selection
                RaceList.DataTextField = "description"; //Text
                RaceList.DataValueField = "idrace"; //Value
                if (isQueryValid)
                {
                    RaceList.SelectedValue = selectedRaceID.ToString();
                     
                }
                else if (Request.QueryString["r"] != null)
                {
                    int justRaceIdQuery = 0;
                    bool isQueryRaceId = int.TryParse(Request.QueryString["r"].ToString(), out justRaceIdQuery);
                    if(isQueryRaceId)
                      RaceList.SelectedValue = justRaceIdQuery.ToString();
                          
                }
                else
                {
                    RaceList.SelectedValue = raceData.Tables[0].Rows[1]["idrace"].ToString();
                }
                RaceList.OptionGroupField = "raceyear"; //Grouping in Drop Down List

                RaceList.DataSource = raceData;
                RaceList.DataBind();

            }

            //Bind event list
            BindEventList(3);

        }

        try
        {
            if (RaceList != null && !string.IsNullOrEmpty(RaceList.SelectedValue))
                selectedRaceID = Convert.ToInt32(RaceList.SelectedValue);
            LiteralRaceTitle.Text = fbDescriptionEventName =  RaceList.SelectedItem.Text;
            if ((string.IsNullOrEmpty(TextBoxSearch.Text) || TextBoxSearch.Text.ToLower().Equals("race number or surname")) && isQueryValid)
            {
                TextBoxSearch.Text = selectedBibID.ToString();
            }

            searchText = TextBoxSearch.Text;

            //Helper.RaceInfo info = Helper.GetRaceInfo("km", selectedRaceID);
            //if (info != null && info.Distance > 0) raceDistance = info.Distance;

            if (!Page.IsPostBack && (string.IsNullOrEmpty(TextBoxSearch.Text) || TextBoxSearch.Text.ToLower().Equals("race number or surname")) && isQueryValid == false)
            {
                PanelDefaultView.Visible = true;
               
            }
            else
            {
                PanelDefaultView.Visible = false;
                if (isQueryValid)
                {
                    DetermineSearch();
                    DoSearch();
                    DoAgeGroupSearch();
                    DoGenderGroupSearch();
                }
            }
            //display either results table or top finishers (default) table
            PanelResults.Visible = !PanelDefaultView.Visible;
            GetRaceInfo();
        }
        catch(Exception ex)
        {
            isException = true;
        }
       
    }
    private void GetRaceInfo()
    {
        
            DataSet raceData = _dataOperations.GetRace(selectedRaceID.ToString());
            if (raceData.Tables[0].Rows.Count > 0)
            {
                theRaceDistance = raceData.Tables[0].Rows[0]["Distance"].ToString();
            }
          
        
    }
    protected void ButtonGo_Click(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(TextBoxSearch.Text) || TextBoxSearch.Text.ToLower().Equals("race number"))
        {
            selectedRaceID = Convert.ToInt32(RaceList.SelectedValue);
            PanelDefaultView.Visible = true;
            PanelResults.Visible = false;
        }
        else
        {
            PanelDefaultView.Visible = false;
            PanelResults.Visible = true;
            DetermineSearch();
            DoSearch();
            DoAgeGroupSearch();
            DoGenderGroupSearch();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        Page.Header.Controls.Add(new LiteralControl(Helper.GenerateFlotGraph(graphData, "myGraph-dispbox", "Average speed per lap", "Day", "mph")));


        TextBoxSearch.Attributes.Add("onfocus", "if(this.value==='Race Number or Surname'){this.value=''}");
        TextBoxSearch.Attributes.Add("onblur", "if(this.value===''){this.value='Race Number or Surname'}");       
        //Page.Header.Controls.Add(new LiteralControl(Helper.GenerateGraphScript(graphData)));
        if (isException)
        {
            PanelAgeGroupResults.Visible = false;
            PanelDefaultView.Visible = false;
            LiteralRaceTitle.Text = "<h2 class='box-title'>Invalid Request</h2><div class='box-content' style='height:123px;'>" +
            "<p class='error-text'>An error has occured whilst processing your request. Please make a correction to your request and try again.<p>" +
            "</div>";
        }
        else
        {
            PanelAgeGroupResults.Visible = PanelResults.Visible;
            if (!string.IsNullOrEmpty(fbDescriptionName) && !string.IsNullOrEmpty(fbDescriptionTime)
                && !string.IsNullOrEmpty(fbDescriptionEventName))
            {
                AddFbDescriptionMeta(fbDescriptionName.ToUpper() + " completed the " + fbDescriptionEventName + " in a time of " + fbDescriptionTime+".");
            }
            else
            {
                AddFbDescriptionMeta("Great Cycle results, find your time and compare it with others.");
            }
        }
        if (string.IsNullOrEmpty(CompareInfo.Value))
        {
            BtnAddToCompare.Visible = false;
        }
        else
        {
            BtnAddToCompare.Visible = true;
        }
    }
    private void DoSearch()
    {
        try
        {
            if (ResultsGrid != null)
            {

                DataSet resultsData ;
                if (isCompare)
                {
                    resultsData = _dataOperations.CompareByRaceNumber(selectedRaceID.ToString(), searchText);
                }
                else
                {
                    resultsData = _dataOperations.GetResultsByRaceNumber(selectedRaceID.ToString(), searchText);
                }
                //We have found results, populate the grid
                if (resultsData != null && resultsData.Tables.Count > 0 && resultsData.Tables[0].Rows.Count > 0)
                {

                    ResultsGrid.DataSource = resultsData;
                    ResultsGrid.DataBind();
                }
                else
                {
                    //We have no results, bind to an empty dataset to clear grid
                    DataTable EmptyDataTable = new DataTable();
                    ResultsGrid.DataSource = EmptyDataTable;
                    ResultsGrid.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            //Literal1.Text = ex.Message;
        }
    }
    private string DoTimeSplitSearch(string finishTime)
    {   

        DataSet timeSplitData;
        DataSet splitNameData;
        string splitTimes = string.Empty;
        if (isCompare)
        {
            timeSplitData = null;
            splitNameData = null;
        }
        else
        {
            timeSplitData = _dataOperations.GetTimeSplits(selectedRaceID.ToString(), searchText);
            splitNameData = _dataOperations.GetSplitNames(selectedRaceID.ToString());
        }

        //We have found results, populate the grid
        if (timeSplitData != null && timeSplitData.Tables.Count > 0 && timeSplitData.Tables[0].Rows.Count > 0 &&
            splitNameData != null && splitNameData.Tables.Count > 0 && splitNameData.Tables[0].Rows.Count > 0)
        {
            DataRow dr = timeSplitData.Tables[0].Rows[0];

            //if there are equal number of split times as split names, do the for loop
            if (dr.ItemArray.Length == splitNameData.Tables[0].Rows.Count)
            {
                //splitTimes = dr.ItemArray.Length.ToString() + ", " + splitNameData.Tables[0].Rows.Count.ToString();

                List<string> splitTime = new List<string>();
                int splits = 0;
                splitTimes += "<h3>Split times</h3>";
                for (int split = 1; split <= dr.ItemArray.Length; split++)
                {
                    splitTimes += "<span><b>" + splitNameData.Tables[0].Rows[split - 1][0] + " </b>" + dr["Split " + split] + "</span>";
                    if (split == dr.ItemArray.Length)
                    {
                        splits = dr.ItemArray.Length;
                    }
                    splitTime.Add(dr["Split " + split].ToString());

                }
                if (splits > 0 && splitTime.Count > 0) //if finish point is greater than one lap
                {
                    string lastlap = computeLastLap(finishTime, splitTime);
                    if (!string.IsNullOrEmpty(lastlap))
                    {
                        splitTimes += "<span><b>Lap " + (splits + 1).ToString() + " </b>" + lastlap + "</span>";
                        splitTime.Add(lastlap);
                        createGraphData(finishTime, splitTime);
                    }
                }
            }
        }

        return splitTimes;
    }
    private void createGraphData(string finishTime, List<string> times)
    {
        graphData = string.Empty;
        string name = "avgspeed";
        int lapDistance = 13;
        int counter = 1;
        graphData += "{label: '" + name + "', data: [";

        if (times != null && times.Count > 0)
        {
            graphData += "[0,0], ";
            foreach (string t in times)
            {
                graphData += "[" + (counter * lapDistance).ToString() + "," + Helper.ComputeAverageSpeed(t, lapDistance.ToString()) + "], ";
                counter += 1;
            }
        }

        if (graphData.EndsWith(", ")) graphData = graphData.Remove(graphData.LastIndexOf(", "));

        graphData += "]},";
    }
    private string computeLastLap(string finishTime, List<string> times)
    {
        string lastLap = string.Empty;
        try
        {
            string[] t1Array = finishTime.Split(":".ToCharArray());
            DateTime cummulativeSplit = new DateTime(2000, 1, 1);
            if (times != null && times.Count > 0 && t1Array.Length > 0)
            {
                DateTime finish = new DateTime(2000, 1, 1, Convert.ToInt32(t1Array[0]),
                Convert.ToInt32(t1Array[1]), Convert.ToInt32(t1Array[2]));
                for (int i = 0; i < times.Count; i++)
                {
                    string[] t2Array = times.ToArray()[i].Split(":".ToCharArray());
                    cummulativeSplit = cummulativeSplit.AddHours(Convert.ToInt32(t2Array[0]));
                    cummulativeSplit = cummulativeSplit.AddMinutes(Convert.ToInt32(t2Array[1]));
                    cummulativeSplit = cummulativeSplit.AddSeconds(Convert.ToInt32(t2Array[2]));
                }
                TimeSpan timeSpan = finish.Subtract(cummulativeSplit);
                lastLap = timeSpan.Hours.ToString("00") + ":"
                    + timeSpan.Minutes.ToString("00") + ":" + timeSpan.Seconds.ToString("00");

            }
        }
        catch
        {
            lastLap = string.Empty;
        }

        return lastLap;
    }
    //private string computeAverageSpeed(string finishTime)
    //{
    //    int dist = GetDistance(theRaceDistance);
    //    string[] t1Array = finishTime.Split(":".ToCharArray());
    //    string avgSpeed = string.Empty;
    //    if (t1Array.Length > 0 && dist > 0)
    //    {
    //        decimal hours = Convert.ToDecimal(t1Array[0]);
    //        decimal munites = Convert.ToDecimal(t1Array[1]);
    //        decimal totalMunites = (hours * 60) + munites;
    //        decimal speed = (60 * dist) / totalMunites;
    //        avgSpeed = Math.Round(speed, 2) + " <span>mph</span>";

    //    }

    //    return avgSpeed;
    //}
    private void DoAgeGroupSearch()
    {
        if (AgeGroupGrid != null)
        {
           
            DataSet resultsData;
            if (isCompare)
            {
                resultsData = _dataOperations.CompareByAgeGroup(selectedRaceID.ToString(), searchText, false);
            }
            else
            {
                
                resultsData = _dataOperations.GetResultsByAgeGroup(selectedRaceID.ToString(), searchText);
            }
            //We have found results, populate the grid
            if (resultsData != null && resultsData.Tables.Count > 0 && resultsData.Tables[0].Rows.Count > 0)
            {
                if (isCompare == false)
                {
                    ageGroupTitle = " (" + resultsData.Tables[0].Rows[0][4].ToString() + ")";
                    resultsData.Tables[0].Columns.Remove("Age Group");
                }
                //resultsData.Tables[0].Columns.Add(" ");
                AgeGroupGrid.DataSource = resultsData;
                AgeGroupGrid.DataBind();
            }
            else
            {
                //We have no results, bind to an empty dataset to clear grid
                DataTable EmptyDataTable = new DataTable();

                AgeGroupGrid.DataSource = EmptyDataTable;
                AgeGroupGrid.DataBind();
            }
        }
    }
    private void DoGenderGroupSearch()
    {
        if (GenderGroupGrid != null)
        {

            DataSet resultsData;
            if (isCompare)
            {
                resultsData = _dataOperations.CompareByGenderGroup(selectedRaceID.ToString(), searchText);
            }
            else
            {

                resultsData = _dataOperations.GetResultsByGenderGroup(selectedRaceID.ToString(), searchText);
            }
            //We have found results, populate the grid
            if (resultsData != null && resultsData.Tables.Count > 0 && resultsData.Tables[0].Rows.Count > 0)
            {
                
                GenderGroupGrid.DataSource = resultsData;
                GenderGroupGrid.DataBind();
            }
            else
            {
                //We have no results, bind to an empty dataset to clear grid
                DataTable EmptyDataTable = new DataTable();
                
                GenderGroupGrid.DataSource = EmptyDataTable;
                GenderGroupGrid.DataBind();
            }
        }
    }
    private void DetermineSearch()
    {
        int num;
        bool isNum = true;
        if (!string.IsNullOrEmpty(searchText) && selectedRaceID > 0)
        {
            if (searchText.StartsWith(",")) searchText.Remove(0, 1);
            if (searchText.EndsWith(",")) searchText.Remove(searchText.Length - 1, 1);
            string[] raceIDs = searchText.Split(",".ToCharArray());
            if (raceIDs.Length > 0)
            {
                if (raceIDs.Length == 1) isSingleRaceIDSearch = true;
                foreach (string rid in raceIDs)
                {
                    isNum = int.TryParse(rid, out num);
                    if (isNum == false) break;                    
                }
            }
            if (!isNum)
            {
                if (searchText.ToLower().Equals("race number or surname"))
                {
                    Response.Redirect("~/Results/Name-Search/" + selectedRaceID.ToString());
                }
                else
                {
                    Response.Redirect("~/Results/Name-Search/" + selectedRaceID.ToString() + "/" + searchText);
                }
            }
            else
            {
                if (raceIDs.Length > 1)
                {
                    isCompare = true;
                }
            }

        }
        
        
    }

   
    /// <summary>
    /// This function is fired when data is bound to the results grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ResultsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Attributes.Add("class", "head");
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            TableCellCollection cells = e.Row.Cells;

            cells[2].Attributes.Add("class", "athlete-name");

            //if the BIB number equals to the selected race number, highlight this row to indicate the individual's result
            if (cells[1].Text.Equals(searchText))
            {
                e.Row.CssClass = "selected";
                //get comparison info
                CompareInfo.Value = cells[2].Text + "|" + cells[1].Text + "|" + selectedRaceID.ToString();
                facebook = "<a href='http://www.greatcycle.org/results/" + selectedRaceID.ToString() + "/" + cells[1].Text + "' " +
                "rel='nofollow' class='fb_share_button' onclick='return fbs_click(this)' target='_blank' title='Share on Facebook'>" +
                        "<img src='/results/images/facebook.png' alt='Share on Facebook' /><p>Facebook</p></a>";
                twitter = "<a href='http://www.greatcycle.org/results/" + selectedRaceID.ToString() + "/" + cells[1].Text + "' onclick='return twt_click(this);' title='Share on Twitter'><img src='/results/images/twitter.png'  alt='Share on Twitter' /><p>Twitter</p></a>";
                CreateBigResultDisplay(cells);

            }


            SetTableStructure(ref cells);


        }
     
    }
   
    protected void AgeGroupGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
            e.Row.Attributes.Add("class", "head");
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cells = e.Row.Cells;
            //if the BIB number equals to the selected race number, highlight this row to indicate the individual's result
            cells[2].Attributes.Add("class", "athlete-name");
            if (cells[1].Text.Equals(searchText))
                e.Row.CssClass = "selected";
         
            SetTableStructure(ref cells);
            
        }
        if(e.Row.Cells.Count > 1)
            e.Row.Cells[1].Visible = false; //Hides bib column
    }
   
    protected void GenderGroupGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
            e.Row.Attributes.Add("class", "head");
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cells = e.Row.Cells;
            //if the BIB number equals to the selected race number, highlight this row to indicate the individual's result
            cells[2].Attributes.Add("class", "athlete-name");
            if (cells[1].Text.Equals(searchText))
                e.Row.CssClass = "selected";
            SetTableStructure(ref cells);

        }
        if (e.Row.Cells.Count > 1)
        e.Row.Cells[1].Visible = false; //Hides bib column
    }

    protected void Page_Error(object sender, EventArgs e)
    {
 
    }
    private void SetTableStructure(ref TableCellCollection cells)
    {
        if (cells != null && cells.Count > 0)
        {
            for (int c = 0; c < cells.Count; c++)
            {
                cells[c].Text = Server.HtmlDecode(cells[c].Text);
                switch (c)
                {
                    case 0:
                        cells[c].Width = 50;
                        break;
                    case 1:
                        cells[c].Width = 50;
                        break;
                    case 2:
                        cells[c].Width = 260;
                        break;
                    case 3:
                        cells[c].Width = 120;
                        break;
                    case 4:
                        cells[c].Width = 140;
                        break;

                    default:
                        break;
                }

            }
        }
    }

    private void CreateBigResultDisplay(TableCellCollection cells)
    {
        DataSet ds = _dataOperations.GetRace(selectedRaceID.ToString());
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            theRaceDistance= ds.Tables[0].Rows[0]["Distance"].ToString();
        PanelBigResultShow.Controls.Clear();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendLine(@"<div id=""stats"">");
        sb.AppendLine(@"<div class=""stat-third name"">");
        sb.AppendLine(@"<span>Person</span>");
        sb.AppendLine(@"<h1>" + cells[2].Text + "</h1>");
        sb.AppendLine(@"</div>");
        sb.AppendLine(@"<div class=""stat-third position"">");
        sb.AppendLine(@"<span>Position</span>");
        sb.AppendLine(@"<h2>" + cells[0].Text + "<sup>");
        /*start of fb description name and time */
        fbDescriptionName = cells[2].Text;
        fbDescriptionTime = cells[3].Text;
        
        /*end of fb description name and time*/
	if (cells[0].Text.EndsWith("11") || cells[0].Text.EndsWith("12")
            || cells[0].Text.EndsWith("13"))
        {
            sb.Append("th");
        }
        else if (cells[0].Text.EndsWith("1"))
        {           
                sb.Append("st");
               
        }
        else if (cells[0].Text.EndsWith("2"))
        {
            sb.Append("nd");
         
        }
        else if (cells[0].Text.EndsWith("3"))
        {
            sb.Append("rd");
            
        }
        else
        {
            if (!cells[0].Text.ToLower().Equals("&nbsp;"))
            {
                sb.Append("th");
            }
            else
                sb.Append("-");
            
            
        }                

        sb.Append("</sup></h2>");
        sb.AppendLine(@"</div>");
        sb.AppendLine(@"<div class=""stat-third time"">");
        sb.AppendLine(@"<span>Finish Time</span>");
        sb.AppendLine(@"<h2>" + cells[3].Text + "</h2>");
        sb.AppendLine(@"<p>(This is your total ride time minus Feed Station stops)</p></div>");
        string averageSpeed = Helper.ComputeAverageSpeed(cells[3].Text, theRaceDistance);
        if (!string.IsNullOrEmpty(averageSpeed))
        {
            sb.AppendLine(@"<div class=""stat-third speed"">");
            sb.AppendLine(@"<span>Average Speed ");
	    if (!theRaceDistance.StartsWith("13")) 
            sb.AppendLine(@"<a href=""#"" id=""speed"" class=""hasGraph""><div id='graph'></div></a>");
            sb.Append("</span>");
            sb.AppendLine("<h2>");
            sb.Append(averageSpeed + "<span>mph</span>");
            sb.AppendLine("</h2>");
            sb.AppendLine("</div>");
            //splitTimes += "<span><b>Average Speed </b>" + computeAverageSpeed(finishTime) + "</span>";
        }
        sb.AppendLine(@"<div class=""clearer""></div>");
        sb.AppendLine(@"</div>");
        sb.AppendLine(@"<div id=""split-times-container"">");  
        sb.AppendLine(@"<div id=""split-times"">");
        //sb.AppendLine(@"<a href=""#""><img src=""images/add.png"" alt=""Facebook""/>Compare with Others</a>");
        if (!string.IsNullOrEmpty(facebook))
        sb.AppendLine(facebook);
        if (!string.IsNullOrEmpty(twitter))
        sb.AppendLine(twitter);
        if(!string.IsNullOrEmpty(photoLink))
        sb.AppendLine(photoLink);
        sb.Append(DoTimeSplitSearch(cells[3].Text));        
   		sb.AppendLine(@"<div class=""clearer""></div>");
        sb.AppendLine("</div>");
        sb.AppendLine("</div>");
        PanelBigResultShow.Controls.Add(new LiteralControl(sb.ToString()));
        PanelBigResultShow.Visible = true;
    }
    protected void BindEventList(int top)
    {
        //// gr_front_select_event_information_all
        //MsSqlConnection connection = new MsSqlConnection(ConfigurationManager.ConnectionStrings["MsSqlGreatRun"].ConnectionString);
        //SqlParam[] paras = { new SqlParam("@top", SqlDbType.Int, top) };
        //DataSetResponse res = connection.RunCommand("gr_front_select_top_few_next_events", paras, "event");

        //if (res.ReturnCode == 0)
        //{
        //    eventListRepeater.DataSource = res.Result.Tables["event"].DefaultView;
        //    eventListRepeater.DataBind();
        //}
    }
    protected void AddStatusConfirmation(Object sender, RepeaterItemEventArgs e)
    {
        //if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        //{
        //    // adds a confirmation javascript to ensure that deleting the template is really what the user wants.
        //    DataRowView myRow = (DataRowView)e.Item.DataItem;

        //    if (Convert.ToInt32(myRow["event_info_status"]) == 1)
        //    {
        //        ((HyperLink)e.Item.FindControl("HyperLink20")).Visible = false;
        //    }
        //    if (Convert.ToInt32(myRow["event_info_status"]) == 3)
        //    {
        //        ((HyperLink)e.Item.FindControl("HyperLink20")).NavigateUrl = "http://www.greatrun.org/reminderservice/?raceid=" + Convert.ToInt32(myRow["event_info_stream_id"]).ToString();
        //        ((HyperLink)e.Item.FindControl("HyperLink20")).Text = "Register your interest";
        //    }
        //    if (Convert.ToInt32(myRow["event_info_status"]) == 2)
        //    {
        //        ((HyperLink)e.Item.FindControl("HyperLink20")).NavigateUrl = "~/Events/Charities.aspx?id=" + Convert.ToInt32(myRow["event_info_event_id"]).ToString();
        //        ((HyperLink)e.Item.FindControl("HyperLink20")).Text = "Find a charity";
        //    }
           
        //}
    }
    protected string CreateRegisterNavLink(object eventId)
    {
        //"https://entry.enteronline.org/login.aspx?StreamID="
        string navLink = ConfigurationManager.AppSettings["EntrySystemUrl"] + eventId.ToString(); 
        if (linkCookie != String.Empty && linkCookie != null)
        {
            navLink = navLink + "&" + linkCookie;
        }
        return navLink;
    }

    public string DateGeneration(object tbc, object month, object day)
    {
        if (tbc.ToString() == "True" || tbc.ToString() == "1")
            return "<span class=\"date\"><span class=\"month\" id=\"span_month\">&nbsp;</span> <span id=\"span_day\"  class=\"day\">TBC</span></span>";
        else
            return "<span class=\"date\"><span class=\"month\" id=\"span_month\">" + month.ToString() + "</span> <span id=\"span_day\"  class=\"day\">" + day.ToString() + "</span></span>";
    }
    protected void BtnAddToCompare_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(CompareInfo.Value))
        {
            string[] param = CompareInfo.Value.Split("|".ToCharArray());
            if (param.Length == 3)
            {
                int raceID = 0;
                int bib = 0;
                string name = param[0];
                string strBib = param[1];
                string strRaceID = param[2];
                bool isNum = false;
                isNum = int.TryParse(strRaceID, out raceID);
                if (isNum && !string.IsNullOrEmpty(name))
                {
                    isNum = int.TryParse(strBib, out bib);
                    if (isNum)
                    {
                        MaintainComparisonList(bib, name, raceID);
                    }
                }
                Response.Redirect("~/Results/Name-Search", true);
            }
        }
    }
    public void MaintainComparisonList(int bib, string name, int raceID)
    {

        if (Session["CompareAll"] != null)
        {
            List<ComparePerson> cList = (List<ComparePerson>)Session["CompareAll"];
            bool isExisted = false;
            if (cList != null && cList.Count > 0)
            {
                foreach (ComparePerson p in cList)
                {
                    if (p.Bib == bib && p.RaceID == raceID)
                    {
                        isExisted = true;
                    }
                }
                if (isExisted == false)
                {
                    ManageComparorList(cList, bib, name, raceID);
                }

            }
        }
        else  //if post back and null
        {
            if (Session["CompareAll"] == null)
            {
                ManageComparorList(new List<ComparePerson>(), bib, name, raceID);
            }
        }

    }
    private void ManageComparorList(List<ComparePerson> list, int bib, string name, int raceID)
    {

        ComparePerson cp;
        cp = new ComparePerson(bib, name, raceID);
        list.Add(cp);

        if (list != null && list.Count > 0)
        {
            if (Session["CompareAll"] != null)
            {
                Session["CompareAll"] = list;
            }
            else
            {
                Session.Add("CompareAll", list);
            }
        }

    }
    private void AddFbDescriptionMeta(string description)
    {
        HtmlMeta _description = new HtmlMeta();
        _description.Attributes.Add("property", "og:description");
        _description.Content = description;

        Page.Header.Controls.Add(_description);
    }
    //private int GetDistance(string str)
    //{
    //    char[] chars = str.ToCharArray();
    //    string filtered = string.Empty;
    //    int response = 0;
    //    foreach (char c in chars)
    //    {
    //        if (char.IsDigit(c))
    //        {
    //            filtered += c;
    //        }
    //    }
    //    if (!string.IsNullOrEmpty(filtered))
    //        response = Convert.ToInt32(filtered);
    //    return response;
    //}
}
