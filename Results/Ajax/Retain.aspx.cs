﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Results2_Ajax_Retain : System.Web.UI.Page
{
    string name = string.Empty;
    string strRaceID = string.Empty;
    string strBib = string.Empty;
    int bib = 0;
    int raceID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["name"] != null && Request.QueryString["bib"] != null 
            && Request.QueryString["rid"] != null)
        {            
            name = Request.QueryString["name"];
            strBib = Request.QueryString["bib"];
            strRaceID = Request.QueryString["rid"];
            bool isNum = false;
            isNum = int.TryParse(strRaceID, out raceID);
            if (isNum && !string.IsNullOrEmpty(name))
            {
                isNum = int.TryParse(strBib, out bib);
                if (isNum)
                {
                    MaintainComparisonList();
                }
            }
        }
    }
    public void MaintainComparisonList()
    {

        if (Session["CompareAll"] != null)
        {
            bool isExisted = false;
            List<ComparePerson> cList = (List<ComparePerson>)Session["CompareAll"];
            if (cList != null && cList.Count > 0)
            {
                foreach (ComparePerson p in cList)
                {
                    if (p.Bib == bib && p.RaceID == raceID)
                    {
                        isExisted = true;
                    }
                }
                if (isExisted == false)
                {
                    ManageComparorList(cList);
                }
            }
        }
        else  //if post back and null
        {
            if (Session["CompareAll"] == null)
            {
                ManageComparorList(new List<ComparePerson>());
            }
        }

    }
    private void ManageComparorList(List<ComparePerson> list)
    {

        ComparePerson cp;
        cp = new ComparePerson(bib, name, raceID);
        list.Add(cp);                  
            
        if (list != null && list.Count > 0)
        {
            if (Session["CompareAll"] != null)
            {
                Session["CompareAll"] = list;
            }
            else
            {
                Session.Add("CompareAll", list);
            }
        }
        
    }
}
