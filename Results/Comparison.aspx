<%@ Page Title="Daily Mirror Great Cycle Results" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Comparison.aspx.cs" 
Inherits="Results_Comparison" %>
<%@ Register TagPrefix="ddlb" Assembly="OptionDropDownList" Namespace="OptionDropDownList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  <link href="ExtendedCss/results.css" type="text/css" rel="stylesheet" />
  <meta property="og:image" content="http://www.greatcycle.org/results/images/compare_fb_share_thumb.png" />
  <meta property="og:title" content="Daily Mirror Great Cycle Results" />
  <meta property="og:description" content="Daily Mirror Great Cycle results, find your time and compare it with others" />
  <meta property="og:type" content="article"/>
  <meta property="og:url" content="http://www.greatcycle.org/results/Comparison.aspx"/>
  <style type="text/css">
#r-search h1 {
	border:none;
	padding:0 0 5px 0;
}
</style>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!--  meta http-equiv="X-UA-Compatible" content="chrome=1" -->
  
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
  <script type="text/javascript">
jQuery.noConflict();
</script>
  <script type="text/javascript">
    function fbs_click() {
        u = location.href;
        t = document.title;
        //pageTracker._trackPageview('/EXIT/RESULTS/COMPARISON_PAGE/FACEBOOK.html');
        window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
        return false; 
    }
    function twt_click() {
        u = location.href;
        txt = '#greatcycle';
        //pageTracker._trackPageview('/EXIT/RESULTS/COMPARISON_PAGE/TWITTER.html');
        window.open('http://twitter.com/share?text=' + encodeURIComponent(txt), 'sharer', 'toolbar=0,status=0,width=626,height=436');
        return false; 
    }
    </script>
<%--  <script type="text/javascript" src="./Scripts/highcharts.js"></script>
  <script type="text/javascript" src="./Scripts/exporting.js"></script>
  <!-- Highslide code -->
  <script type="text/javascript" src="./Scripts/highslide-full.min.js"></script>
  <script type="text/javascript" src="./Scripts/highslide.config.js" charset="utf-8"></script>
  <link rel="stylesheet" type="text/css" href="./ExtendedCss/highslide.css" />
  <script type="text/javascript">
	Highcharts.theme = { colors: ['#4572A7'] };// prevent errors in default theme
	var highchartsOptions = Highcharts.getOptions(); 
</script>--%>
  <!-- End Highslide code -->
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="span7 abouttheswim">
            <div id="r-search">
                <h1>Results Comparison
    <asp:HyperLink ID="HyperLinkBack" runat="server" />
                </h1>
            </div>
            <asp:Panel ID="PanelBigResultShow" runat="server">
            </asp:Panel>
            <div id="content">

                <div class="right-col">
                    <div id="primary">
                        <div class="com75">
                            <div class="header">
                                <asp:Literal ID="LiteralRaceTitle" runat="server"></asp:Literal>
                            </div>
                            <div class="content">
                                <asp:GridView ID="ResultsGrid" CssClass="results-table" runat="server" EnableViewState="False"
                                    OnRowDataBound="ResultsGrid_RowDataBound" Visible="false" EmptyDataRowStyle-CssClass="error"
                                    EmptyDataText="There were no records found that met your search criteria. Please use our <a href='/Results/Advanced-Search'>advanced search</a>.">
                                    <AlternatingRowStyle CssClass="altrow" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="com75">
                            <div class="header">
                                <h2 class="box-title">Age Group Result Comparison</h2>
                            </div>
                            <div class="box-content">
                                <asp:GridView ID="AgeGroupGrid" runat="server" CssClass="results-table" OnRowDataBound="AgeGroupGrid_RowDataBound" EmptyDataRowStyle-CssClass="error"
                                    EmptyDataText="There were no records found that met your search criteria. Please use our <a href='/Results/Advanced-Search'>advanced search</a>.">
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="left-col">
                    <h2 class="box-title">Share</h2>
                    <div class="box-content" style="min-height: 130px">
                        <p>Use the links below to share this page:</p>
                        <p>
                            <a href="javascript:twt_click();" title="Share on Twitter">
                                <img src="/Results/images/twitter_share.png" style="margin-left: 10px; height: 30px;" alt="Share on Twitter" /></a>
                            <br />
                            <br />
                            <a href="javascript:fbs_click();" title="Share on Facebook">
                                <img src="/Results/images/facebook_share.png" style="margin-left: 10px; height: 30px;" alt="Share on Facebook" /></a>
                        </p>
                    </div>
                </div>
                <div class="clearer"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content  ID="Content3" runat="server" ContentPlaceHolderID="AdditionalFooter"></asp:Content>