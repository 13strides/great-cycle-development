﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Contact_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="row pageheading">
        <div class="span6">
            <h1>Contact</h1>
            <ul class="breadcrumb">
             <strong>You are here:</strong>
                <li><a href="/">Home</a> <span class="divider">/</span></li>
                <li class="active">Contact</li>
            </ul>
        </div>
		
      </div>
<div>
<div class="row">
		<div class="span11 contact">
            <gs:ContactForm ID="Contact1" runat="server" />

        </div>
</div>
</div>	  
	  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" Runat="Server">
</asp:Content>

