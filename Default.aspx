<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs"
	Inherits="_Default" Title="Home | Great Manchester Cycle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<style type="text/css">
	span.s1,span.s2,span.s3,span.s4,span.s5{display:none;}
	
	span.countdown-time {width: 160px;
	height: 12px;
	position: absolute;
	top: 0px;
	right: 0;
	padding: 14px;
	color:#602f6e !important;}
	span.countdown-time span.s6{
	position: absolute;
	left: 10px;
top:10px;
	opacity: 1;
	color:#602f6e !important;
	}
</style>
<![if !IE]>
	<style type="text/css">
		span.countdown-time span.s6{opacity: 0;}
</style>
<link href="css/Counting.css" rel="stylesheet" type="text/css" />
<![endif]>

	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<!-- Main hero unit for a primary marketing message or call to action -->
	<ni:HeroSlider runat="server" ID="heroSlider" />
	<div id="countdown">
		<asp:Literal ID="LiteralEventDay" runat="server"></asp:Literal>
		<!--<span class="countdown-date">Sunday 30 June 2013</span> <span class="countdown-time">-->
			<asp:Literal runat="server" ID="countDownLiteral"></asp:Literal>
			<!--<span class="s1">17 days</span> <span class="s2">16 days</span> <span class="s3">15
				days</span> <span class="s4">14 days</span> <span class="s5">13 days</span>
			<span class="s6">12 days</span>
			12 days 3 hours
		</span>-->
	</div>
	<div class="row">
		<div class="span7 abouttheswim">
			<gs:ContentPage ID="contentTest" runat="server" ContentPage="2" />
			<br />
			<gs:ContentPage ID="ContentPage1" runat="server" ContentPage="10" />
		</div>
		<div class="span3 rhc">
			<h2>
				Blog</h2>
			<gs:BlogThumbnails ID="BlogThumbnail1" runat="server" LiveOnly="1" TopX="2" />
			<h2>
				News</h2>
			<gs:NewsThumbnails ID="NewsThumbnails1" runat="server" LiveOnly="1" TopX="1" />
		</div>
		<div class="clearer">
		</div>
		<div class="clearer">
		</div>
		<div class="row social">
			<gs:Twitter ID="TwitterView" runat="server" />
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" runat="Server">
</asp:Content>