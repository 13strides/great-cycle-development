﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class Sitemap : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BuildSiteMap();
        }
    }
    private void BuildSiteMap()
    {
        //dawit would use controls


        foreach (SiteMapNode topNode in SiteMap.Provider.GetChildNodes(SiteMap.RootNode)) 
        {
            if (topNode.Title != "Error")
            {
                if (topNode.Title == "SUPPORT")
                {
                    siteMapLiteral.Text += "<div class=\"column clearboth\">";
                }
                else
                {
                    siteMapLiteral.Text += "<div class=\"column\">";
                }
                siteMapLiteral.Text += "<h2>" + FriendlyTitle(topNode.Title) + "</h2>";
                foreach (SiteMapNode mainNav in topNode.ChildNodes)
                {
                    siteMapLiteral.Text += "<p>&nbsp; - <a href=\"" + mainNav.Url + "\">" + Server.HtmlEncode(mainNav.Title) + "</a></p>";
                    foreach (SiteMapNode microSite in mainNav.ChildNodes)
                    {
                        siteMapLiteral.Text += "<p> &nbsp; &nbsp; - <a href=\"" + mainNav.Url + "\">" + Server.HtmlEncode(microSite.Title) + "</a></p>";
                    }
                }
                siteMapLiteral.Text += "</div>";
            }
        }
       
    }

    private string FriendlyTitle(string title)
    {
        string niceTitle = "";
        switch (title)
        {
            case "THEEVENT":
                niceTitle = "The Event";
                break;
            case "RESPECT":
                niceTitle = "Respect The Challenge";
                break;
            case "NEWS":
                niceTitle = "Latest News";
                break;
            case "SUPPORT":
                niceTitle = "Policy and References";
                break;
        }
        return niceTitle;
    }
}
