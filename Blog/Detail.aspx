﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Blog_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="keywords" content="<%=metaKey %>" />
    <meta name="description" content="<%=metaDescription %>" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
       <div class="row pageheading">
        <div class="span6">
            <h1 style="font-size:28px; text-transform:none;"><%=blogTitle %></h1>
            <ul class="breadcrumb">
             <strong>You are here:</strong>
                <li><a href="/">Home</a> <span class="divider">/</span></li>
                <li><a href="/Blog">Blog</a> <span class="divider">/</span></li>
                <li class="active"><%=blogTitle %></li>
<%--                <li class="active">Current Page</li>--%>
            </ul>
        </div>
        
    </div>
    <div class="row">
        <div class="span8 blog-post" style="padding: 0; margin: 0;">
            <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                     <div class="blog-heading">
                    <h2><a href="#"><%# Eval("Title") %></a></h2>
<%--                     <span><%# Eval("PublishDate", "{0:dd MMMM yyyy}") %></span>--%>
                     </div>
                    <asp:Image ID="NewsImg" runat="server" ImageUrl='<%# GetImageFile(Eval("ImageFilename")) %>'/>
                    <div class="para"><%# Eval("ArticleContent") %></div>
                  
                </ItemTemplate>
            </asp:Repeater>



            <div class="pagination">
                <asp:HiddenField ID="HfPage" runat="server" Value="1" />
                <asp:Literal ID="LtPagination" runat="server"></asp:Literal>
                <a id="ctl00_MainContent_EnterOnlineLink" class="btn btn-primary" href="/Event/">Enter Now »</a>
            </div>

            <div class="article-comment">
                <div id="disqus_thread"></div>
                <script type="text/javascript">
                    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                    var disqus_shortname = 'greatcyclelive'; // required: replace example with your forum shortname

                    /* * * DON'T EDIT BELOW THIS LINE * * */
                    (function () {
                        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
            </div>



        </div>



        <div class="span3 rhc">
            <h3>Categories</h3>
          <div id="tag-container">
              <gs:BlogTags ID="BlogTag1" runat="server" />
          </div>
            <asp:Literal ID="LiteralRelatedBlogTitle" runat="server"></asp:Literal>
            <gs:BlogThumbnails ID="BlogThumbnail1" TopX="4" LiveOnly="1" runat="server" />
        </div>
        <div class="clearer"></div>

        <div class="row social">
        <gs:Twitter ID="TwitterView" runat="server" />        
       
      </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdditionalFooter" Runat="Server">
</asp:Content>

