﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThirteenStrides.Database;
using Gr.Model;
public partial class Blog_Detail : System.Web.UI.Page
{
    public string blogTitle = string.Empty;
    public string metaKey = string.Empty;
    public string metaDescription = string.Empty;
    string friendlyUrl = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        LiteralRelatedBlogTitle.Text = string.Empty;
        if (Request.QueryString["furl"] != null)
        {
            friendlyUrl = Request.QueryString["furl"];
            if (!string.IsNullOrEmpty(friendlyUrl))
            {
                BindDataWithFriendly();
            }
            else
            {
                Response.Redirect("~/Blog");
            }

        }
    }


    private void BindDataWithFriendly()
    {
        BlogArticle article = BlogArticle.GetByFriendlyUrl(friendlyUrl, false, 1);
        if (article != null && article.ID > 0)
        {
            BlogThumbnail1.ArticleId = article.ID;
            List<BlogArticle> list = new List<BlogArticle>();
            list.Add(article);
            Repeater1.DataSource = list;
            Repeater1.DataBind();

            string metaTitle = article.MetaTitle;
            blogTitle = article.Title;
            metaKey =article.MetaKey;
            metaDescription = article.MetaDescription;

            this.Page.Title = article.Title;
            if (!string.IsNullOrEmpty(metaTitle))
                this.Page.Title = metaTitle;

        }        
        else
        {
            Response.Redirect("~/Blog");
        }
    }
    protected string GetImageFile(object obj)
    {
        string defaultPath = "~/App_Files/ImageLibrary/NewsFull/";
        string defaultImage = "~/img/css/blog1.jpg";
        string str = obj.ToString().Trim();
        if (str.Length > 0)
        {
            if (!str.StartsWith("~"))
            {
                str = defaultPath + str;
                if (!File.Exists(Server.MapPath(str)))
                {
                    str = defaultImage;
                }
            }
        }
        return str;
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (BlogThumbnail1.ItemCount > 0)
        {
            LiteralRelatedBlogTitle.Text = "<h3>Related Blogs</h3>";
        }
    }
}