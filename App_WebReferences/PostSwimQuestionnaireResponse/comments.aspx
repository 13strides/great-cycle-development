<%@ page language="C#" autoeventwireup="true" inherits="comments, App_Web_cwxlxxbb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Post-Swim Questionnaire >> Responses >> Comments</title>
    <style type="text/css">
/* Remove padding and margin */
* {
	margin: 0;
	padding: 0;
}
/* Put it back on certain elements */
h1, h2, h3, h4, h5, h6, p, pre, blockquote, form, fieldset, table, ul {
	margin: 1em 0;
}
/* Class for clearing floats */
.clear {
	clear:both;
}
/* Remove border around linked images */
img {
	border: 0;
}
/* =Layout
-----------------------------------------------------------------------------*/
body {
	margin:25px 100px 25px 25px;
	color:#000000;
	background-color:#ffffff;
}
#wrapper {
    width:600px;
}
/* =Typography
-----------------------------------------------------------------------------*/
h2 {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	font-size:1em;
	margin-top:0;
	color:#1794E4;
}
h3 {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	font-size:0.8em;
	margin-top:2em;
	color:#1794E4;
}
p {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	font-size:0.6em;
	text-align:justify;
	line-height:1.5;
}
a:link, a:visited {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	color:#1794E4;
}
/* =Form
-----------------------------------------------------------------------------*/
label {
	float:left;
	width:260px;
	padding-right:10px;
	font-weight:normal;
	text-align:right;
}
select, input {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	font-size:1em;
	padding:2px;
	border:1px solid #000;
}
.btn {
	background-color:#1794E4;
	color:white;
	font-weight:bold;
	font-size:1em;
	width:100px;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
    <h2>Post-Swim Questionnaire</h2>
<p>To view the comments for an event, please select an event from the list below, if one isn't already selected. Also, to view particular categories of comment, select the category from the other list. Or leave it blank to see all comments.</p>
<p>To return to the questionnaire responses, click <asp:HyperLink ID="lnkQuestionnaire" runat="server">here</asp:HyperLink></p>
<p><asp:RequiredFieldValidator
    ID="valEventDDL" runat="server" ErrorMessage="Please select an event" ControlToValidate="ddlEvent" Display="Dynamic" Font-Bold="True" Visible="true" InitialValue="select event"></asp:RequiredFieldValidator></p>
<p class="formfield"><asp:DropDownList ID="ddlEvent" runat="server" AutoPostBack="false" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged"></asp:DropDownList>&nbsp;<asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="false"></asp:DropDownList>&nbsp;<asp:Button ID="btnSubmit" runat="server" Text="View Comments" class="btn" OnClick="btnSubmit_Click" /></p>
<p><br /></p>
<asp:Literal ID="litComments" runat="server"></asp:Literal>&nbsp;
    
    </div>
    </form>
</body>
</html>
