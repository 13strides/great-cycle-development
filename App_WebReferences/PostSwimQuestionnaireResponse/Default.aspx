﻿<%@ page language="C#" autoeventwireup="true" inherits="_Default, App_Web_cwxlxxbb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Post-Swim Questionnaire >> Responses</title>
    <style type="text/css">
/* Remove padding and margin */
* {
	margin: 0;
	padding: 0;
}
/* Put it back on certain elements */
h1, h2, h3, h4, h5, h6, p, pre, blockquote, form, fieldset, table, ul {
	margin: 1em 0;
}
/* Class for clearing floats */
.clear {
	clear:both;
}
/* Remove border around linked images */
img {
	border: 0;
}
/* =Layout
-----------------------------------------------------------------------------*/
body {
	margin:25px 0 25px 25px;
	color:#000000;
	background-color:#ffffff;
}
/* =Typography
-----------------------------------------------------------------------------*/
h2 {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	font-size:1em;
	margin-top:0;
	color:#1794E4;
}
h3 {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	font-size:0.8em;
	margin-top:2em;
	color:#1794E4;
}
p {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	font-size:0.6em;
	text-align:justify;
	line-height:1.5;
}
a:link, a:visited {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	color:#1794E4;
}
/* =Form
-----------------------------------------------------------------------------*/
label {
	float:left;
	width:260px;
	padding-right:10px;
	font-weight:normal;
	text-align:right;
	clear:both;
}
select {
	font-family:Verdana,Arial,Helvetica,Sans-serif;
	font-size:1em;
	padding:2px;
	border:1px solid #000;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
<h2>Questionnaire Responses</h2>
<p>To view the questionnaire responses for an event, please select it from the list below.</p>
<p><asp:DropDownList ID="ddlEvent" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged"></asp:DropDownList></p>
<div id="dvResponse" runat="server">
<p>To view the comments, click <asp:HyperLink ID="lnkComments" runat="server">here</asp:HyperLink></p>
<asp:Literal ID="litResponse" runat="server"></asp:Literal>
</div>
    </div>
    </form>
</body>
</html>
