﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeFile="Photos.aspx.cs" Inherits="Photos_Photos" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="row pageheading">
		<div class="span6">
			<h1>
				Great Cycle Photos</h1>
			<ul class="breadcrumb">
             <strong>You are here:</strong>
				<li><a href="/">Home</a> <span class="divider">/</span></li>
				<li class="active">Photos</li>
				<asp:Literal ID="LiteralBlogLink" runat="server"></asp:Literal>
			</ul>
		</div>
		<a href="/Event/" class="btn btn-primary btn-large">Enter Now »</a>
	</div>
	<div class="row">
		<div class="span11" style="padding: 0;">
			<div class="right-col">
				<div id="primary">
					<div class="com75">
						<div class="content">
							<div class="infoPages">
								<div id="embed">
								</div>
								<script type="text/javascript">
									function get_url_query(ji) {
										hu = window.location.search.substring(1);
										gy = hu.split("&");
										for (i = 0; i < gy.length; i++) {
											ft = gy[i].split("=");
											if (ft[0] == ji) {
												return ft[1];
											}
										}
										return '';
									}

									var event = get_url_query('event')
									var bib = get_url_query('bib')
									var iframe_code = "<iframe class='photos-iframe' src='http://www.marathon-photos.com/scripts/embed.py?event=Sports/GRUK/2012/Great%20Manchester%20Cycle&match=" + bib + "' style='width: 920px; height: 800px;'  frameborder=0></iframe>"
									document.getElementById('embed').innerHTML = iframe_code
								</script>
							</div>
						</div>
					</div>
				</div>
				<div class="clearer">
				</div>
			</div>
			<div class="clearer">
			</div>
		</div>
	</div>
</asp:Content>