﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeFile="Default.aspx.cs" Inherits="Photos_Default" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="row pageheading">
		<div class="span6">
			<h1>
				Great Cycle Photos</h1>
			<ul class="breadcrumb">
             <strong>You are here:</strong>
				<li><a href="/">Home</a> <span class="divider">/</span></li>
				<li class="active">Photos</li>
				<asp:Literal ID="LiteralBlogLink" runat="server"></asp:Literal>
			</ul>
		</div>
		<a href="/Event/" class="btn btn-primary btn-large">Enter Now »</a>
	</div>
	<div class="row">
		<div class="span6" style="padding: 0;">
			<div class="right-col">
				<div class="box-content" style="min-height: 332px;">
					<h2>
						Choose your event year</h2>
					<div class="charityPartner" style="float: left; margin: 10px;">
						<a href="../photos/photos.aspx?ref=greatrun&event=Sports/GRUK/2012/Great%20Manchester%20Cycle&match="
							title="Daily Mirror Great Manchester Cycle 2012" onclick="javascript:pageTracker._trackPageview('/EXIT/PHOTOS/EVENT/2012.html');">
							<img src="/App_files/gr_files/DMGMC2012.jpg" alt="Daily Mirror Great Manchester Cycle 2012"
								title="Daily Mirror Great Manchester Cycle 2012" height="100" width="135" class="img" /></a></div>
					<div class="clearfix">
					</div>
				</div>
				<div class="clearer">
				</div>
			</div>
			<div class="clearer">
			</div>
		</div>
	</div>
</asp:Content>